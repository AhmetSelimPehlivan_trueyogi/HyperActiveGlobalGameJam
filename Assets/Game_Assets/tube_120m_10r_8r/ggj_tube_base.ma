//Maya ASCII 2023 scene
//Name: ggj_tube_base.ma
//Last modified: Sat, Feb 04, 2023 12:51:05 AM
//Codeset: UTF-8
requires maya "2023";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "5.2.1.1";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2023";
fileInfo "version" "2023";
fileInfo "cutIdentifier" "202211021031-847a9f9623";
fileInfo "osv" "Mac OS X 10.16";
fileInfo "UUID" "D684505D-C546-534D-BF43-809B06C372C2";
createNode transform -s -n "persp";
	rename -uid "CF193ADC-BC46-516D-096A-C4B72EAC0790";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -137.44389863131093 198.92230809844045 96.303909161297483 ;
	setAttr ".r" -type "double3" -41.138352778488034 -1855.000000000276 -5.5451278677333887e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "96B085C2-3045-64E6-6F10-4EBE65EE3DC7";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 215.37973551041227;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "22E61976-0D4B-3A5B-3048-CFB732C183EB";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "8B9F4B00-734F-DAE0-4ED1-0A8D37F83A0E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 94.983013415723747;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "78B8D4FE-3C47-F18E-E074-C99735173D3F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.5485671705854207 76.217206718376374 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "BC609A23-3749-0F61-B59E-E7AD79E312E8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 23.579034296705981;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "58BD388C-6547-12EA-AF5B-E7940EA0949C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "97121EB2-FC44-7E87-EE74-EE91D9BBF464";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "polySurface1";
	rename -uid "D0B0ED5C-EE4C-F4BE-2E63-D38B4F1AACA3";
	setAttr ".s" -type "double3" 1 4 1 ;
createNode mesh -n "polySurfaceShape1" -p "polySurface1";
	rename -uid "51C95230-3C44-3343-8ADE-2A9F5EECD48B";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999999999999989 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2781 ".pt";
	setAttr ".pt[2700]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2701]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2704]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".pt[2705]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2706]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2707]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2709]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2711]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2712]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2714]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2715]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2716]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2721]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".pt[2723]" -type "float3" 0 0 -1.4210855e-14 ;
	setAttr ".pt[2726]" -type "float3" 0 0 3.5527137e-15 ;
	setAttr ".pt[2729]" -type "float3" 0 0 1.1920929e-07 ;
	setAttr ".pt[2731]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".pt[2732]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2733]" -type "float3" 0 0 1.1920929e-07 ;
	setAttr ".pt[2734]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2735]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2739]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2742]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2744]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".pt[2748]" -type "float3" 0 0 1.1920929e-07 ;
	setAttr ".pt[2751]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2752]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2754]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2755]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".pt[2758]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2764]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2765]" -type "float3" 0 0 1.1920929e-07 ;
	setAttr ".pt[2766]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2767]" -type "float3" 0 0 -1.4901161e-08 ;
	setAttr ".pt[2771]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2772]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2773]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2775]" -type "float3" 0 0 -7.4505806e-09 ;
	setAttr ".pt[2777]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2778]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2780]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2782]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2787]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2788]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2790]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2794]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2795]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2797]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2798]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2801]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".pt[2804]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2805]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2806]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2808]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2812]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2814]" -type "float3" 0 0 -1.4901161e-08 ;
	setAttr ".pt[2815]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".pt[2816]" -type "float3" 0 0 1.1920929e-07 ;
	setAttr ".pt[2818]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2825]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2829]" -type "float3" 0 0 1.1920929e-07 ;
	setAttr ".pt[2832]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2833]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2834]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2836]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2838]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2841]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2844]" -type "float3" 0 0 -1.4210855e-14 ;
	setAttr ".pt[2845]" -type "float3" 0 0 2.8421709e-14 ;
	setAttr ".pt[2848]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".pt[2850]" -type "float3" 0 0 1.1920929e-07 ;
	setAttr ".pt[2853]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2854]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2857]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".pt[2859]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2860]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2864]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2866]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2869]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2873]" -type "float3" 0 0 7.4505806e-09 ;
	setAttr ".pt[2874]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2876]" -type "float3" 0 0 1.4901161e-08 ;
	setAttr ".pt[2877]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2880]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2882]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2883]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2884]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".pt[2886]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2889]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".pt[2893]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2895]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2897]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2899]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2901]" -type "float3" 0 0 -1.4901161e-08 ;
	setAttr ".pt[2903]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2905]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2906]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2907]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2910]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2911]" -type "float3" 0 0 -1.1920929e-07 ;
	setAttr ".pt[2912]" -type "float3" 0 0 2.9802322e-08 ;
	setAttr ".pt[2913]" -type "float3" 0 0 -2.9802322e-08 ;
	setAttr ".pt[2914]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2915]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2916]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2918]" -type "float3" 0 0 -5.9604645e-08 ;
	setAttr ".pt[2919]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2920]" -type "float3" 0.2143746 0 0.13224962 ;
	setAttr ".pt[2921]" -type "float3" 0.22946781 0 0.090017974 ;
	setAttr ".pt[2922]" -type "float3" 0.2421713 0 0.081564479 ;
	setAttr ".pt[2923]" -type "float3" 0.22627574 0 0.11982988 ;
	setAttr ".pt[2924]" -type "float3" 0.068091482 0 0.27704683 ;
	setAttr ".pt[2925]" -type "float3" 0.10453072 0 0.25955412 ;
	setAttr ".pt[2926]" -type "float3" 0.11059466 0 0.2351792 ;
	setAttr ".pt[2927]" -type "float3" 0.072218925 0 0.25102916 ;
	setAttr ".pt[2928]" -type "float3" -0.21106319 0 0.1777311 ;
	setAttr ".pt[2929]" -type "float3" -0.18107253 0 0.20334648 ;
	setAttr ".pt[2930]" -type "float3" -0.22601427 0 0.25207853 ;
	setAttr ".pt[2931]" -type "float3" -0.26995161 0 0.22032458 ;
	setAttr ".pt[2932]" -type "float3" 0.19376656 0 -0.14773972 ;
	setAttr ".pt[2933]" -type "float3" 0.1681512 0 -0.1777311 ;
	setAttr ".pt[2934]" -type "float3" 0.25079158 0 -0.22032467 ;
	setAttr ".pt[2935]" -type "float3" 0.28831694 0 -0.18314572 ;
	setAttr ".pt[2936]" -type "float3" -0.043233965 0 0.23469213 ;
	setAttr ".pt[2937]" -type "float3" 0.0037211045 0 0.23761769 ;
	setAttr ".pt[2938]" -type "float3" 0.0037211117 0 0.31662577 ;
	setAttr ".pt[2939]" -type "float3" -0.048957393 0 0.31272766 ;
	setAttr ".pt[2940]" -type "float3" 0.30387914 0 6.3888081e-08 ;
	setAttr ".pt[2941]" -type "float3" 0.30018413 0 -0.037171524 ;
	setAttr ".pt[2942]" -type "float3" 0.33632028 0 -0.049531069 ;
	setAttr ".pt[2943]" -type "float3" 0.34046572 0 4.8406328e-08 ;
	setAttr ".pt[2944]" -type "float3" -0.31972894 0 -0.050746843 ;
	setAttr ".pt[2945]" -type "float3" -0.32372233 0 -3.2842891e-09 ;
	setAttr ".pt[2946]" -type "float3" -0.29004878 0 -4.0672772e-09 ;
	setAttr ".pt[2947]" -type "float3" -0.28669229 0 -0.042642184 ;
	setAttr ".pt[2948]" -type "float3" -0.0080039278 0 -0.32439759 ;
	setAttr ".pt[2949]" -type "float3" -0.058750752 0 -0.32040399 ;
	setAttr ".pt[2950]" -type "float3" -0.052222237 0 -0.26923278 ;
	setAttr ".pt[2951]" -type "float3" -0.0095799686 0 -0.27258852 ;
	setAttr ".pt[2952]" -type "float3" -0.20517094 0 0.26920763 ;
	setAttr ".pt[2953]" -type "float3" -0.16064943 0 0.29649037 ;
	setAttr ".pt[2954]" -type "float3" -0.13572009 0 0.22737841 ;
	setAttr ".pt[2955]" -type "float3" -0.17876756 0 0.2064555 ;
	setAttr ".pt[2956]" -type "float3" 0.28691074 0 -0.15106918 ;
	setAttr ".pt[2957]" -type "float3" 0.259628 0 -0.19559085 ;
	setAttr ".pt[2958]" -type "float3" 0.30499929 0 -0.14999864 ;
	setAttr ".pt[2959]" -type "float3" 0.33687973 0 -0.11585502 ;
	setAttr ".pt[2960]" -type "float3" -0.056797467 0 0.22714244 ;
	setAttr ".pt[2961]" -type "float3" -0.026915431 0 0.23589116 ;
	setAttr ".pt[2962]" -type "float3" -0.03752239 0 0.2972129 ;
	setAttr ".pt[2963]" -type "float3" -0.077750333 0 0.28618994 ;
	setAttr ".pt[2964]" -type "float3" 0.25681213 0 -0.037361424 ;
	setAttr ".pt[2965]" -type "float3" 0.24963845 0 -0.073803037 ;
	setAttr ".pt[2966]" -type "float3" 0.25446448 0 -0.092988744 ;
	setAttr ".pt[2967]" -type "float3" 0.26412246 0 -0.047073774 ;
	setAttr ".pt[2968]" -type "float3" -0.13141632 0 0.22395456 ;
	setAttr ".pt[2969]" -type "float3" -0.073950343 0 0.23904815 ;
	setAttr ".pt[2970]" -type "float3" -0.074310094 0 0.22575632 ;
	setAttr ".pt[2971]" -type "float3" -0.10872295 0 0.21150216 ;
	setAttr ".pt[2972]" -type "float3" 0.21613458 0 -0.077671565 ;
	setAttr ".pt[2973]" -type "float3" 0.20104112 0 -0.11411037 ;
	setAttr ".pt[2974]" -type "float3" 0.21054463 0 -0.10776573 ;
	setAttr ".pt[2975]" -type "float3" 0.22479944 0 -0.073352657 ;
	setAttr ".pt[2976]" -type "float3" 0.19376655 0 0.17122467 ;
	setAttr ".pt[2977]" -type "float3" 0.20457287 0 0.15514471 ;
	setAttr ".pt[2978]" -type "float3" 0.1381598 0 0.23567012 ;
	setAttr ".pt[2979]" -type "float3" 0.14601107 0 0.21353827 ;
	setAttr ".pt[2980]" -type "float3" -0.19964555 0 -0.16225743 ;
	setAttr ".pt[2981]" -type "float3" -0.23363748 0 -0.13487709 ;
	setAttr ".pt[2982]" -type "float3" -0.26588058 0 -0.12833378 ;
	setAttr ".pt[2983]" -type "float3" -0.23176101 0 -0.15438612 ;
	setAttr ".pt[2984]" -type "float3" -0.16438928 0 -0.1856423 ;
	setAttr ".pt[2985]" -type "float3" -0.19368318 0 -0.17663637 ;
	setAttr ".pt[2986]" -type "float3" -0.2946302 0 -0.097109646 ;
	setAttr ".pt[2987]" -type "float3" -0.30614191 0 -0.049160097 ;
	setAttr ".pt[2988]" -type "float3" -0.25822777 0 -0.038128048 ;
	setAttr ".pt[2989]" -type "float3" -0.22419938 0 -0.075317189 ;
	setAttr ".pt[2990]" -type "float3" -0.058740042 0 -0.31038386 ;
	setAttr ".pt[2991]" -type "float3" -0.10668966 0 -0.29887316 ;
	setAttr ".pt[2992]" -type "float3" -0.084460199 0 -0.23180264 ;
	setAttr ".pt[2993]" -type "float3" -0.049190868 0 -0.24073087 ;
	setAttr ".pt[2994]" -type "float3" 0.26245743 0 0.068065308 ;
	setAttr ".pt[2995]" -type "float3" 0.27293509 0 0.034456983 ;
	setAttr ".pt[2996]" -type "float3" 0.25106144 0 0.031525701 ;
	setAttr ".pt[2997]" -type "float3" 0.24157499 0 0.062274899 ;
	setAttr ".pt[2998]" -type "float3" 0.03516598 0 0.21755311 ;
	setAttr ".pt[2999]" -type "float3" 0.078810297 0 0.20948341 ;
	setAttr ".pt[3000]" -type "float3" 0.075306877 0 0.19166276 ;
	setAttr ".pt[3001]" -type "float3" 0.035792805 0 0.19904517 ;
	setAttr ".pt[3002]" -type "float3" 0.16815127 0 0.20598322 ;
	setAttr ".pt[3003]" -type "float3" 0.17759636 0 0.1866394 ;
	setAttr ".pt[3004]" -type "float3" -0.27227467 0 -0.10417561 ;
	setAttr ".pt[3005]" -type "float3" -0.29333103 0 -0.099121757 ;
	setAttr ".pt[3006]" -type "float3" -0.12485686 0 -0.20445625 ;
	setAttr ".pt[3007]" -type "float3" -0.14888832 0 -0.19453752 ;
	setAttr ".pt[3008]" -type "float3" -0.28173548 0 0.093618654 ;
	setAttr ".pt[3009]" -type "float3" -0.2636067 0 0.13753925 ;
	setAttr ".pt[3010]" -type "float3" -0.20369852 0 0.12930018 ;
	setAttr ".pt[3011]" -type "float3" -0.21743521 0 0.088010639 ;
	setAttr ".pt[3012]" -type "float3" 0.1424415 0 -0.26993597 ;
	setAttr ".pt[3013]" -type "float3" 0.09867461 0 -0.28812864 ;
	setAttr ".pt[3014]" -type "float3" 0.066364765 0 -0.27086911 ;
	setAttr ".pt[3015]" -type "float3" 0.099527769 0 -0.25376612 ;
	setAttr ".pt[3016]" -type "float3" -0.32752779 0 0.060251404 ;
	setAttr ".pt[3017]" -type "float3" -0.31341887 0 0.11901934 ;
	setAttr ".pt[3018]" -type "float3" -0.27596858 0 0.086554699 ;
	setAttr ".pt[3019]" -type "float3" -0.28622907 0 0.043816868 ;
	setAttr ".pt[3020]" -type "float3" 0.10943958 0 -0.36630425 ;
	setAttr ".pt[3021]" -type "float3" 0.050671563 0 -0.38041303 ;
	setAttr ".pt[3022]" -type "float3" 0.03423699 0 -0.27664894 ;
	setAttr ".pt[3023]" -type "float3" 0.076974899 0 -0.26638883 ;
	setAttr ".pt[3024]" -type "float3" -0.1965843 0 0.11744812 ;
	setAttr ".pt[3025]" -type "float3" -0.17969604 0 0.15206076 ;
	setAttr ".pt[3026]" -type "float3" -0.23885459 0 0.17807341 ;
	setAttr ".pt[3027]" -type "float3" 0.13827345 0 -0.20929351 ;
	setAttr ".pt[3028]" -type "float3" 0.1107143 0 -0.23050444 ;
	setAttr ".pt[3029]" -type "float3" 0.18283339 0 -0.24509656 ;
	setAttr ".pt[3030]" -type "float3" 0.30018422 0 0.037171599 ;
	setAttr ".pt[3031]" -type "float3" 0.33631986 0 0.049531192 ;
	setAttr ".pt[3032]" -type "float3" 0.050676148 0 0.23469213 ;
	setAttr ".pt[3033]" -type "float3" 0.056399673 0 0.31272763 ;
	setAttr ".pt[3034]" -type "float3" -0.31972894 0 0.050746832 ;
	setAttr ".pt[3035]" -type "float3" -0.28669229 0 0.042642202 ;
	setAttr ".pt[3036]" -type "float3" 0.016657336 0 -0.32040399 ;
	setAttr ".pt[3037]" -type "float3" 0.018008607 0 -0.26923278 ;
	setAttr ".pt[3038]" -type "float3" -0.19796275 0 0.14773987 ;
	setAttr ".pt[3039]" -type "float3" -0.17234735 0 0.1777311 ;
	setAttr ".pt[3040]" -type "float3" -0.16129531 0 0.22373533 ;
	setAttr ".pt[3041]" -type "float3" -0.17364912 0 0.18598127 ;
	setAttr ".pt[3042]" -type "float3" 0.18311511 0 -0.17773096 ;
	setAttr ".pt[3043]" -type "float3" 0.15312386 0 -0.2033466 ;
	setAttr ".pt[3044]" -type "float3" 0.14393657 0 -0.25598115 ;
	setAttr ".pt[3045]" -type "float3" 0.17206293 0 -0.22373559 ;
	setAttr ".pt[3046]" -type "float3" -0.29001805 0 -0.070908993 ;
	setAttr ".pt[3047]" -type "float3" -0.31301099 0 -0.06746906 ;
	setAttr ".pt[3048]" -type "float3" -0.085900895 0 -0.21823582 ;
	setAttr ".pt[3049]" -type "float3" -0.10035078 0 -0.20764884 ;
	setAttr ".pt[3050]" -type "float3" 0.24528058 0 0.099998087 ;
	setAttr ".pt[3051]" -type "float3" 0.22602396 0 0.091491021 ;
	setAttr ".pt[3052]" -type "float3" 0.12027781 0 0.19625705 ;
	setAttr ".pt[3053]" -type "float3" 0.11285058 0 0.17956124 ;
	setAttr ".pt[3054]" -type "float3" -0.22687283 0 0.20754875 ;
	setAttr ".pt[3055]" -type "float3" -0.2709837 0 0.18140435 ;
	setAttr ".pt[3056]" -type "float3" -0.20028406 0 0.20622681 ;
	setAttr ".pt[3057]" -type "float3" -0.23995325 0 0.18024895 ;
	setAttr ".pt[3058]" -type "float3" 0.22702871 0 -0.18140446 ;
	setAttr ".pt[3059]" -type "float3" 0.26470309 0 -0.15079305 ;
	setAttr ".pt[3060]" -type "float3" 0.23020849 0 -0.18024857 ;
	setAttr ".pt[3061]" -type "float3" 0.26408884 0 -0.14983264 ;
	setAttr ".pt[3062]" -type "float3" 0.0037211045 0 0.23883153 ;
	setAttr ".pt[3063]" -type "float3" 0.0037211101 0 0.30091816 ;
	setAttr ".pt[3064]" -type "float3" 0.25922316 0 4.8844012e-08 ;
	setAttr ".pt[3065]" -type "float3" 0.26736763 0 4.541026e-08 ;
	setAttr ".pt[3066]" -type "float3" -0.31001136 0 1.1864926e-07 ;
	setAttr ".pt[3067]" -type "float3" -0.26107419 0 5.5654912e-08 ;
	setAttr ".pt[3068]" -type "float3" -0.0095799444 0 -0.31425381 ;
	setAttr ".pt[3069]" -type "float3" -0.013031163 0 -0.24373125 ;
	setAttr ".pt[3070]" -type "float3" -0.1236906 0 0.22395456 ;
	setAttr ".pt[3071]" -type "float3" -0.17674898 0 0.27762496 ;
	setAttr ".pt[3072]" -type "float3" 0.21437488 0 -0.11411049 ;
	setAttr ".pt[3073]" -type "float3" 0.31850746 0 -0.14145708 ;
	setAttr ".pt[3074]" -type "float3" -0.035598714 0 0.24825558 ;
	setAttr ".pt[3075]" -type "float3" -0.038091011 0 0.23445237 ;
	setAttr ".pt[3076]" -type "float3" 0.22534221 0 -0.039319672 ;
	setAttr ".pt[3077]" -type "float3" 0.23349454 0 -0.037133455 ;
	setAttr ".pt[3078]" -type "float3" -0.11240815 0 0.31647256 ;
	setAttr ".pt[3079]" -type "float3" -0.079348229 0 0.24270298 ;
	setAttr ".pt[3080]" -type "float3" 0.30689266 0 -0.10282789 ;
	setAttr ".pt[3081]" -type "float3" 0.36022982 0 -0.078858964 ;
	setAttr ".pt[3082]" -type "float3" 0.22182882 0 0.12946807 ;
	setAttr ".pt[3083]" -type "float3" 0.20479092 0 0.11845417 ;
	setAttr ".pt[3084]" -type "float3" 0.15854828 0 0.1781977 ;
	setAttr ".pt[3085]" -type "float3" 0.14749901 0 0.16303822 ;
	setAttr ".pt[3086]" -type "float3" -0.18797727 0 -0.1477399 ;
	setAttr ".pt[3087]" -type "float3" -0.15455313 0 -0.17773139 ;
	setAttr ".pt[3088]" -type "float3" -0.2164069 0 -0.16981459 ;
	setAttr ".pt[3089]" -type "float3" -0.18846595 0 -0.20428742 ;
	setAttr ".pt[3090]" -type "float3" -0.1293909 0 -0.20334618 ;
	setAttr ".pt[3091]" -type "float3" -0.16086766 0 -0.23372978 ;
	setAttr ".pt[3092]" -type "float3" -0.30084154 0 -0.035896536 ;
	setAttr ".pt[3093]" -type "float3" -0.3381097 0 -0.034155041 ;
	setAttr ".pt[3094]" -type "float3" -0.040816966 0 -0.22664127 ;
	setAttr ".pt[3095]" -type "float3" -0.046969578 0 -0.21564661 ;
	setAttr ".pt[3096]" -type "float3" 0.28918859 0 0.073427945 ;
	setAttr ".pt[3097]" -type "float3" 0.3239845 0 0.097842827 ;
	setAttr ".pt[3098]" -type "float3" 0.096475057 0 0.22598799 ;
	setAttr ".pt[3099]" -type "float3" 0.10778111 0 0.3011286 ;
	setAttr ".pt[3100]" -type "float3" 0.19267836 0 0.15575047 ;
	setAttr ".pt[3101]" -type "float3" 0.1783995 0 0.14250062 ;
	setAttr ".pt[3102]" -type "float3" -0.2188926 0 -0.11411075 ;
	setAttr ".pt[3103]" -type "float3" -0.24692728 0 -0.13116047 ;
	setAttr ".pt[3104]" -type "float3" -0.099124439 0 -0.22395459 ;
	setAttr ".pt[3105]" -type "float3" -0.12418461 0 -0.25741723 ;
	setAttr ".pt[3106]" -type "float3" -0.29029047 0 0.17485665 ;
	setAttr ".pt[3107]" -type "float3" -0.25914896 0 0.12716159 ;
	setAttr ".pt[3108]" -type "float3" 0.16527668 0 -0.34317544 ;
	setAttr ".pt[3109]" -type "float3" 0.11758139 0 -0.24956895 ;
	setAttr ".pt[3110]" -type "float3" -0.30784518 0 0.10024429 ;
	setAttr ".pt[3111]" -type "float3" -0.2767072 0 0.084234595 ;
	setAttr ".pt[3112]" -type "float3" 0.066154689 0 -0.30852023 ;
	setAttr ".pt[3113]" -type "float3" 0.059600905 0 -0.25924721 ;
	setAttr ".pt[3114]" -type "float3" -0.18494321 0 0.16740632 ;
	setAttr ".pt[3115]" -type "float3" 0.13013378 0 -0.23041457 ;
	setAttr ".pt[3116]" -type "float3" 0.25681201 0 0.037361588 ;
	setAttr ".pt[3117]" -type "float3" 0.26412165 0 0.047074001 ;
	setAttr ".pt[3118]" -type "float3" 0.034357581 0 0.23589116 ;
	setAttr ".pt[3119]" -type "float3" 0.044964563 0 0.2972129 ;
	setAttr ".pt[3120]" -type "float3" -0.30614191 0 0.049160026 ;
	setAttr ".pt[3121]" -type "float3" -0.25822777 0 0.038127974 ;
	setAttr ".pt[3122]" -type "float3" 0.048525445 0 -0.31038409 ;
	setAttr ".pt[3123]" -type "float3" 0.031796511 0 -0.24073063 ;
	setAttr ".pt[3124]" -type "float3" -0.15870439 0 0.18292923 ;
	setAttr ".pt[3125]" -type "float3" -0.20808837 0 0.21422236 ;
	setAttr ".pt[3126]" -type "float3" 0.16285151 0 -0.18292923 ;
	setAttr ".pt[3127]" -type "float3" 0.21885614 0 -0.21422221 ;
	setAttr ".pt[3128]" -type "float3" -0.23247705 0 -0.07767152 ;
	setAttr ".pt[3129]" -type "float3" -0.26339158 0 -0.08927682 ;
	setAttr ".pt[3130]" -type "float3" -0.066329278 0 -0.23904815 ;
	setAttr ".pt[3131]" -type "float3" -0.084436439 0 -0.27476606 ;
	setAttr ".pt[3132]" -type "float3" 0.27116388 0 0.10787612 ;
	setAttr ".pt[3133]" -type "float3" 0.30376253 0 0.14374501 ;
	setAttr ".pt[3134]" -type "float3" 0.1399902 0 0.21171927 ;
	setAttr ".pt[3135]" -type "float3" 0.15660009 0 0.28211546 ;
	setAttr ".pt[3136]" -type "float3" -0.14235589 0 0.20334648 ;
	setAttr ".pt[3137]" -type "float3" -0.13316907 0 0.25598115 ;
	setAttr ".pt[3138]" -type "float3" 0.20873001 0 -0.14773972 ;
	setAttr ".pt[3139]" -type "float3" 0.1960852 0 -0.18598124 ;
	setAttr ".pt[3140]" -type "float3" 0.0037211045 0 0.25135055 ;
	setAttr ".pt[3141]" -type "float3" -0.00095744128 0 0.23737434 ;
	setAttr ".pt[3142]" -type "float3" 0.22843607 0 3.5955935e-08 ;
	setAttr ".pt[3143]" -type "float3" 0.23641685 0 3.3290352e-08 ;
	setAttr ".pt[3144]" -type "float3" -0.30447879 0 -5.8305254e-09 ;
	setAttr ".pt[3145]" -type "float3" -0.34223238 0 -6.7231923e-08 ;
	setAttr ".pt[3146]" -type "float3" 0.012088713 0 -0.22946636 ;
	setAttr ".pt[3147]" -type "float3" 0.036184371 0 -0.21833454 ;
	setAttr ".pt[3148]" -type "float3" -0.17741148 0 0.22858283 ;
	setAttr ".pt[3149]" -type "float3" -0.15580353 0 0.22712667 ;
	setAttr ".pt[3150]" -type "float3" 0.29501289 0 -0.11646888 ;
	setAttr ".pt[3151]" -type "float3" 0.29134634 0 -0.11572688 ;
	setAttr ".pt[3152]" -type "float3" -0.06163504 0 0.32866213 ;
	setAttr ".pt[3153]" -type "float3" -0.041236985 0 0.2520507 ;
	setAttr ".pt[3154]" -type "float3" 0.31908223 0 -0.052054882 ;
	setAttr ".pt[3155]" -type "float3" 0.37447402 0 -0.039920866 ;
	setAttr ".pt[3156]" -type "float3" -0.08725144 0 0.23904815 ;
	setAttr ".pt[3157]" -type "float3" -0.12336659 0 0.29633611 ;
	setAttr ".pt[3158]" -type "float3" 0.22946811 0 -0.077671252 ;
	setAttr ".pt[3159]" -type "float3" 0.34061912 0 -0.096285217 ;
	setAttr ".pt[3160]" -type "float3" 0.24655421 0 0.13966826 ;
	setAttr ".pt[3161]" -type "float3" 0.276153 0 0.18610786 ;
	setAttr ".pt[3162]" -type "float3" 0.18014948 0 0.19223699 ;
	setAttr ".pt[3163]" -type "float3" 0.20165479 0 0.25615564 ;
	setAttr ".pt[3164]" -type "float3" -0.19518842 0 -0.19225925 ;
	setAttr ".pt[3165]" -type "float3" -0.22289732 0 -0.15981616 ;
	setAttr ".pt[3166]" -type "float3" -0.22289732 0 -0.15981616 ;
	setAttr ".pt[3167]" -type "float3" -0.19518842 0 -0.19225925 ;
	setAttr ".pt[3168]" -type "float3" -0.16274554 0 -0.21996818 ;
	setAttr ".pt[3169]" -type "float3" -0.16274554 0 -0.21996795 ;
	setAttr ".pt[3170]" -type "float3" -0.1836261 0 -0.1477399 ;
	setAttr ".pt[3171]" -type "float3" -0.17232613 0 -0.17773139 ;
	setAttr ".pt[3172]" -type "float3" -0.17125975 0 -0.13516076 ;
	setAttr ".pt[3173]" -type "float3" -0.15999365 0 -0.16259889 ;
	setAttr ".pt[3174]" -type "float3" -0.14376619 0 -0.20334618 ;
	setAttr ".pt[3175]" -type "float3" -0.1325556 0 -0.1860331 ;
	setAttr ".pt[3176]" -type "float3" -0.24076338 0 -0.039319828 ;
	setAttr ".pt[3177]" -type "float3" -0.27343524 0 -0.045194834 ;
	setAttr ".pt[3178]" -type "float3" -0.029982755 0 -0.24825594 ;
	setAttr ".pt[3179]" -type "float3" -0.027251082 0 -0.28534934 ;
	setAttr ".pt[3180]" -type "float3" 0.2496381 0 0.073803246 ;
	setAttr ".pt[3181]" -type "float3" 0.25446394 0 0.092988797 ;
	setAttr ".pt[3182]" -type "float3" 0.064239688 0 0.22714244 ;
	setAttr ".pt[3183]" -type "float3" 0.085192472 0 0.28618994 ;
	setAttr ".pt[3184]" -type "float3" 0.21596491 0 0.16802116 ;
	setAttr ".pt[3185]" -type "float3" 0.24183616 0 0.22388847 ;
	setAttr ".pt[3186]" -type "float3" -0.24518968 0 -0.12343796 ;
	setAttr ".pt[3187]" -type "float3" -0.24518968 0 -0.12343796 ;
	setAttr ".pt[3188]" -type "float3" -0.12636735 0 -0.24226084 ;
	setAttr ".pt[3189]" -type "float3" -0.12636735 0 -0.24226049 ;
	setAttr ".pt[3190]" -type "float3" -0.20423381 0 -0.11411075 ;
	setAttr ".pt[3191]" -type "float3" -0.19011308 0 -0.10439491 ;
	setAttr ".pt[3192]" -type "float3" -0.11013698 0 -0.22395459 ;
	setAttr ".pt[3193]" -type "float3" -0.10178988 0 -0.20488629 ;
	setAttr ".pt[3194]" -type "float3" -0.2062757 0 -0.1477399 ;
	setAttr ".pt[3195]" -type "float3" -0.18066102 0 -0.17773139 ;
	setAttr ".pt[3196]" -type "float3" -0.15066941 0 -0.20334618 ;
	setAttr ".pt[3197]" -type "float3" -0.28836536 0 0.14727318 ;
	setAttr ".pt[3198]" -type "float3" -0.26033831 0 0.12375277 ;
	setAttr ".pt[3199]" -type "float3" 0.11318363 0 -0.28903991 ;
	setAttr ".pt[3200]" -type "float3" 0.099118896 0 -0.2428783 ;
	setAttr ".pt[3201]" -type "float3" -0.2946302 0 0.09710981 ;
	setAttr ".pt[3202]" -type "float3" -0.22419938 0 0.07531742 ;
	setAttr ".pt[3203]" -type "float3" 0.096475065 0 -0.29887316 ;
	setAttr ".pt[3204]" -type "float3" 0.056033883 0 -0.23180264 ;
	setAttr ".pt[3205]" -type "float3" -0.25871173 0 0.22638847 ;
	setAttr ".pt[3206]" -type "float3" -0.23618433 0 0.16463728 ;
	setAttr ".pt[3207]" -type "float3" 0.21680836 0 -0.31159657 ;
	setAttr ".pt[3208]" -type "float3" 0.1550574 0 -0.22660375 ;
	setAttr ".pt[3209]" -type "float3" 0.2253418 0 0.03931975 ;
	setAttr ".pt[3210]" -type "float3" 0.23349473 0 0.0371335 ;
	setAttr ".pt[3211]" -type "float3" 0.04304089 0 0.24825558 ;
	setAttr ".pt[3212]" -type "float3" 0.036176123 0 0.23445237 ;
	setAttr ".pt[3213]" -type "float3" -0.22688426 0 -0.11411075 ;
	setAttr ".pt[3214]" -type "float3" -0.11704002 0 -0.22395459 ;
	setAttr ".pt[3215]" -type "float3" -0.30084154 0 0.035896428 ;
	setAttr ".pt[3216]" -type "float3" -0.3381097 0 0.034155 ;
	setAttr ".pt[3217]" -type "float3" 0.058310777 0 -0.22664127 ;
	setAttr ".pt[3218]" -type "float3" 0.088559039 0 -0.21564661 ;
	setAttr ".pt[3219]" -type "float3" -0.161631 0 0.20138972 ;
	setAttr ".pt[3220]" -type "float3" 0.15742891 0 -0.2013898 ;
	setAttr ".pt[3221]" -type "float3" -0.26151749 0 -0.084020443 ;
	setAttr ".pt[3222]" -type "float3" -0.26151749 0 -0.084020443 ;
	setAttr ".pt[3223]" -type "float3" -0.086949833 0 -0.25858781 ;
	setAttr ".pt[3224]" -type "float3" -0.086949833 0 -0.25858781 ;
	setAttr ".pt[3225]" -type "float3" -0.21932799 0 -0.07767152 ;
	setAttr ".pt[3226]" -type "float3" -0.203922 0 -0.071058333 ;
	setAttr ".pt[3227]" -type "float3" -0.073697805 0 -0.23904815 ;
	setAttr ".pt[3228]" -type "float3" -0.065590076 0 -0.21869487 ;
	setAttr ".pt[3229]" -type "float3" 0.23787764 0 0.10842744 ;
	setAttr ".pt[3230]" -type "float3" 0.23863231 0 0.13661423 ;
	setAttr ".pt[3231]" -type "float3" 0.092631623 0 0.21280092 ;
	setAttr ".pt[3232]" -type "float3" 0.12341429 0 0.26812032 ;
	setAttr ".pt[3233]" -type "float3" -0.13412668 0 0.20929351 ;
	setAttr ".pt[3234]" -type "float3" -0.17206585 0 0.24509719 ;
	setAttr ".pt[3235]" -type "float3" 0.18384269 0 -0.15206073 ;
	setAttr ".pt[3236]" -type "float3" 0.24962243 0 -0.17807311 ;
	setAttr ".pt[3237]" -type "float3" -0.009580059 0 0.33275843 ;
	setAttr ".pt[3238]" -type "float3" 0.03754564 0 0.255193 ;
	setAttr ".pt[3239]" -type "float3" 0.32317904 0 5.1483376e-08 ;
	setAttr ".pt[3240]" -type "float3" 0.37926078 0 6.724035e-08 ;
	setAttr ".pt[3241]" -type "float3" -0.24354821 0 5.5859253e-08 ;
	setAttr ".pt[3242]" -type "float3" -0.27681041 0 -4.2362571e-09 ;
	setAttr ".pt[3243]" -type "float3" 0.0054051224 0 -0.25135058 ;
	setAttr ".pt[3244]" -type "float3" 0.015639216 0 -0.28890601 ;
	setAttr ".pt[3245]" -type "float3" -0.10872674 0 0.22395456 ;
	setAttr ".pt[3246]" -type "float3" -0.10163075 0 0.28192386 ;
	setAttr ".pt[3247]" -type "float3" 0.22933821 0 -0.11411037 ;
	setAttr ".pt[3248]" -type "float3" 0.21541212 0 -0.14364731 ;
	setAttr ".pt[3249]" -type "float3" -0.04889987 0 0.24825558 ;
	setAttr ".pt[3250]" -type "float3" -0.067182481 0 0.30775008 ;
	setAttr ".pt[3251]" -type "float3" 0.23867598 0 -0.039319731 ;
	setAttr ".pt[3252]" -type "float3" 0.35410747 0 -0.048742708 ;
	setAttr ".pt[3253]" -type "float3" -0.12381763 0 0.24398835 ;
	setAttr ".pt[3254]" -type "float3" -0.10760655 0 0.24243417 ;
	setAttr ".pt[3255]" -type "float3" 0.31721178 0 -0.079276487 ;
	setAttr ".pt[3256]" -type "float3" 0.31131098 0 -0.078771383 ;
	setAttr ".pt[3257]" -type "float3" -0.24197759 0 -0.077671587 ;
	setAttr ".pt[3258]" -type "float3" -0.080600858 0 -0.23904815 ;
	setAttr ".pt[3259]" -type "float3" 0.22182123 0 0.14038172 ;
	setAttr ".pt[3260]" -type "float3" 0.21701579 0 0.17687526 ;
	setAttr ".pt[3261]" -type "float3" 0.11883418 0 0.19321935 ;
	setAttr ".pt[3262]" -type "float3" 0.15868874 0 0.24344806 ;
	setAttr ".pt[3263]" -type "float3" -0.2285348 0 -0.039319921 ;
	setAttr ".pt[3264]" -type "float3" -0.21234487 0 -0.035971988 ;
	setAttr ".pt[3265]" -type "float3" -0.03391473 0 -0.24825594 ;
	setAttr ".pt[3266]" -type "float3" -0.03050378 0 -0.22711842 ;
	setAttr ".pt[3267]" -type "float3" -0.27147716 0 -0.042533793 ;
	setAttr ".pt[3268]" -type "float3" -0.27147716 0 -0.042533793 ;
	setAttr ".pt[3269]" -type "float3" -0.045463201 0 -0.26854831 ;
	setAttr ".pt[3270]" -type "float3" -0.045463201 0 -0.26854831 ;
	setAttr ".pt[3271]" -type "float3" 0.21613476 0 0.077671468 ;
	setAttr ".pt[3272]" -type "float3" 0.22479963 0 0.073352806 ;
	setAttr ".pt[3273]" -type "float3" 0.068471394 0 0.23904815 ;
	setAttr ".pt[3274]" -type "float3" 0.072395273 0 0.22575632 ;
	setAttr ".pt[3275]" -type "float3" 0.20186254 0 0.16887991 ;
	setAttr ".pt[3276]" -type "float3" 0.19014737 0 0.21278131 ;
	setAttr ".pt[3277]" -type "float3" -0.25118452 0 -0.039319921 ;
	setAttr ".pt[3278]" -type "float3" -0.042249154 0 -0.24825594 ;
	setAttr ".pt[3279]" -type "float3" -0.27575943 0 0.14266813 ;
	setAttr ".pt[3280]" -type "float3" -0.20874254 0 0.11065175 ;
	setAttr ".pt[3281]" -type "float3" 0.14203341 0 -0.28000173 ;
	setAttr ".pt[3282]" -type "float3" 0.089544468 0 -0.21716687 ;
	setAttr ".pt[3283]" -type "float3" -0.29001805 0 0.070909031 ;
	setAttr ".pt[3284]" -type "float3" -0.31301099 0 0.067469135 ;
	setAttr ".pt[3285]" -type "float3" 0.10339471 0 -0.21823594 ;
	setAttr ".pt[3286]" -type "float3" 0.13065635 0 -0.20764884 ;
	setAttr ".pt[3287]" -type "float3" -0.25388774 0 0.19067593 ;
	setAttr ".pt[3288]" -type "float3" -0.2379889 0 0.16022338 ;
	setAttr ".pt[3289]" -type "float3" 0.15658656 0 -0.26244307 ;
	setAttr ".pt[3290]" -type "float3" 0.14031775 0 -0.22052862 ;
	setAttr ".pt[3291]" -type "float3" 0.31908226 0 0.052055039 ;
	setAttr ".pt[3292]" -type "float3" 0.37447351 0 0.039920986 ;
	setAttr ".pt[3293]" -type "float3" 0.042474974 0 0.32866213 ;
	setAttr ".pt[3294]" -type "float3" 0.069202609 0 0.2520507 ;
	setAttr ".pt[3295]" -type "float3" -0.24076338 0 0.039319843 ;
	setAttr ".pt[3296]" -type "float3" -0.27343524 0 0.04519482 ;
	setAttr ".pt[3297]" -type "float3" 0.034692585 0 -0.2482557 ;
	setAttr ".pt[3298]" -type "float3" 0.05852956 0 -0.28534934 ;
	setAttr ".pt[3299]" -type "float3" -0.21946025 0 0.27234536 ;
	setAttr ".pt[3300]" -type "float3" -0.2076389 0 0.19805862 ;
	setAttr ".pt[3301]" -type "float3" 0.26276606 0 -0.27234557 ;
	setAttr ".pt[3302]" -type "float3" 0.18847895 0 -0.19805883 ;
	setAttr ".pt[3303]" -type "float3" 0.20104083 0 0.11411069 ;
	setAttr ".pt[3304]" -type "float3" 0.21054494 0 0.107766 ;
	setAttr ".pt[3305]" -type "float3" 0.10491058 0 0.22395423 ;
	setAttr ".pt[3306]" -type "float3" 0.10680853 0 0.21150246 ;
	setAttr ".pt[3307]" -type "float3" -0.13433576 0 0.23041457 ;
	setAttr ".pt[3308]" -type "float3" 0.18074101 0 -0.16740604 ;
	setAttr ".pt[3309]" -type "float3" -0.0095800525 0 0.25135055 ;
	setAttr ".pt[3310]" -type "float3" -0.0095800404 0 0.31158546 ;
	setAttr ".pt[3311]" -type "float3" 0.2417701 0 3.5955935e-08 ;
	setAttr ".pt[3312]" -type "float3" 0.35864061 0 1.964203e-08 ;
	setAttr ".pt[3313]" -type "float3" -0.23162946 0 -5.2435696e-09 ;
	setAttr ".pt[3314]" -type "float3" -0.21517603 0 -6.6920386e-08 ;
	setAttr ".pt[3315]" -type "float3" 0.046030749 0 -0.25135058 ;
	setAttr ".pt[3316]" -type "float3" 0.0054682246 0 -0.22994892 ;
	setAttr ".pt[3317]" -type "float3" -0.088129424 0 0.2305042 ;
	setAttr ".pt[3318]" -type "float3" -0.13167386 0 0.26993585 ;
	setAttr ".pt[3319]" -type "float3" 0.20073095 0 -0.11744781 ;
	setAttr ".pt[3320]" -type "float3" 0.27437463 0 -0.1375391 ;
	setAttr ".pt[3321]" -type "float3" -0.067410879 0 0.25338617 ;
	setAttr ".pt[3322]" -type "float3" -0.056879759 0 0.25177225 ;
	setAttr ".pt[3323]" -type "float3" 0.33075407 0 -0.040132258 ;
	setAttr ".pt[3324]" -type "float3" 0.32348913 0 -0.039876707 ;
	setAttr ".pt[3325]" -type "float3" -0.072287761 0 0.23904815 ;
	setAttr ".pt[3326]" -type "float3" -0.067457847 0 0.30092409 ;
	setAttr ".pt[3327]" -type "float3" 0.24443202 0 -0.077671438 ;
	setAttr ".pt[3328]" -type "float3" 0.22956674 0 -0.097776055 ;
	setAttr ".pt[3329]" -type "float3" -0.27482486 0 -4.6925002e-09 ;
	setAttr ".pt[3330]" -type "float3" -0.27482486 0 -4.6925002e-09 ;
	setAttr ".pt[3331]" -type "float3" -0.002929379 0 -0.27189556 ;
	setAttr ".pt[3332]" -type "float3" -0.002929379 0 -0.27189556 ;
	setAttr ".pt[3333]" -type "float3" 0.1804329 0 0.14773989 ;
	setAttr ".pt[3334]" -type "float3" 0.19108273 0 0.13952541 ;
	setAttr ".pt[3335]" -type "float3" 0.13853973 0 0.20334642 ;
	setAttr ".pt[3336]" -type "float3" 0.13856782 0 0.19203986 ;
	setAttr ".pt[3337]" -type "float3" -0.25427943 0 -3.5794969e-08 ;
	setAttr ".pt[3338]" -type "float3" -0.0029293913 0 -0.25135058 ;
	setAttr ".pt[3339]" -type "float3" 0.30689195 0 0.10282808 ;
	setAttr ".pt[3340]" -type "float3" 0.36022982 0 0.078858942 ;
	setAttr ".pt[3341]" -type "float3" 0.093248107 0 0.3164728 ;
	setAttr ".pt[3342]" -type "float3" 0.11057851 0 0.24270298 ;
	setAttr ".pt[3343]" -type "float3" 0.16853113 0 0.17773138 ;
	setAttr ".pt[3344]" -type "float3" 0.16689159 0 0.16784909 ;
	setAttr ".pt[3345]" -type "float3" -0.27227467 0 0.10417547 ;
	setAttr ".pt[3346]" -type "float3" -0.29333103 0 0.099121913 ;
	setAttr ".pt[3347]" -type "float3" 0.1395469 0 -0.20445634 ;
	setAttr ".pt[3348]" -type "float3" 0.17919384 0 -0.19453724 ;
	setAttr ".pt[3349]" -type "float3" -0.23247705 0 0.077671446 ;
	setAttr ".pt[3350]" -type "float3" -0.26339158 0 0.089276858 ;
	setAttr ".pt[3351]" -type "float3" 0.069209136 0 -0.23904815 ;
	setAttr ".pt[3352]" -type "float3" 0.09012977 0 -0.27476606 ;
	setAttr ".pt[3353]" -type "float3" -0.2499937 0 0.18471384 ;
	setAttr ".pt[3354]" -type "float3" -0.18979087 0 0.14326188 ;
	setAttr ".pt[3355]" -type "float3" 0.19747473 0 -0.25423682 ;
	setAttr ".pt[3356]" -type "float3" 0.12047108 0 -0.19718319 ;
	setAttr ".pt[3357]" -type "float3" 0.23867545 0 0.03931975 ;
	setAttr ".pt[3358]" -type "float3" 0.35410723 0 0.0487427 ;
	setAttr ".pt[3359]" -type "float3" 0.029739732 0 0.24825534 ;
	setAttr ".pt[3360]" -type "float3" 0.048022404 0 0.30775008 ;
	setAttr ".pt[3361]" -type "float3" -0.2285348 0 0.03931975 ;
	setAttr ".pt[3362]" -type "float3" -0.21234487 0 0.035971969 ;
	setAttr ".pt[3363]" -type "float3" 0.08535067 0 -0.24825594 ;
	setAttr ".pt[3364]" -type "float3" 0.038577154 0 -0.22711842 ;
	setAttr ".pt[3365]" -type "float3" -0.22082874 0 0.22938357 ;
	setAttr ".pt[3366]" -type "float3" -0.21020925 0 0.19274908 ;
	setAttr ".pt[3367]" -type "float3" 0.207114 0 -0.22938357 ;
	setAttr ".pt[3368]" -type "float3" 0.17284338 0 -0.19274908 ;
	setAttr ".pt[3369]" -type "float3" 0.2869103 0 0.15106942 ;
	setAttr ".pt[3370]" -type "float3" 0.33687946 0 0.11585522 ;
	setAttr ".pt[3371]" -type "float3" 0.14148945 0 0.29649016 ;
	setAttr ".pt[3372]" -type "float3" 0.14328268 0 0.22737855 ;
	setAttr ".pt[3373]" -type "float3" -0.27147716 0 0.042533778 ;
	setAttr ".pt[3374]" -type "float3" -0.27147716 0 0.042533778 ;
	setAttr ".pt[3375]" -type "float3" 0.03960444 0 -0.26854831 ;
	setAttr ".pt[3376]" -type "float3" 0.03960444 0 -0.26854831 ;
	setAttr ".pt[3377]" -type "float3" -0.18671668 0 0.31159657 ;
	setAttr ".pt[3378]" -type "float3" -0.17421722 0 0.22660372 ;
	setAttr ".pt[3379]" -type "float3" 0.30201724 0 -0.22638804 ;
	setAttr ".pt[3380]" -type "float3" 0.21702383 0 -0.16463751 ;
	setAttr ".pt[3381]" -type "float3" -0.009580059 0 0.25654465 ;
	setAttr ".pt[3382]" -type "float3" -0.0048724748 0 0.25491026 ;
	setAttr ".pt[3383]" -type "float3" 0.33530518 0 -2.4156172e-08 ;
	setAttr ".pt[3384]" -type "float3" 0.3275817 0 -2.446783e-08 ;
	setAttr ".pt[3385]" -type "float3" -0.10372981 0 0.25376612 ;
	setAttr ".pt[3386]" -type "float3" 0.19949611 0 -0.12930022 ;
	setAttr ".pt[3387]" -type "float3" -0.033936087 0 0.24825558 ;
	setAttr ".pt[3388]" -type "float3" -0.031490952 0 0.31251478 ;
	setAttr ".pt[3389]" -type "float3" 0.25363928 0 -0.039319731 ;
	setAttr ".pt[3390]" -type "float3" 0.23820181 0 -0.049497422 ;
	setAttr ".pt[3391]" -type "float3" -0.058267806 0 0.24603969 ;
	setAttr ".pt[3392]" -type "float3" -0.087907083 0 0.28812888 ;
	setAttr ".pt[3393]" -type "float3" 0.21310009 0 -0.079942949 ;
	setAttr ".pt[3394]" -type "float3" 0.29250365 0 -0.093618691 ;
	setAttr ".pt[3395]" -type "float3" -0.25118452 0 0.039319843 ;
	setAttr ".pt[3396]" -type "float3" 0.036390435 0 -0.24825594 ;
	setAttr ".pt[3397]" -type "float3" 0.25962818 0 0.19559069 ;
	setAttr ".pt[3398]" -type "float3" 0.30499905 0 0.14999871 ;
	setAttr ".pt[3399]" -type "float3" 0.18601073 0 0.26920757 ;
	setAttr ".pt[3400]" -type "float3" 0.19530767 0 0.20645578 ;
	setAttr ".pt[3401]" -type "float3" 0.22946781 0 0.077671349 ;
	setAttr ".pt[3402]" -type "float3" 0.34061888 0 0.096285343 ;
	setAttr ".pt[3403]" -type "float3" 0.068091482 0 0.23904815 ;
	setAttr ".pt[3404]" -type "float3" 0.10420655 0 0.29633611 ;
	setAttr ".pt[3405]" -type "float3" 0.22571589 0 0.23529625 ;
	setAttr ".pt[3406]" -type "float3" 0.24904975 0 0.18044873 ;
	setAttr ".pt[3407]" -type "float3" -0.2188926 0 0.11411073 ;
	setAttr ".pt[3408]" -type "float3" -0.24692728 0 0.13116065 ;
	setAttr ".pt[3409]" -type "float3" 0.10200422 0 -0.22395432 ;
	setAttr ".pt[3410]" -type "float3" 0.11745057 0 -0.25741723 ;
	setAttr ".pt[3411]" -type "float3" -0.21932799 0 0.077671565 ;
	setAttr ".pt[3412]" -type "float3" -0.203922 0 0.071058139 ;
	setAttr ".pt[3413]" -type "float3" 0.12370202 0 -0.23904815 ;
	setAttr ".pt[3414]" -type "float3" 0.086547315 0 -0.21869487 ;
	setAttr ".pt[3415]" -type "float3" -0.23363765 0 0.13487718 ;
	setAttr ".pt[3416]" -type "float3" -0.26588061 0 0.12833375 ;
	setAttr ".pt[3417]" -type "float3" 0.17959371 0 -0.18564251 ;
	setAttr ".pt[3418]" -type "float3" 0.22398908 0 -0.17663635 ;
	setAttr ".pt[3419]" -type "float3" 0.33075336 0 0.040132459 ;
	setAttr ".pt[3420]" -type "float3" 0.32348892 0 0.039876763 ;
	setAttr ".pt[3421]" -type "float3" 0.048250772 0 0.25338617 ;
	setAttr ".pt[3422]" -type "float3" 0.047134735 0 0.25177225 ;
	setAttr ".pt[3423]" -type "float3" -0.21718055 0 0.22221079 ;
	setAttr ".pt[3424]" -type "float3" -0.1662343 0 0.17234431 ;
	setAttr ".pt[3425]" -type "float3" 0.23497233 0 -0.22221091 ;
	setAttr ".pt[3426]" -type "float3" 0.15908392 0 -0.17234431 ;
	setAttr ".pt[3427]" -type "float3" 0.2143746 0 0.11411069 ;
	setAttr ".pt[3428]" -type "float3" 0.31850725 0 0.14145721 ;
	setAttr ".pt[3429]" -type "float3" 0.10453072 0 0.22395423 ;
	setAttr ".pt[3430]" -type "float3" 0.15758888 0 0.27762502 ;
	setAttr ".pt[3431]" -type "float3" -0.26151749 0 0.084020361 ;
	setAttr ".pt[3432]" -type "float3" -0.26151749 0 0.084020361 ;
	setAttr ".pt[3433]" -type "float3" 0.081090949 0 -0.25858805 ;
	setAttr ".pt[3434]" -type "float3" 0.081090949 0 -0.25858805 ;
	setAttr ".pt[3435]" -type "float3" -0.18212086 0 0.26244295 ;
	setAttr ".pt[3436]" -type "float3" -0.17768343 0 0.22052851 ;
	setAttr ".pt[3437]" -type "float3" 0.24017335 0 -0.19067565 ;
	setAttr ".pt[3438]" -type "float3" 0.20062312 0 -0.16022335 ;
	setAttr ".pt[3439]" -type "float3" 0.0053837416 0 0.25135055 ;
	setAttr ".pt[3440]" -type "float3" 0.0053837439 0 0.31641027 ;
	setAttr ".pt[3441]" -type "float3" 0.25673366 0 3.5955935e-08 ;
	setAttr ".pt[3442]" -type "float3" 0.24110386 0 4.836512e-08 ;
	setAttr ".pt[3443]" -type "float3" -0.13518535 0 0.34317499 ;
	setAttr ".pt[3444]" -type "float3" -0.13674174 0 0.24956892 ;
	setAttr ".pt[3445]" -type "float3" 0.33359617 0 -0.17485668 ;
	setAttr ".pt[3446]" -type "float3" 0.23998892 0 -0.12716141 ;
	setAttr ".pt[3447]" -type "float3" -0.24197759 0 0.077671446 ;
	setAttr ".pt[3448]" -type "float3" 0.074742019 0 -0.23904815 ;
	setAttr ".pt[3449]" -type "float3" -0.026838696 0 0.2555159 ;
	setAttr ".pt[3450]" -type "float3" -0.041843109 0 0.29922649 ;
	setAttr ".pt[3451]" -type "float3" 0.22064553 0 -0.040469669 ;
	setAttr ".pt[3452]" -type "float3" 0.3035624 0 -0.047392707 ;
	setAttr ".pt[3453]" -type "float3" -0.070566989 0 0.27086911 ;
	setAttr ".pt[3454]" -type "float3" 0.21323282 0 -0.088010505 ;
	setAttr ".pt[3455]" -type "float3" 0.19376655 0 0.14773989 ;
	setAttr ".pt[3456]" -type "float3" 0.28831747 0 0.18314567 ;
	setAttr ".pt[3457]" -type "float3" 0.1381598 0 0.20334618 ;
	setAttr ".pt[3458]" -type "float3" 0.20685503 0 0.25207847 ;
	setAttr ".pt[3459]" -type "float3" 0.3172113 0 0.079276636 ;
	setAttr ".pt[3460]" -type "float3" 0.31131053 0 0.078771591 ;
	setAttr ".pt[3461]" -type "float3" 0.10465759 0 0.24398859 ;
	setAttr ".pt[3462]" -type "float3" 0.097861588 0 0.24243417 ;
	setAttr ".pt[3463]" -type "float3" 0.16815127 0 0.17773138 ;
	setAttr ".pt[3464]" -type "float3" 0.25079158 0 0.22032462 ;
	setAttr ".pt[3465]" -type "float3" -0.20423381 0 0.11411073 ;
	setAttr ".pt[3466]" -type "float3" -0.19011308 0 0.10439494 ;
	setAttr ".pt[3467]" -type "float3" 0.16014118 0 -0.22395432 ;
	setAttr ".pt[3468]" -type "float3" 0.11988379 0 -0.20488626 ;
	setAttr ".pt[3469]" -type "float3" -0.1879773 0 0.14773999 ;
	setAttr ".pt[3470]" -type "float3" -0.21640681 0 0.16981457 ;
	setAttr ".pt[3471]" -type "float3" 0.13837099 0 -0.20334648 ;
	setAttr ".pt[3472]" -type "float3" 0.15413398 0 -0.23373009 ;
	setAttr ".pt[3473]" -type "float3" 0.25363925 0 0.03931975 ;
	setAttr ".pt[3474]" -type "float3" 0.23820156 0 0.04949766 ;
	setAttr ".pt[3475]" -type "float3" 0.044703536 0 0.24825534 ;
	setAttr ".pt[3476]" -type "float3" 0.042258415 0 0.31251478 ;
	setAttr ".pt[3477]" -type "float3" -0.19964546 0 0.16225751 ;
	setAttr ".pt[3478]" -type "float3" -0.2317607 0 0.15438581 ;
	setAttr ".pt[3479]" -type "float3" 0.21484999 0 -0.16225727 ;
	setAttr ".pt[3480]" -type "float3" 0.27292594 0 -0.15438592 ;
	setAttr ".pt[3481]" -type "float3" 0.29501271 0 0.11646874 ;
	setAttr ".pt[3482]" -type "float3" 0.2913464 0 0.1157269 ;
	setAttr ".pt[3483]" -type "float3" 0.15825151 0 0.22858286 ;
	setAttr ".pt[3484]" -type "float3" 0.14605859 0 0.22712696 ;
	setAttr ".pt[3485]" -type "float3" -0.17968319 0 0.25423646 ;
	setAttr ".pt[3486]" -type "float3" -0.14022927 0 0.19718333 ;
	setAttr ".pt[3487]" -type "float3" 0.26699758 0 -0.1847138 ;
	setAttr ".pt[3488]" -type "float3" 0.18421659 0 -0.14326186 ;
	setAttr ".pt[3489]" -type "float3" 0.0053837379 0 0.25870126 ;
	setAttr ".pt[3490]" -type "float3" 0.0053837555 0 0.30295634 ;
	setAttr ".pt[3491]" -type "float3" 0.22318146 0 5.2633776e-08 ;
	setAttr ".pt[3492]" -type "float3" 0.30727926 0 4.5799073e-08 ;
	setAttr ".pt[3493]" -type "float3" -0.24518968 0 0.12343793 ;
	setAttr ".pt[3494]" -type "float3" -0.24518968 0 0.12343793 ;
	setAttr ".pt[3495]" -type "float3" 0.12050854 0 -0.24226081 ;
	setAttr ".pt[3496]" -type "float3" 0.12050854 0 -0.24226034 ;
	setAttr ".pt[3497]" -type "float3" -0.13871835 0 0.28903964 ;
	setAttr ".pt[3498]" -type "float3" -0.14121249 0 0.24287827 ;
	setAttr ".pt[3499]" -type "float3" 0.26677057 0 -0.14727311 ;
	setAttr ".pt[3500]" -type "float3" 0.21824446 0 -0.12375253 ;
	setAttr ".pt[3501]" -type "float3" -0.035663255 0 0.28130203 ;
	setAttr ".pt[3502]" -type "float3" 0.22161202 0 -0.044553723 ;
	setAttr ".pt[3503]" -type "float3" -0.22688426 0 0.11411073 ;
	setAttr ".pt[3504]" -type "float3" 0.11118114 0 -0.22395432 ;
	setAttr ".pt[3505]" -type "float3" -0.094964273 0 0.36630404 ;
	setAttr ".pt[3506]" -type "float3" -0.096134819 0 0.2663888 ;
	setAttr ".pt[3507]" -type "float3" 0.35672468 0 -0.11901911 ;
	setAttr ".pt[3508]" -type "float3" 0.25680855 0 -0.086554937 ;
	setAttr ".pt[3509]" -type "float3" 0.26470298 0 0.15079285 ;
	setAttr ".pt[3510]" -type "float3" 0.26408842 0 0.14983268 ;
	setAttr ".pt[3511]" -type "float3" 0.18291737 0 0.2075488 ;
	setAttr ".pt[3512]" -type "float3" 0.19053909 0 0.20622699 ;
	setAttr ".pt[3513]" -type "float3" 0.2444315 0 0.077671468 ;
	setAttr ".pt[3514]" -type "float3" 0.22956672 0 0.097776182 ;
	setAttr ".pt[3515]" -type "float3" 0.08305525 0 0.23904815 ;
	setAttr ".pt[3516]" -type "float3" 0.078225262 0 0.30092409 ;
	setAttr ".pt[3517]" -type "float3" 0.22702847 0 0.18140452 ;
	setAttr ".pt[3518]" -type "float3" 0.23020814 0 0.18024887 ;
	setAttr ".pt[3519]" -type "float3" -0.183626 0 0.14773987 ;
	setAttr ".pt[3520]" -type "float3" -0.17125988 0 0.13516083 ;
	setAttr ".pt[3521]" -type "float3" 0.18804422 0 -0.2033466 ;
	setAttr ".pt[3522]" -type "float3" 0.15065004 0 -0.18603282 ;
	setAttr ".pt[3523]" -type "float3" 0.2206455 0 0.040469788 ;
	setAttr ".pt[3524]" -type "float3" 0.30356193 0 0.047392726 ;
	setAttr ".pt[3525]" -type "float3" 0.037606228 0 0.2555159 ;
	setAttr ".pt[3526]" -type "float3" 0.052610528 0 0.29922649 ;
	setAttr ".pt[3527]" -type "float3" -0.15455328 0 0.1777311 ;
	setAttr ".pt[3528]" -type "float3" -0.18846573 0 0.20428738 ;
	setAttr ".pt[3529]" -type "float3" 0.16536364 0 -0.17773096 ;
	setAttr ".pt[3530]" -type "float3" 0.18684906 0 -0.20428735 ;
	setAttr ".pt[3531]" -type "float3" 0.22933851 0 0.11411069 ;
	setAttr ".pt[3532]" -type "float3" 0.21541184 0 0.1436474 ;
	setAttr ".pt[3533]" -type "float3" 0.11949441 0 0.22395423 ;
	setAttr ".pt[3534]" -type "float3" 0.11239836 0 0.28192386 ;
	setAttr ".pt[3535]" -type "float3" -0.16438958 0 0.18564226 ;
	setAttr ".pt[3536]" -type "float3" -0.19368301 0 0.17663632 ;
	setAttr ".pt[3537]" -type "float3" 0.2449619 0 -0.13487703 ;
	setAttr ".pt[3538]" -type "float3" 0.2960602 0 -0.12833391 ;
	setAttr ".pt[3539]" -type "float3" -0.004323822 0 0.28480873 ;
	setAttr ".pt[3540]" -type "float3" 0.22442904 0 1.034404e-07 ;
	setAttr ".pt[3541]" -type "float3" -0.16012803 0 0.28000209 ;
	setAttr ".pt[3542]" -type "float3" -0.10930267 0 0.21716654 ;
	setAttr ".pt[3543]" -type "float3" 0.29276311 0 -0.14266793 ;
	setAttr ".pt[3544]" -type "float3" 0.20316863 0 -0.11065164 ;
	setAttr ".pt[3545]" -type "float3" -0.062623993 0 0.38041282 ;
	setAttr ".pt[3546]" -type "float3" -0.053396866 0 0.27664942 ;
	setAttr ".pt[3547]" -type "float3" 0.37083286 0 -0.060251359 ;
	setAttr ".pt[3548]" -type "float3" 0.2670691 0 -0.043816838 ;
	setAttr ".pt[3549]" -type "float3" -0.10824838 0 0.30852047 ;
	setAttr ".pt[3550]" -type "float3" -0.09381447 0 0.25924721 ;
	setAttr ".pt[3551]" -type "float3" 0.28625059 0 -0.10024427 ;
	setAttr ".pt[3552]" -type "float3" 0.24091747 0 -0.084234469 ;
	setAttr ".pt[3553]" -type "float3" -0.22289771 0 0.15981624 ;
	setAttr ".pt[3554]" -type "float3" -0.22289771 0 0.15981624 ;
	setAttr ".pt[3555]" -type "float3" 0.15688677 0 -0.21996775 ;
	setAttr ".pt[3556]" -type "float3" 0.15688677 0 -0.21996775 ;
	setAttr ".pt[3557]" -type "float3" -0.20627621 0 0.14773987 ;
	setAttr ".pt[3558]" -type "float3" 0.14481089 0 -0.2033466 ;
	setAttr ".pt[3559]" -type "float3" 0.20872986 0 0.14773989 ;
	setAttr ".pt[3560]" -type "float3" 0.19608541 0 0.18598093 ;
	setAttr ".pt[3561]" -type "float3" 0.15312386 0 0.20334642 ;
	setAttr ".pt[3562]" -type "float3" 0.14393656 0 0.255981 ;
	setAttr ".pt[3563]" -type "float3" 0.21309997 0 0.079942979 ;
	setAttr ".pt[3564]" -type "float3" 0.29250345 0 0.093618743 ;
	setAttr ".pt[3565]" -type "float3" 0.069035366 0 0.24603969 ;
	setAttr ".pt[3566]" -type "float3" 0.098674588 0 0.28812888 ;
	setAttr ".pt[3567]" -type "float3" 0.18311508 0 0.17773138 ;
	setAttr ".pt[3568]" -type "float3" 0.17206278 0 0.223736 ;
	setAttr ".pt[3569]" -type "float3" 0.22161214 0 0.04455376 ;
	setAttr ".pt[3570]" -type "float3" 0.031460926 0 0.28130168 ;
	setAttr ".pt[3571]" -type "float3" -0.1723263 0 0.1777311 ;
	setAttr ".pt[3572]" -type "float3" -0.15999369 0 0.16259862 ;
	setAttr ".pt[3573]" -type "float3" 0.21803583 0 -0.17773096 ;
	setAttr ".pt[3574]" -type "float3" 0.16520394 0 -0.16259885 ;
	setAttr ".pt[3575]" -type "float3" 0.20073095 0 0.117448 ;
	setAttr ".pt[3576]" -type "float3" 0.27437454 0 0.13753921 ;
	setAttr ".pt[3577]" -type "float3" 0.11071403 0 0.23050457 ;
	setAttr ".pt[3578]" -type "float3" 0.14244123 0 0.269936 ;
	setAttr ".pt[3579]" -type "float3" -0.12939112 0 0.20334648 ;
	setAttr ".pt[3580]" -type "float3" -0.16086772 0 0.23372985 ;
	setAttr ".pt[3581]" -type "float3" 0.17865655 0 -0.14773972 ;
	setAttr ".pt[3582]" -type "float3" 0.23671976 0 -0.16981439 ;
	setAttr ".pt[3583]" -type "float3" -0.009580059 0 0.38515535 ;
	setAttr ".pt[3584]" -type "float3" -0.009580058 0 0.28009763 ;
	setAttr ".pt[3585]" -type "float3" 0.37557447 0 3.7435655e-10 ;
	setAttr ".pt[3586]" -type "float3" 0.2705175 0 4.1438984e-08 ;
	setAttr ".pt[3587]" -type "float3" -0.12485693 0 0.20445623 ;
	setAttr ".pt[3588]" -type "float3" -0.14888817 0 0.19453749 ;
	setAttr ".pt[3589]" -type "float3" 0.26867321 0 -0.10417523 ;
	setAttr ".pt[3590]" -type "float3" 0.32351097 0 -0.09912169 ;
	setAttr ".pt[3591]" -type "float3" -0.05875086 0 0.32040375 ;
	setAttr ".pt[3592]" -type "float3" -0.05222239 0 0.26923218 ;
	setAttr ".pt[3593]" -type "float3" 0.29813385 0 -0.050746813 ;
	setAttr ".pt[3594]" -type "float3" 0.25090289 0 -0.042642146 ;
	setAttr ".pt[3595]" -type "float3" -0.10668962 0 0.29887292 ;
	setAttr ".pt[3596]" -type "float3" -0.084460326 0 0.231803 ;
	setAttr ".pt[3597]" -type "float3" 0.31163403 0 -0.097109661 ;
	setAttr ".pt[3598]" -type "float3" 0.21704867 0 -0.075317293 ;
	setAttr ".pt[3599]" -type "float3" 0.18384267 0 0.15206042 ;
	setAttr ".pt[3600]" -type "float3" 0.24962218 0 0.17807341 ;
	setAttr ".pt[3601]" -type "float3" 0.1382734 0 0.20929344 ;
	setAttr ".pt[3602]" -type "float3" 0.18283325 0 0.24509664 ;
	setAttr ".pt[3603]" -type "float3" -0.19518855 0 0.19225945 ;
	setAttr ".pt[3604]" -type "float3" -0.19518855 0 0.19225945 ;
	setAttr ".pt[3605]" -type "float3" 0.18932976 0 -0.19225919 ;
	setAttr ".pt[3606]" -type "float3" 0.18932976 0 -0.19225919 ;
	setAttr ".pt[3607]" -type "float3" -0.18066095 0 0.1777311 ;
	setAttr ".pt[3608]" -type "float3" 0.17480189 0 -0.17773096 ;
	setAttr ".pt[3609]" -type "float3" 0.21323255 0 0.08801043 ;
	setAttr ".pt[3610]" -type "float3" 0.066364445 0 0.27086911 ;
	setAttr ".pt[3611]" -type "float3" 0.16285126 0 0.18292928 ;
	setAttr ".pt[3612]" -type "float3" 0.21885608 0 0.21422252 ;
	setAttr ".pt[3613]" -type "float3" 0.37083259 0 0.060251661 ;
	setAttr ".pt[3614]" -type "float3" 0.26706931 0 0.043816965 ;
	setAttr ".pt[3615]" -type "float3" 0.050671544 0 0.38041282 ;
	setAttr ".pt[3616]" -type "float3" 0.034236882 0 0.27664942 ;
	setAttr ".pt[3617]" -type "float3" 0.19949596 0 0.12930049 ;
	setAttr ".pt[3618]" -type "float3" 0.09952759 0 0.25376588 ;
	setAttr ".pt[3619]" -type "float3" -0.14376612 0 0.20334648 ;
	setAttr ".pt[3620]" -type "float3" -0.13255583 0 0.18603306 ;
	setAttr ".pt[3621]" -type "float3" 0.25152469 0 -0.14773972 ;
	setAttr ".pt[3622]" -type "float3" 0.18863823 0 -0.1351608 ;
	setAttr ".pt[3623]" -type "float3" -0.0080040582 0 0.32439756 ;
	setAttr ".pt[3624]" -type "float3" -0.009580058 0 0.2725884 ;
	setAttr ".pt[3625]" -type "float3" 0.30212778 0 1.9337129e-08 ;
	setAttr ".pt[3626]" -type "float3" 0.25425899 0 1.0061981e-08 ;
	setAttr ".pt[3627]" -type "float3" -0.099124499 0 0.22395456 ;
	setAttr ".pt[3628]" -type "float3" -0.12418467 0 0.2574172 ;
	setAttr ".pt[3629]" -type "float3" 0.1972037 0 -0.11411061 ;
	setAttr ".pt[3630]" -type "float3" 0.25919968 0 -0.13116056 ;
	setAttr ".pt[3631]" -type "float3" -0.058740139 0 0.31038508 ;
	setAttr ".pt[3632]" -type "float3" -0.049190909 0 0.24073061 ;
	setAttr ".pt[3633]" -type "float3" 0.32314616 0 -0.049160007 ;
	setAttr ".pt[3634]" -type "float3" 0.22551619 0 -0.038127955 ;
	setAttr ".pt[3635]" -type "float3" -0.085901029 0 0.21823558 ;
	setAttr ".pt[3636]" -type "float3" -0.10035099 0 0.20764861 ;
	setAttr ".pt[3637]" -type "float3" 0.29104352 0 -0.070908912 ;
	setAttr ".pt[3638]" -type "float3" 0.34361541 0 -0.067468934 ;
	setAttr ".pt[3639]" -type "float3" 0.180741 0 0.1674061 ;
	setAttr ".pt[3640]" -type "float3" 0.13013342 0 0.23041438 ;
	setAttr ".pt[3641]" -type "float3" 0.35672414 0 0.11901949 ;
	setAttr ".pt[3642]" -type "float3" 0.25680882 0 0.086555034 ;
	setAttr ".pt[3643]" -type "float3" 0.10943959 0 0.36630404 ;
	setAttr ".pt[3644]" -type "float3" 0.076974958 0 0.2663888 ;
	setAttr ".pt[3645]" -type "float3" 0.15742865 0 0.20138974 ;
	setAttr ".pt[3646]" -type "float3" -0.15066934 0 0.20334648 ;
	setAttr ".pt[3647]" -type "float3" -0.16274558 0 0.21996789 ;
	setAttr ".pt[3648]" -type "float3" 0.20041715 0 -0.14773972 ;
	setAttr ".pt[3649]" -type "float3" 0.21703844 0 -0.15981595 ;
	setAttr ".pt[3650]" -type "float3" -0.16274558 0 0.21996813 ;
	setAttr ".pt[3651]" -type "float3" 0.21703844 0 -0.15981595 ;
	setAttr ".pt[3652]" -type "float3" 0.29813343 0 0.050747022 ;
	setAttr ".pt[3653]" -type "float3" 0.25090235 0 0.042642239 ;
	setAttr ".pt[3654]" -type "float3" 0.016657235 0 0.32040375 ;
	setAttr ".pt[3655]" -type "float3" 0.018008474 0 0.26923218 ;
	setAttr ".pt[3656]" -type "float3" 0.33359563 0 0.1748568 ;
	setAttr ".pt[3657]" -type "float3" 0.23998886 0 0.12716168 ;
	setAttr ".pt[3658]" -type "float3" 0.16527672 0 0.34317553 ;
	setAttr ".pt[3659]" -type "float3" 0.11758147 0 0.24956895 ;
	setAttr ".pt[3660]" -type "float3" -0.0095800404 0 0.31425381 ;
	setAttr ".pt[3661]" -type "float3" -0.013031246 0 0.24373123 ;
	setAttr ".pt[3662]" -type "float3" 0.32701448 0 4.7953815e-08 ;
	setAttr ".pt[3663]" -type "float3" 0.22836153 0 3.4502815e-08 ;
	setAttr ".pt[3664]" -type "float3" -0.11013704 0 0.22395456 ;
	setAttr ".pt[3665]" -type "float3" -0.10178994 0 0.20488626 ;
	setAttr ".pt[3666]" -type "float3" 0.27213264 0 -0.11411049 ;
	setAttr ".pt[3667]" -type "float3" 0.22968084 0 -0.10439465 ;
	setAttr ".pt[3668]" -type "float3" -0.04081703 0 0.22664151 ;
	setAttr ".pt[3669]" -type "float3" -0.046969593 0 0.21564649 ;
	setAttr ".pt[3670]" -type "float3" 0.30186722 0 -0.035896417 ;
	setAttr ".pt[3671]" -type "float3" 0.35587981 0 -0.034154881 ;
	setAttr ".pt[3672]" -type "float3" -0.066329367 0 0.23904815 ;
	setAttr ".pt[3673]" -type "float3" -0.084436454 0 0.27476597 ;
	setAttr ".pt[3674]" -type "float3" 0.21078779 0 -0.077671438 ;
	setAttr ".pt[3675]" -type "float3" 0.27566373 0 -0.089276716 ;
	setAttr ".pt[3676]" -type "float3" 0.30201676 0 0.22638801 ;
	setAttr ".pt[3677]" -type "float3" 0.21702369 0 0.1646373 ;
	setAttr ".pt[3678]" -type "float3" 0.21680829 0 0.31159663 ;
	setAttr ".pt[3679]" -type "float3" 0.15505724 0 0.22660382 ;
	setAttr ".pt[3680]" -type "float3" 0.2862497 0 0.10024446 ;
	setAttr ".pt[3681]" -type "float3" 0.24091709 0 0.084234573 ;
	setAttr ".pt[3682]" -type "float3" 0.066154532 0 0.30852047 ;
	setAttr ".pt[3683]" -type "float3" 0.059600733 0 0.25924698 ;
	setAttr ".pt[3684]" -type "float3" 0.26276553 0 0.27234566 ;
	setAttr ".pt[3685]" -type "float3" 0.18847866 0 0.19805887 ;
	setAttr ".pt[3686]" -type "float3" -0.1170402 0 0.22395456 ;
	setAttr ".pt[3687]" -type "float3" -0.12636746 0 0.24226046 ;
	setAttr ".pt[3688]" -type "float3" 0.22102511 0 -0.11411049 ;
	setAttr ".pt[3689]" -type "float3" 0.23933126 0 -0.12343775 ;
	setAttr ".pt[3690]" -type "float3" -0.12636746 0 0.24226046 ;
	setAttr ".pt[3691]" -type "float3" 0.23933126 0 -0.12343775 ;
	setAttr ".pt[3692]" -type "float3" 0.32314524 0 0.04916006 ;
	setAttr ".pt[3693]" -type "float3" 0.22551626 0 0.038128033 ;
	setAttr ".pt[3694]" -type "float3" 0.048525356 0 0.31038508 ;
	setAttr ".pt[3695]" -type "float3" 0.031796455 0 0.24073061 ;
	setAttr ".pt[3696]" -type "float3" 0.26677051 0 0.14727318 ;
	setAttr ".pt[3697]" -type "float3" 0.21824391 0 0.12375285 ;
	setAttr ".pt[3698]" -type "float3" 0.11318345 0 0.28904018 ;
	setAttr ".pt[3699]" -type "float3" 0.099119037 0 0.24287842 ;
	setAttr ".pt[3700]" -type "float3" 0.012088603 0 0.22946633 ;
	setAttr ".pt[3701]" -type "float3" 0.036184236 0 0.21833451 ;
	setAttr ".pt[3702]" -type "float3" 0.30550504 0 3.1781997e-08 ;
	setAttr ".pt[3703]" -type "float3" 0.36000204 0 -8.9271346e-10 ;
	setAttr ".pt[3704]" -type "float3" -0.029982764 0 0.24825558 ;
	setAttr ".pt[3705]" -type "float3" -0.027251203 0 0.28534886 ;
	setAttr ".pt[3706]" -type "float3" 0.21907456 0 -0.039319672 ;
	setAttr ".pt[3707]" -type "float3" 0.28570747 0 -0.045194846 ;
	setAttr ".pt[3708]" -type "float3" -0.073697872 0 0.23904815 ;
	setAttr ".pt[3709]" -type "float3" -0.065590136 0 0.21869498 ;
	setAttr ".pt[3710]" -type "float3" 0.2872262 0 -0.077671252 ;
	setAttr ".pt[3711]" -type "float3" 0.24348882 0 -0.071058139 ;
	setAttr ".pt[3712]" -type "float3" 0.24017353 0 0.19067608 ;
	setAttr ".pt[3713]" -type "float3" 0.2006231 0 0.16022328 ;
	setAttr ".pt[3714]" -type "float3" 0.15658611 0 0.26244274 ;
	setAttr ".pt[3715]" -type "float3" 0.1403178 0 0.22052866 ;
	setAttr ".pt[3716]" -type "float3" 0.31163383 0 0.09710978 ;
	setAttr ".pt[3717]" -type "float3" 0.21704853 0 0.075317375 ;
	setAttr ".pt[3718]" -type "float3" 0.096474931 0 0.29887292 ;
	setAttr ".pt[3719]" -type "float3" 0.056033779 0 0.231803 ;
	setAttr ".pt[3720]" -type "float3" 0.20711407 0 0.22938299 ;
	setAttr ".pt[3721]" -type "float3" 0.17284371 0 0.19274923 ;
	setAttr ".pt[3722]" -type "float3" -0.080600984 0 0.23904815 ;
	setAttr ".pt[3723]" -type "float3" -0.086949781 0 0.25858805 ;
	setAttr ".pt[3724]" -type "float3" 0.23611867 0 -0.077671438 ;
	setAttr ".pt[3725]" -type "float3" 0.25565892 0 -0.084020227 ;
	setAttr ".pt[3726]" -type "float3" 0.30186725 0 0.035896488 ;
	setAttr ".pt[3727]" -type "float3" 0.35587958 0 0.034155022 ;
	setAttr ".pt[3728]" -type "float3" 0.058310725 0 0.22664151 ;
	setAttr ".pt[3729]" -type "float3" 0.088558845 0 0.21564649 ;
	setAttr ".pt[3730]" -type "float3" -0.086949781 0 0.25858805 ;
	setAttr ".pt[3731]" -type "float3" 0.25565892 0 -0.084020227 ;
	setAttr ".pt[3732]" -type "float3" 0.29276291 0 0.14266802 ;
	setAttr ".pt[3733]" -type "float3" 0.20316848 0 0.11065188 ;
	setAttr ".pt[3734]" -type "float3" 0.14203359 0 0.28000224 ;
	setAttr ".pt[3735]" -type "float3" 0.089544341 0 0.21716654 ;
	setAttr ".pt[3736]" -type "float3" 0.0054050572 0 0.25135055 ;
	setAttr ".pt[3737]" -type "float3" 0.015639145 0 0.28890598 ;
	setAttr ".pt[3738]" -type "float3" 0.22185926 0 3.5955935e-08 ;
	setAttr ".pt[3739]" -type "float3" 0.28908309 0 5.0757059e-08 ;
	setAttr ".pt[3740]" -type "float3" -0.033914804 0 0.24825558 ;
	setAttr ".pt[3741]" -type "float3" -0.030503822 0 0.22711809 ;
	setAttr ".pt[3742]" -type "float3" 0.29643336 0 -0.039319731 ;
	setAttr ".pt[3743]" -type "float3" 0.25191227 0 -0.035971899 ;
	setAttr ".pt[3744]" -type "float3" 0.26699716 0 0.18471387 ;
	setAttr ".pt[3745]" -type "float3" 0.18421647 0 0.14326203 ;
	setAttr ".pt[3746]" -type "float3" 0.1974749 0 0.25423688 ;
	setAttr ".pt[3747]" -type "float3" 0.12047078 0 0.197183 ;
	setAttr ".pt[3748]" -type "float3" 0.2910431 0 0.070909053 ;
	setAttr ".pt[3749]" -type "float3" 0.34361589 0 0.067469187 ;
	setAttr ".pt[3750]" -type "float3" 0.10339458 0 0.21823582 ;
	setAttr ".pt[3751]" -type "float3" 0.13065632 0 0.20764861 ;
	setAttr ".pt[3752]" -type "float3" 0.23497187 0 0.22221138 ;
	setAttr ".pt[3753]" -type "float3" 0.15908398 0 0.17234436 ;
	setAttr ".pt[3754]" -type "float3" -0.042249292 0 0.24825558 ;
	setAttr ".pt[3755]" -type "float3" -0.04546326 0 0.26854831 ;
	setAttr ".pt[3756]" -type "float3" 0.24532607 0 -0.039319731 ;
	setAttr ".pt[3757]" -type "float3" 0.26561835 0 -0.042533752 ;
	setAttr ".pt[3758]" -type "float3" 0.21907404 0 0.03931975 ;
	setAttr ".pt[3759]" -type "float3" 0.28570706 0 0.045194823 ;
	setAttr ".pt[3760]" -type "float3" 0.034692578 0 0.24825558 ;
	setAttr ".pt[3761]" -type "float3" 0.058529574 0 0.28534886 ;
	setAttr ".pt[3762]" -type "float3" -0.04546326 0 0.26854831 ;
	setAttr ".pt[3763]" -type "float3" 0.26561835 0 -0.042533875 ;
	setAttr ".pt[3764]" -type "float3" 0.26867324 0 0.10417548 ;
	setAttr ".pt[3765]" -type "float3" 0.323511 0 0.099121861 ;
	setAttr ".pt[3766]" -type "float3" 0.13954674 0 0.20445625 ;
	setAttr ".pt[3767]" -type "float3" 0.17919376 0 0.19453749 ;
	setAttr ".pt[3768]" -type "float3" 0.046030633 0 0.25135055 ;
	setAttr ".pt[3769]" -type "float3" 0.0054681525 0 0.22994889 ;
	setAttr ".pt[3770]" -type "float3" 0.29952797 0 3.5955935e-08 ;
	setAttr ".pt[3771]" -type "float3" 0.25474328 0 3.187413e-08 ;
	setAttr ".pt[3772]" -type "float3" 0.24496138 0 0.13487707 ;
	setAttr ".pt[3773]" -type "float3" 0.29606053 0 0.12833375 ;
	setAttr ".pt[3774]" -type "float3" 0.1795938 0 0.18564206 ;
	setAttr ".pt[3775]" -type "float3" 0.22398795 0 0.17663649 ;
	setAttr ".pt[3776]" -type "float3" 0.21078788 0 0.077671468 ;
	setAttr ".pt[3777]" -type "float3" 0.27566406 0 0.089276887 ;
	setAttr ".pt[3778]" -type "float3" 0.069209106 0 0.23904815 ;
	setAttr ".pt[3779]" -type "float3" 0.090129651 0 0.27476597 ;
	setAttr ".pt[3780]" -type "float3" 0.21485004 0 0.16225743 ;
	setAttr ".pt[3781]" -type "float3" 0.27292538 0 0.15438597 ;
	setAttr ".pt[3782]" -type "float3" -0.00292947 0 0.25135055 ;
	setAttr ".pt[3783]" -type "float3" -0.0029294719 0 0.27189553 ;
	setAttr ".pt[3784]" -type "float3" 0.24842007 0 3.5955935e-08 ;
	setAttr ".pt[3785]" -type "float3" 0.26896551 0 1.0097733e-07 ;
	setAttr ".pt[3786]" -type "float3" 0.29643345 0 0.03931975 ;
	setAttr ".pt[3787]" -type "float3" 0.25191179 0 0.035971995 ;
	setAttr ".pt[3788]" -type "float3" 0.085350454 0 0.24825558 ;
	setAttr ".pt[3789]" -type "float3" 0.038577098 0 0.22711809 ;
	setAttr ".pt[3790]" -type "float3" -0.0029294719 0 0.27189553 ;
	setAttr ".pt[3791]" -type "float3" 0.26896551 0 3.9874557e-08 ;
	setAttr ".pt[3792]" -type "float3" 0.19720367 0 0.11411069 ;
	setAttr ".pt[3793]" -type "float3" 0.2591995 0 0.13116071 ;
	setAttr ".pt[3794]" -type "float3" 0.1020041 0 0.22395423 ;
	setAttr ".pt[3795]" -type "float3" 0.11745048 0 0.25741738 ;
	setAttr ".pt[3796]" -type "float3" 0.17865653 0 0.14773989 ;
	setAttr ".pt[3797]" -type "float3" 0.23671998 0 0.16981457 ;
	setAttr ".pt[3798]" -type "float3" 0.13837087 0 0.20334642 ;
	setAttr ".pt[3799]" -type "float3" 0.15413372 0 0.23372954 ;
	setAttr ".pt[3800]" -type "float3" 0.28722629 0 0.077671468 ;
	setAttr ".pt[3801]" -type "float3" 0.24348891 0 0.071058162 ;
	setAttr ".pt[3802]" -type "float3" 0.12370202 0 0.23904815 ;
	setAttr ".pt[3803]" -type "float3" 0.086547159 0 0.21869498 ;
	setAttr ".pt[3804]" -type "float3" 0.16536313 0 0.17773138 ;
	setAttr ".pt[3805]" -type "float3" 0.18684864 0 0.20428739 ;
	setAttr ".pt[3806]" -type "float3" 0.24532579 0 0.039319627 ;
	setAttr ".pt[3807]" -type "float3" 0.26561853 0 0.042533781 ;
	setAttr ".pt[3808]" -type "float3" 0.036390342 0 0.24825558 ;
	setAttr ".pt[3809]" -type "float3" 0.03960437 0 0.26854831 ;
	setAttr ".pt[3810]" -type "float3" 0.26561853 0 0.042533781 ;
	setAttr ".pt[3811]" -type "float3" 0.03960437 0 0.26854831 ;
	setAttr ".pt[3812]" -type "float3" 0.27213222 0 0.11411081 ;
	setAttr ".pt[3813]" -type "float3" 0.22968058 0 0.10439471 ;
	setAttr ".pt[3814]" -type "float3" 0.16014107 0 0.22395423 ;
	setAttr ".pt[3815]" -type "float3" 0.1198836 0 0.20488629 ;
	setAttr ".pt[3816]" -type "float3" 0.23611851 0 0.077671468 ;
	setAttr ".pt[3817]" -type "float3" 0.2556583 0 0.084020272 ;
	setAttr ".pt[3818]" -type "float3" 0.074741967 0 0.23904815 ;
	setAttr ".pt[3819]" -type "float3" 0.081090763 0 0.25858805 ;
	setAttr ".pt[3820]" -type "float3" 0.25152478 0 0.14773989 ;
	setAttr ".pt[3821]" -type "float3" 0.18863812 0 0.13516073 ;
	setAttr ".pt[3822]" -type "float3" 0.18804434 0 0.20334642 ;
	setAttr ".pt[3823]" -type "float3" 0.15064979 0 0.18603323 ;
	setAttr ".pt[3824]" -type "float3" 0.2556583 0 0.084020272 ;
	setAttr ".pt[3825]" -type "float3" 0.081090763 0 0.25858805 ;
	setAttr ".pt[3826]" -type "float3" 0.2180358 0 0.17773138 ;
	setAttr ".pt[3827]" -type "float3" 0.16520379 0 0.16259865 ;
	setAttr ".pt[3828]" -type "float3" 0.22102529 0 0.11411069 ;
	setAttr ".pt[3829]" -type "float3" 0.23933095 0 0.12343795 ;
	setAttr ".pt[3830]" -type "float3" 0.11118101 0 0.22395423 ;
	setAttr ".pt[3831]" -type "float3" 0.12050851 0 0.24226025 ;
	setAttr ".pt[3832]" -type "float3" 0.23933095 0 0.12343795 ;
	setAttr ".pt[3833]" -type "float3" 0.12050851 0 0.24226025 ;
	setAttr ".pt[3834]" -type "float3" 0.20041735 0 0.14773989 ;
	setAttr ".pt[3835]" -type "float3" 0.21703865 0 0.15981613 ;
	setAttr ".pt[3836]" -type "float3" 0.14481027 0 0.20334642 ;
	setAttr ".pt[3837]" -type "float3" 0.15688652 0 0.21996795 ;
	setAttr ".pt[3838]" -type "float3" 0.17480174 0 0.17773114 ;
	setAttr ".pt[3839]" -type "float3" 0.18932959 0 0.19225924 ;
	setAttr ".pt[3840]" -type "float3" 0.21703865 0 0.15981613 ;
	setAttr ".pt[3841]" -type "float3" 0.15688652 0 0.21996795 ;
	setAttr ".pt[3842]" -type "float3" 0.18932959 0 0.19225924 ;
	setAttr ".pt[3843]" -type "float3" -0.16012821 0 -0.28000209 ;
	setAttr ".pt[3844]" -type "float3" -0.10930291 0 -0.21716657 ;
	setAttr ".pt[3845]" -type "float3" -0.27575943 0 -0.14266816 ;
	setAttr ".pt[3846]" -type "float3" -0.20874254 0 -0.11065178 ;
	setAttr ".pt[3847]" -type "float3" 0.18291758 0 -0.20754875 ;
	setAttr ".pt[3848]" -type "float3" 0.19053915 0 -0.20622705 ;
	setAttr ".pt[3849]" -type "float3" -0.30865854 0 0.15079296 ;
	setAttr ".pt[3850]" -type "float3" -0.27383336 0 0.14983267 ;
	setAttr ".pt[3851]" -type "float3" -0.0095799174 0 -0.38515535 ;
	setAttr ".pt[3852]" -type "float3" -0.0095799658 0 -0.28009766 ;
	setAttr ".pt[3853]" -type "float3" -0.33226991 0 -1.6546604e-09 ;
	setAttr ".pt[3854]" -type "float3" -0.28967732 0 -3.5023909e-08 ;
	setAttr ".pt[3855]" -type "float3" -0.0095800525 0 0.22026467 ;
	setAttr ".pt[3856]" -type "float3" -0.00471874 0 0.20152627 ;
	setAttr ".pt[3857]" -type "float3" 0.2764568 0 3.002679e-08 ;
	setAttr ".pt[3858]" -type "float3" 0.25424942 0 5.8330674e-08 ;
	setAttr ".pt[3859]" -type "float3" 0.11949436 0 -0.22395432 ;
	setAttr ".pt[3860]" -type "float3" 0.11239859 0 -0.28192332 ;
	setAttr ".pt[3861]" -type "float3" -0.2185708 0 0.11411073 ;
	setAttr ".pt[3862]" -type "float3" -0.19297577 0 0.14364727 ;
	setAttr ".pt[3863]" -type "float3" 0.052610725 0 -0.29922649 ;
	setAttr ".pt[3864]" -type "float3" 0.031461075 0 -0.28130171 ;
	setAttr ".pt[3865]" -type "float3" -0.2927947 0 0.047392748 ;
	setAttr ".pt[3866]" -type "float3" -0.22581463 0 0.04455379 ;
	setAttr ".pt[3867]" -type "float3" 0.06903553 0 -0.24603933 ;
	setAttr ".pt[3868]" -type "float3" -0.20895332 0 0.079943016 ;
	setAttr ".pt[3869]" -type "float3" -0.17968288 0 -0.2542364 ;
	setAttr ".pt[3870]" -type "float3" -0.14022923 0 -0.19718312 ;
	setAttr ".pt[3871]" -type "float3" -0.24999425 0 -0.18471389 ;
	setAttr ".pt[3872]" -type "float3" -0.18979071 0 -0.14326206 ;
	setAttr ".pt[3873]" -type "float3" 0.11967658 0 0.17700647 ;
	setAttr ".pt[3874]" -type "float3" 0.14591552 0 0.15470922 ;
	setAttr ".pt[3875]" -type "float3" 0.17443404 0 0.1982466 ;
	setAttr ".pt[3876]" -type "float3" 0.14338206 0 0.22681835 ;
	setAttr ".pt[3877]" -type "float3" 0.16832614 0 0.12860261 ;
	setAttr ".pt[3878]" -type "float3" 0.20095463 0 0.16479312 ;
	setAttr ".pt[3879]" -type "float3" 0.029739732 0 0.287718 ;
	setAttr ".pt[3880]" -type "float3" 0.031829271 0 0.26069811 ;
	setAttr ".pt[3881]" -type "float3" 0.23867545 0 0.04557002 ;
	setAttr ".pt[3882]" -type "float3" 0.25186834 0 0.041290611 ;
	setAttr ".pt[3883]" -type "float3" -0.10824811 0 -0.30852047 ;
	setAttr ".pt[3884]" -type "float3" -0.093814388 0 -0.25924721 ;
	setAttr ".pt[3885]" -type "float3" -0.30784518 0 -0.1002443 ;
	setAttr ".pt[3886]" -type "float3" -0.2767072 0 -0.084234558 ;
	setAttr ".pt[3887]" -type "float3" -0.21718012 0 -0.22221123 ;
	setAttr ".pt[3888]" -type "float3" -0.16623437 0 -0.17234437 ;
	setAttr ".pt[3889]" -type "float3" 0.090254523 0 0.19494495 ;
	setAttr ".pt[3890]" -type "float3" 0.10856418 0 0.24980503 ;
	setAttr ".pt[3891]" -type "float3" 0.18635598 0 0.09932936 ;
	setAttr ".pt[3892]" -type "float3" 0.22229092 0 0.12728202 ;
	setAttr ".pt[3893]" -type "float3" 0.23787779 0 -0.10842748 ;
	setAttr ".pt[3894]" -type "float3" 0.23863259 0 -0.13661397 ;
	setAttr ".pt[3895]" -type "float3" -0.085189432 0 0.21280089 ;
	setAttr ".pt[3896]" -type "float3" -0.11597197 0 0.26812002 ;
	setAttr ".pt[3897]" -type "float3" 0.28918904 0 -0.073427856 ;
	setAttr ".pt[3898]" -type "float3" 0.32398471 0 -0.097842589 ;
	setAttr ".pt[3899]" -type "float3" -0.089032724 0 0.22598799 ;
	setAttr ".pt[3900]" -type "float3" -0.10033853 0 0.3011286 ;
	setAttr ".pt[3901]" -type "float3" 0.18043317 0 -0.14773972 ;
	setAttr ".pt[3902]" -type "float3" 0.19108286 0 -0.13952549 ;
	setAttr ".pt[3903]" -type "float3" -0.16504587 0 0.20334648 ;
	setAttr ".pt[3904]" -type "float3" -0.14048265 0 0.19204043 ;
	setAttr ".pt[3905]" -type "float3" -0.062623806 0 -0.38041303 ;
	setAttr ".pt[3906]" -type "float3" -0.05339681 0 -0.27664894 ;
	setAttr ".pt[3907]" -type "float3" -0.32752779 0 -0.060251623 ;
	setAttr ".pt[3908]" -type "float3" -0.28622907 0 -0.043816939 ;
	setAttr ".pt[3909]" -type "float3" 0.27293518 0 -0.034456905 ;
	setAttr ".pt[3910]" -type "float3" 0.25106189 0 -0.031525567 ;
	setAttr ".pt[3911]" -type "float3" -0.054326065 0 0.21755311 ;
	setAttr ".pt[3912]" -type "float3" -0.045230314 0 0.19904517 ;
	setAttr ".pt[3913]" -type "float3" 0.22571622 0 -0.23529582 ;
	setAttr ".pt[3914]" -type "float3" 0.24905001 0 -0.18044855 ;
	setAttr ".pt[3915]" -type "float3" -0.24487635 0 0.23529632 ;
	setAttr ".pt[3916]" -type "float3" -0.24638376 0 0.18044856 ;
	setAttr ".pt[3917]" -type "float3" 0.058374077 0 0.20808341 ;
	setAttr ".pt[3918]" -type "float3" 0.070837013 0 0.2666412 ;
	setAttr ".pt[3919]" -type "float3" 0.19956134 0 0.067610256 ;
	setAttr ".pt[3920]" -type "float3" 0.2379178 0 0.086637072 ;
	setAttr ".pt[3921]" -type "float3" -0.13871801 0 -0.28903967 ;
	setAttr ".pt[3922]" -type "float3" -0.14121258 0 -0.2428783 ;
	setAttr ".pt[3923]" -type "float3" -0.28836536 0 -0.1472732 ;
	setAttr ".pt[3924]" -type "float3" -0.26033831 0 -0.12375279 ;
	setAttr ".pt[3925]" -type "float3" 0.13815983 0 -0.2033466 ;
	setAttr ".pt[3926]" -type "float3" 0.20685512 0 -0.25207862 ;
	setAttr ".pt[3927]" -type "float3" -0.23667867 0 0.14773987 ;
	setAttr ".pt[3928]" -type "float3" -0.30747682 0 0.18314564 ;
	setAttr ".pt[3929]" -type "float3" 0.0053838524 0 -0.30295637 ;
	setAttr ".pt[3930]" -type "float3" -0.0043237517 0 -0.28480875 ;
	setAttr ".pt[3931]" -type "float3" -0.29651132 0 -3.8594101e-09 ;
	setAttr ".pt[3932]" -type "float3" -0.22863105 0 -4.3461656e-09 ;
	setAttr ".pt[3933]" -type "float3" -0.0095800525 0 0.29130414 ;
	setAttr ".pt[3934]" -type "float3" -0.0095800525 0 0.26394817 ;
	setAttr ".pt[3935]" -type "float3" 0.2417701 0 7.4127954e-08 ;
	setAttr ".pt[3936]" -type "float3" 0.25512698 0 3.8358692e-08 ;
	setAttr ".pt[3937]" -type "float3" 0.15825184 0 -0.22858295 ;
	setAttr ".pt[3938]" -type "float3" 0.14605889 0 -0.22712652 ;
	setAttr ".pt[3939]" -type "float3" -0.33896828 0 0.11646884 ;
	setAttr ".pt[3940]" -type "float3" -0.30109087 0 0.11572695 ;
	setAttr ".pt[3941]" -type "float3" 0.037606277 0 -0.25551638 ;
	setAttr ".pt[3942]" -type "float3" -0.21649888 0 0.040469754 ;
	setAttr ".pt[3943]" -type "float3" 0.083055489 0 -0.23904791 ;
	setAttr ".pt[3944]" -type "float3" 0.078225255 0 -0.30092409 ;
	setAttr ".pt[3945]" -type "float3" -0.23366426 0 0.077671446 ;
	setAttr ".pt[3946]" -type "float3" -0.22624195 0 0.097776026 ;
	setAttr ".pt[3947]" -type "float3" -0.1821208 0 -0.26244298 ;
	setAttr ".pt[3948]" -type "float3" -0.17768337 0 -0.22052887 ;
	setAttr ".pt[3949]" -type "float3" -0.25388783 0 -0.19067611 ;
	setAttr ".pt[3950]" -type "float3" -0.23798826 0 -0.16022329 ;
	setAttr ".pt[3951]" -type "float3" 0.15163341 0 0.20334618 ;
	setAttr ".pt[3952]" -type "float3" 0.18162511 0 0.17773138 ;
	setAttr ".pt[3953]" -type "float3" 0.20222077 0 0.16969135 ;
	setAttr ".pt[3954]" -type "float3" 0.16811253 0 0.19414802 ;
	setAttr ".pt[3955]" -type "float3" 0.20724025 0 0.14773989 ;
	setAttr ".pt[3956]" -type "float3" 0.23135154 0 0.14105682 ;
	setAttr ".pt[3957]" -type "float3" 0.024820575 0 0.21609826 ;
	setAttr ".pt[3958]" -type "float3" 0.031129658 0 0.27691165 ;
	setAttr ".pt[3959]" -type "float3" 0.20761672 0 0.034226559 ;
	setAttr ".pt[3960]" -type "float3" 0.24745066 0 0.043858495 ;
	setAttr ".pt[3961]" -type "float3" -0.094964214 0 -0.36630425 ;
	setAttr ".pt[3962]" -type "float3" -0.096134856 0 -0.26638883 ;
	setAttr ".pt[3963]" -type "float3" -0.31341887 0 -0.11901954 ;
	setAttr ".pt[3964]" -type "float3" -0.27596858 0 -0.086554907 ;
	setAttr ".pt[3965]" -type "float3" -0.22082831 0 -0.22938362 ;
	setAttr ".pt[3966]" -type "float3" -0.21020907 0 -0.19274935 ;
	setAttr ".pt[3967]" -type "float3" 0.11800408 0 0.22395423 ;
	setAttr ".pt[3968]" -type "float3" 0.12986757 0 0.2138236 ;
	setAttr ".pt[3969]" -type "float3" 0.22784807 0 0.11411069 ;
	setAttr ".pt[3970]" -type "float3" 0.25478852 0 0.10894852 ;
	setAttr ".pt[3971]" -type "float3" 0.27116397 0 -0.10787623 ;
	setAttr ".pt[3972]" -type "float3" 0.3037625 0 -0.1437448 ;
	setAttr ".pt[3973]" -type "float3" -0.13254781 0 0.21171924 ;
	setAttr ".pt[3974]" -type "float3" -0.149158 0 0.28211543 ;
	setAttr ".pt[3975]" -type "float3" 0.26245677 0 -0.06806539 ;
	setAttr ".pt[3976]" -type "float3" 0.24157456 0 -0.062274918 ;
	setAttr ".pt[3977]" -type "float3" -0.097970344 0 0.20948341 ;
	setAttr ".pt[3978]" -type "float3" -0.08474429 0 0.19166276 ;
	setAttr ".pt[3979]" -type "float3" 0.22182074 0 -0.1403818 ;
	setAttr ".pt[3980]" -type "float3" 0.2170158 0 -0.17687519 ;
	setAttr ".pt[3981]" -type "float3" -0.11139204 0 0.19321918 ;
	setAttr ".pt[3982]" -type "float3" -0.15124667 0 0.24344791 ;
	setAttr ".pt[3983]" -type "float3" -0.041843019 0 -0.29922649 ;
	setAttr ".pt[3984]" -type "float3" -0.035663247 0 -0.28130206 ;
	setAttr ".pt[3985]" -type "float3" -0.2927947 0 -0.047392674 ;
	setAttr ".pt[3986]" -type "float3" -0.22581463 0 -0.044553839 ;
	setAttr ".pt[3987]" -type "float3" 0.23867598 0 -0.045569975 ;
	setAttr ".pt[3988]" -type "float3" 0.25186843 0 -0.041290432 ;
	setAttr ".pt[3989]" -type "float3" -0.04889987 0 0.287718 ;
	setAttr ".pt[3990]" -type "float3" -0.050989423 0 0.26069811 ;
	setAttr ".pt[3991]" -type "float3" 0.16853116 0 -0.17773096 ;
	setAttr ".pt[3992]" -type "float3" 0.16689162 0 -0.16784866 ;
	setAttr ".pt[3993]" -type "float3" -0.19503711 0 0.1777311 ;
	setAttr ".pt[3994]" -type "float3" -0.16880636 0 0.16784929 ;
	setAttr ".pt[3995]" -type "float3" 0.081565037 0 0.23904815 ;
	setAttr ".pt[3996]" -type "float3" 0.088426903 0 0.22823478 ;
	setAttr ".pt[3997]" -type "float3" 0.24294177 0 0.077671468 ;
	setAttr ".pt[3998]" -type "float3" 0.27195391 0 0.074157968 ;
	setAttr ".pt[3999]" -type "float3" -0.13518517 0 -0.34317502 ;
	setAttr ".pt[4000]" -type "float3" -0.13674164 0 -0.24956897 ;
	setAttr ".pt[4001]" -type "float3" -0.29029047 0 -0.17485668 ;
	setAttr ".pt[4002]" -type "float3" -0.25914875 0 -0.12716162 ;
	setAttr ".pt[4003]" -type "float3" 0.18601067 0 -0.26920763 ;
	setAttr ".pt[4004]" -type "float3" 0.19530752 0 -0.20645535 ;
	setAttr ".pt[4005]" -type "float3" -0.27878746 0 0.19559091 ;
	setAttr ".pt[4006]" -type "float3" -0.28601077 0 0.14999844 ;
	setAttr ".pt[4007]" -type "float3" 0.0053838091 0 -0.25870106 ;
	setAttr ".pt[4008]" -type "float3" -0.21903509 0 -5.0463869e-09 ;
	setAttr ".pt[4009]" -type "float3" -0.0095800636 0 0.21879195 ;
	setAttr ".pt[4010]" -type "float3" -0.0095800525 0 0.28036323 ;
	setAttr ".pt[4011]" -type "float3" 0.21032421 0 2.9745886e-08 ;
	setAttr ".pt[4012]" -type "float3" 0.25065506 0 1.6369532e-07 ;
	setAttr ".pt[4013]" -type "float3" 0.10453057 0 -0.22395432 ;
	setAttr ".pt[4014]" -type "float3" 0.15758909 0 -0.27762532 ;
	setAttr ".pt[4015]" -type "float3" -0.25728637 0 0.11411073 ;
	setAttr ".pt[4016]" -type "float3" -0.33766681 0 0.14145729 ;
	setAttr ".pt[4017]" -type "float3" 0.044703662 0 -0.24825594 ;
	setAttr ".pt[4018]" -type "float3" 0.042258594 0 -0.31251496 ;
	setAttr ".pt[4019]" -type "float3" -0.24287179 0 0.039319843 ;
	setAttr ".pt[4020]" -type "float3" -0.23487644 0 0.049497444 ;
	setAttr ".pt[4021]" -type "float3" 0.10465783 0 -0.24398799 ;
	setAttr ".pt[4022]" -type "float3" 0.097861752 0 -0.2424342 ;
	setAttr ".pt[4023]" -type "float3" -0.36116767 0 0.079276554 ;
	setAttr ".pt[4024]" -type "float3" -0.32105574 0 0.078771628 ;
	setAttr ".pt[4025]" -type "float3" -0.18671669 0 -0.31159711 ;
	setAttr ".pt[4026]" -type "float3" -0.17421742 0 -0.22660331 ;
	setAttr ".pt[4027]" -type "float3" -0.25871181 0 -0.22638804 ;
	setAttr ".pt[4028]" -type "float3" -0.2361839 0 -0.16463734 ;
	setAttr ".pt[4029]" -type "float3" 0.1655793 0 0.23193309 ;
	setAttr ".pt[4030]" -type "float3" 0.1997872 0 0.20271647 ;
	setAttr ".pt[4031]" -type "float3" 0.24437121 0 0.2031952 ;
	setAttr ".pt[4032]" -type "float3" 0.20306879 0 0.23247926 ;
	setAttr ".pt[4033]" -type "float3" 0.22900316 0 0.16850917 ;
	setAttr ".pt[4034]" -type "float3" 0.27964798 0 0.16890618 ;
	setAttr ".pt[4035]" -type "float3" 0.043213442 0 0.24825558 ;
	setAttr ".pt[4036]" -type "float3" 0.044811267 0 0.2370258 ;
	setAttr ".pt[4037]" -type "float3" 0.25214881 0 0.039319843 ;
	setAttr ".pt[4038]" -type "float3" 0.28242472 0 0.037541281 ;
	setAttr ".pt[4039]" -type "float3" -0.087906867 0 -0.28812864 ;
	setAttr ".pt[4040]" -type "float3" -0.07056687 0 -0.27086911 ;
	setAttr ".pt[4041]" -type "float3" -0.28173548 0 -0.093618803 ;
	setAttr ".pt[4042]" -type "float3" -0.21743521 0 -0.088010661 ;
	setAttr ".pt[4043]" -type "float3" -0.21946043 0 -0.27234554 ;
	setAttr ".pt[4044]" -type "float3" -0.2076391 0 -0.19805863 ;
	setAttr ".pt[4045]" -type "float3" 0.12722282 0 0.25543785 ;
	setAttr ".pt[4046]" -type "float3" 0.15675634 0 0.2560398 ;
	setAttr ".pt[4047]" -type "float3" 0.2525084 0 0.13015226 ;
	setAttr ".pt[4048]" -type "float3" 0.30802846 0 0.13045885 ;
	setAttr ".pt[4049]" -type "float3" 0.24528077 0 -0.099998027 ;
	setAttr ".pt[4050]" -type "float3" 0.22602448 0 -0.09149085 ;
	setAttr ".pt[4051]" -type "float3" -0.13943803 0 0.19625728 ;
	setAttr ".pt[4052]" -type "float3" -0.12228759 0 0.17956121 ;
	setAttr ".pt[4053]" -type "float3" 0.22946811 0 -0.090018041 ;
	setAttr ".pt[4054]" -type "float3" 0.24217218 0 -0.081564382 ;
	setAttr ".pt[4055]" -type "float3" -0.08725144 0 0.27704683 ;
	setAttr ".pt[4056]" -type "float3" -0.091379158 0 0.25102916 ;
	setAttr ".pt[4057]" -type "float3" 0.24655402 0 -0.13966829 ;
	setAttr ".pt[4058]" -type "float3" 0.27615345 0 -0.1861079 ;
	setAttr ".pt[4059]" -type "float3" -0.17270751 0 0.19223681 ;
	setAttr ".pt[4060]" -type "float3" -0.19421247 0 0.25615582 ;
	setAttr ".pt[4061]" -type "float3" -0.026838634 0 -0.25551638 ;
	setAttr ".pt[4062]" -type "float3" -0.21649888 0 -0.040469646 ;
	setAttr ".pt[4063]" -type "float3" 0.20761734 0 -0.034226526 ;
	setAttr ".pt[4064]" -type "float3" 0.2474512 0 -0.043858308 ;
	setAttr ".pt[4065]" -type "float3" -0.043980673 0 0.21609826 ;
	setAttr ".pt[4066]" -type "float3" -0.050289735 0 0.27691165 ;
	setAttr ".pt[4067]" -type "float3" 0.20186266 0 -0.16887973 ;
	setAttr ".pt[4068]" -type "float3" 0.19014765 0 -0.21278122 ;
	setAttr ".pt[4069]" -type "float3" -0.13476017 0 0.16887975 ;
	setAttr ".pt[4070]" -type "float3" -0.18270528 0 0.21278138 ;
	setAttr ".pt[4071]" -type "float3" 0.085661024 0 0.27265361 ;
	setAttr ".pt[4072]" -type "float3" 0.10657382 0 0.27329627 ;
	setAttr ".pt[4073]" -type "float3" 0.26972407 0 0.088590287 ;
	setAttr ".pt[4074]" -type "float3" 0.32881466 0 0.088799261 ;
	setAttr ".pt[4075]" -type "float3" -0.13167378 0 -0.26993588 ;
	setAttr ".pt[4076]" -type "float3" -0.10373 0 -0.25376612 ;
	setAttr ".pt[4077]" -type "float3" -0.2636067 0 -0.13753934 ;
	setAttr ".pt[4078]" -type "float3" -0.20369852 0 -0.12930007 ;
	setAttr ".pt[4079]" -type "float3" 0.13853985 0 -0.2033466 ;
	setAttr ".pt[4080]" -type "float3" 0.13856806 0 -0.19204055 ;
	setAttr ".pt[4081]" -type "float3" -0.22065239 0 0.14773987 ;
	setAttr ".pt[4082]" -type "float3" -0.19299728 0 0.13952552 ;
	setAttr ".pt[4083]" -type "float3" 0.0053838277 0 -0.25135058 ;
	setAttr ".pt[4084]" -type "float3" 0.0053838156 0 -0.3164103 ;
	setAttr ".pt[4085]" -type "float3" -0.24596633 0 -5.2435696e-09 ;
	setAttr ".pt[4086]" -type "float3" -0.2377784 0 -3.4985201e-09 ;
	setAttr ".pt[4087]" -type "float3" 0.0038936576 0 0.25135055 ;
	setAttr ".pt[4088]" -type "float3" 9.4558483e-05 0 0.23998021 ;
	setAttr ".pt[4089]" -type "float3" 0.25524345 0 9.7058688e-08 ;
	setAttr ".pt[4090]" -type "float3" 0.28594375 0 3.3787227e-08 ;
	setAttr ".pt[4091]" -type "float3" 0.14148951 0 -0.29649037 ;
	setAttr ".pt[4092]" -type "float3" 0.14328301 0 -0.22737865 ;
	setAttr ".pt[4093]" -type "float3" -0.30607066 0 0.15106952 ;
	setAttr ".pt[4094]" -type "float3" -0.31789091 0 0.11585522 ;
	setAttr ".pt[4095]" -type "float3" 0.048250914 0 -0.25338617 ;
	setAttr ".pt[4096]" -type "float3" 0.047135051 0 -0.25177228 ;
	setAttr ".pt[4097]" -type "float3" -0.3747097 0 0.040132307 ;
	setAttr ".pt[4098]" -type "float3" -0.33323342 0 0.039876729 ;
	setAttr ".pt[4099]" -type "float3" 0.068091594 0 -0.23904815 ;
	setAttr ".pt[4100]" -type "float3" 0.10420679 0 -0.29633608 ;
	setAttr ".pt[4101]" -type "float3" -0.27238017 0 0.077671446 ;
	setAttr ".pt[4102]" -type "float3" -0.35977927 0 0.096285373 ;
	setAttr ".pt[4103]" -type "float3" -0.17206566 0 -0.24509688 ;
	setAttr ".pt[4104]" -type "float3" -0.13433577 0 -0.23041424 ;
	setAttr ".pt[4105]" -type "float3" -0.23885456 0 -0.17807344 ;
	setAttr ".pt[4106]" -type "float3" -0.18494321 0 -0.16740613 ;
	setAttr ".pt[4107]" -type "float3" 0.041917894 0 0.28315514 ;
	setAttr ".pt[4108]" -type "float3" 0.053757749 0 0.28382245 ;
	setAttr ".pt[4109]" -type "float3" 0.28022537 0 0.044847369 ;
	setAttr ".pt[4110]" -type "float3" 0.34149444 0 0.044953093 ;
	setAttr ".pt[4111]" -type "float3" -0.058267806 0 -0.24603933 ;
	setAttr ".pt[4112]" -type "float3" -0.20895332 0 -0.079943091 ;
	setAttr ".pt[4113]" -type "float3" -0.20808807 0 -0.21422204 ;
	setAttr ".pt[4114]" -type "float3" -0.16163108 0 -0.20138974 ;
	setAttr ".pt[4115]" -type "float3" 0.21437488 0 -0.13224947 ;
	setAttr ".pt[4116]" -type "float3" 0.22627565 0 -0.11982972 ;
	setAttr ".pt[4117]" -type "float3" -0.1236906 0 0.25955412 ;
	setAttr ".pt[4118]" -type "float3" -0.12975489 0 0.2351792 ;
	setAttr ".pt[4119]" -type "float3" 0.19956161 0 -0.067610309 ;
	setAttr ".pt[4120]" -type "float3" 0.23791821 0 -0.086636968 ;
	setAttr ".pt[4121]" -type "float3" -0.077534288 0 0.20808317 ;
	setAttr ".pt[4122]" -type "float3" -0.089997008 0 0.2666412 ;
	setAttr ".pt[4123]" -type "float3" 0.22182859 0 -0.12946814 ;
	setAttr ".pt[4124]" -type "float3" 0.20479141 0 -0.1184539 ;
	setAttr ".pt[4125]" -type "float3" -0.17770815 0 0.17819752 ;
	setAttr ".pt[4126]" -type "float3" -0.15693663 0 0.16303806 ;
	setAttr ".pt[4127]" -type "float3" -0.033936054 0 -0.2482557 ;
	setAttr ".pt[4128]" -type "float3" -0.031490937 0 -0.31251523 ;
	setAttr ".pt[4129]" -type "float3" -0.24287179 0 -0.039319769 ;
	setAttr ".pt[4130]" -type "float3" -0.23487644 0 -0.049497478 ;
	setAttr ".pt[4131]" -type "float3" 0.25214943 0 -0.039319791 ;
	setAttr ".pt[4132]" -type "float3" 0.28242534 0 -0.037541024 ;
	setAttr ".pt[4133]" -type "float3" -0.035426132 0 0.24825558 ;
	setAttr ".pt[4134]" -type "float3" -0.044622127 0 0.2370258 ;
	setAttr ".pt[4135]" -type "float3" 0.21596523 0 -0.16802123 ;
	setAttr ".pt[4136]" -type "float3" 0.24183573 0 -0.22388828 ;
	setAttr ".pt[4137]" -type "float3" -0.20852259 0 0.16802101 ;
	setAttr ".pt[4138]" -type "float3" -0.23439343 0 0.22388807 ;
	setAttr ".pt[4139]" -type "float3" -0.088129491 0 -0.23050421 ;
	setAttr ".pt[4140]" -type "float3" -0.1965843 0 -0.11744814 ;
	setAttr ".pt[4141]" -type "float3" 0.11883463 0 -0.1932193 ;
	setAttr ".pt[4142]" -type "float3" 0.1586891 0 -0.24344777 ;
	setAttr ".pt[4143]" -type "float3" -0.15471852 0 0.14038189 ;
	setAttr ".pt[4144]" -type "float3" -0.20957375 0 0.17687526 ;
	setAttr ".pt[4145]" -type "float3" -0.0095799407 0 -0.25654465 ;
	setAttr ".pt[4146]" -type "float3" -0.0048723682 0 -0.2549105 ;
	setAttr ".pt[4147]" -type "float3" -0.37926078 0 -6.6207051e-08 ;
	setAttr ".pt[4148]" -type "float3" -0.33732668 0 1.0127613e-08 ;
	setAttr ".pt[4149]" -type "float3" -0.0029294707 0 0.28668469 ;
	setAttr ".pt[4150]" -type "float3" -0.00039171666 0 0.28736052 ;
	setAttr ".pt[4151]" -type "float3" 0.28375539 0 1.0379826e-07 ;
	setAttr ".pt[4152]" -type "float3" 0.34575659 0 4.2824407e-08 ;
	setAttr ".pt[4153]" -type "float3" 0.1049104 0 -0.22395432 ;
	setAttr ".pt[4154]" -type "float3" 0.10680865 0 -0.21150219 ;
	setAttr ".pt[4155]" -type "float3" -0.24126032 0 0.11411073 ;
	setAttr ".pt[4156]" -type "float3" -0.21245985 0 0.10776591 ;
	setAttr ".pt[4157]" -type "float3" 0.029739873 0 -0.24825594 ;
	setAttr ".pt[4158]" -type "float3" 0.048022516 0 -0.30775008 ;
	setAttr ".pt[4159]" -type "float3" -0.28158784 0 0.039319843 ;
	setAttr ".pt[4160]" -type "float3" -0.3732681 0 0.048742883 ;
	setAttr ".pt[4161]" -type "float3" 0.093248144 0 -0.31647307 ;
	setAttr ".pt[4162]" -type "float3" 0.11057882 0 -0.24270286 ;
	setAttr ".pt[4163]" -type "float3" -0.32605258 0 0.10282798 ;
	setAttr ".pt[4164]" -type "float3" -0.34124151 0 0.078858912 ;
	setAttr ".pt[4165]" -type "float3" -0.13412657 0 -0.20929347 ;
	setAttr ".pt[4166]" -type "float3" -0.17969614 0 -0.15206045 ;
	setAttr ".pt[4167]" -type "float3" -0.072287574 0 -0.23904815 ;
	setAttr ".pt[4168]" -type "float3" -0.067457817 0 -0.30092409 ;
	setAttr ".pt[4169]" -type "float3" -0.23366426 0 -0.07767152 ;
	setAttr ".pt[4170]" -type "float3" -0.22624195 0 -0.097776227 ;
	setAttr ".pt[4171]" -type "float3" -0.15870458 0 -0.18292916 ;
	setAttr ".pt[4172]" -type "float3" 0.18635623 0 -0.09932936 ;
	setAttr ".pt[4173]" -type "float3" 0.22229111 0 -0.12728213 ;
	setAttr ".pt[4174]" -type "float3" -0.10941458 0 0.19494483 ;
	setAttr ".pt[4175]" -type "float3" -0.12772429 0 0.24980514 ;
	setAttr ".pt[4176]" -type "float3" 0.24294193 0 -0.077671438 ;
	setAttr ".pt[4177]" -type "float3" 0.27195385 0 -0.074157879 ;
	setAttr ".pt[4178]" -type "float3" -0.073777795 0 0.23904815 ;
	setAttr ".pt[4179]" -type "float3" -0.088237852 0 0.22823478 ;
	setAttr ".pt[4180]" -type "float3" 0.19376656 0 -0.17122439 ;
	setAttr ".pt[4181]" -type "float3" 0.2045729 0 -0.15514478 ;
	setAttr ".pt[4182]" -type "float3" -0.1573198 0 0.23567067 ;
	setAttr ".pt[4183]" -type "float3" -0.16517107 0 0.21353833 ;
	setAttr ".pt[4184]" -type "float3" -0.067410715 0 -0.25338617 ;
	setAttr ".pt[4185]" -type "float3" -0.056879651 0 -0.25177228 ;
	setAttr ".pt[4186]" -type "float3" -0.3747097 0 -0.040132444 ;
	setAttr ".pt[4187]" -type "float3" -0.33323342 0 -0.039876748 ;
	setAttr ".pt[4188]" -type "float3" 0.28022566 0 -0.044847116 ;
	setAttr ".pt[4189]" -type "float3" 0.34149492 0 -0.044952992 ;
	setAttr ".pt[4190]" -type "float3" -0.047776841 0 0.28315514 ;
	setAttr ".pt[4191]" -type "float3" -0.054541245 0 0.28382245 ;
	setAttr ".pt[4192]" -type "float3" 0.19267878 0 -0.15575022 ;
	setAttr ".pt[4193]" -type "float3" 0.17839979 0 -0.14250049 ;
	setAttr ".pt[4194]" -type "float3" -0.2118386 0 0.15575033 ;
	setAttr ".pt[4195]" -type "float3" -0.18783689 0 0.14250061 ;
	setAttr ".pt[4196]" -type "float3" -0.10872668 0 -0.22395459 ;
	setAttr ".pt[4197]" -type "float3" -0.10163063 0 -0.28192386 ;
	setAttr ".pt[4198]" -type "float3" -0.2185708 0 -0.11411075 ;
	setAttr ".pt[4199]" -type "float3" -0.19297577 0 -0.1436473 ;
	setAttr ".pt[4200]" -type "float3" 0.18014963 0 -0.19223681 ;
	setAttr ".pt[4201]" -type "float3" 0.20165506 0 -0.25615582 ;
	setAttr ".pt[4202]" -type "float3" -0.23911165 0 0.13966849 ;
	setAttr ".pt[4203]" -type "float3" -0.2687113 0 0.1861082 ;
	setAttr ".pt[4204]" -type "float3" -0.0095799621 0 -0.25135058 ;
	setAttr ".pt[4205]" -type "float3" -0.0095799286 0 -0.31158602 ;
	setAttr ".pt[4206]" -type "float3" -0.28468212 0 -5.2435696e-09 ;
	setAttr ".pt[4207]" -type "float3" -0.37780121 0 -8.7963137e-10 ;
	setAttr ".pt[4208]" -type "float3" 0.09263166 0 -0.21280092 ;
	setAttr ".pt[4209]" -type "float3" 0.12341436 0 -0.26812002 ;
	setAttr ".pt[4210]" -type "float3" -0.17077564 0 0.1084276 ;
	setAttr ".pt[4211]" -type "float3" -0.23118953 0 0.13661405 ;
	setAttr ".pt[4212]" -type "float3" 0.042475011 0 -0.32866189 ;
	setAttr ".pt[4213]" -type "float3" 0.069202729 0 -0.25205082 ;
	setAttr ".pt[4214]" -type "float3" -0.3382419 0 0.052055053 ;
	setAttr ".pt[4215]" -type "float3" -0.35548496 0 0.039921008 ;
	setAttr ".pt[4216]" -type "float3" 0.068471231 0 -0.23904815 ;
	setAttr ".pt[4217]" -type "float3" 0.072395377 0 -0.22575665 ;
	setAttr ".pt[4218]" -type "float3" -0.25635412 0 0.077671506 ;
	setAttr ".pt[4219]" -type "float3" -0.22671384 0 0.07335265 ;
	setAttr ".pt[4220]" -type "float3" -0.14235607 0 -0.20334618 ;
	setAttr ".pt[4221]" -type "float3" -0.13316903 0 -0.25598112 ;
	setAttr ".pt[4222]" -type "float3" -0.19796284 0 -0.1477399 ;
	setAttr ".pt[4223]" -type "float3" -0.17364922 0 -0.18598108 ;
	setAttr ".pt[4224]" -type "float3" -0.12381762 0 -0.24398799 ;
	setAttr ".pt[4225]" -type "float3" -0.1076063 0 -0.2424342 ;
	setAttr ".pt[4226]" -type "float3" -0.36116767 0 -0.079276688 ;
	setAttr ".pt[4227]" -type "float3" -0.32105574 0 -0.07877171 ;
	setAttr ".pt[4228]" -type "float3" -0.17234744 0 -0.17773139 ;
	setAttr ".pt[4229]" -type "float3" -0.1612955 0 -0.22373526 ;
	setAttr ".pt[4230]" -type "float3" 0.22784813 0 -0.11411049 ;
	setAttr ".pt[4231]" -type "float3" 0.25478822 0 -0.10894863 ;
	setAttr ".pt[4232]" -type "float3" -0.11021677 0 0.22395456 ;
	setAttr ".pt[4233]" -type "float3" -0.12967822 0 0.2138242 ;
	setAttr ".pt[4234]" -type "float3" 0.26972413 0 -0.088590302 ;
	setAttr ".pt[4235]" -type "float3" 0.32881519 0 -0.088799268 ;
	setAttr ".pt[4236]" -type "float3" -0.091519915 0 0.27265361 ;
	setAttr ".pt[4237]" -type "float3" -0.10735726 0 0.27329627 ;
	setAttr ".pt[4238]" -type "float3" 0.16832638 0 -0.12860264 ;
	setAttr ".pt[4239]" -type "float3" 0.2009543 0 -0.16479331 ;
	setAttr ".pt[4240]" -type "float3" -0.13883656 0 0.17700644 ;
	setAttr ".pt[4241]" -type "float3" -0.1625423 0 0.22681831 ;
	setAttr ".pt[4242]" -type "float3" -0.048899822 0 -0.24825594 ;
	setAttr ".pt[4243]" -type "float3" -0.067182481 0 -0.30775008 ;
	setAttr ".pt[4244]" -type "float3" -0.28158784 0 -0.039319828 ;
	setAttr ".pt[4245]" -type "float3" -0.3732681 0 -0.048742827 ;
	setAttr ".pt[4246]" -type "float3" 0.1681512 0 -0.20598292 ;
	setAttr ".pt[4247]" -type "float3" 0.17759651 0 -0.1866391 ;
	setAttr ".pt[4248]" -type "float3" -0.18731153 0 0.20598318 ;
	setAttr ".pt[4249]" -type "float3" -0.19675674 0 0.18663898 ;
	setAttr ".pt[4250]" -type "float3" -0.17741151 0 -0.22858286 ;
	setAttr ".pt[4251]" -type "float3" -0.15580346 0 -0.22712669 ;
	setAttr ".pt[4252]" -type "float3" -0.33896828 0 -0.11646886 ;
	setAttr ".pt[4253]" -type "float3" -0.30109087 0 -0.11572678 ;
	setAttr ".pt[4254]" -type "float3" 0.15854841 0 -0.17819777 ;
	setAttr ".pt[4255]" -type "float3" 0.14749904 0 -0.16303818 ;
	setAttr ".pt[4256]" -type "float3" -0.2409886 0 0.12946817 ;
	setAttr ".pt[4257]" -type "float3" -0.21422829 0 0.11845414 ;
	setAttr ".pt[4258]" -type "float3" -0.0095799444 0 -0.33275846 ;
	setAttr ".pt[4259]" -type "float3" 0.037545856 0 -0.25519302 ;
	setAttr ".pt[4260]" -type "float3" -0.34233871 0 -6.4162826e-08 ;
	setAttr ".pt[4261]" -type "float3" -0.36027253 0 -5.1404925e-09 ;
	setAttr ".pt[4262]" -type "float3" 0.13999024 0 -0.21171948 ;
	setAttr ".pt[4263]" -type "float3" 0.15660042 0 -0.28211546 ;
	setAttr ".pt[4264]" -type "float3" -0.26372167 0 0.10787623 ;
	setAttr ".pt[4265]" -type "float3" -0.29632059 0 0.14374489 ;
	setAttr ".pt[4266]" -type "float3" 0.043041077 0 -0.24825594 ;
	setAttr ".pt[4267]" -type "float3" 0.036176156 0 -0.23445225 ;
	setAttr ".pt[4268]" -type "float3" -0.26556131 0 0.039319843 ;
	setAttr ".pt[4269]" -type "float3" -0.23540941 0 0.0371335 ;
	setAttr ".pt[4270]" -type "float3" 0.064239778 0 -0.22714256 ;
	setAttr ".pt[4271]" -type "float3" 0.085192591 0 -0.28619018 ;
	setAttr ".pt[4272]" -type "float3" -0.1825362 0 0.073803157 ;
	setAttr ".pt[4273]" -type "float3" -0.24702235 0 0.092988946 ;
	setAttr ".pt[4274]" -type "float3" -0.226873 0 -0.20754868 ;
	setAttr ".pt[4275]" -type "float3" -0.20028424 0 -0.20622723 ;
	setAttr ".pt[4276]" -type "float3" -0.30865854 0 -0.15079288 ;
	setAttr ".pt[4277]" -type "float3" -0.27383336 0 -0.14983258 ;
	setAttr ".pt[4278]" -type "float3" -0.087251373 0 -0.23904815 ;
	setAttr ".pt[4279]" -type "float3" -0.1233665 0 -0.29633608 ;
	setAttr ".pt[4280]" -type "float3" -0.27238017 0 -0.07767152 ;
	setAttr ".pt[4281]" -type "float3" -0.35977927 0 -0.096285447 ;
	setAttr ".pt[4282]" -type "float3" -0.2709834 0 -0.18140465 ;
	setAttr ".pt[4283]" -type "float3" -0.23995329 0 -0.18024901 ;
	setAttr ".pt[4284]" -type "float3" 0.25250861 0 -0.13015211 ;
	setAttr ".pt[4285]" -type "float3" 0.30802873 0 -0.13045882 ;
	setAttr ".pt[4286]" -type "float3" -0.13308166 0 0.25543794 ;
	setAttr ".pt[4287]" -type "float3" -0.15753956 0 0.25603959 ;
	setAttr ".pt[4288]" -type "float3" 0.20724028 0 -0.14773972 ;
	setAttr ".pt[4289]" -type "float3" 0.23135188 0 -0.14105678 ;
	setAttr ".pt[4290]" -type "float3" -0.14384644 0 0.20334648 ;
	setAttr ".pt[4291]" -type "float3" -0.16792366 0 0.19414774 ;
	setAttr ".pt[4292]" -type "float3" -0.061634883 0 -0.32866189 ;
	setAttr ".pt[4293]" -type "float3" -0.041236926 0 -0.25205082 ;
	setAttr ".pt[4294]" -type "float3" -0.3382419 0 -0.052054971 ;
	setAttr ".pt[4295]" -type "float3" -0.35548496 0 -0.039920993 ;
	setAttr ".pt[4296]" -type "float3" 0.14591566 0 -0.15470906 ;
	setAttr ".pt[4297]" -type "float3" 0.17443408 0 -0.19824642 ;
	setAttr ".pt[4298]" -type "float3" -0.165076 0 0.15470955 ;
	setAttr ".pt[4299]" -type "float3" -0.19359417 0 0.19824657 ;
	setAttr ".pt[4300]" -type "float3" -0.12369072 0 -0.22395459 ;
	setAttr ".pt[4301]" -type "float3" -0.17674878 0 -0.27762499 ;
	setAttr ".pt[4302]" -type "float3" -0.25728637 0 -0.11411075 ;
	setAttr ".pt[4303]" -type "float3" -0.33766681 0 -0.14145732 ;
	setAttr ".pt[4304]" -type "float3" 0.13815983 0 -0.23567082 ;
	setAttr ".pt[4305]" -type "float3" 0.14601132 0 -0.21353835 ;
	setAttr ".pt[4306]" -type "float3" -0.21292642 0 0.17122467 ;
	setAttr ".pt[4307]" -type "float3" -0.22373253 0 0.15514471 ;
	setAttr ".pt[4308]" -type "float3" 0.0037211906 0 -0.25135058 ;
	setAttr ".pt[4309]" -type "float3" -0.00095736503 0 -0.23737437 ;
	setAttr ".pt[4310]" -type "float3" -0.26865584 0 -3.5794969e-08 ;
	setAttr ".pt[4311]" -type "float3" -0.23833236 0 -3.6169826e-08 ;
	setAttr ".pt[4312]" -type "float3" 0.12027802 0 -0.19625704 ;
	setAttr ".pt[4313]" -type "float3" 0.11285055 0 -0.17956109 ;
	setAttr ".pt[4314]" -type "float3" -0.2644408 0 0.099998072 ;
	setAttr ".pt[4315]" -type "float3" -0.23546121 0 0.091491021 ;
	setAttr ".pt[4316]" -type "float3" 0.034357626 0 -0.23589166 ;
	setAttr ".pt[4317]" -type "float3" 0.044964664 0 -0.29721314 ;
	setAttr ".pt[4318]" -type "float3" -0.18970978 0 0.037361529 ;
	setAttr ".pt[4319]" -type "float3" -0.25668001 0 0.047074024 ;
	setAttr ".pt[4320]" -type "float3" 0.096475191 0 -0.22598834 ;
	setAttr ".pt[4321]" -type "float3" 0.10778103 0 -0.30112883 ;
	setAttr ".pt[4322]" -type "float3" -0.2817457 0 0.073427916 ;
	setAttr ".pt[4323]" -type "float3" -0.31654182 0 0.097842611 ;
	setAttr ".pt[4324]" -type "float3" -0.18107224 0 -0.20334654 ;
	setAttr ".pt[4325]" -type "float3" -0.22601485 0 -0.25207835 ;
	setAttr ".pt[4326]" -type "float3" -0.23667888 0 -0.1477399 ;
	setAttr ".pt[4327]" -type "float3" -0.30747682 0 -0.18314569 ;
	setAttr ".pt[4328]" -type "float3" -0.11240802 0 -0.31647307 ;
	setAttr ".pt[4329]" -type "float3" -0.079348192 0 -0.2427031 ;
	setAttr ".pt[4330]" -type "float3" -0.32605258 0 -0.10282801 ;
	setAttr ".pt[4331]" -type "float3" -0.34124151 0 -0.078858934 ;
	setAttr ".pt[4332]" -type "float3" -0.21106349 0 -0.17773139 ;
	setAttr ".pt[4333]" -type "float3" -0.2699514 0 -0.22032465 ;
	setAttr ".pt[4334]" -type "float3" 0.22900322 0 -0.16850911 ;
	setAttr ".pt[4335]" -type "float3" 0.27964818 0 -0.16890612 ;
	setAttr ".pt[4336]" -type "float3" -0.17143862 0 0.23193265 ;
	setAttr ".pt[4337]" -type "float3" -0.20385273 0 0.23247966 ;
	setAttr ".pt[4338]" -type "float3" -0.035598576 0 -0.24825594 ;
	setAttr ".pt[4339]" -type "float3" -0.038090941 0 -0.23445225 ;
	setAttr ".pt[4340]" -type "float3" -0.26556131 0 -0.039319828 ;
	setAttr ".pt[4341]" -type "float3" -0.23540941 0 -0.037133515 ;
	setAttr ".pt[4342]" -type "float3" 0.18162502 0 -0.17773096 ;
	setAttr ".pt[4343]" -type "float3" 0.20222069 0 -0.16969143 ;
	setAttr ".pt[4344]" -type "float3" -0.17383769 0 0.1777311 ;
	setAttr ".pt[4345]" -type "float3" -0.20203142 0 0.16969134 ;
	setAttr ".pt[4346]" -type "float3" -0.16064936 0 -0.2964904 ;
	setAttr ".pt[4347]" -type "float3" -0.13571998 0 -0.22737843 ;
	setAttr ".pt[4348]" -type "float3" -0.30607066 0 -0.15106954 ;
	setAttr ".pt[4349]" -type "float3" -0.31789091 0 -0.11585524 ;
	setAttr ".pt[4350]" -type "float3" 0.11967674 0 -0.17700632 ;
	setAttr ".pt[4351]" -type "float3" 0.14338233 0 -0.22681816 ;
	setAttr ".pt[4352]" -type "float3" -0.18748648 0 0.12860261 ;
	setAttr ".pt[4353]" -type "float3" -0.22011477 0 0.16479312 ;
	setAttr ".pt[4354]" -type "float3" 0.0037211629 0 -0.23883156 ;
	setAttr ".pt[4355]" -type "float3" 0.0037211911 0 -0.30091819 ;
	setAttr ".pt[4356]" -type "float3" -0.19212104 0 -1.2778494e-07 ;
	setAttr ".pt[4357]" -type "float3" -0.25992602 0 5.7188728e-08 ;
	setAttr ".pt[4358]" -type "float3" 0.10453057 0 -0.25955361 ;
	setAttr ".pt[4359]" -type "float3" 0.11059481 0 -0.23517907 ;
	setAttr ".pt[4360]" -type "float3" -0.23353483 0 0.13224949 ;
	setAttr ".pt[4361]" -type "float3" -0.24543586 0 0.11983016 ;
	setAttr ".pt[4362]" -type "float3" 0.05067632 0 -0.23469248 ;
	setAttr ".pt[4363]" -type "float3" 0.056399703 0 -0.3127279 ;
	setAttr ".pt[4364]" -type "float3" -0.29274154 0 0.037171569 ;
	setAttr ".pt[4365]" -type "float3" -0.32887745 0 0.049531151 ;
	setAttr ".pt[4366]" -type "float3" 0.078810193 0 -0.20948404 ;
	setAttr ".pt[4367]" -type "float3" 0.075306989 0 -0.19166288 ;
	setAttr ".pt[4368]" -type "float3" -0.28161743 0 0.068065502 ;
	setAttr ".pt[4369]" -type "float3" -0.25101224 0 0.062275086 ;
	setAttr ".pt[4370]" -type "float3" -0.20517078 0 -0.26920757 ;
	setAttr ".pt[4371]" -type "float3" -0.17876764 0 -0.20645551 ;
	setAttr ".pt[4372]" -type "float3" -0.27878788 0 -0.1955907 ;
	setAttr ".pt[4373]" -type "float3" -0.28601074 0 -0.14999872 ;
	setAttr ".pt[4374]" -type "float3" -0.073950276 0 -0.23904815 ;
	setAttr ".pt[4375]" -type "float3" -0.074310102 0 -0.22575665 ;
	setAttr ".pt[4376]" -type "float3" -0.25635412 0 -0.07767152 ;
	setAttr ".pt[4377]" -type "float3" -0.22671384 0 -0.073352911 ;
	setAttr ".pt[4378]" -type "float3" -0.24487598 0 -0.23529613 ;
	setAttr ".pt[4379]" -type "float3" -0.2463831 0 -0.18044861 ;
	setAttr ".pt[4380]" -type "float3" -0.026915355 0 -0.23589166 ;
	setAttr ".pt[4381]" -type "float3" -0.037522342 0 -0.29721314 ;
	setAttr ".pt[4382]" -type "float3" -0.18970978 0 -0.037361547 ;
	setAttr ".pt[4383]" -type "float3" -0.25668001 0 -0.047074195 ;
	setAttr ".pt[4384]" -type "float3" 0.19978748 0 -0.20271677 ;
	setAttr ".pt[4385]" -type "float3" 0.2443718 0 -0.20319477 ;
	setAttr ".pt[4386]" -type "float3" -0.20564604 0 0.20271668 ;
	setAttr ".pt[4387]" -type "float3" -0.24515516 0 0.2031948 ;
	setAttr ".pt[4388]" -type "float3" -0.13141643 0 -0.22395459 ;
	setAttr ".pt[4389]" -type "float3" -0.10872312 0 -0.21150219 ;
	setAttr ".pt[4390]" -type "float3" -0.24126032 0 -0.11411075 ;
	setAttr ".pt[4391]" -type "float3" -0.21245985 0 -0.10776601 ;
	setAttr ".pt[4392]" -type "float3" 0.15163353 0 -0.2033466 ;
	setAttr ".pt[4393]" -type "float3" 0.1681128 0 -0.1941476 ;
	setAttr ".pt[4394]" -type "float3" -0.19945283 0 0.14773987 ;
	setAttr ".pt[4395]" -type "float3" -0.23116289 0 0.14105679 ;
	setAttr ".pt[4396]" -type "float3" 0.0037212148 0 -0.23761772 ;
	setAttr ".pt[4397]" -type "float3" 0.0037212202 0 -0.3166258 ;
	setAttr ".pt[4398]" -type "float3" -0.29643697 0 -5.6118954e-09 ;
	setAttr ".pt[4399]" -type "float3" -0.33302379 0 -3.492755e-09 ;
	setAttr ".pt[4400]" -type "float3" 0.090254582 0 -0.19494483 ;
	setAttr ".pt[4401]" -type "float3" 0.10856426 0 -0.2498049 ;
	setAttr ".pt[4402]" -type "float3" -0.20551598 0 0.099329352 ;
	setAttr ".pt[4403]" -type "float3" -0.24145114 0 0.12728202 ;
	setAttr ".pt[4404]" -type "float3" 0.0351661 0 -0.21755297 ;
	setAttr ".pt[4405]" -type "float3" 0.035792932 0 -0.19904515 ;
	setAttr ".pt[4406]" -type "float3" -0.29209504 0 0.034456927 ;
	setAttr ".pt[4407]" -type "float3" -0.26049897 0 0.031525705 ;
	setAttr ".pt[4408]" -type "float3" 0.068091594 0 -0.27704659 ;
	setAttr ".pt[4409]" -type "float3" 0.072219223 0 -0.25102925 ;
	setAttr ".pt[4410]" -type "float3" -0.24862841 0 0.090018012 ;
	setAttr ".pt[4411]" -type "float3" -0.26133192 0 0.081564389 ;
	setAttr ".pt[4412]" -type "float3" -0.16504571 0 -0.20334618 ;
	setAttr ".pt[4413]" -type "float3" -0.14048268 0 -0.19203989 ;
	setAttr ".pt[4414]" -type "float3" -0.22065224 0 -0.1477399 ;
	setAttr ".pt[4415]" -type "float3" -0.19299763 0 -0.13952543 ;
	setAttr ".pt[4416]" -type "float3" -0.056797497 0 -0.22714256 ;
	setAttr ".pt[4417]" -type "float3" -0.077750139 0 -0.28619018 ;
	setAttr ".pt[4418]" -type "float3" -0.1825362 0 -0.073803119 ;
	setAttr ".pt[4419]" -type "float3" -0.24702235 0 -0.092988782 ;
	setAttr ".pt[4420]" -type "float3" -0.19503744 0 -0.17773114 ;
	setAttr ".pt[4421]" -type "float3" -0.16880634 0 -0.16784921 ;
	setAttr ".pt[4422]" -type "float3" -0.043233871 0 -0.23469248 ;
	setAttr ".pt[4423]" -type "float3" -0.048957288 0 -0.3127279 ;
	setAttr ".pt[4424]" -type "float3" -0.29274154 0 -0.037171643 ;
	setAttr ".pt[4425]" -type "float3" -0.32887745 0 -0.049531039 ;
	setAttr ".pt[4426]" -type "float3" -0.08518938 0 -0.21280092 ;
	setAttr ".pt[4427]" -type "float3" -0.11597189 0 -0.26812005 ;
	setAttr ".pt[4428]" -type "float3" -0.17077564 0 -0.10842762 ;
	setAttr ".pt[4429]" -type "float3" -0.23118953 0 -0.13661397 ;
	setAttr ".pt[4430]" -type "float3" 0.16557947 0 -0.23193277 ;
	setAttr ".pt[4431]" -type "float3" 0.20306911 0 -0.23248002 ;
	setAttr ".pt[4432]" -type "float3" -0.23486237 0 0.16850892 ;
	setAttr ".pt[4433]" -type "float3" -0.2804313 0 0.16890627 ;
	setAttr ".pt[4434]" -type "float3" -0.0095799686 0 -0.22026469 ;
	setAttr ".pt[4435]" -type "float3" -0.0047186548 0 -0.20152639 ;
	setAttr ".pt[4436]" -type "float3" -0.29561734 0 -6.0773613e-09 ;
	setAttr ".pt[4437]" -type "float3" -0.26368758 0 -2.0529132e-08 ;
	setAttr ".pt[4438]" -type "float3" 0.11800402 0 -0.22395432 ;
	setAttr ".pt[4439]" -type "float3" 0.12986772 0 -0.21382371 ;
	setAttr ".pt[4440]" -type "float3" -0.22006112 0 0.11411073 ;
	setAttr ".pt[4441]" -type "float3" -0.25459927 0 0.10894857 ;
	setAttr ".pt[4442]" -type "float3" 0.029739873 0 -0.28771797 ;
	setAttr ".pt[4443]" -type "float3" 0.031829413 0 -0.26069844 ;
	setAttr ".pt[4444]" -type "float3" -0.25783607 0 0.045570072 ;
	setAttr ".pt[4445]" -type "float3" -0.27102873 0 0.041290361 ;
	setAttr ".pt[4446]" -type "float3" 0.058374271 0 -0.20808341 ;
	setAttr ".pt[4447]" -type "float3" 0.07083714 0 -0.26664081 ;
	setAttr ".pt[4448]" -type "float3" -0.21872121 0 0.067610331 ;
	setAttr ".pt[4449]" -type "float3" -0.25707802 0 0.086637117 ;
	setAttr ".pt[4450]" -type "float3" -0.11139201 0 -0.19321923 ;
	setAttr ".pt[4451]" -type "float3" -0.15124661 0 -0.24344772 ;
	setAttr ".pt[4452]" -type "float3" -0.1547185 0 -0.14038217 ;
	setAttr ".pt[4453]" -type "float3" -0.20957407 0 -0.17687529 ;
	setAttr ".pt[4454]" -type "float3" -0.089032762 0 -0.22598834 ;
	setAttr ".pt[4455]" -type "float3" -0.10033862 0 -0.30112883 ;
	setAttr ".pt[4456]" -type "float3" -0.2817457 0 -0.073428057 ;
	setAttr ".pt[4457]" -type "float3" -0.31654182 0 -0.09784276 ;
	setAttr ".pt[4458]" -type "float3" -0.13476022 0 -0.16887979 ;
	setAttr ".pt[4459]" -type "float3" -0.18270576 0 -0.21278167 ;
	setAttr ".pt[4460]" -type "float3" -0.054325916 0 -0.21755297 ;
	setAttr ".pt[4461]" -type "float3" -0.045230288 0 -0.19904515 ;
	setAttr ".pt[4462]" -type "float3" -0.29209504 0 -0.034456912 ;
	setAttr ".pt[4463]" -type "float3" -0.26049897 0 -0.031525604 ;
	setAttr ".pt[4464]" -type "float3" -0.13254768 0 -0.21171927 ;
	setAttr ".pt[4465]" -type "float3" -0.14915769 0 -0.28211546 ;
	setAttr ".pt[4466]" -type "float3" -0.26372167 0 -0.10787626 ;
	setAttr ".pt[4467]" -type "float3" -0.29632059 0 -0.1437449 ;
	setAttr ".pt[4468]" -type "float3" -0.0095799621 0 -0.29130417 ;
	setAttr ".pt[4469]" -type "float3" -0.0095799584 0 -0.2639482 ;
	setAttr ".pt[4470]" -type "float3" -0.26093 0 -4.1719153e-09 ;
	setAttr ".pt[4471]" -type "float3" -0.27428713 0 -4.9056683e-09 ;
	setAttr ".pt[4472]" -type "float3" 0.12722285 0 -0.25543755 ;
	setAttr ".pt[4473]" -type "float3" 0.15675631 0 -0.25603998 ;
	setAttr ".pt[4474]" -type "float3" -0.25836712 0 0.130152 ;
	setAttr ".pt[4475]" -type "float3" -0.30881193 0 0.13045889 ;
	setAttr ".pt[4476]" -type "float3" 0.024820626 0 -0.21609829 ;
	setAttr ".pt[4477]" -type "float3" 0.03112971 0 -0.2769115 ;
	setAttr ".pt[4478]" -type "float3" -0.22677697 0 0.034226608 ;
	setAttr ".pt[4479]" -type "float3" -0.26661128 0 0.043858495 ;
	setAttr ".pt[4480]" -type "float3" 0.081565157 0 -0.23904815 ;
	setAttr ".pt[4481]" -type "float3" 0.08842703 0 -0.22823443 ;
	setAttr ".pt[4482]" -type "float3" -0.23515433 0 0.077671446 ;
	setAttr ".pt[4483]" -type "float3" -0.27176455 0 0.074157879 ;
	setAttr ".pt[4484]" -type "float3" -0.17270708 0 -0.19223711 ;
	setAttr ".pt[4485]" -type "float3" -0.19421238 0 -0.25615552 ;
	setAttr ".pt[4486]" -type "float3" -0.23911162 0 -0.13966835 ;
	setAttr ".pt[4487]" -type "float3" -0.26871118 0 -0.18610787 ;
	setAttr ".pt[4488]" -type "float3" -0.097970158 0 -0.20948404 ;
	setAttr ".pt[4489]" -type "float3" -0.084744282 0 -0.19166288 ;
	setAttr ".pt[4490]" -type "float3" -0.28161743 0 -0.068065614 ;
	setAttr ".pt[4491]" -type "float3" -0.25101224 0 -0.062275011 ;
	setAttr ".pt[4492]" -type "float3" -0.20852254 0 -0.16802141 ;
	setAttr ".pt[4493]" -type "float3" -0.23439303 0 -0.22388835 ;
	setAttr ".pt[4494]" -type "float3" -0.048899822 0 -0.28771797 ;
	setAttr ".pt[4495]" -type "float3" -0.050989248 0 -0.26069844 ;
	setAttr ".pt[4496]" -type "float3" -0.25783607 0 -0.045570087 ;
	setAttr ".pt[4497]" -type "float3" -0.27102873 0 -0.041290563 ;
	setAttr ".pt[4498]" -type "float3" -0.13943785 0 -0.19625729 ;
	setAttr ".pt[4499]" -type "float3" -0.12228765 0 -0.17956124 ;
	setAttr ".pt[4500]" -type "float3" -0.2644408 0 -0.099998094 ;
	setAttr ".pt[4501]" -type "float3" -0.23546121 0 -0.091491029 ;
	setAttr ".pt[4502]" -type "float3" -0.009579978 0 -0.21879208 ;
	setAttr ".pt[4503]" -type "float3" -0.0095799686 0 -0.28036326 ;
	setAttr ".pt[4504]" -type "float3" -0.22948411 0 -6.1168475e-09 ;
	setAttr ".pt[4505]" -type "float3" -0.2698153 0 2.6086008e-08 ;
	setAttr ".pt[4506]" -type "float3" 0.043213539 0 -0.24825594 ;
	setAttr ".pt[4507]" -type "float3" 0.044811346 0 -0.23702519 ;
	setAttr ".pt[4508]" -type "float3" -0.24436188 0 0.03931975 ;
	setAttr ".pt[4509]" -type "float3" -0.28223541 0 0.037541188 ;
	setAttr ".pt[4510]" -type "float3" 0.085660972 0 -0.27265337 ;
	setAttr ".pt[4511]" -type "float3" 0.1065741 0 -0.27329639 ;
	setAttr ".pt[4512]" -type "float3" -0.27558267 0 0.088590384 ;
	setAttr ".pt[4513]" -type "float3" -0.32959786 0 0.088799283 ;
	setAttr ".pt[4514]" -type "float3" -0.17770821 0 -0.1781977 ;
	setAttr ".pt[4515]" -type "float3" -0.15693657 0 -0.16303824 ;
	setAttr ".pt[4516]" -type "float3" -0.24098866 0 -0.12946808 ;
	setAttr ".pt[4517]" -type "float3" -0.2142286 0 -0.11845414 ;
	setAttr ".pt[4518]" -type "float3" -0.087251373 0 -0.27704659 ;
	setAttr ".pt[4519]" -type "float3" -0.091379143 0 -0.25102925 ;
	setAttr ".pt[4520]" -type "float3" -0.24862841 0 -0.090018079 ;
	setAttr ".pt[4521]" -type "float3" -0.26133192 0 -0.081564464 ;
	setAttr ".pt[4522]" -type "float3" -0.21183844 0 -0.1557505 ;
	setAttr ".pt[4523]" -type "float3" -0.18783696 0 -0.14250065 ;
	setAttr ".pt[4524]" -type "float3" -0.043980598 0 -0.21609829 ;
	setAttr ".pt[4525]" -type "float3" -0.050289683 0 -0.2769115 ;
	setAttr ".pt[4526]" -type "float3" -0.22677697 0 -0.034226567 ;
	setAttr ".pt[4527]" -type "float3" -0.26661128 0 -0.043858506 ;
	setAttr ".pt[4528]" -type "float3" -0.12369072 0 -0.25955415 ;
	setAttr ".pt[4529]" -type "float3" -0.12975471 0 -0.23517923 ;
	setAttr ".pt[4530]" -type "float3" -0.23353483 0 -0.13224934 ;
	setAttr ".pt[4531]" -type "float3" -0.24543586 0 -0.11983019 ;
	setAttr ".pt[4532]" -type "float3" 0.0038937381 0 -0.25135058 ;
	setAttr ".pt[4533]" -type "float3" 9.4651616e-05 0 -0.23998024 ;
	setAttr ".pt[4534]" -type "float3" -0.2474563 0 5.5859253e-08 ;
	setAttr ".pt[4535]" -type "float3" -0.28575489 0 -6.6651346e-08 ;
	setAttr ".pt[4536]" -type "float3" 0.041918062 0 -0.28315538 ;
	setAttr ".pt[4537]" -type "float3" 0.053757887 0 -0.28382245 ;
	setAttr ".pt[4538]" -type "float3" -0.28608483 0 0.044847336 ;
	setAttr ".pt[4539]" -type "float3" -0.34227818 0 0.044953056 ;
	setAttr ".pt[4540]" -type "float3" -0.15731975 0 -0.23567036 ;
	setAttr ".pt[4541]" -type "float3" -0.16517125 0 -0.21353827 ;
	setAttr ".pt[4542]" -type "float3" -0.21292664 0 -0.1712247 ;
	setAttr ".pt[4543]" -type "float3" -0.22373284 0 -0.15514462 ;
	setAttr ".pt[4544]" -type "float3" -0.077534109 0 -0.20808341 ;
	setAttr ".pt[4545]" -type "float3" -0.089996882 0 -0.26664081 ;
	setAttr ".pt[4546]" -type "float3" -0.21872121 0 -0.067610353 ;
	setAttr ".pt[4547]" -type "float3" -0.25707802 0 -0.086636946 ;
	setAttr ".pt[4548]" -type "float3" -0.18731125 0 -0.20598298 ;
	setAttr ".pt[4549]" -type "float3" -0.19675657 0 -0.18663916 ;
	setAttr ".pt[4550]" -type "float3" -0.035426084 0 -0.24825594 ;
	setAttr ".pt[4551]" -type "float3" -0.044622034 0 -0.23702519 ;
	setAttr ".pt[4552]" -type "float3" -0.24436188 0 -0.039319828 ;
	setAttr ".pt[4553]" -type "float3" -0.28223541 0 -0.037541118 ;
	setAttr ".pt[4554]" -type "float3" -0.10941447 0 -0.19494496 ;
	setAttr ".pt[4555]" -type "float3" -0.12772423 0 -0.24980506 ;
	setAttr ".pt[4556]" -type "float3" -0.20551598 0 -0.099329375 ;
	setAttr ".pt[4557]" -type "float3" -0.24145114 0 -0.1272819 ;
	setAttr ".pt[4558]" -type "float3" -0.0029293767 0 -0.28668472 ;
	setAttr ".pt[4559]" -type "float3" -0.00039160205 0 -0.28736055 ;
	setAttr ".pt[4560]" -type "float3" -0.28961438 0 2.6255588e-08 ;
	setAttr ".pt[4561]" -type "float3" -0.34653983 0 -4.2776978e-09 ;
	setAttr ".pt[4562]" -type "float3" -0.13883658 0 -0.17700647 ;
	setAttr ".pt[4563]" -type "float3" -0.16254213 0 -0.22681834 ;
	setAttr ".pt[4564]" -type "float3" -0.18748623 0 -0.12860264 ;
	setAttr ".pt[4565]" -type "float3" -0.22011472 0 -0.16479315 ;
	setAttr ".pt[4566]" -type "float3" -0.073777728 0 -0.23904815 ;
	setAttr ".pt[4567]" -type "float3" -0.08823771 0 -0.22823443 ;
	setAttr ".pt[4568]" -type "float3" -0.23515433 0 -0.07767152 ;
	setAttr ".pt[4569]" -type "float3" -0.27176455 0 -0.074158028 ;
	setAttr ".pt[4570]" -type "float3" -0.16507584 0 -0.15470923 ;
	setAttr ".pt[4571]" -type "float3" -0.19359389 0 -0.19824661 ;
	setAttr ".pt[4572]" -type "float3" -0.047776781 0 -0.28315538 ;
	setAttr ".pt[4573]" -type "float3" -0.054541055 0 -0.28382245 ;
	setAttr ".pt[4574]" -type "float3" -0.28608483 0 -0.04484747 ;
	setAttr ".pt[4575]" -type "float3" -0.34227818 0 -0.044953164 ;
	setAttr ".pt[4576]" -type "float3" -0.11021695 0 -0.22395459 ;
	setAttr ".pt[4577]" -type "float3" -0.12967831 0 -0.21382423 ;
	setAttr ".pt[4578]" -type "float3" -0.22006112 0 -0.11411075 ;
	setAttr ".pt[4579]" -type "float3" -0.25459927 0 -0.10894877 ;
	setAttr ".pt[4580]" -type "float3" -0.14384627 0 -0.20334618 ;
	setAttr ".pt[4581]" -type "float3" -0.16792348 0 -0.19414753 ;
	setAttr ".pt[4582]" -type "float3" -0.1994528 0 -0.1477399 ;
	setAttr ".pt[4583]" -type "float3" -0.23116273 0 -0.14105684 ;
	setAttr ".pt[4584]" -type "float3" -0.09151984 0 -0.27265337 ;
	setAttr ".pt[4585]" -type "float3" -0.1073571 0 -0.27329639 ;
	setAttr ".pt[4586]" -type "float3" -0.27558267 0 -0.088590458 ;
	setAttr ".pt[4587]" -type "float3" -0.32959786 0 -0.088799238 ;
	setAttr ".pt[4588]" -type "float3" -0.17383753 0 -0.17773139 ;
	setAttr ".pt[4589]" -type "float3" -0.20203137 0 -0.16969138 ;
	setAttr ".pt[4590]" -type "float3" -0.13308164 0 -0.25543797 ;
	setAttr ".pt[4591]" -type "float3" -0.15753973 0 -0.25603962 ;
	setAttr ".pt[4592]" -type "float3" -0.25836712 0 -0.13015209 ;
	setAttr ".pt[4593]" -type "float3" -0.30881193 0 -0.13045891 ;
	setAttr ".pt[4594]" -type "float3" -0.17143854 0 -0.23193294 ;
	setAttr ".pt[4595]" -type "float3" -0.20385276 0 -0.23247959 ;
	setAttr ".pt[4596]" -type "float3" -0.23486248 0 -0.16850919 ;
	setAttr ".pt[4597]" -type "float3" -0.2804313 0 -0.1689062 ;
	setAttr ".pt[4598]" -type "float3" -0.20564623 0 -0.20271648 ;
	setAttr ".pt[4599]" -type "float3" -0.24515535 0 -0.20319496 ;
	setAttr ".pt[4600]" -type "float3" -0.21460164 0 -0.1477399 ;
	setAttr ".pt[4601]" -type "float3" -0.18898699 0 -0.17773139 ;
	setAttr ".pt[4602]" -type "float3" -0.15326932 0 -0.20334618 ;
	setAttr ".pt[4603]" -type "float3" -0.11964009 0 -0.22395459 ;
	setAttr ".pt[4604]" -type "float3" -0.083200879 0 -0.23904815 ;
	setAttr ".pt[4605]" -type "float3" -0.049859673 0 -0.24825555 ;
	setAttr ".pt[4606]" -type "float3" 0.00091252796 0 -0.25135058 ;
	setAttr ".pt[4607]" -type "float3" 0.040232386 0 -0.24825555 ;
	setAttr ".pt[4608]" -type "float3" 0.07786835 0 -0.23904815 ;
	setAttr ".pt[4609]" -type "float3" 0.11144421 0 -0.22395432 ;
	setAttr ".pt[4610]" -type "float3" 0.14507346 0 -0.2033466 ;
	setAttr ".pt[4611]" -type "float3" 0.16218106 0 -0.17773107 ;
	setAttr ".pt[4612]" -type "float3" 0.1877964 0 -0.14773972 ;
	setAttr ".pt[4613]" -type "float3" 0.20840415 0 -0.11411049 ;
	setAttr ".pt[4614]" -type "float3" 0.22349808 0 -0.077671371 ;
	setAttr ".pt[4615]" -type "float3" 0.23270535 0 -0.039319672 ;
	setAttr ".pt[4616]" -type "float3" 0.23579936 0 -2.51469e-08 ;
	setAttr ".pt[4617]" -type "float3" 0.23270507 0 0.039319746 ;
	setAttr ".pt[4618]" -type "float3" 0.22349791 0 0.077671468 ;
	setAttr ".pt[4619]" -type "float3" 0.20840399 0 0.11411063 ;
	setAttr ".pt[4620]" -type "float3" 0.18779616 0 0.14773989 ;
	setAttr ".pt[4621]" -type "float3" 0.16218089 0 0.17773138 ;
	setAttr ".pt[4622]" -type "float3" 0.1450737 0 0.20334642 ;
	setAttr ".pt[4623]" -type "float3" 0.11144407 0 0.22395423 ;
	setAttr ".pt[4624]" -type "float3" 0.077868119 0 0.23904815 ;
	setAttr ".pt[4625]" -type "float3" 0.040232323 0 0.24825558 ;
	setAttr ".pt[4626]" -type "float3" 0.00091244612 0 0.25135055 ;
	setAttr ".pt[4627]" -type "float3" -0.049859751 0 0.24825558 ;
	setAttr ".pt[4628]" -type "float3" -0.083200946 0 0.23904815 ;
	setAttr ".pt[4629]" -type "float3" -0.11964022 0 0.22395456 ;
	setAttr ".pt[4630]" -type "float3" -0.15326913 0 0.20334648 ;
	setAttr ".pt[4631]" -type "float3" -0.18898703 0 0.1777311 ;
	setAttr ".pt[4632]" -type "float3" -0.21460192 0 0.14773999 ;
	setAttr ".pt[4633]" -type "float3" -0.23521009 0 0.11411073 ;
	setAttr ".pt[4634]" -type "float3" -0.25030354 0 0.077671565 ;
	setAttr ".pt[4635]" -type "float3" -0.25951096 0 0.039319843 ;
	setAttr ".pt[4636]" -type "float3" -0.26260585 0 1.1696204e-07 ;
	setAttr ".pt[4637]" -type "float3" -0.25951096 0 -0.039319828 ;
	setAttr ".pt[4638]" -type "float3" -0.25030354 0 -0.077671401 ;
	setAttr ".pt[4639]" -type "float3" -0.23521009 0 -0.11411075 ;
	setAttr ".pt[4640]" -type "float3" -0.19340844 0 -0.19108617 ;
	setAttr ".pt[4641]" -type "float3" -0.16805694 0 -0.22987679 ;
	setAttr ".pt[4642]" -type "float3" -0.15184003 0 -0.26300773 ;
	setAttr ".pt[4643]" -type "float3" -0.1185567 0 -0.28966147 ;
	setAttr ".pt[4644]" -type "float3" -0.084932745 0 -0.30918393 ;
	setAttr ".pt[4645]" -type "float3" -0.049416095 0 -0.32109258 ;
	setAttr ".pt[4646]" -type "float3" -0.010500912 0 -0.32509497 ;
	setAttr ".pt[4647]" -type "float3" 0.025363974 0 -0.32109258 ;
	setAttr ".pt[4648]" -type "float3" 0.068811342 0 -0.30918393 ;
	setAttr ".pt[4649]" -type "float3" 0.10914578 0 -0.28966147 ;
	setAttr ".pt[4650]" -type "float3" 0.16019051 0 -0.2630077 ;
	setAttr ".pt[4651]" -type "float3" 0.18987331 0 -0.22987646 ;
	setAttr ".pt[4652]" -type "float3" 0.21095477 0 -0.19108595 ;
	setAttr ".pt[4653]" -type "float3" 0.23135048 0 -0.14758983 ;
	setAttr ".pt[4654]" -type "float3" 0.24628825 0 -0.10045983 ;
	setAttr ".pt[4655]" -type "float3" 0.25540146 0 -0.050855938 ;
	setAttr ".pt[4656]" -type "float3" 0.25846377 0 -7.2184001e-08 ;
	setAttr ".pt[4657]" -type "float3" 0.25540069 0 0.050856125 ;
	setAttr ".pt[4658]" -type "float3" 0.24628845 0 0.10045995 ;
	setAttr ".pt[4659]" -type "float3" 0.23135023 0 0.14758991 ;
	setAttr ".pt[4660]" -type "float3" 0.21095464 0 0.19108616 ;
	setAttr ".pt[4661]" -type "float3" 0.18987305 0 0.22987677 ;
	setAttr ".pt[4662]" -type "float3" 0.16019039 0 0.26300761 ;
	setAttr ".pt[4663]" -type "float3" 0.10914548 0 0.2896615 ;
	setAttr ".pt[4664]" -type "float3" 0.068811111 0 0.30918372 ;
	setAttr ".pt[4665]" -type "float3" 0.025364004 0 0.32109255 ;
	setAttr ".pt[4666]" -type "float3" -0.010500981 0 0.32509497 ;
	setAttr ".pt[4667]" -type "float3" -0.049416143 0 0.32109255 ;
	setAttr ".pt[4668]" -type "float3" -0.084932804 0 0.30918372 ;
	setAttr ".pt[4669]" -type "float3" -0.1185567 0 0.2896615 ;
	setAttr ".pt[4670]" -type "float3" -0.15183996 0 0.2630077 ;
	setAttr ".pt[4671]" -type "float3" -0.16805698 0 0.22987695 ;
	setAttr ".pt[4672]" -type "float3" -0.19340862 0 0.19108616 ;
	setAttr ".pt[4673]" -type "float3" -0.22173493 0 0.14758991 ;
	setAttr ".pt[4674]" -type "float3" -0.236673 0 0.10045987 ;
	setAttr ".pt[4675]" -type "float3" -0.24578577 0 0.050855998 ;
	setAttr ".pt[4676]" -type "float3" -0.24884821 0 5.783723e-08 ;
	setAttr ".pt[4677]" -type "float3" -0.24578577 0 -0.050856158 ;
	setAttr ".pt[4678]" -type "float3" -0.236673 0 -0.10046 ;
	setAttr ".pt[4679]" -type "float3" -0.22173493 0 -0.14759028 ;
	setAttr ".pt[4680]" -type "float3" -0.32244134 0 -0.036774334 ;
	setAttr ".pt[4681]" -type "float3" -0.31075937 0 -0.072643273 ;
	setAttr ".pt[4682]" -type "float3" -0.2825695 0 -0.10672309 ;
	setAttr ".pt[4683]" -type "float3" -0.25524992 0 -0.13817555 ;
	setAttr ".pt[4684]" -type "float3" -0.22275221 0 -0.16622551 ;
	setAttr ".pt[4685]" -type "float3" -0.17883424 0 -0.19018239 ;
	setAttr ".pt[4686]" -type "float3" -0.13616866 0 -0.20945671 ;
	setAttr ".pt[4687]" -type "float3" -0.089938238 0 -0.22357291 ;
	setAttr ".pt[4688]" -type "float3" -0.048322786 0 -0.23218451 ;
	setAttr ".pt[4689]" -type "float3" 0.0015623688 0 -0.2350781 ;
	setAttr ".pt[4690]" -type "float3" 0.051447473 0 -0.23218451 ;
	setAttr ".pt[4691]" -type "float3" 0.10010436 0 -0.22357291 ;
	setAttr ".pt[4692]" -type "float3" 0.14633466 0 -0.20945667 ;
	setAttr ".pt[4693]" -type "float3" 0.18900035 0 -0.19018248 ;
	setAttr ".pt[4694]" -type "float3" 0.2270505 0 -0.16622546 ;
	setAttr ".pt[4695]" -type "float3" 0.25954822 0 -0.13817531 ;
	setAttr ".pt[4696]" -type "float3" 0.28569347 0 -0.10672314 ;
	setAttr ".pt[4697]" -type "float3" 0.30484298 0 -0.07264325 ;
	setAttr ".pt[4698]" -type "float3" 0.31652486 0 -0.036774274 ;
	setAttr ".pt[4699]" -type "float3" 0.32045048 0 3.2852284e-08 ;
	setAttr ".pt[4700]" -type "float3" 0.31652418 0 0.036774438 ;
	setAttr ".pt[4701]" -type "float3" 0.30484277 0 0.072643213 ;
	setAttr ".pt[4702]" -type "float3" 0.28569397 0 0.10672314 ;
	setAttr ".pt[4703]" -type "float3" 0.25954816 0 0.13817552 ;
	setAttr ".pt[4704]" -type "float3" 0.22705053 0 0.16622549 ;
	setAttr ".pt[4705]" -type "float3" 0.18899995 0 0.19018237 ;
	setAttr ".pt[4706]" -type "float3" 0.14633463 0 0.20945668 ;
	setAttr ".pt[4707]" -type "float3" 0.10010415 0 0.22357255 ;
	setAttr ".pt[4708]" -type "float3" 0.051447406 0 0.23218451 ;
	setAttr ".pt[4709]" -type "float3" 0.0015622667 0 0.23507808 ;
	setAttr ".pt[4710]" -type "float3" -0.048322856 0 0.23218428 ;
	setAttr ".pt[4711]" -type "float3" -0.089938439 0 0.22357255 ;
	setAttr ".pt[4712]" -type "float3" -0.13616872 0 0.20945668 ;
	setAttr ".pt[4713]" -type "float3" -0.17883441 0 0.19018224 ;
	setAttr ".pt[4714]" -type "float3" -0.22275214 0 0.16622546 ;
	setAttr ".pt[4715]" -type "float3" -0.25525013 0 0.13817544 ;
	setAttr ".pt[4716]" -type "float3" -0.2825695 0 0.10672306 ;
	setAttr ".pt[4717]" -type "float3" -0.31075937 0 0.07264331 ;
	setAttr ".pt[4718]" -type "float3" -0.32244134 0 0.036774348 ;
	setAttr ".pt[4719]" -type "float3" -0.32636738 0 -5.6800094e-09 ;
	setAttr ".pt[4720]" -type "float3" -0.27929884 0 0.093017399 ;
	setAttr ".pt[4721]" -type "float3" -0.29032505 0 0.047088344 ;
	setAttr ".pt[4722]" -type "float3" -0.29403102 0 -3.9115831e-09 ;
	setAttr ".pt[4723]" -type "float3" -0.29032505 0 -0.047088478 ;
	setAttr ".pt[4724]" -type "float3" -0.27929887 0 -0.093017541 ;
	setAttr ".pt[4725]" -type "float3" -0.26122308 0 -0.13665599 ;
	setAttr ".pt[4726]" -type "float3" -0.23654327 0 -0.17692915 ;
	setAttr ".pt[4727]" -type "float3" -0.20586772 0 -0.21284689 ;
	setAttr ".pt[4728]" -type "float3" -0.16995057 0 -0.24352194 ;
	setAttr ".pt[4729]" -type "float3" -0.12967667 0 -0.26820177 ;
	setAttr ".pt[4730]" -type "float3" -0.10259723 0 -0.28627777 ;
	setAttr ".pt[4731]" -type "float3" -0.05666839 0 -0.29730436 ;
	setAttr ".pt[4732]" -type "float3" -0.0095799491 0 -0.30101001 ;
	setAttr ".pt[4733]" -type "float3" 0.03750838 0 -0.29730436 ;
	setAttr ".pt[4734]" -type "float3" 0.083437428 0 -0.28627801 ;
	setAttr ".pt[4735]" -type "float3" 0.12707607 0 -0.26820174 ;
	setAttr ".pt[4736]" -type "float3" 0.16734923 0 -0.24352203 ;
	setAttr ".pt[4737]" -type "float3" 0.20326674 0 -0.21284685 ;
	setAttr ".pt[4738]" -type "float3" 0.23394248 0 -0.17692895 ;
	setAttr ".pt[4739]" -type "float3" 0.25862205 0 -0.13665576 ;
	setAttr ".pt[4740]" -type "float3" 0.27669775 0 -0.093017265 ;
	setAttr ".pt[4741]" -type "float3" 0.28772461 0 -0.047088243 ;
	setAttr ".pt[4742]" -type "float3" 0.29143012 0 1.0653066e-07 ;
	setAttr ".pt[4743]" -type "float3" 0.28772444 0 0.047088504 ;
	setAttr ".pt[4744]" -type "float3" 0.27669767 0 0.09301731 ;
	setAttr ".pt[4745]" -type "float3" 0.25862238 0 0.13665605 ;
	setAttr ".pt[4746]" -type "float3" 0.23394231 0 0.17692913 ;
	setAttr ".pt[4747]" -type "float3" 0.20326595 0 0.21284644 ;
	setAttr ".pt[4748]" -type "float3" 0.16734906 0 0.24352208 ;
	setAttr ".pt[4749]" -type "float3" 0.12707584 0 0.26820174 ;
	setAttr ".pt[4750]" -type "float3" 0.083437167 0 0.28627753 ;
	setAttr ".pt[4751]" -type "float3" 0.037508357 0 0.29730412 ;
	setAttr ".pt[4752]" -type "float3" -0.0095800487 0 0.30101025 ;
	setAttr ".pt[4753]" -type "float3" -0.056668427 0 0.29730412 ;
	setAttr ".pt[4754]" -type "float3" -0.10259743 0 0.28627753 ;
	setAttr ".pt[4755]" -type "float3" -0.12967683 0 0.26820174 ;
	setAttr ".pt[4756]" -type "float3" -0.1699505 0 0.243522 ;
	setAttr ".pt[4757]" -type "float3" -0.2058672 0 0.21284688 ;
	setAttr ".pt[4758]" -type "float3" -0.2365434 0 0.17692922 ;
	setAttr ".pt[4759]" -type "float3" -0.26122296 0 0.13665596 ;
	setAttr ".pt[4760]" -type "float3" -0.22305654 0 0.11944468 ;
	setAttr ".pt[4761]" -type "float3" -0.2376737 0 0.081302397 ;
	setAttr ".pt[4762]" -type "float3" -0.24659064 0 0.04115798 ;
	setAttr ".pt[4763]" -type "float3" -0.24958722 0 -6.603122e-08 ;
	setAttr ".pt[4764]" -type "float3" -0.24659064 0 -0.041157845 ;
	setAttr ".pt[4765]" -type "float3" -0.2376737 0 -0.081302479 ;
	setAttr ".pt[4766]" -type "float3" -0.22305654 0 -0.11944494 ;
	setAttr ".pt[4767]" -type "float3" -0.20309912 0 -0.15464616 ;
	setAttr ".pt[4768]" -type "float3" -0.17829281 0 -0.18603988 ;
	setAttr ".pt[4769]" -type "float3" -0.14924835 0 -0.21285234 ;
	setAttr ".pt[4770]" -type "float3" -0.11668061 0 -0.23442362 ;
	setAttr ".pt[4771]" -type "float3" -0.081392214 0 -0.25022301 ;
	setAttr ".pt[4772]" -type "float3" -0.044251408 0 -0.25986084 ;
	setAttr ".pt[4773]" -type "float3" -0.0061729881 0 -0.26309994 ;
	setAttr ".pt[4774]" -type "float3" 0.031905383 0 -0.25986084 ;
	setAttr ".pt[4775]" -type "float3" 0.06904617 0 -0.25022301 ;
	setAttr ".pt[4776]" -type "float3" 0.10433496 0 -0.23442349 ;
	setAttr ".pt[4777]" -type "float3" 0.13690217 0 -0.21285252 ;
	setAttr ".pt[4778]" -type "float3" 0.16594723 0 -0.18603958 ;
	setAttr ".pt[4779]" -type "float3" 0.19075325 0 -0.15464608 ;
	setAttr ".pt[4780]" -type "float3" 0.21071094 0 -0.11944479 ;
	setAttr ".pt[4781]" -type "float3" 0.22532807 0 -0.08130233 ;
	setAttr ".pt[4782]" -type "float3" 0.23424417 0 -0.041157775 ;
	setAttr ".pt[4783]" -type "float3" 0.23724096 0 3.8196887e-08 ;
	setAttr ".pt[4784]" -type "float3" 0.23424436 0 0.04115792 ;
	setAttr ".pt[4785]" -type "float3" 0.22532718 0 0.081302367 ;
	setAttr ".pt[4786]" -type "float3" 0.21071064 0 0.11944488 ;
	setAttr ".pt[4787]" -type "float3" 0.1907531 0 0.15464614 ;
	setAttr ".pt[4788]" -type "float3" 0.16594681 0 0.18604012 ;
	setAttr ".pt[4789]" -type "float3" 0.13690244 0 0.2128527 ;
	setAttr ".pt[4790]" -type "float3" 0.10433488 0 0.23442374 ;
	setAttr ".pt[4791]" -type "float3" 0.069046117 0 0.25022277 ;
	setAttr ".pt[4792]" -type "float3" 0.031905334 0 0.25986072 ;
	setAttr ".pt[4793]" -type "float3" -0.0061730682 0 0.26309991 ;
	setAttr ".pt[4794]" -type "float3" -0.044251453 0 0.25986072 ;
	setAttr ".pt[4795]" -type "float3" -0.081392281 0 0.25022277 ;
	setAttr ".pt[4796]" -type "float3" -0.11668072 0 0.23442361 ;
	setAttr ".pt[4797]" -type "float3" -0.14924839 0 0.21285217 ;
	setAttr ".pt[4798]" -type "float3" -0.17829297 0 0.1860396 ;
	setAttr ".pt[4799]" -type "float3" -0.20309912 0 0.15464625 ;
	setAttr ".pt[4800]" -type "float3" -0.17660169 0 0.18153232 ;
	setAttr ".pt[4801]" -type "float3" -0.19883569 0 0.15089935 ;
	setAttr ".pt[4802]" -type "float3" -0.21672381 0 0.11655074 ;
	setAttr ".pt[4803]" -type "float3" -0.22982581 0 0.079332486 ;
	setAttr ".pt[4804]" -type "float3" -0.25510567 0 0.040160716 ;
	setAttr ".pt[4805]" -type "float3" -0.25779152 0 -5.0993973e-09 ;
	setAttr ".pt[4806]" -type "float3" -0.25510567 0 -0.040160727 ;
	setAttr ".pt[4807]" -type "float3" -0.22982581 0 -0.079332381 ;
	setAttr ".pt[4808]" -type "float3" -0.21672381 0 -0.11655076 ;
	setAttr ".pt[4809]" -type "float3" -0.19883578 0 -0.15089963 ;
	setAttr ".pt[4810]" -type "float3" -0.17660153 0 -0.18153225 ;
	setAttr ".pt[4811]" -type "float3" -0.12285613 0 -0.20769532 ;
	setAttr ".pt[4812]" -type "float3" -0.093665399 0 -0.22874407 ;
	setAttr ".pt[4813]" -type "float3" -0.062035888 0 -0.24416021 ;
	setAttr ".pt[4814]" -type "float3" -0.028746219 0 -0.25356451 ;
	setAttr ".pt[4815]" -type "float3" 0.0053838114 0 -0.25672522 ;
	setAttr ".pt[4816]" -type "float3" 0.039513852 0 -0.25356439 ;
	setAttr ".pt[4817]" -type "float3" 0.072803557 0 -0.24416021 ;
	setAttr ".pt[4818]" -type "float3" 0.1044331 0 -0.22874418 ;
	setAttr ".pt[4819]" -type "float3" 0.13362366 0 -0.20769502 ;
	setAttr ".pt[4820]" -type "float3" 0.15965648 0 -0.18153194 ;
	setAttr ".pt[4821]" -type "float3" 0.18189093 0 -0.15089931 ;
	setAttr ".pt[4822]" -type "float3" 0.19977859 0 -0.11655086 ;
	setAttr ".pt[4823]" -type "float3" 0.21287987 0 -0.079332367 ;
	setAttr ".pt[4824]" -type "float3" 0.22087234 0 -0.040160574 ;
	setAttr ".pt[4825]" -type "float3" 0.22355889 0 3.6981181e-08 ;
	setAttr ".pt[4826]" -type "float3" 0.22087207 0 0.040160809 ;
	setAttr ".pt[4827]" -type "float3" 0.21287975 0 0.079332516 ;
	setAttr ".pt[4828]" -type "float3" 0.19977869 0 0.11655081 ;
	setAttr ".pt[4829]" -type "float3" 0.18189055 0 0.15089962 ;
	setAttr ".pt[4830]" -type "float3" 0.15965647 0 0.18153237 ;
	setAttr ".pt[4831]" -type "float3" 0.1336235 0 0.20769532 ;
	setAttr ".pt[4832]" -type "float3" 0.10443287 0 0.22874419 ;
	setAttr ".pt[4833]" -type "float3" 0.072803393 0 0.24416021 ;
	setAttr ".pt[4834]" -type "float3" 0.039513774 0 0.25356427 ;
	setAttr ".pt[4835]" -type "float3" 0.0053837425 0 0.25672483 ;
	setAttr ".pt[4836]" -type "float3" -0.028746242 0 0.25356427 ;
	setAttr ".pt[4837]" -type "float3" -0.062035918 0 0.24416021 ;
	setAttr ".pt[4838]" -type "float3" -0.093665451 0 0.22874382 ;
	setAttr ".pt[4839]" -type "float3" -0.12285624 0 0.20769492 ;
	setAttr ".pt[4840]" -type "float3" -0.17729141 0 0.25141537 ;
	setAttr ".pt[4841]" -type "float3" -0.2198794 0 0.21974552 ;
	setAttr ".pt[4842]" -type "float3" -0.25625366 0 0.1826646 ;
	setAttr ".pt[4843]" -type "float3" -0.28551665 0 0.14108525 ;
	setAttr ".pt[4844]" -type "float3" -0.30695021 0 0.096032202 ;
	setAttr ".pt[4845]" -type "float3" -0.32002458 0 0.048614688 ;
	setAttr ".pt[4846]" -type "float3" -0.32441908 0 2.6901517e-08 ;
	setAttr ".pt[4847]" -type "float3" -0.32002458 0 -0.04861464 ;
	setAttr ".pt[4848]" -type "float3" -0.30695021 0 -0.096032225 ;
	setAttr ".pt[4849]" -type "float3" -0.28551665 0 -0.14108539 ;
	setAttr ".pt[4850]" -type "float3" -0.25625366 0 -0.1826645 ;
	setAttr ".pt[4851]" -type "float3" -0.2198801 0 -0.21974547 ;
	setAttr ".pt[4852]" -type "float3" -0.17729124 0 -0.25141516 ;
	setAttr ".pt[4853]" -type "float3" -0.12953751 0 -0.27689564 ;
	setAttr ".pt[4854]" -type "float3" -0.077793442 0 -0.29555747 ;
	setAttr ".pt[4855]" -type "float3" -0.055421829 0 -0.3069407 ;
	setAttr ".pt[4856]" -type "float3" 0.0004127314 0 -0.31076699 ;
	setAttr ".pt[4857]" -type "float3" 0.056247372 0 -0.3069407 ;
	setAttr ".pt[4858]" -type "float3" 0.11070735 0 -0.29555747 ;
	setAttr ".pt[4859]" -type "float3" 0.14640705 0 -0.27689514 ;
	setAttr ".pt[4860]" -type "float3" 0.19416107 0 -0.25141573 ;
	setAttr ".pt[4861]" -type "float3" 0.23674934 0 -0.21974528 ;
	setAttr ".pt[4862]" -type "float3" 0.27312312 0 -0.18266393 ;
	setAttr ".pt[4863]" -type "float3" 0.30238691 0 -0.14108504 ;
	setAttr ".pt[4864]" -type "float3" 0.32381999 0 -0.09603212 ;
	setAttr ".pt[4865]" -type "float3" 0.33689463 0 -0.048614491 ;
	setAttr ".pt[4866]" -type "float3" 0.34128836 0 1.083916e-07 ;
	setAttr ".pt[4867]" -type "float3" 0.33689362 0 0.048614699 ;
	setAttr ".pt[4868]" -type "float3" 0.32381999 0 0.096032232 ;
	setAttr ".pt[4869]" -type "float3" 0.30238643 0 0.14108515 ;
	setAttr ".pt[4870]" -type "float3" 0.27312255 0 0.18266448 ;
	setAttr ".pt[4871]" -type "float3" 0.2367491 0 0.21974544 ;
	setAttr ".pt[4872]" -type "float3" 0.19416089 0 0.25141528 ;
	setAttr ".pt[4873]" -type "float3" 0.14640674 0 0.27689552 ;
	setAttr ".pt[4874]" -type "float3" 0.11070701 0 0.29555696 ;
	setAttr ".pt[4875]" -type "float3" 0.056247145 0 0.3069407 ;
	setAttr ".pt[4876]" -type "float3" 0.00041261589 0 0.31076697 ;
	setAttr ".pt[4877]" -type "float3" -0.055421919 0 0.3069407 ;
	setAttr ".pt[4878]" -type "float3" -0.077793412 0 0.29555696 ;
	setAttr ".pt[4879]" -type "float3" -0.1295374 0 0.27689564 ;
	setAttr ".pt[4880]" -type "float3" -0.21023601 0 0.20654353 ;
	setAttr ".pt[4881]" -type "float3" -0.25096944 0 0.18052578 ;
	setAttr ".pt[4882]" -type "float3" -0.28575903 0 0.15006287 ;
	setAttr ".pt[4883]" -type "float3" -0.31374857 0 0.11590464 ;
	setAttr ".pt[4884]" -type "float3" -0.33424875 0 0.078892663 ;
	setAttr ".pt[4885]" -type "float3" -0.34675324 0 0.039938033 ;
	setAttr ".pt[4886]" -type "float3" -0.35095635 0 -6.6240382e-08 ;
	setAttr ".pt[4887]" -type "float3" -0.34675324 0 -0.039937925 ;
	setAttr ".pt[4888]" -type "float3" -0.33424875 0 -0.078892626 ;
	setAttr ".pt[4889]" -type "float3" -0.31374857 0 -0.11590467 ;
	setAttr ".pt[4890]" -type "float3" -0.28575903 0 -0.1500629 ;
	setAttr ".pt[4891]" -type "float3" -0.25096986 0 -0.18052571 ;
	setAttr ".pt[4892]" -type "float3" -0.21023606 0 -0.20654348 ;
	setAttr ".pt[4893]" -type "float3" -0.16456179 0 -0.2274753 ;
	setAttr ".pt[4894]" -type "float3" -0.11507093 0 -0.24280667 ;
	setAttr ".pt[4895]" -type "float3" -0.062983088 0 -0.25215873 ;
	setAttr ".pt[4896]" -type "float3" -0.0095799416 0 -0.25530267 ;
	setAttr ".pt[4897]" -type "float3" 0.043823168 0 -0.25215873 ;
	setAttr ".pt[4898]" -type "float3" 0.095911197 0 -0.24280667 ;
	setAttr ".pt[4899]" -type "float3" 0.14540175 0 -0.22747551 ;
	setAttr ".pt[4900]" -type "float3" 0.19107646 0 -0.20654365 ;
	setAttr ".pt[4901]" -type "float3" 0.23180988 0 -0.18052565 ;
	setAttr ".pt[4902]" -type "float3" 0.26659951 0 -0.15006272 ;
	setAttr ".pt[4903]" -type "float3" 0.29458866 0 -0.11590475 ;
	setAttr ".pt[4904]" -type "float3" 0.31508833 0 -0.078892596 ;
	setAttr ".pt[4905]" -type "float3" 0.32759401 0 -0.039937951 ;
	setAttr ".pt[4906]" -type "float3" 0.33179599 0 -2.4393099e-08 ;
	setAttr ".pt[4907]" -type "float3" 0.32759309 0 0.039938029 ;
	setAttr ".pt[4908]" -type "float3" 0.3150886 0 0.078892685 ;
	setAttr ".pt[4909]" -type "float3" 0.29458824 0 0.11590496 ;
	setAttr ".pt[4910]" -type "float3" 0.26659918 0 0.15006278 ;
	setAttr ".pt[4911]" -type "float3" 0.23180932 0 0.18052559 ;
	setAttr ".pt[4912]" -type "float3" 0.19107589 0 0.20654348 ;
	setAttr ".pt[4913]" -type "float3" 0.14540143 0 0.22747566 ;
	setAttr ".pt[4914]" -type "float3" 0.095911026 0 0.24280643 ;
	setAttr ".pt[4915]" -type "float3" 0.043823004 0 0.25215873 ;
	setAttr ".pt[4916]" -type "float3" -0.009580059 0 0.25530264 ;
	setAttr ".pt[4917]" -type "float3" -0.062983237 0 0.25215873 ;
	setAttr ".pt[4918]" -type "float3" -0.11507107 0 0.24280643 ;
	setAttr ".pt[4919]" -type "float3" -0.16456164 0 0.22747527 ;
	setAttr ".pt[4920]" -type "float3" -0.13373102 0 0.24365979 ;
	setAttr ".pt[4921]" -type "float3" -0.1703193 0 0.22123821 ;
	setAttr ".pt[4922]" -type "float3" -0.2029493 0 0.19336949 ;
	setAttr ".pt[4923]" -type "float3" -0.23081863 0 0.16073938 ;
	setAttr ".pt[4924]" -type "float3" -0.25323981 0 0.12415098 ;
	setAttr ".pt[4925]" -type "float3" -0.26966119 0 0.084505633 ;
	setAttr ".pt[4926]" -type "float3" -0.27967924 0 0.042779468 ;
	setAttr ".pt[4927]" -type "float3" -0.28304556 0 -4.650373e-09 ;
	setAttr ".pt[4928]" -type "float3" -0.27967924 0 -0.042779449 ;
	setAttr ".pt[4929]" -type "float3" -0.26966119 0 -0.084505588 ;
	setAttr ".pt[4930]" -type "float3" -0.25323981 0 -0.12415101 ;
	setAttr ".pt[4931]" -type "float3" -0.23081847 0 -0.16073942 ;
	setAttr ".pt[4932]" -type "float3" -0.20294951 0 -0.19336954 ;
	setAttr ".pt[4933]" -type "float3" -0.17031926 0 -0.22123815 ;
	setAttr ".pt[4934]" -type "float3" -0.13373096 0 -0.24365981 ;
	setAttr ".pt[4935]" -type "float3" -0.094085552 0 -0.26008144 ;
	setAttr ".pt[4936]" -type "float3" -0.05235941 0 -0.27009875 ;
	setAttr ".pt[4937]" -type "float3" -0.0095799686 0 -0.27346569 ;
	setAttr ".pt[4938]" -type "float3" 0.033199538 0 -0.27009875 ;
	setAttr ".pt[4939]" -type "float3" 0.074925639 0 -0.26008144 ;
	setAttr ".pt[4940]" -type "float3" 0.11457089 0 -0.24365944 ;
	setAttr ".pt[4941]" -type "float3" 0.15115923 0 -0.22123784 ;
	setAttr ".pt[4942]" -type "float3" 0.1837896 0 -0.1933696 ;
	setAttr ".pt[4943]" -type "float3" 0.21165857 0 -0.16073921 ;
	setAttr ".pt[4944]" -type "float3" 0.23407964 0 -0.1241511 ;
	setAttr ".pt[4945]" -type "float3" 0.25050151 0 -0.084505498 ;
	setAttr ".pt[4946]" -type "float3" 0.26051906 0 -0.042779438 ;
	setAttr ".pt[4947]" -type "float3" 0.2638852 0 4.0174168e-08 ;
	setAttr ".pt[4948]" -type "float3" 0.26051849 0 0.042779475 ;
	setAttr ".pt[4949]" -type "float3" 0.25050122 0 0.084505714 ;
	setAttr ".pt[4950]" -type "float3" 0.23407935 0 0.124151 ;
	setAttr ".pt[4951]" -type "float3" 0.2116584 0 0.16073941 ;
	setAttr ".pt[4952]" -type "float3" 0.18378943 0 0.19336978 ;
	setAttr ".pt[4953]" -type "float3" 0.15115909 0 0.22123839 ;
	setAttr ".pt[4954]" -type "float3" 0.11457068 0 0.24366017 ;
	setAttr ".pt[4955]" -type "float3" 0.07492546 0 0.26008132 ;
	setAttr ".pt[4956]" -type "float3" 0.033199418 0 0.27009851 ;
	setAttr ".pt[4957]" -type "float3" -0.0095800571 0 0.27346566 ;
	setAttr ".pt[4958]" -type "float3" -0.052359466 0 0.27009851 ;
	setAttr ".pt[4959]" -type "float3" -0.094085619 0 0.26008132 ;
	setAttr ".pt[4960]" -type "float3" -0.099405788 0 0.29029647 ;
	setAttr ".pt[4961]" -type "float3" -0.14365694 0 0.27196625 ;
	setAttr ".pt[4962]" -type "float3" -0.18449581 0 0.24693988 ;
	setAttr ".pt[4963]" -type "float3" -0.22091717 0 0.21583392 ;
	setAttr ".pt[4964]" -type "float3" -0.25202391 0 0.1794129 ;
	setAttr ".pt[4965]" -type "float3" -0.27704975 0 0.13857354 ;
	setAttr ".pt[4966]" -type "float3" -0.29537851 0 0.094322756 ;
	setAttr ".pt[4967]" -type "float3" -0.30656058 0 0.047749251 ;
	setAttr ".pt[4968]" -type "float3" -0.31031802 0 -3.7982488e-09 ;
	setAttr ".pt[4969]" -type "float3" -0.30656058 0 -0.047749296 ;
	setAttr ".pt[4970]" -type "float3" -0.29537851 0 -0.094322845 ;
	setAttr ".pt[4971]" -type "float3" -0.27704975 0 -0.13857369 ;
	setAttr ".pt[4972]" -type "float3" -0.25202352 0 -0.17941293 ;
	setAttr ".pt[4973]" -type "float3" -0.22091714 0 -0.21583395 ;
	setAttr ".pt[4974]" -type "float3" -0.18449575 0 -0.24694055 ;
	setAttr ".pt[4975]" -type "float3" -0.14365686 0 -0.27196628 ;
	setAttr ".pt[4976]" -type "float3" -0.09940578 0 -0.29029575 ;
	setAttr ".pt[4977]" -type "float3" -0.052832194 0 -0.30147731 ;
	setAttr ".pt[4978]" -type "float3" -0.0050829258 0 -0.30523551 ;
	setAttr ".pt[4979]" -type "float3" 0.042666424 0 -0.30147731 ;
	setAttr ".pt[4980]" -type "float3" 0.089239925 0 -0.29029575 ;
	setAttr ".pt[4981]" -type "float3" 0.13349108 0 -0.27196625 ;
	setAttr ".pt[4982]" -type "float3" 0.17433001 0 -0.24694027 ;
	setAttr ".pt[4983]" -type "float3" 0.2107514 0 -0.21583399 ;
	setAttr ".pt[4984]" -type "float3" 0.24185762 0 -0.17941284 ;
	setAttr ".pt[4985]" -type "float3" 0.26688373 0 -0.13857388 ;
	setAttr ".pt[4986]" -type "float3" 0.28521287 0 -0.094322853 ;
	setAttr ".pt[4987]" -type "float3" 0.29639417 0 -0.04774924 ;
	setAttr ".pt[4988]" -type "float3" 0.30015239 0 -1.4869126e-08 ;
	setAttr ".pt[4989]" -type "float3" 0.29639423 0 0.047749296 ;
	setAttr ".pt[4990]" -type "float3" 0.28521267 0 0.094322912 ;
	setAttr ".pt[4991]" -type "float3" 0.26688364 0 0.13857393 ;
	setAttr ".pt[4992]" -type "float3" 0.24185733 0 0.17941292 ;
	setAttr ".pt[4993]" -type "float3" 0.21075061 0 0.21583383 ;
	setAttr ".pt[4994]" -type "float3" 0.17432998 0 0.24694067 ;
	setAttr ".pt[4995]" -type "float3" 0.1334908 0 0.27196676 ;
	setAttr ".pt[4996]" -type "float3" 0.089239895 0 0.29029647 ;
	setAttr ".pt[4997]" -type "float3" 0.042666309 0 0.30147755 ;
	setAttr ".pt[4998]" -type "float3" -0.0050830315 0 0.30523548 ;
	setAttr ".pt[4999]" -type "float3" -0.052832313 0 0.30147755 ;
	setAttr ".pt[5000]" -type "float3" -0.032907162 0 0.24442294 ;
	setAttr ".pt[5001]" -type "float3" -0.068633571 0 0.23535791 ;
	setAttr ".pt[5002]" -type "float3" -0.10257849 0 0.22049767 ;
	setAttr ".pt[5003]" -type "float3" -0.13390574 0 0.20020737 ;
	setAttr ".pt[5004]" -type "float3" -0.16184399 0 0.17498778 ;
	setAttr ".pt[5005]" -type "float3" -0.20770793 0 0.14545941 ;
	setAttr ".pt[5006]" -type "float3" -0.22690545 0 0.11234906 ;
	setAttr ".pt[5007]" -type "float3" -0.2409651 0 0.076472595 ;
	setAttr ".pt[5008]" -type "float3" -0.24954271 0 0.038712848 ;
	setAttr ".pt[5009]" -type "float3" -0.25242496 0 -6.6450454e-08 ;
	setAttr ".pt[5010]" -type "float3" -0.24954271 0 -0.038712896 ;
	setAttr ".pt[5011]" -type "float3" -0.2409651 0 -0.076472484 ;
	setAttr ".pt[5012]" -type "float3" -0.22690545 0 -0.11234909 ;
	setAttr ".pt[5013]" -type "float3" -0.20770742 0 -0.1454592 ;
	setAttr ".pt[5014]" -type "float3" -0.16184384 0 -0.17498782 ;
	setAttr ".pt[5015]" -type "float3" -0.13390557 0 -0.20020717 ;
	setAttr ".pt[5016]" -type "float3" -0.10257817 0 -0.22049744 ;
	setAttr ".pt[5017]" -type "float3" -0.068633452 0 -0.23535815 ;
	setAttr ".pt[5018]" -type "float3" -0.032907087 0 -0.24442294 ;
	setAttr ".pt[5019]" -type "float3" 0.0037211832 0 -0.24746971 ;
	setAttr ".pt[5020]" -type "float3" 0.040349469 0 -0.24442294 ;
	setAttr ".pt[5021]" -type "float3" 0.076075792 0 -0.23535779 ;
	setAttr ".pt[5022]" -type "float3" 0.11002062 0 -0.22049743 ;
	setAttr ".pt[5023]" -type "float3" 0.14134768 0 -0.20020737 ;
	setAttr ".pt[5024]" -type "float3" 0.16928643 0 -0.17498776 ;
	setAttr ".pt[5025]" -type "float3" 0.19314802 0 -0.14545913 ;
	setAttr ".pt[5026]" -type "float3" 0.21234491 0 -0.11234899 ;
	setAttr ".pt[5027]" -type "float3" 0.22640602 0 -0.076472349 ;
	setAttr ".pt[5028]" -type "float3" 0.23498283 0 -0.03871268 ;
	setAttr ".pt[5029]" -type "float3" 0.23786548 0 9.6318658e-08 ;
	setAttr ".pt[5030]" -type "float3" 0.23498289 0 0.038712908 ;
	setAttr ".pt[5031]" -type "float3" 0.22640567 0 0.076472618 ;
	setAttr ".pt[5032]" -type "float3" 0.21234515 0 0.11234919 ;
	setAttr ".pt[5033]" -type "float3" 0.19314802 0 0.14545918 ;
	setAttr ".pt[5034]" -type "float3" 0.16928643 0 0.17498769 ;
	setAttr ".pt[5035]" -type "float3" 0.14134769 0 0.20020753 ;
	setAttr ".pt[5036]" -type "float3" 0.11002056 0 0.22049744 ;
	setAttr ".pt[5037]" -type "float3" 0.076075613 0 0.23535791 ;
	setAttr ".pt[5038]" -type "float3" 0.040349316 0 0.24442294 ;
	setAttr ".pt[5039]" -type "float3" 0.0037211045 0 0.24746968 ;
	setAttr ".pt[5040]" -type "float3" 0.0037211045 0 0.23854554 ;
	setAttr ".pt[5041]" -type "float3" -0.030761136 0 0.23560883 ;
	setAttr ".pt[5042]" -type "float3" -0.064394303 0 0.22687057 ;
	setAttr ".pt[5043]" -type "float3" -0.096350357 0 0.2125461 ;
	setAttr ".pt[5044]" -type "float3" -0.12584209 0 0.19298744 ;
	setAttr ".pt[5045]" -type "float3" -0.15214366 0 0.16867742 ;
	setAttr ".pt[5046]" -type "float3" -0.17460735 0 0.14021364 ;
	setAttr ".pt[5047]" -type "float3" -0.19268019 0 0.10829771 ;
	setAttr ".pt[5048]" -type "float3" -0.20591652 0 0.073714554 ;
	setAttr ".pt[5049]" -type "float3" -0.21399139 0 0.037316769 ;
	setAttr ".pt[5050]" -type "float3" -0.21670483 0 5.5515798e-08 ;
	setAttr ".pt[5051]" -type "float3" -0.21399139 0 -0.037316818 ;
	setAttr ".pt[5052]" -type "float3" -0.20591652 0 -0.073714949 ;
	setAttr ".pt[5053]" -type "float3" -0.19268019 0 -0.10829761 ;
	setAttr ".pt[5054]" -type "float3" -0.1746072 0 -0.1402138 ;
	setAttr ".pt[5055]" -type "float3" -0.15214339 0 -0.1686776 ;
	setAttr ".pt[5056]" -type "float3" -0.12584193 0 -0.19298787 ;
	setAttr ".pt[5057]" -type "float3" -0.096350297 0 -0.21254611 ;
	setAttr ".pt[5058]" -type "float3" -0.064394236 0 -0.22687057 ;
	setAttr ".pt[5059]" -type "float3" -0.030761067 0 -0.23560916 ;
	setAttr ".pt[5060]" -type "float3" 0.0037211741 0 -0.23854584 ;
	setAttr ".pt[5061]" -type "float3" 0.038203422 0 -0.23560892 ;
	setAttr ".pt[5062]" -type "float3" 0.071836598 0 -0.22687057 ;
	setAttr ".pt[5063]" -type "float3" 0.10379267 0 -0.21254598 ;
	setAttr ".pt[5064]" -type "float3" 0.13328446 0 -0.19298759 ;
	setAttr ".pt[5065]" -type "float3" 0.159586 0 -0.16867766 ;
	setAttr ".pt[5066]" -type "float3" 0.18204953 0 -0.14021362 ;
	setAttr ".pt[5067]" -type "float3" 0.20012216 0 -0.10829765 ;
	setAttr ".pt[5068]" -type "float3" 0.21335924 0 -0.073714681 ;
	setAttr ".pt[5069]" -type "float3" 0.22143321 0 -0.037316754 ;
	setAttr ".pt[5070]" -type "float3" 0.22414701 0 -2.7589131e-08 ;
	setAttr ".pt[5071]" -type "float3" 0.22143357 0 0.037316766 ;
	setAttr ".pt[5072]" -type "float3" 0.21335861 0 0.073714703 ;
	setAttr ".pt[5073]" -type "float3" 0.20012213 0 0.10829765 ;
	setAttr ".pt[5074]" -type "float3" 0.18204941 0 0.14021367 ;
	setAttr ".pt[5075]" -type "float3" 0.15958598 0 0.16867723 ;
	setAttr ".pt[5076]" -type "float3" 0.1332844 0 0.19298749 ;
	setAttr ".pt[5077]" -type "float3" 0.10379243 0 0.2125461 ;
	setAttr ".pt[5078]" -type "float3" 0.071836434 0 0.22687057 ;
	setAttr ".pt[5079]" -type "float3" 0.038203306 0 0.2356088 ;
	setAttr ".pt[5080]" -type "float3" 0.29259065 0 0.036415163 ;
	setAttr ".pt[5081]" -type "float3" 0.28173947 0 0.071933694 ;
	setAttr ".pt[5082]" -type "float3" 0.26395082 0 0.10568087 ;
	setAttr ".pt[5083]" -type "float3" 0.23966366 0 0.13682595 ;
	setAttr ".pt[5084]" -type "float3" 0.20947576 0 0.16460182 ;
	setAttr ".pt[5085]" -type "float3" 0.17412975 0 0.18832444 ;
	setAttr ".pt[5086]" -type "float3" 0.13449691 0 0.20741016 ;
	setAttr ".pt[5087]" -type "float3" 0.091552377 0 0.22138858 ;
	setAttr ".pt[5088]" -type "float3" 0.046353962 0 0.22991629 ;
	setAttr ".pt[5089]" -type "float3" 1.4511385e-05 0 0.23278245 ;
	setAttr ".pt[5090]" -type "float3" -0.046324939 0 0.22991668 ;
	setAttr ".pt[5091]" -type "float3" -0.091523349 0 0.22138906 ;
	setAttr ".pt[5092]" -type "float3" -0.13446783 0 0.20740998 ;
	setAttr ".pt[5093]" -type "float3" -0.17410086 0 0.18832454 ;
	setAttr ".pt[5094]" -type "float3" -0.20944662 0 0.16460191 ;
	setAttr ".pt[5095]" -type "float3" -0.23963442 0 0.13682605 ;
	setAttr ".pt[5096]" -type "float3" -0.26392269 0 0.10568087 ;
	setAttr ".pt[5097]" -type "float3" -0.28171048 0 0.071933731 ;
	setAttr ".pt[5098]" -type "float3" -0.2925612 0 0.036415104 ;
	setAttr ".pt[5099]" -type "float3" -0.29620796 0 -5.7416067e-09 ;
	setAttr ".pt[5100]" -type "float3" -0.2925612 0 -0.036415152 ;
	setAttr ".pt[5101]" -type "float3" -0.28171048 0 -0.071933694 ;
	setAttr ".pt[5102]" -type "float3" -0.26392269 0 -0.1056807 ;
	setAttr ".pt[5103]" -type "float3" -0.23963438 0 -0.13682596 ;
	setAttr ".pt[5104]" -type "float3" -0.20944658 0 -0.16460194 ;
	setAttr ".pt[5105]" -type "float3" -0.17410102 0 -0.1883247 ;
	setAttr ".pt[5106]" -type "float3" -0.13446793 0 -0.20740989 ;
	setAttr ".pt[5107]" -type "float3" -0.091523342 0 -0.2213887 ;
	setAttr ".pt[5108]" -type "float3" -0.046324875 0 -0.22991593 ;
	setAttr ".pt[5109]" -type "float3" 1.4612488e-05 0 -0.23278245 ;
	setAttr ".pt[5110]" -type "float3" 0.046354078 0 -0.22991593 ;
	setAttr ".pt[5111]" -type "float3" 0.09155263 0 -0.2213887 ;
	setAttr ".pt[5112]" -type "float3" 0.13449696 0 -0.20740989 ;
	setAttr ".pt[5113]" -type "float3" 0.17413014 0 -0.18832454 ;
	setAttr ".pt[5114]" -type "float3" 0.20947607 0 -0.16460215 ;
	setAttr ".pt[5115]" -type "float3" 0.23966394 0 -0.13682567 ;
	setAttr ".pt[5116]" -type "float3" 0.26395124 0 -0.10568067 ;
	setAttr ".pt[5117]" -type "float3" 0.28173992 0 -0.07193362 ;
	setAttr ".pt[5118]" -type "float3" 0.29259086 0 -0.036415122 ;
	setAttr ".pt[5119]" -type "float3" 0.29623765 0 -2.8688408e-08 ;
	setAttr ".pt[5120]" -type "float3" 0.30934593 0 0.074478582 ;
	setAttr ".pt[5121]" -type "float3" 0.28920886 0 0.10941976 ;
	setAttr ".pt[5122]" -type "float3" 0.26171452 0 0.14166676 ;
	setAttr ".pt[5123]" -type "float3" 0.22754005 0 0.17042525 ;
	setAttr ".pt[5124]" -type "float3" 0.18752718 0 0.19498707 ;
	setAttr ".pt[5125]" -type "float3" 0.14266053 0 0.21474846 ;
	setAttr ".pt[5126]" -type "float3" 0.094045237 0 0.22922167 ;
	setAttr ".pt[5127]" -type "float3" 0.042878482 0 0.23805052 ;
	setAttr ".pt[5128]" -type "float3" -0.009580059 0 0.2410178 ;
	setAttr ".pt[5129]" -type "float3" -0.062038537 0 0.23805052 ;
	setAttr ".pt[5130]" -type "float3" -0.11320539 0 0.22922167 ;
	setAttr ".pt[5131]" -type "float3" -0.16182061 0 0.21474844 ;
	setAttr ".pt[5132]" -type "float3" -0.20668694 0 0.19498739 ;
	setAttr ".pt[5133]" -type "float3" -0.24670064 0 0.17042522 ;
	setAttr ".pt[5134]" -type "float3" -0.28087449 0 0.14166676 ;
	setAttr ".pt[5135]" -type "float3" -0.29684836 0 0.1094198 ;
	setAttr ".pt[5136]" -type "float3" -0.31698617 0 0.074478552 ;
	setAttr ".pt[5137]" -type "float3" -0.32926971 0 0.037703495 ;
	setAttr ".pt[5138]" -type "float3" -0.33339834 0 -5.5206928e-09 ;
	setAttr ".pt[5139]" -type "float3" -0.32926971 0 -0.037703544 ;
	setAttr ".pt[5140]" -type "float3" -0.31698617 0 -0.074478514 ;
	setAttr ".pt[5141]" -type "float3" -0.29684836 0 -0.10941993 ;
	setAttr ".pt[5142]" -type "float3" -0.28087473 0 -0.14166678 ;
	setAttr ".pt[5143]" -type "float3" -0.24670035 0 -0.17042539 ;
	setAttr ".pt[5144]" -type "float3" -0.20668723 0 -0.19498731 ;
	setAttr ".pt[5145]" -type "float3" -0.16182065 0 -0.21474849 ;
	setAttr ".pt[5146]" -type "float3" -0.11320524 0 -0.22922179 ;
	setAttr ".pt[5147]" -type "float3" -0.062038347 0 -0.23805083 ;
	setAttr ".pt[5148]" -type "float3" -0.009579936 0 -0.24101797 ;
	setAttr ".pt[5149]" -type "float3" 0.042878609 0 -0.23805083 ;
	setAttr ".pt[5150]" -type "float3" 0.094045393 0 -0.22922179 ;
	setAttr ".pt[5151]" -type "float3" 0.14266081 0 -0.2147482 ;
	setAttr ".pt[5152]" -type "float3" 0.18752714 0 -0.1949874 ;
	setAttr ".pt[5153]" -type "float3" 0.22754039 0 -0.17042509 ;
	setAttr ".pt[5154]" -type "float3" 0.26171455 0 -0.14166673 ;
	setAttr ".pt[5155]" -type "float3" 0.2892091 0 -0.10941956 ;
	setAttr ".pt[5156]" -type "float3" 0.30934638 0 -0.074478552 ;
	setAttr ".pt[5157]" -type "float3" 0.32163021 0 -0.037703365 ;
	setAttr ".pt[5158]" -type "float3" 0.32575855 0 4.9260802e-08 ;
	setAttr ".pt[5159]" -type "float3" 0.32162973 0 0.037703466 ;
	setAttr ".pt[5160]" -type "float3" 0.20207772 0 0.17351098 ;
	setAttr ".pt[5161]" -type "float3" 0.16714297 0 0.19851749 ;
	setAttr ".pt[5162]" -type "float3" 0.12796991 0 0.21863638 ;
	setAttr ".pt[5163]" -type "float3" 0.085524112 0 0.23337203 ;
	setAttr ".pt[5164]" -type "float3" 0.040850479 0 0.24236053 ;
	setAttr ".pt[5165]" -type "float3" -0.004950942 0 0.2453817 ;
	setAttr ".pt[5166]" -type "float3" -0.050752308 0 0.24236029 ;
	setAttr ".pt[5167]" -type "float3" -0.09542606 0 0.23337142 ;
	setAttr ".pt[5168]" -type "float3" -0.13787197 0 0.21863638 ;
	setAttr ".pt[5169]" -type "float3" -0.17704462 0 0.19851732 ;
	setAttr ".pt[5170]" -type "float3" -0.2119801 0 0.1735107 ;
	setAttr ".pt[5171]" -type "float3" -0.24181771 0 0.14423133 ;
	setAttr ".pt[5172]" -type "float3" -0.26582298 0 0.11140049 ;
	setAttr ".pt[5173]" -type "float3" -0.28340453 0 0.075826898 ;
	setAttr ".pt[5174]" -type "float3" -0.29413024 0 0.038386099 ;
	setAttr ".pt[5175]" -type "float3" -0.29773462 0 -6.6506473e-08 ;
	setAttr ".pt[5176]" -type "float3" -0.29413024 0 -0.038386118 ;
	setAttr ".pt[5177]" -type "float3" -0.28340453 0 -0.075826921 ;
	setAttr ".pt[5178]" -type "float3" -0.26582298 0 -0.11140082 ;
	setAttr ".pt[5179]" -type "float3" -0.24181768 0 -0.14423111 ;
	setAttr ".pt[5180]" -type "float3" -0.2119803 0 -0.17351098 ;
	setAttr ".pt[5181]" -type "float3" -0.17704466 0 -0.19851737 ;
	setAttr ".pt[5182]" -type "float3" -0.13787171 0 -0.21863639 ;
	setAttr ".pt[5183]" -type "float3" -0.095425978 0 -0.23337154 ;
	setAttr ".pt[5184]" -type "float3" -0.050752249 0 -0.24236029 ;
	setAttr ".pt[5185]" -type "float3" -0.0049508479 0 -0.24538134 ;
	setAttr ".pt[5186]" -type "float3" 0.040850651 0 -0.24236029 ;
	setAttr ".pt[5187]" -type "float3" 0.085524306 0 -0.23337154 ;
	setAttr ".pt[5188]" -type "float3" 0.12797 0 -0.21863614 ;
	setAttr ".pt[5189]" -type "float3" 0.16714312 0 -0.19851756 ;
	setAttr ".pt[5190]" -type "float3" 0.20207801 0 -0.17351058 ;
	setAttr ".pt[5191]" -type "float3" 0.23191598 0 -0.14423154 ;
	setAttr ".pt[5192]" -type "float3" 0.2559213 0 -0.11140061 ;
	setAttr ".pt[5193]" -type "float3" 0.27350274 0 -0.075826839 ;
	setAttr ".pt[5194]" -type "float3" 0.284228 0 -0.038386021 ;
	setAttr ".pt[5195]" -type "float3" 0.28783244 0 6.5368809e-08 ;
	setAttr ".pt[5196]" -type "float3" 0.28422779 0 0.038386039 ;
	setAttr ".pt[5197]" -type "float3" 0.27350238 0 0.075826988 ;
	setAttr ".pt[5198]" -type "float3" 0.25592124 0 0.11140069 ;
	setAttr ".pt[5199]" -type "float3" 0.23191619 0 0.1442311 ;
	setAttr ".pt[5200]" -type "float3" 0.14921816 0 0.18369022 ;
	setAttr ".pt[5201]" -type "float3" 0.12442107 0 0.21016394 ;
	setAttr ".pt[5202]" -type "float3" 0.096615598 0 0.23146254 ;
	setAttr ".pt[5203]" -type "float3" 0.066486895 0 0.2470624 ;
	setAttr ".pt[5204]" -type "float3" 0.034776922 0 0.25657895 ;
	setAttr ".pt[5205]" -type "float3" 0.0022664338 0 0.25977698 ;
	setAttr ".pt[5206]" -type "float3" -0.030244071 0 0.25657895 ;
	setAttr ".pt[5207]" -type "float3" -0.06195388 0 0.2470624 ;
	setAttr ".pt[5208]" -type "float3" -0.092082419 0 0.23146252 ;
	setAttr ".pt[5209]" -type "float3" -0.11988796 0 0.21016377 ;
	setAttr ".pt[5210]" -type "float3" -0.14468567 0 0.18368997 ;
	setAttr ".pt[5211]" -type "float3" -0.16586466 0 0.15269312 ;
	setAttr ".pt[5212]" -type "float3" -0.18290386 0 0.1179361 ;
	setAttr ".pt[5213]" -type "float3" -0.19538353 0 0.080275401 ;
	setAttr ".pt[5214]" -type "float3" -0.20299655 0 0.040638052 ;
	setAttr ".pt[5215]" -type "float3" -0.20555505 0 -3.5568945e-08 ;
	setAttr ".pt[5216]" -type "float3" -0.20299655 0 -0.040638037 ;
	setAttr ".pt[5217]" -type "float3" -0.19538353 0 -0.080275483 ;
	setAttr ".pt[5218]" -type "float3" -0.18290386 0 -0.11793613 ;
	setAttr ".pt[5219]" -type "float3" -0.16586466 0 -0.15269317 ;
	setAttr ".pt[5220]" -type "float3" -0.14468551 0 -0.18369025 ;
	setAttr ".pt[5221]" -type "float3" -0.11988785 0 -0.21016394 ;
	setAttr ".pt[5222]" -type "float3" -0.092082433 0 -0.23146254 ;
	setAttr ".pt[5223]" -type "float3" -0.06195382 0 -0.24706201 ;
	setAttr ".pt[5224]" -type "float3" -0.030243943 0 -0.25657871 ;
	setAttr ".pt[5225]" -type "float3" 0.0022665095 0 -0.25977701 ;
	setAttr ".pt[5226]" -type "float3" 0.034776974 0 -0.25657871 ;
	setAttr ".pt[5227]" -type "float3" 0.066486873 0 -0.24706201 ;
	setAttr ".pt[5228]" -type "float3" 0.09661559 0 -0.23146264 ;
	setAttr ".pt[5229]" -type "float3" 0.124421 0 -0.21016353 ;
	setAttr ".pt[5230]" -type "float3" 0.14921854 0 -0.18369021 ;
	setAttr ".pt[5231]" -type "float3" 0.17039765 0 -0.15269308 ;
	setAttr ".pt[5232]" -type "float3" 0.18743697 0 -0.1179361 ;
	setAttr ".pt[5233]" -type "float3" 0.19991642 0 -0.080275394 ;
	setAttr ".pt[5234]" -type "float3" 0.2075294 0 -0.040638026 ;
	setAttr ".pt[5235]" -type "float3" 0.21008813 0 7.0118098e-09 ;
	setAttr ".pt[5236]" -type "float3" 0.20752916 0 0.040638112 ;
	setAttr ".pt[5237]" -type "float3" 0.19991651 0 0.080275483 ;
	setAttr ".pt[5238]" -type "float3" 0.1874366 0 0.11793636 ;
	setAttr ".pt[5239]" -type "float3" 0.17039768 0 0.15269314 ;
	setAttr ".pt[5240]" -type "float3" 0.15784895 0 0.21608731 ;
	setAttr ".pt[5241]" -type "float3" 0.18971969 0 0.18886721 ;
	setAttr ".pt[5242]" -type "float3" 0.1221128 0 0.23798585 ;
	setAttr ".pt[5243]" -type "float3" 0.083390601 0 0.254026 ;
	setAttr ".pt[5244]" -type "float3" 0.042636011 0 0.26380992 ;
	setAttr ".pt[5245]" -type "float3" 0.00085262017 0 0.26709878 ;
	setAttr ".pt[5246]" -type "float3" -0.04093086 0 0.26380992 ;
	setAttr ".pt[5247]" -type "float3" -0.081685372 0 0.254026 ;
	setAttr ".pt[5248]" -type "float3" -0.12040776 0 0.23798609 ;
	setAttr ".pt[5249]" -type "float3" -0.15614372 0 0.21608774 ;
	setAttr ".pt[5250]" -type "float3" -0.18801458 0 0.1888673 ;
	setAttr ".pt[5251]" -type "float3" -0.21523452 0 0.15699671 ;
	setAttr ".pt[5252]" -type "float3" -0.23713404 0 0.12126032 ;
	setAttr ".pt[5253]" -type "float3" -0.25317329 0 0.082538053 ;
	setAttr ".pt[5254]" -type "float3" -0.26295802 0 0.041783389 ;
	setAttr ".pt[5255]" -type "float3" -0.26624566 0 -6.592397e-08 ;
	setAttr ".pt[5256]" -type "float3" -0.26295802 0 -0.041783433 ;
	setAttr ".pt[5257]" -type "float3" -0.25317329 0 -0.082538128 ;
	setAttr ".pt[5258]" -type "float3" -0.23713404 0 -0.12126034 ;
	setAttr ".pt[5259]" -type "float3" -0.21523452 0 -0.15699676 ;
	setAttr ".pt[5260]" -type "float3" -0.18801442 0 -0.18886723 ;
	setAttr ".pt[5261]" -type "float3" -0.15614392 0 -0.21608731 ;
	setAttr ".pt[5262]" -type "float3" -0.1204076 0 -0.23798612 ;
	setAttr ".pt[5263]" -type "float3" -0.081685305 0 -0.25402573 ;
	setAttr ".pt[5264]" -type "float3" -0.040930782 0 -0.26380995 ;
	setAttr ".pt[5265]" -type "float3" 0.00085270865 0 -0.26709881 ;
	setAttr ".pt[5266]" -type "float3" 0.042636111 0 -0.26380995 ;
	setAttr ".pt[5267]" -type "float3" 0.083390735 0 -0.25402573 ;
	setAttr ".pt[5268]" -type "float3" 0.12211328 0 -0.23798716 ;
	setAttr ".pt[5269]" -type "float3" 0.15784924 0 -0.21608739 ;
	setAttr ".pt[5270]" -type "float3" 0.18972009 0 -0.18886718 ;
	setAttr ".pt[5271]" -type "float3" 0.21693994 0 -0.15699644 ;
	setAttr ".pt[5272]" -type "float3" 0.23883943 0 -0.12126008 ;
	setAttr ".pt[5273]" -type "float3" 0.25487897 0 -0.082538038 ;
	setAttr ".pt[5274]" -type "float3" 0.26466289 0 -0.041783329 ;
	setAttr ".pt[5275]" -type "float3" 0.26795113 0 3.8959715e-08 ;
	setAttr ".pt[5276]" -type "float3" 0.26466307 0 0.041783419 ;
	setAttr ".pt[5277]" -type "float3" 0.25487843 0 0.082538083 ;
	setAttr ".pt[5278]" -type "float3" 0.23883939 0 0.12126023 ;
	setAttr ".pt[5279]" -type "float3" 0.21693939 0 0.15699662 ;
	setAttr ".pt[5280]" -type "float3" 0.1997872 0 0.20271647 ;
	setAttr ".pt[5281]" -type "float3" 0.1655793 0 0.23193333 ;
	setAttr ".pt[5282]" -type "float3" 0.1655793 0 0.23193333 ;
	setAttr ".pt[5283]" -type "float3" 0.1997872 0 0.20271647 ;
	setAttr ".pt[5284]" -type "float3" 0.22900316 0 0.16850917 ;
	setAttr ".pt[5285]" -type "float3" 0.22900316 0 0.16850917 ;
	setAttr ".pt[5286]" -type "float3" 0.12722282 0 0.25543785 ;
	setAttr ".pt[5287]" -type "float3" 0.12722282 0 0.25543785 ;
	setAttr ".pt[5288]" -type "float3" 0.2525084 0 0.13015226 ;
	setAttr ".pt[5289]" -type "float3" 0.2525084 0 0.13015226 ;
	setAttr ".pt[5290]" -type "float3" 0.085661024 0 0.27265361 ;
	setAttr ".pt[5291]" -type "float3" 0.085661024 0 0.27265361 ;
	setAttr ".pt[5292]" -type "float3" 0.26972407 0 0.088590473 ;
	setAttr ".pt[5293]" -type "float3" 0.26972407 0 0.088590473 ;
	setAttr ".pt[5294]" -type "float3" 0.041917894 0 0.28315514 ;
	setAttr ".pt[5295]" -type "float3" 0.041917894 0 0.28315514 ;
	setAttr ".pt[5296]" -type "float3" 0.28022537 0 0.044847369 ;
	setAttr ".pt[5297]" -type "float3" 0.28022537 0 0.044847369 ;
	setAttr ".pt[5298]" -type "float3" -0.0029294707 0 0.28668469 ;
	setAttr ".pt[5299]" -type "float3" -0.0029294707 0 0.28668469 ;
	setAttr ".pt[5300]" -type "float3" 0.28375539 0 4.2695476e-08 ;
	setAttr ".pt[5301]" -type "float3" 0.28375539 0 4.2695476e-08 ;
	setAttr ".pt[5302]" -type "float3" 0.28022566 0 -0.044847209 ;
	setAttr ".pt[5303]" -type "float3" 0.28022566 0 -0.044847209 ;
	setAttr ".pt[5304]" -type "float3" -0.047776841 0 0.28315514 ;
	setAttr ".pt[5305]" -type "float3" -0.047776841 0 0.28315514 ;
	setAttr ".pt[5306]" -type "float3" 0.26972413 0 -0.088590302 ;
	setAttr ".pt[5307]" -type "float3" 0.26972413 0 -0.088590302 ;
	setAttr ".pt[5308]" -type "float3" -0.091519915 0 0.27265361 ;
	setAttr ".pt[5309]" -type "float3" -0.091519915 0 0.27265361 ;
	setAttr ".pt[5310]" -type "float3" 0.25250861 0 -0.13015211 ;
	setAttr ".pt[5311]" -type "float3" 0.25250861 0 -0.13015211 ;
	setAttr ".pt[5312]" -type "float3" -0.13308166 0 0.25543794 ;
	setAttr ".pt[5313]" -type "float3" -0.13308166 0 0.25543794 ;
	setAttr ".pt[5314]" -type "float3" 0.22900322 0 -0.16850911 ;
	setAttr ".pt[5315]" -type "float3" 0.22900322 0 -0.16850911 ;
	setAttr ".pt[5316]" -type "float3" -0.17143862 0 0.23193265 ;
	setAttr ".pt[5317]" -type "float3" -0.17143862 0 0.23193265 ;
	setAttr ".pt[5318]" -type "float3" 0.19978748 0 -0.20271653 ;
	setAttr ".pt[5319]" -type "float3" 0.19978748 0 -0.20271653 ;
	setAttr ".pt[5320]" -type "float3" -0.20564604 0 0.20271668 ;
	setAttr ".pt[5321]" -type "float3" -0.20564604 0 0.20271668 ;
	setAttr ".pt[5322]" -type "float3" 0.16557947 0 -0.23193277 ;
	setAttr ".pt[5323]" -type "float3" 0.16557947 0 -0.23193277 ;
	setAttr ".pt[5324]" -type "float3" -0.23486237 0 0.16850916 ;
	setAttr ".pt[5325]" -type "float3" -0.23486237 0 0.16850916 ;
	setAttr ".pt[5326]" -type "float3" 0.12722285 0 -0.25543755 ;
	setAttr ".pt[5327]" -type "float3" 0.12722285 0 -0.25543755 ;
	setAttr ".pt[5328]" -type "float3" -0.25836712 0 0.13015206 ;
	setAttr ".pt[5329]" -type "float3" -0.25836712 0 0.13015206 ;
	setAttr ".pt[5330]" -type "float3" 0.085660972 0 -0.27265337 ;
	setAttr ".pt[5331]" -type "float3" 0.085660972 0 -0.27265337 ;
	setAttr ".pt[5332]" -type "float3" -0.27558267 0 0.088590384 ;
	setAttr ".pt[5333]" -type "float3" -0.27558267 0 0.088590384 ;
	setAttr ".pt[5334]" -type "float3" 0.041918062 0 -0.28315538 ;
	setAttr ".pt[5335]" -type "float3" 0.041918062 0 -0.28315538 ;
	setAttr ".pt[5336]" -type "float3" -0.28608483 0 0.044847336 ;
	setAttr ".pt[5337]" -type "float3" -0.28608483 0 0.044847336 ;
	setAttr ".pt[5338]" -type "float3" -0.0029293767 0 -0.28668472 ;
	setAttr ".pt[5339]" -type "float3" -0.0029293767 0 -0.28668472 ;
	setAttr ".pt[5340]" -type "float3" -0.28961438 0 -4.2958259e-09 ;
	setAttr ".pt[5341]" -type "float3" -0.28961438 0 -4.2958259e-09 ;
	setAttr ".pt[5342]" -type "float3" -0.047776781 0 -0.28315538 ;
	setAttr ".pt[5343]" -type "float3" -0.047776781 0 -0.28315538 ;
	setAttr ".pt[5344]" -type "float3" -0.28608483 0 -0.044847377 ;
	setAttr ".pt[5345]" -type "float3" -0.28608483 0 -0.044847377 ;
	setAttr ".pt[5346]" -type "float3" -0.09151984 0 -0.27265337 ;
	setAttr ".pt[5347]" -type "float3" -0.09151984 0 -0.27265337 ;
	setAttr ".pt[5348]" -type "float3" -0.27558267 0 -0.088590458 ;
	setAttr ".pt[5349]" -type "float3" -0.27558267 0 -0.088590458 ;
	setAttr ".pt[5350]" -type "float3" -0.13308164 0 -0.25543797 ;
	setAttr ".pt[5351]" -type "float3" -0.13308164 0 -0.25543797 ;
	setAttr ".pt[5352]" -type "float3" -0.25836712 0 -0.13015209 ;
	setAttr ".pt[5353]" -type "float3" -0.25836712 0 -0.13015209 ;
	setAttr ".pt[5354]" -type "float3" -0.17143854 0 -0.23193294 ;
	setAttr ".pt[5355]" -type "float3" -0.17143854 0 -0.23193294 ;
	setAttr ".pt[5356]" -type "float3" -0.23486248 0 -0.16850919 ;
	setAttr ".pt[5357]" -type "float3" -0.23486248 0 -0.16850919 ;
	setAttr ".pt[5358]" -type "float3" -0.20564623 0 -0.20271648 ;
	setAttr ".pt[5359]" -type "float3" -0.20564623 0 -0.20271648 ;
	setAttr ".pt[5360]" -type "float3" 0.087235011 0 0.17695795 ;
	setAttr ".pt[5361]" -type "float3" 0.12722282 0 0.25543785 ;
	setAttr ".pt[5362]" -type "float3" 0.1655793 0 0.23193333 ;
	setAttr ".pt[5363]" -type "float3" 0.11526242 0 0.16267721 ;
	setAttr ".pt[5364]" -type "float3" 0.15974759 0 0.118192 ;
	setAttr ".pt[5365]" -type "float3" 0.22900316 0 0.16850917 ;
	setAttr ".pt[5366]" -type "float3" 0.2525084 0 0.13015226 ;
	setAttr ".pt[5367]" -type "float3" 0.17402834 0 0.090164505 ;
	setAttr ".pt[5368]" -type "float3" 0.02813912 0 0.19615905 ;
	setAttr ".pt[5369]" -type "float3" 0.041917894 0 0.28315514 ;
	setAttr ".pt[5370]" -type "float3" 0.085661024 0 0.27265361 ;
	setAttr ".pt[5371]" -type "float3" 0.059207723 0 0.19123837 ;
	setAttr ".pt[5372]" -type "float3" 0.18830876 0 0.062137004 ;
	setAttr ".pt[5373]" -type "float3" 0.26972407 0 0.088590473 ;
	setAttr ".pt[5374]" -type "float3" 0.28022537 0 0.044847369 ;
	setAttr ".pt[5375]" -type "float3" 0.1932299 0 0.031068601 ;
	setAttr ".pt[5376]" -type "float3" 0.1981508 0 2.6367642e-08 ;
	setAttr ".pt[5377]" -type "float3" 0.28375539 0 4.2695476e-08 ;
	setAttr ".pt[5378]" -type "float3" 0.28022566 0 -0.044847209 ;
	setAttr ".pt[5379]" -type "float3" 0.19323008 0 -0.031068563 ;
	setAttr ".pt[5380]" -type "float3" -0.033998061 0 0.19615905 ;
	setAttr ".pt[5381]" -type "float3" -0.047776841 0 0.28315514 ;
	setAttr ".pt[5382]" -type "float3" -0.0029294707 0 0.28668469 ;
	setAttr ".pt[5383]" -type "float3" -0.0029294761 0 0.20108019 ;
	setAttr ".pt[5384]" -type "float3" 0.1997872 0 0.20271647 ;
	setAttr ".pt[5385]" -type "float3" 0.13750504 0 0.14043443 ;
	setAttr ".pt[5386]" -type "float3" 0.11526223 0 0.1626773 ;
	setAttr ".pt[5387]" -type "float3" 0.15974769 0 0.11819173 ;
	setAttr ".pt[5388]" -type "float3" 0.26972413 0 -0.088590302 ;
	setAttr ".pt[5389]" -type "float3" 0.18830925 0 -0.062137067 ;
	setAttr ".pt[5390]" -type "float3" 0.059207715 0 0.19123837 ;
	setAttr ".pt[5391]" -type "float3" 0.18830876 0 0.062137116 ;
	setAttr ".pt[5392]" -type "float3" 0.11526223 0 0.1626773 ;
	setAttr ".pt[5393]" -type "float3" 0.15974769 0 0.11819173 ;
	setAttr ".pt[5394]" -type "float3" -0.093094021 0 0.17695794 ;
	setAttr ".pt[5395]" -type "float3" -0.13308166 0 0.25543794 ;
	setAttr ".pt[5396]" -type "float3" -0.091519915 0 0.27265361 ;
	setAttr ".pt[5397]" -type "float3" -0.065066583 0 0.19123851 ;
	setAttr ".pt[5398]" -type "float3" -0.0029294516 0 0.20108019 ;
	setAttr ".pt[5399]" -type "float3" 0.1981508 0 3.5955935e-08 ;
	setAttr ".pt[5400]" -type "float3" 0.17402859 0 -0.090164453 ;
	setAttr ".pt[5401]" -type "float3" 0.25250861 0 -0.13015211 ;
	setAttr ".pt[5402]" -type "float3" 0.22900322 0 -0.16850911 ;
	setAttr ".pt[5403]" -type "float3" 0.1597477 0 -0.11819189 ;
	setAttr ".pt[5404]" -type "float3" -0.14336412 0 0.14043461 ;
	setAttr ".pt[5405]" -type "float3" -0.20564604 0 0.20271668 ;
	setAttr ".pt[5406]" -type "float3" -0.17143862 0 0.23193265 ;
	setAttr ".pt[5407]" -type "float3" -0.12112134 0 0.1626773 ;
	setAttr ".pt[5408]" -type "float3" 0.13750504 0 -0.14043447 ;
	setAttr ".pt[5409]" -type "float3" 0.19978748 0 -0.20271653 ;
	setAttr ".pt[5410]" -type "float3" 0.16557947 0 -0.23193277 ;
	setAttr ".pt[5411]" -type "float3" 0.11526249 0 -0.16267718 ;
	setAttr ".pt[5412]" -type "float3" -0.065066688 0 0.19123851 ;
	setAttr ".pt[5413]" -type "float3" 0.059207715 0 0.19123837 ;
	setAttr ".pt[5414]" -type "float3" 0.18830876 0 0.062137116 ;
	setAttr ".pt[5415]" -type "float3" -0.17988718 0 0.090164594 ;
	setAttr ".pt[5416]" -type "float3" -0.25836712 0 0.13015206 ;
	setAttr ".pt[5417]" -type "float3" -0.23486237 0 0.16850916 ;
	setAttr ".pt[5418]" -type "float3" -0.16560678 0 0.11819185 ;
	setAttr ".pt[5419]" -type "float3" 0.18830876 0 -0.062137078 ;
	setAttr ".pt[5420]" -type "float3" 0.087235115 0 -0.17695782 ;
	setAttr ".pt[5421]" -type "float3" 0.12722285 0 -0.25543755 ;
	setAttr ".pt[5422]" -type "float3" 0.085660972 0 -0.27265337 ;
	setAttr ".pt[5423]" -type "float3" 0.059207812 0 -0.19123837 ;
	setAttr ".pt[5424]" -type "float3" -0.0029294516 0 0.20108019 ;
	setAttr ".pt[5425]" -type "float3" 0.1981508 0 3.5955935e-08 ;
	setAttr ".pt[5426]" -type "float3" -0.19908872 0 0.031068532 ;
	setAttr ".pt[5427]" -type "float3" -0.28608483 0 0.044847336 ;
	setAttr ".pt[5428]" -type "float3" -0.27558267 0 0.088590384 ;
	setAttr ".pt[5429]" -type "float3" -0.19416833 0 0.062136974 ;
	setAttr ".pt[5430]" -type "float3" -0.12112145 0 0.16267745 ;
	setAttr ".pt[5431]" -type "float3" 0.028139174 0 -0.19615917 ;
	setAttr ".pt[5432]" -type "float3" 0.041918062 0 -0.28315538 ;
	setAttr ".pt[5433]" -type "float3" -0.0029293767 0 -0.28668472 ;
	setAttr ".pt[5434]" -type "float3" -0.002929416 0 -0.20108007 ;
	setAttr ".pt[5435]" -type "float3" 0.15974756 0 -0.11819196 ;
	setAttr ".pt[5436]" -type "float3" -0.1990886 0 -0.031068567 ;
	setAttr ".pt[5437]" -type "float3" -0.28608483 0 -0.044847377 ;
	setAttr ".pt[5438]" -type "float3" -0.28961438 0 -4.2958259e-09 ;
	setAttr ".pt[5439]" -type "float3" -0.20400974 0 -6.5919141e-09 ;
	setAttr ".pt[5440]" -type "float3" -0.065066688 0 0.19123851 ;
	setAttr ".pt[5441]" -type "float3" -0.033997998 0 -0.19615917 ;
	setAttr ".pt[5442]" -type "float3" -0.047776781 0 -0.28315538 ;
	setAttr ".pt[5443]" -type "float3" -0.09151984 0 -0.27265337 ;
	setAttr ".pt[5444]" -type "float3" -0.065066531 0 -0.19123863 ;
	setAttr ".pt[5445]" -type "float3" -0.19416806 0 -0.062137086 ;
	setAttr ".pt[5446]" -type "float3" -0.27558267 0 -0.088590458 ;
	setAttr ".pt[5447]" -type "float3" -0.093094118 0 -0.17695782 ;
	setAttr ".pt[5448]" -type "float3" -0.13308164 0 -0.25543797 ;
	setAttr ".pt[5449]" -type "float3" -0.17143854 0 -0.23193294 ;
	setAttr ".pt[5450]" -type "float3" -0.12112119 0 -0.16267718 ;
	setAttr ".pt[5451]" -type "float3" -0.14336409 0 -0.14043449 ;
	setAttr ".pt[5452]" -type "float3" -0.20564623 0 -0.20271648 ;
	setAttr ".pt[5453]" -type "float3" -0.23486248 0 -0.16850919 ;
	setAttr ".pt[5454]" -type "float3" -0.1656065 0 -0.11819187 ;
	setAttr ".pt[5455]" -type "float3" -0.1656065 0 0.11819198 ;
	setAttr ".pt[5456]" -type "float3" 0.11526255 0 -0.16267732 ;
	setAttr ".pt[5457]" -type "float3" -0.12112145 0 0.16267745 ;
	setAttr ".pt[5458]" -type "float3" 0.18830876 0 -0.062137078 ;
	setAttr ".pt[5459]" -type "float3" 0.15974756 0 -0.11819196 ;
	setAttr ".pt[5460]" -type "float3" -0.19416806 0 0.062137127 ;
	setAttr ".pt[5461]" -type "float3" 0.059207812 0 -0.19123863 ;
	setAttr ".pt[5462]" -type "float3" -0.20400974 0 3.5955935e-08 ;
	setAttr ".pt[5463]" -type "float3" -0.1656065 0 0.11819198 ;
	setAttr ".pt[5464]" -type "float3" -0.0029294456 0 -0.20108007 ;
	setAttr ".pt[5465]" -type "float3" 0.11526255 0 -0.16267732 ;
	setAttr ".pt[5466]" -type "float3" -0.16560663 0 -0.11819174 ;
	setAttr ".pt[5467]" -type "float3" -0.16560663 0 -0.11819174 ;
	setAttr ".pt[5468]" -type "float3" -0.17988715 0 -0.090164602 ;
	setAttr ".pt[5469]" -type "float3" -0.19416806 0 -0.062137086 ;
	setAttr ".pt[5470]" -type "float3" -0.19416806 0 -0.062137086 ;
	setAttr ".pt[5471]" -type "float3" -0.065066531 0 -0.19123837 ;
	setAttr ".pt[5472]" -type "float3" -0.25836712 0 -0.13015209 ;
	setAttr ".pt[5473]" -type "float3" -0.12112135 0 -0.16267717 ;
	setAttr ".pt[5474]" -type "float3" -0.19416806 0 0.062137127 ;
	setAttr ".pt[5475]" -type "float3" 0.059207812 0 -0.19123863 ;
	setAttr ".pt[5476]" -type "float3" -0.20400974 0 3.5955935e-08 ;
	setAttr ".pt[5477]" -type "float3" -0.0029294456 0 -0.20108007 ;
	setAttr ".pt[5478]" -type "float3" -0.065066531 0 -0.19123837 ;
	setAttr ".pt[5479]" -type "float3" -0.12112135 0 -0.16267717 ;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape2" -p "polySurface1";
	rename -uid "441E3D00-2F46-A7E6-DB3A-EAA0479D935A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:979]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 11 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "booleanIntersection";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 36 "e[1330]" "e[1334]" "e[1407]" "e[1411]" "e[1510]" "e[1514]" "e[1525:1526]" "e[1528:1529]" "e[1577]" "e[1579:1580]" "e[1582:1583]" "e[1650]" "e[1707:1708]" "e[1710:1711]" "e[1719]" "e[1768]" "e[1811]" "e[1826:1827]" "e[1861]" "e[1876:1877]" "e[1905]" "e[1940]" "e[1946:1947]" "e[1976]" "e[1995:1996]" "e[2000]" "e[2023]" "e[2026]" "e[2047]" "e[2051]" "e[2063:2064]" "e[2080:2081]" "e[2086:2087]" "e[2089:2090]" "e[2092:2093]" "e[2096:2097]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 11 "f[630]" "f[639:640]" "f[661:662]" "f[673:674]" "f[699:700]" "f[719:720]" "f[745:746]" "f[764:765]" "f[782:783]" "f[788:789]" "f[792]";
	setAttr ".gtag[2].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 133 "e[1206]" "e[1209]" "e[1222]" "e[1224]" "e[1250]" "e[1254]" "e[1257]" "e[1259]" "e[1277]" "e[1280]" "e[1299]" "e[1302]" "e[1305]" "e[1307]" "e[1322]" "e[1328]" "e[1332]" "e[1344]" "e[1346]" "e[1355]" "e[1358]" "e[1373]" "e[1375]" "e[1390]" "e[1392]" "e[1405]" "e[1409]" "e[1412]" "e[1415]" "e[1419]" "e[1421]" "e[1438]" "e[1440]" "e[1471]" "e[1473]" "e[1478]" "e[1481]" "e[1495]" "e[1497]" "e[1508]" "e[1512]" "e[1516]" "e[1518]" "e[1523]" "e[1527]" "e[1554]" "e[1557]" "e[1572]" "e[1574:1575]" "e[1578]" "e[1581]" "e[1584]" "e[1586]" "e[1613]" "e[1615]" "e[1630]" "e[1633]" "e[1648]" "e[1652]" "e[1654:1655]" "e[1657]" "e[1664]" "e[1666]" "e[1671]" "e[1696]" "e[1699]" "e[1706]" "e[1709]" "e[1717]" "e[1727]" "e[1729]" "e[1759]" "e[1762]" "e[1766]" "e[1784]" "e[1786]" "e[1809]" "e[1819]" "e[1822]" "e[1825]" "e[1842]" "e[1844]" "e[1859]" "e[1869]" "e[1872]" "e[1875]" "e[1894]" "e[1896]" "e[1903]" "e[1915]" "e[1918]" "e[1933]" "e[1935]" "e[1938]" "e[1945]" "e[1955]" "e[1958]" "e[1969]" "e[1971]" "e[1974]" "e[1988]" "e[1991]" "e[1994]" "e[1998]" "e[2001]" "e[2003]" "e[2014]" "e[2017]" "e[2021]" "e[2025]" "e[2027]" "e[2029]" "e[2034]" "e[2037]" "e[2040]" "e[2042]" "e[2045]" "e[2049]" "e[2052]" "e[2054]" "e[2056]" "e[2059]" "e[2062]" "e[2065]" "e[2067:2068]" "e[2071]" "e[2074]" "e[2077]" "e[2079]" "e[2085]" "e[2088]" "e[2091]" "e[2095]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 46 "vtx[630:632]" "vtx[638:639]" "vtx[652:656]" "vtx[665:666]" "vtx[675:678]" "vtx[684:686]" "vtx[697:698]" "vtx[703:704]" "vtx[711:712]" "vtx[719:720]" "vtx[730:733]" "vtx[742:743]" "vtx[758:759]" "vtx[762:763]" "vtx[769:770]" "vtx[779:780]" "vtx[798:799]" "vtx[805:806]" "vtx[810:811]" "vtx[824:825]" "vtx[832:833]" "vtx[841:844]" "vtx[847:848]" "vtx[863:864]" "vtx[876:877]" "vtx[892:893]" "vtx[903:904]" "vtx[920:921]" "vtx[929:930]" "vtx[942:943]" "vtx[953:954]" "vtx[962:963]" "vtx[970:971]" "vtx[980:981]" "vtx[986:987]" "vtx[995:996]" "vtx[999]" "vtx[1001]" "vtx[1006:1007]" "vtx[1009]" "vtx[1012]" "vtx[1014:1017]" "vtx[1019]" "vtx[1022:1023]" "vtx[1025:1026]" "vtx[1028:1030]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 47 "vtx[630:632]" "vtx[638:639]" "vtx[652:656]" "vtx[665:666]" "vtx[675:678]" "vtx[684]" "vtx[686]" "vtx[697:698]" "vtx[703:704]" "vtx[711:712]" "vtx[719:720]" "vtx[730:733]" "vtx[742:743]" "vtx[758:759]" "vtx[762:763]" "vtx[769:770]" "vtx[779:780]" "vtx[798:799]" "vtx[805:806]" "vtx[810:811]" "vtx[824:825]" "vtx[832:833]" "vtx[841:844]" "vtx[847:848]" "vtx[863:864]" "vtx[876:877]" "vtx[892:893]" "vtx[903:904]" "vtx[920:921]" "vtx[929:930]" "vtx[942:943]" "vtx[953:954]" "vtx[962:963]" "vtx[970:971]" "vtx[980:981]" "vtx[986:987]" "vtx[995:996]" "vtx[999]" "vtx[1001]" "vtx[1006:1007]" "vtx[1009]" "vtx[1012]" "vtx[1014:1017]" "vtx[1019]" "vtx[1022:1023]" "vtx[1025:1026]" "vtx[1028:1030]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 49 "vtx[0:163]" "vtx[170:177]" "vtx[182:195]" "vtx[200:219]" "vtx[224:252]" "vtx[257:275]" "vtx[280:306]" "vtx[311:339]" "vtx[344:372]" "vtx[377:399]" "vtx[404:425]" "vtx[430:446]" "vtx[451:465]" "vtx[468:469]" "vtx[472:482]" "vtx[485:486]" "vtx[489:497]" "vtx[500:501]" "vtx[504:510]" "vtx[515:516]" "vtx[519:520]" "vtx[523]" "vtx[534:684]" "vtx[686:688]" "vtx[693:725]" "vtx[730:774]" "vtx[779:782]" "vtx[786:806]" "vtx[810:838]" "vtx[841:866]" "vtx[869:870]" "vtx[873:893]" "vtx[896:914]" "vtx[917:921]" "vtx[923:936]" "vtx[939:943]" "vtx[945:956]" "vtx[959:971]" "vtx[974:975]" "vtx[977:987]" "vtx[990:996]" "vtx[999]" "vtx[1001:1007]" "vtx[1009]" "vtx[1012:1017]" "vtx[1019]" "vtx[1022:1023]" "vtx[1025:1026]" "vtx[1028:1030]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 20 "vtx[170:172]" "vtx[182:183]" "vtx[200:201]" "vtx[218:219]" "vtx[245:246]" "vtx[270:271]" "vtx[297:298]" "vtx[326:327]" "vtx[355:356]" "vtx[384:385]" "vtx[411:412]" "vtx[436:437]" "vtx[459:460]" "vtx[476:477]" "vtx[491:492]" "vtx[500:501]" "vtx[508:509]" "vtx[515:516]" "vtx[519:520]" "vtx[523]";
	setAttr ".gtag[7].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 20 "vtx[170:172]" "vtx[182:183]" "vtx[200:201]" "vtx[218:219]" "vtx[245:246]" "vtx[270:271]" "vtx[297:298]" "vtx[326:327]" "vtx[355:356]" "vtx[384:385]" "vtx[411:412]" "vtx[436:437]" "vtx[459:460]" "vtx[476:477]" "vtx[491:492]" "vtx[500:501]" "vtx[508:509]" "vtx[515:516]" "vtx[519:520]" "vtx[523]";
	setAttr ".gtag[8].gtagnm" -type "string" "sides";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 12 "f[0:629]" "f[631:638]" "f[641:660]" "f[663:672]" "f[675:698]" "f[701:718]" "f[721:744]" "f[747:763]" "f[766:781]" "f[784:787]" "f[790:791]" "f[793:979]";
	setAttr ".gtag[9].gtagnm" -type "string" "top";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[10].gtagnm" -type "string" "topRing";
	setAttr ".gtag[10].gtagcmp" -type "componentList" 26 "e[268]" "e[270]" "e[290:291]" "e[329:330]" "e[367:368]" "e[422:423]" "e[473:474]" "e[528:529]" "e[587:588]" "e[646:647]" "e[705:706]" "e[760:761]" "e[811:812]" "e[858:859]" "e[893:894]" "e[924:925]" "e[942]" "e[945]" "e[958]" "e[961]" "e[973]" "e[976]" "e[981]" "e[984]" "e[989]" "e[991]";
	setAttr ".pv" -type "double2" 0.50000017881393433 0.55625015497207642 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 1523 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.60000026 0.36875004 0.60625029
		 0.36875004 0.60625029 0.38750005 0.60000026 0.38750005 0.56875014 0.36875004 0.57500017
		 0.36875004 0.57500017 0.38750005 0.56875014 0.38750005 0.52499998 0.48125011 0.53125
		 0.48125011 0.53125 0.50000012 0.52499998 0.50000012 0.39374998 0.48125011 0.39999998
		 0.48125011 0.39999998 0.50000012 0.39374998 0.50000012 0.55000007 0.40625006 0.5562501
		 0.40625006 0.5562501 0.42500007 0.55000007 0.42500007 0.61875033 0.40625006 0.62500036
		 0.40625006 0.62500036 0.42500007 0.61875033 0.42500007 0.48749989 0.59375018 0.49374989
		 0.59375018 0.49374989 0.61250019 0.48749989 0.61250019 0.43124995 0.59375018 0.43749994
		 0.59375018 0.43749994 0.61250019 0.43124995 0.61250019 0.53125 0.4625001 0.53750002
		 0.4625001 0.53750002 0.48125011 0.38749999 0.4625001 0.39374998 0.4625001 0.38749999
		 0.48125011 0.54375005 0.42500007 0.55000007 0.44375008 0.54375005 0.44375008 0.375
		 0.42500007 0.38124999 0.42500007 0.38124999 0.44375008 0.375 0.44375008 0.53750002
		 0.44375008 0.54375005 0.4625001 0.38749999 0.44375008 0.38124999 0.4625001 0.59375024
		 0.36875004 0.59375024 0.38750005 0.58125019 0.36875004 0.58125019 0.38750005 0.46249992
		 0.6312502 0.46874991 0.6312502 0.46874991 0.65000021 0.46249992 0.65000021 0.45624992
		 0.6312502 0.45624992 0.65000021 0.4812499 0.61250019 0.48749989 0.6312502 0.4812499
		 0.6312502 0.44374993 0.61250019 0.44374993 0.6312502 0.43749994 0.6312502 0.61250031
		 0.38750005 0.61250031 0.40625006 0.60625029 0.40625006 0.56250012 0.38750005 0.56875014
		 0.40625006 0.56250012 0.40625006 0.58750021 0.36875004 0.58750021 0.38750005 0.4749999
		 0.6312502 0.4749999 0.65000021 0.44999993 0.6312502 0.44999993 0.65000021 0.5062499
		 0.55625015 0.51249993 0.55625015 0.51249993 0.57500017 0.5062499 0.57500017 0.41249996
		 0.55625015 0.41874996 0.55625015 0.41874996 0.57500017 0.41249996 0.57500017 0.49999988
		 0.57500017 0.5062499 0.59375018 0.49999988 0.59375018 0.42499995 0.57500017 0.42499995
		 0.59375018 0.41874996 0.59375018 0.51249993 0.53750014 0.51874995 0.53750014 0.51874995
		 0.55625015 0.40624997 0.53750014 0.41249996 0.53750014 0.40624997 0.55625015 0.61250031
		 0.42500007 0.56250012 0.42500007 0.49999988 0.61250019 0.42499995 0.61250019 0.51874995
		 0.51875013 0.52499998 0.51875013 0.52499998 0.53750014 0.39999998 0.51875013 0.40624997
		 0.51875013 0.39999998 0.53750014 0.4812499 0.65000021 0.44374993 0.65000021 0.60000026
		 0.40625006 0.57500017 0.40625006 0.53125 0.51875013 0.39374998 0.51875013 0.5562501
		 0.44375008 0.62500036 0.44375008 0.61875033 0.44375008 0.49374989 0.6312502 0.43124995
		 0.6312502 0.53750002 0.50000012 0.38749999 0.50000012 0.55000007 0.4625001 0.375
		 0.4625001 0.54375005 0.48125011 0.38124999 0.48125011 0.59375024 0.40625006 0.58125019
		 0.40625006 0.46874991 0.66875023 0.46249992 0.66875023 0.45624992 0.66875023 0.48749989
		 0.65000021 0.43749994 0.65000021 0.60625029 0.42500007 0.56875014 0.42500007 0.58750021
		 0.40625006 0.4749999 0.66875023 0.44999993 0.66875023 0.51249993 0.59375018 0.41249996
		 0.59375018 0.5062499 0.61250019 0.41874996 0.61250019 0.51874995 0.57500017 0.40624997
		 0.57500017 0.61250031 0.44375008 0.56250012 0.44375008 0.49999988 0.6312502 0.42499995
		 0.6312502 0.52499998 0.55625015 0.39999998 0.55625015 0.4812499 0.66875023 0.44374993
		 0.66875023 0.60000026 0.42500007 0.57500017 0.42500007 0.53125 0.53750014 0.39374998
		 0.53750014 0.5562501 0.4625001 0.62500036 0.4625001 0.61875033 0.4625001 0.49374989
		 0.65000021 0.43124995 0.65000021 0.53750002 0.51875013 0.38749999 0.51875013 0.55000007
		 0.48125011 0.375 0.48125011 0.54375005 0.50000012 0.38124999 0.50000012 0.59375024
		 0.42500007 0.58125019 0.42500007 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.46874991 0.68750024
		 0.46249992 0.68750024 0.45624992 0.68750024 0.48749989 0.66875023 0.43749994 0.66875023
		 0.60625029 0.44375008 0.56875014 0.44375008 0.58750021 0.42500007 0 0 1 0 1 0 0 0
		 0 0 1 0 1 0 0 0 0.4749999 0.68750024 0.44999993 0.68750024 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0.51249993 0.61250019 0.41249996 0.61250019 0.5062499 0.6312502 0.41874996
		 0.6312502 0.51874995 0.59375018 0.40624997 0.59375018 0.61250031 0.4625001 0.56250012
		 0.4625001 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.49999988 0.65000021 0.42499995 0.65000021
		 0.52499998 0.57500017 0.39999998 0.57500017 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.4812499
		 0.68750024 0.44374993 0.68750024 0.60000026 0.44375008 0.57500017 0.44375008 0.53125
		 0.55625015 0.39374998 0.55625015 0.5562501 0.48125011 0.62500036 0.48125011 0.61875033
		 0.48125011 0.49374989 0.66875023 0.43124995 0.66875023 0.53750002 0.53750014 0.38749999
		 0.53750014 0.55000007 0.50000012 0.375 0.50000012 0.54375005 0.51875013 0.38124999
		 0.51875013 0 0 1 0 1 1 0 1;
	setAttr ".uvst[0].uvsp[250:499]" 0 0 1 0 1 1 0 1 0.59375024 0.44375008 0.58125019
		 0.44375008 0.48749989 0.68750024 0.43749994 0.68750024 0 0 1 0 1 0 0 0 0 0 1 0 1
		 0 0 0 0.60625029 0.4625001 0.56875014 0.4625001 0.58750021 0.44375008 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0.51249993 0.6312502 0.41249996 0.6312502 0.5062499 0.65000021
		 0.41874996 0.65000021 0.51874995 0.61250019 0.40624997 0.61250019 0.61250031 0.48125011
		 0.56250012 0.48125011 0.49999988 0.66875023 0.42499995 0.66875023 0.52499998 0.59375018
		 0.39999998 0.59375018 0.60000026 0.4625001 0.57500017 0.4625001 0.53125 0.57500017
		 0.39374998 0.57500017 0.5562501 0.50000012 0.62500036 0.50000012 0.61875033 0.50000012
		 0.49374989 0.68750024 0.43124995 0.68750024 0.53750002 0.55625015 0.38749999 0.55625015
		 0.55000007 0.51875013 0.375 0.51875013 0.54375005 0.53750014 0.38124999 0.53750014
		 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.59375024 0.4625001 0.58125019 0.4625001 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0.60625029 0.48125011 0.56875014 0.48125011 0.58750021 0.4625001
		 0.51249993 0.65000021 0.41249996 0.65000021 0.5062499 0.66875023 0.41874996 0.66875023
		 0.51874995 0.6312502 0.40624997 0.6312502 0.61250031 0.50000012 0.56250012 0.50000012
		 0.49999988 0.68750024 0.42499995 0.68750024 0.52499998 0.61250019 0.39999998 0.61250019
		 0.60000026 0.48125011 0.57500017 0.48125011 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.53125
		 0.59375018 0.39374998 0.59375018 0.5562501 0.51875013 0.62500036 0.51875013 0.61875033
		 0.51875013 0.53750002 0.57500017 0.38749999 0.57500017 0.55000007 0.53750014 0.375
		 0.53750014 0.54375005 0.55625015 0.38124999 0.55625015 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0.59375024 0.48125011 0.58125019 0.48125011 0.60625029 0.50000012 0.56875014
		 0.50000012 0.58750021 0.48125011 0.51249993 0.66875023 0.41249996 0.66875023 0.5062499
		 0.68750024 0.41874996 0.68750024 0.51874995 0.65000021 0.40624997 0.65000021 0.61250031
		 0.51875013 0.56250012 0.51875013 0.52499998 0.6312502 0.39999998 0.6312502 0.60000026
		 0.50000012 0.57500017 0.50000012 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.53125 0.61250019
		 0.39374998 0.61250019 0.5562501 0.53750014 0.62500036 0.53750014 0.61875033 0.53750014
		 0.53750002 0.59375018 0.38749999 0.59375018 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.55000007
		 0.55625015 0.375 0.55625015 0.54375005 0.57500017 0.38124999 0.57500017 0.59375024
		 0.50000012 0.58125019 0.50000012 0.60625029 0.51875013 0.56875014 0.51875013 0.58750021
		 0.50000012 0.51249993 0.68750024 0.41249996 0.68750024 0.51874995 0.66875023 0.40624997
		 0.66875023 0.61250031 0.53750014 0.56250012 0.53750014 0.52499998 0.65000021 0.39999998
		 0.65000021 0.60000026 0.51875013 0.57500017 0.51875013 0.53125 0.6312502 0.39374998
		 0.6312502 0.5562501 0.55625015 0.62500036 0.55625015 0.61875033 0.55625015 0 0 1
		 0 1 0 0 0 0 0 1 0 1 0 0 0 0.53750002 0.61250019 0.38749999 0.61250019 0.55000007
		 0.57500017 0.375 0.57500017 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.54375005 0.59375018
		 0.38124999 0.59375018 0.59375024 0.51875013 0.58125019 0.51875013 0.60625029 0.53750014
		 0.56875014 0.53750014 0.58750021 0.51875013 0.51874995 0.68750024 0.40624997 0.68750024
		 0.61250031 0.55625015 0.56250012 0.55625015 0.52499998 0.66875023 0.39999998 0.66875023
		 0.60000026 0.53750014 0.57500017 0.53750014 0.53125 0.65000021 0.39374998 0.65000021
		 0.5562501 0.57500017 0.62500036 0.57500017 0.61875033 0.57500017 0.53750002 0.6312502
		 0.38749999 0.6312502 0.55000007 0.59375018 0.375 0.59375018 0.54375005 0.61250019
		 0.38124999 0.61250019 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0.59375024 0.53750014 0.58125019 0.53750014 0.60625029 0.55625015 0.56875014
		 0.55625015 0.58750021 0.53750014 0.61250031 0.57500017 0.56250012 0.57500017 0.52499998
		 0.68750024;
	setAttr ".uvst[0].uvsp[500:749]" 0.39999998 0.68750024 0.60000026 0.55625015
		 0.57500017 0.55625015 0.53125 0.66875023 0.39374998 0.66875023 0.5562501 0.59375018
		 0.62500036 0.59375018 0.61875033 0.59375018 0.53750002 0.65000021 0.38749999 0.65000021
		 0.55000007 0.61250019 0.375 0.61250019 0.54375005 0.6312502 0.38124999 0.6312502
		 0.59375024 0.55625015 0.58125019 0.55625015 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0.60625029 0.57500017 0.56875014 0.57500017 0.58750021
		 0.55625015 0.61250031 0.59375018 0.56250012 0.59375018 0.60000026 0.57500017 0.57500017
		 0.57500017 0.53125 0.68750024 0.39374998 0.68750024 0.5562501 0.61250019 0.62500036
		 0.61250019 0.61875033 0.61250019 0.53750002 0.66875023 0.38749999 0.66875023 0.55000007
		 0.6312502 0.375 0.6312502 0.54375005 0.65000021 0.38124999 0.65000021 0.59375024
		 0.57500017 0.58125019 0.57500017 0.60625029 0.59375018 0.56875014 0.59375018 0.58750021
		 0.57500017 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.61250031
		 0.61250019 0.56250012 0.61250019 0.60000026 0.59375018 0.57500017 0.59375018 0.5562501
		 0.6312502 0.62500036 0.6312502 0.61875033 0.6312502 0.53750002 0.68750024 0.38749999
		 0.68750024 0.55000007 0.65000021 0.375 0.65000021 0.54375005 0.66875023 0.38124999
		 0.66875023 0.59375024 0.59375018 0.58125019 0.59375018 0.60625029 0.61250019 0.56875014
		 0.61250019 0.58750021 0.59375018 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 0 0 0
		 0 0 1 0 1 0 0 0 0.61250031 0.6312502 0.56250012 0.6312502 0.60000026 0.61250019 0.57500017
		 0.61250019 0.5562501 0.65000021 0.62500036 0.65000021 0.61875033 0.65000021 0.55000007
		 0.66875023 0.375 0.66875023 0.54375005 0.68750024 0.38124999 0.68750024 0.59375024
		 0.61250019 0.58125019 0.61250019 0.60625029 0.6312502 0.56875014 0.6312502 0.58750021
		 0.61250019 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.61250031 0.65000021 0.56250012 0.65000021
		 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.60000026 0.6312502 0.57500017 0.6312502 0.5562501
		 0.66875023 0.62500036 0.66875023 0.61875033 0.66875023 0.55000007 0.68750024 0.375
		 0.68750024 0.59375024 0.6312502 0.58125019 0.6312502 0.60625029 0.65000021 0.56875014
		 0.65000021 0.58750021 0.6312502 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.61250031 0.66875023
		 0.56250012 0.66875023 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.60000026 0.65000021 0.57500017
		 0.65000021 0.5562501 0.68750024 0.62500036 0.68750024 0.61875033 0.68750024 0.59375024
		 0.65000021 0.58125019 0.65000021 0.60625029 0.66875023 0.56875014 0.66875023 0.58750021
		 0.65000021 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.61250031 0.68750024 0.56250012 0.68750024
		 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.60000026 0.66875023 0.57500017 0.66875023 0.59375024
		 0.66875023 0.58125019 0.66875023 0.60625029 0.68750024 0.56875014 0.68750024 0.58750021
		 0.66875023 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.60000026
		 0.68750024 0.57500017 0.68750024 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.59375024 0.68750024
		 0.58125019 0.68750024 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.58750021 0.68750024 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0;
	setAttr ".uvst[0].uvsp[750:999]" 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0
		 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.44999993 0.61250019 0.4749999 0.61250019 0.40624997
		 0.50000012 0.51874995 0.50000012 0.43124995 0.57500017 0.49374989 0.57500017 0.5562501
		 0.38750005 0.61875033 0.38750005 0.41249996 0.51875013 0.51249993 0.51875013 0.42499995
		 0.55625015 0.49999988 0.55625015 0.41874996 0.53750014 0.5062499 0.53750014 0.45624992
		 0.61250019 0.46874991 0.61250019 0.58125019 0.35000002 0.58750021 0.35000002 0.59375024
		 0.35000002 0.56250012 0.36875004 0.61250031 0.36875004 0.44374993 0.59375018 0.4812499
		 0.59375018 0.46249992 0.61250019 0.57500017 0.35000002 0.60000026 0.35000002 0.38749999
		 0.42500007 0.53750002 0.42500007 0.375 0.40625006 0.38124999 0.40625006 0.54375005
		 0.40625006 0.39374998 0.44375008 0.53125 0.44375008 0.43749994 0.57500017 0.48749989
		 0.57500017 0.62500036 0.38750005 0.55000007 0.38750005 0.39999998 0.4625001 0.52499998
		 0.4625001 0.56875014 0.35000002 0.60625029 0.35000002 0.44999993 0.59375018 0.4749999
		 0.59375018 0.40624997 0.48125011 0.51874995 0.48125011 0.43124995 0.55625015 0.49374989
		 0.55625015 0.5562501 0.36875004 0.61875033 0.36875004 0.41249996 0.50000012 0.51249993
		 0.50000012 0.42499995 0.53750014 0.49999988 0.53750014 0.41874996 0.51875013 0.5062499
		 0.51875013 0.45624992 0.59375018 0.46874991 0.59375018 0.58125019 0.33125001 0.58750021
		 0.33125001 0.59375024 0.33125001 0.56250012 0.35000002 0.61250031 0.35000002 0.44374993
		 0.57500017 0.4812499 0.57500017 0.46249992 0.59375018 0.57500017 0.33125001 0.60000026
		 0.33125001 0.38749999 0.40625006 0.53750002 0.40625006 0.375 0.38750005 0.38124999
		 0.38750005 0.54375005 0.38750005 0.39374998 0.42500007 0.53125 0.42500007 0.43749994
		 0.55625015 0.48749989 0.55625015 0.62500036 0.36875004 0.55000007 0.36875004 0.39999998
		 0.44375008 0.52499998 0.44375008 0.56875014 0.33125001 0.60625029 0.33125001 0.44999993
		 0.57500017 0.4749999 0.57500017 0.40624997 0.4625001 0.51874995 0.4625001 0.43124995
		 0.53750014 0.49374989 0.53750014 0.5562501 0.35000002 0.61875033 0.35000002 0.41249996
		 0.48125011 0.51249993 0.48125011 0.42499995 0.51875013 0.49999988 0.51875013 0.41874996
		 0.50000012 0.5062499 0.50000012 0.45624992 0.57500017 0.46874991 0.57500017 0.58125019
		 0.3125 0.58750021 0.3125 0.59375024 0.3125 0.56250012 0.33125001 0.61250031 0.33125001
		 0.44374993 0.55625015 0.4812499 0.55625015 0.46249992 0.57500017 0.57500017 0.3125
		 0.60000026 0.3125 0.38749999 0.38750005 0.53750002 0.38750005 0.375 0.36875004 0.38124999
		 0.36875004 0.54375005 0.36875004 0.39374998 0.40625006 0.53125 0.40625006 0.43749994
		 0.53750014 0.48749989 0.53750014 0.62500036 0.35000002 0.55000007 0.35000002 0.39999998
		 0.42500007 0.52499998 0.42500007 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.56875014 0.3125
		 0.60625029 0.3125 0.44999993 0.55625015 0.4749999 0.55625015 0.40624997 0.44375008
		 0.51874995 0.44375008 0.43124995 0.51875013 0.49374989 0.51875013 0.5562501 0.33125001
		 0.61875033 0.33125001 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.41249996 0.4625001 0.51249993
		 0.4625001 0.42499995 0.50000012 0.49999988 0.50000012 0.41874996 0.48125011 0.5062499
		 0.48125011 0.45624992 0.55625015 0.46874991 0.55625015 0 0 1 0 1 0 0 0 0 0 1 0 1
		 0 0 0 0.56250012 0.3125 0.61250031 0.3125 0.44374993 0.53750014 0.4812499 0.53750014
		 0.46249992 0.55625015 0.38749999 0.36875004 0.53750002 0.36875004 0.375 0.35000002
		 0.38124999 0.35000002 0.54375005 0.35000002 2.4705098e-16 0.85846055 1 0.81817079
		 0 0.81817007 1 0.85846066 0.39374998 0.38750005 0.53125 0.38750005 0.43749994 0.51875013
		 0.48749989 0.51875013 0.62500036 0.33125001 0.55000007 0.33125001 0.39999998 0.40625006
		 0.52499998 0.40625006 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.44999993 0.53750014 0.4749999
		 0.53750014 0.40624997 0.42500007 0.51874995 0.42500007 0.43124995 0.50000012 0.49374989
		 0.50000012 0.5562501 0.3125 0.61875033 0.3125 0.41249996 0.44375008 0.51249993 0.44375008
		 0.42499995 0.48125011 0.49999988 0.48125011 0.41874996 0.4625001 0.5062499 0.4625001
		 0.45624992 0.53750014 0.46874991 0.53750014 0.44374993 0.51875013 0.4812499 0.51875013;
	setAttr ".uvst[0].uvsp[1000:1249]" 0.46249992 0.53750014 8.8232411e-17 0.85846186
		 0 0 1 0.81817067 0 0.81817073 1 0 1 0.85846204 1 0 1 0 0 0 0 0 1 0 0 0 0.38749999
		 0.35000002 0.53750002 0.35000002 0.375 0.33125001 0.38124999 0.33125001 0.54375005
		 0.33125001 0.39374998 0.36875004 0.53125 0.36875004 0.43749994 0.50000012 0.48749989
		 0.50000012 0.62500036 0.3125 0.55000007 0.3125 0.39999998 0.38750005 0.52499998 0.38750005
		 0.44999993 0.51875013 0.4749999 0.51875013 0.40624997 0.40625006 0.51874995 0.40625006
		 0.43124995 0.48125011 0.49374989 0.48125011 0.41249996 0.42500007 0.51249993 0.42500007
		 0.42499995 0.4625001 0.49999988 0.4625001 0.41874996 0.44375008 0.5062499 0.44375008
		 0.45624992 0.51875013 0.46874991 0.51875013 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0.44374993
		 0.50000012 0.4812499 0.50000012 0.46249992 0.51875013 0.38749999 0.33125001 0.53750002
		 0.33125001 0.375 0.3125 0.38124999 0.3125 0.54375005 0.3125 0.39374998 0.35000002
		 0.53125 0.35000002 0.43749994 0.48125011 0.48749989 0.48125011 0 0.81817108 1 0 1
		 0.85846317 -7.4997535e-17 0.85846192 0 0 1 0.81817085 0.39999998 0.36875004 0.52499998
		 0.36875004 1 0.85846162 1.5568564e-06 0.81817061 -1.2265659e-16 0.81817079 1 0.81817007
		 0.99999899 0.81816989 1.2352528e-16 0.85846162 0.44999993 0.50000012 0.4749999 0.50000012
		 0.40624997 0.38750005 0.51874995 0.38750005 0.43124995 0.4625001 0.49374989 0.4625001
		 0.41249996 0.40625006 0.51249993 0.40625006 0.42499995 0.44375008 0.49999988 0.44375008
		 0.41874996 0.42500007 0.5062499 0.42500007 0 0 1 0 0 0 1 0 1 0 0 0 0.45624992 0.50000012
		 0.46874991 0.50000012 0.44374993 0.48125011 0.4812499 0.48125011 0.46249992 0.50000012
		 0 0.85846317 1 0 1 0.81817013 1 0.85846055 6.6319802e-07 0.81817055 -8.7611893e-17
		 0.81817067 1 0.81817073 0.99999952 0.81817067 0 0.85846066 0.38749999 0.3125 0.53750002
		 0.3125 0.39374998 0.33125001 0.53125 0.33125001 0.43749994 0.4625001 0.48749989 0.4625001
		 0.39999998 0.35000002 0.52499998 0.35000002 0.44999993 0.48125011 0.4749999 0.48125011
		 0.40624997 0.36875004 0.51874995 0.36875004 0.43124995 0.44375008 0.49374989 0.44375008
		 0.41249996 0.38750005 0.51249993 0.38750005 0.42499995 0.42500007 0.49999988 0.42500007
		 0.41874996 0.40625006 0.5062499 0.40625006 0.57499981 0.3125 0.57499981 0.5 0.58124983
		 0.5 0.5874998 0.5 0.5874998 0.3125 0 0 1 0 0 0 0 0 1 0 1 0 0 0 0.45624992 0.48125011
		 0.46874991 0.48125011 0.44374993 0.4625001 0.4812499 0.4625001 0.46249992 0.48125011
		 -1.0587864e-16 0.85846066 0 0 1 0.81817061 0.39374998 0.3125 0.53125 0.3125 0.43749994
		 0.44375008 0.48749989 0.44375008 0.39999998 0.33125001 0.52499998 0.33125001 0.44999993
		 0.4625001 0.4749999 0.4625001 0.40624997 0.35000002 0.51874995 0.35000002 0.43124995
		 0.42500007 0.49374989 0.42500007 0.41249996 0.36875004 0.51249993 0.36875004 0.42499995
		 0.40625006 0.49999988 0.40625006 0.41874996 0.38750005 0.5062499 0.38750005 0 0 1
		 0 1 0 0 0 1 0 1 0 0 0 0.45624992 0.4625001 0.46874991 0.4625001 1 0.85846186 7.5426476e-07
		 0.81817079 0 0.81817085 1 0.81817108 0.9999997 0.81817102 0 0.85846204 0.44374993
		 0.44375008 0.4812499 0.44375008 8.8232504e-17 0.85846144 1 0 1 0.81817067 0.46249992
		 0.4625001 0.43749994 0.42500007 0.48749989 0.42500007 0.39999998 0.3125 0.52499998
		 0.3125 0.44999993 0.44375008 0.4749999 0.44375008 0.40624997 0.33125001 0.51874995
		 0.33125001 0.43124995 0.40625006 0.49374989 0.40625006 0.41249996 0.35000002 0.51249993
		 0.35000002 0.42499995 0.38750005 0.49999988 0.38750005 0.41874996 0.36875004 0.5062499
		 0.36875004 0.45624992 0.44375008 0.46874991 0.44375008 0 0 1 0 0 0 0 0 1 0 1 0 0
		 0 -3.3528306e-16 0.8584615 0 0 1 0.81817013 0.44374993 0.42500007 0.4812499 0.42500007
		 0.46249992 0.44375008 0.43749994 0.40625006 0.48749989 0.40625006 0.44999993 0.42500007
		 0.4749999 0.42500007 0.40624997 0.3125 0.51874995 0.3125 0.43124995 0.38750005 0.49374989
		 0.38750005 0.41249996 0.33125001 0.51249993 0.33125001 0.42499995 0.36875004 0.49999988
		 0.36875004 0.41874996 0.35000002 0.5062499 0.35000002 0.45624992 0.42500007 0.46874991
		 0.42500007 0 0.8584612 1 0 1 0.81817043 0.44374993 0.40625006 0.4812499 0.40625006
		 0.46249992 0.42500007 0 0 1 0 1 0 0 0 1 0 1 0 0 0 1 0.85846192 3.889412e-07 0.81817055
		 7.0089621e-17 0.81817061 0.56249982 0.3125 0.56249982 0.5;
	setAttr ".uvst[0].uvsp[1250:1499]" 0.56874985 0.5 0.57499981 0.49999997 0.5874998
		 0.5 0.59374976 0.5 0.59999979 0.5 0.59999979 0.3125 0.43749994 0.38750005 0.48749989
		 0.38750005 0.44999993 0.40625006 0.4749999 0.40625006 0.43124995 0.36875004 0.49374989
		 0.36875004 0.41249996 0.3125 0.51249993 0.3125 0.42499995 0.35000002 0.49999988 0.35000002
		 0.41874996 0.33125001 0.5062499 0.33125001 0.45624992 0.40625006 0.46874991 0.40625006
		 0 0.8584612 0 0 1 0.81817079 0.44374993 0.38750005 0.4812499 0.38750005 0.46249992
		 0.40625006 0 0 1 0 0 0 0 0 1 0 1 0 0 0 1 0.85846144 1.3432399e-06 0.81816995 1.4017887e-16
		 0.81817013 0.43749994 0.36875004 0.48749989 0.36875004 0.44999993 0.38750005 0.4749999
		 0.38750005 0.43124995 0.35000002 0.49374989 0.35000002 0.42499995 0.33125001 0.49999988
		 0.33125001 0.41874996 0.3125 0.5062499 0.3125 0.45624992 0.38750005 0.46874991 0.38750005
		 1.9411083e-16 0.85846114 1 0 1 0.81817114 0.54999983 0.3125 0.54999983 0.5 0.55624986
		 0.5 0.56249988 0.5 0.59999985 0.5 0.60624981 0.5 0.61249977 0.5 0.61249977 0.3125
		 0.44374993 0.36875004 0.4812499 0.36875004 0.46249992 0.38750005 0 0 1 0 1 0 0 0
		 1 0 1 0 0 0 0.43749994 0.35000002 0.48749989 0.35000002 0.44999993 0.36875004 0.4749999
		 0.36875004 0.43124995 0.33125001 0.49374989 0.33125001 0.42499995 0.3125 0.49999988
		 0.3125 -9.2644248e-17 0.85846269 0 0 1 0.81817126 0.45624992 0.36875004 0.46874991
		 0.36875004 1 0.85846066 5.9441072e-07 0.81817007 -2.9788105e-16 0.81817013 0.44374993
		 0.35000002 0.4812499 0.35000002 0.46249992 0.36875004 0 0 1 0 0 0 0 0 1 0 1 0 0 0
		 0.43749994 0.33125001 0.48749989 0.33125001 0.44999993 0.35000002 0.4749999 0.35000002
		 0.43124995 0.3125 0.49374989 0.3125 1.1029051e-16 0.85846227 1 0 1 0.81817126 0.45624992
		 0.35000002 0.46874991 0.35000002 0.44374993 0.33125001 0.4812499 0.33125001 0.46249992
		 0.35000002 0 0 1 0 1 0 0 0 1 0 1 0 0 0 1 0.8584612 1.9627687e-06 0.81817043 -8.761186e-17
		 0.81817067 0 0.85846293 0 0 1 0.81817192 0.43749994 0.3125 0.48749989 0.3125 0.44999993
		 0.33125001 0.4749999 0.33125001 0.53749985 0.3125 0.53749985 0.5 0.54374981 0.5 0.54999983
		 0.5 0.45624992 0.33125001 0.46874991 0.33125001 0 0 1 0 0 0 0 0 1 0 1 0 0 0 0 0.85846215
		 1 0 1 0.81817091 -1.22657e-16 0.8181715 0 0 1 0.85846293 0.44374993 0.3125 0.4812499
		 0.3125 0.46249992 0.33125001 0 0 1 0 1 0 0 0 1 0 1 0 0 0 0.44999993 0.3125 0.4749999
		 0.3125 1.4117206e-16 0.85846114 0 0 1 0 1 0.81817102 -1.4117178e-16 0.85846215 0
		 0 1 0 1 0.81817085 0.45624992 0.3125 0.46874991 0.3125 1 0 0 0 1 0 1 0 0 0 1 0.8584615
		 2.3291279e-06 0.81817043 3.1540275e-16 0.81817079 0.46249992 0.3125 0 0 1 0 1 0 0
		 0 0 0 1 0 0 0 1 0 0 0 0 0 1 0 1 0 0 0 1 0.85846114 3.4877368e-07 0.81817037 -1.5770179e-16
		 0.81817043 0.52499986 0.3125 0.52499986 0.5 0.53124988 0.5 0.53749985 0.5 0.375 0.3125
		 0.375 0.5 0.38124996 0.5 0.38749999 0.5 0.38749999 0.5 0.38749999 0.3125 1 0.8584612
		 2.809541e-06 0.81817091 0 0.81817126 1 0.85846227 1.10952e-06 0.81817096 -7.0089568e-17
		 0.81817114 0.61249977 0.5 0.61874974 0.5 0.62499976 0.5 0.62499976 0.5 0.62499976
		 0.3125 1 0.85846269 1.3061657e-06 0.81817174 0 0.81817192 0.51249987 0.3125 0.51249987
		 0.5 0.51874983 0.5 0.5249998 0.5 1 0.85846215 1.0854044e-06 0.81817114 0 0.81817126
		 0.39374998 0.5 0.39999998 0.5 0.39999998 0.5 0.39999998 0.3125 0.46249992 0.3125
		 0.46249992 0.5 0.46874991 0.5 0.4749999 0.5 0.4749999 0.3125 1 0.85846114 1.6445359e-06
		 0.81817067 -7.0089462e-17 0.81817091 1 0.8181715 0.99999994 0.81817144 1.0587866e-16
		 0.8584618 1 0.8584618 6.3438875e-07 0.81817073 0 0.81817085 1 0.85846215 1.7312786e-06
		 0.81817079 2.2779101e-16 0.81817102 0.49999988 0.3125 0.49999988 0.5 0.5062499 0.5
		 0.51249987 0.5 0.40624994 0.49999997 0.41249996 0.5;
	setAttr ".uvst[0].uvsp[1500:1522]" 0.41249996 0.5 0.41249996 0.3125 0.48749989
		 0.3125 0.48749989 0.5 0.49374986 0.49999997 0.49999985 0.49999997 0.41874993 0.5
		 0.42499995 0.5 0.42499995 0.5 0.42499995 0.3125 0.43124992 0.5 0.43749994 0.5 0.43749994
		 0.5 0.43749994 0.3125 0.44374993 0.5 0.44999996 0.5 0.44999993 0.5 0.44999993 0.3125
		 0.45624995 0.5 0.46249989 0.5 0.4749999 0.5 0.48124993 0.5 0.48749986 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 920 ".pt";
	setAttr ".pt[0]" -type "float3" -0.26459432 0.27224904 0.72165811 ;
	setAttr ".pt[1]" -type "float3" -0.26459432 0.27224904 0.49121001 ;
	setAttr ".pt[2]" -type "float3" 1.0478796 -0.045207139 -0.38217637 ;
	setAttr ".pt[3]" -type "float3" 0.96500945 -0.045207139 -0.56147206 ;
	setAttr ".pt[4]" -type "float3" -0.26459432 0.27224904 1.5117888 ;
	setAttr ".pt[5]" -type "float3" -0.26459432 0.27224904 1.4163337 ;
	setAttr ".pt[6]" -type "float3" 0.36192018 -0.045207139 -1.1019508 ;
	setAttr ".pt[7]" -type "float3" 0.16185439 -0.045207139 -1.1762177 ;
	setAttr ".pt[8]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[9]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[10]" -type "float3" -3.0317724 0.54929191 0.16719252 ;
	setAttr ".pt[11]" -type "float3" -3.5935147 0.54929191 0.14613166 ;
	setAttr ".pt[12]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[13]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[14]" -type "float3" 3.0643301 0.54929191 -0.14613174 ;
	setAttr ".pt[15]" -type "float3" 3.5441036 0.54929191 -0.12147249 ;
	setAttr ".pt[16]" -type "float3" -0.039175279 0 -0.53960967 ;
	setAttr ".pt[17]" -type "float3" 0.26459432 0 -0.54633594 ;
	setAttr ".pt[18]" -type "float3" 0.26459432 0 -0.49803221 ;
	setAttr ".pt[19]" -type "float3" 0.61006194 0 -0.49190068 ;
	setAttr ".pt[20]" -type "float3" 2.2064285 0 -1.0420527e-07 ;
	setAttr ".pt[21]" -type "float3" 2.1825223 0 0.08546555 ;
	setAttr ".pt[22]" -type "float3" -1.916605 0 0.077909216 ;
	setAttr ".pt[23]" -type "float3" -1.9437926 0 -9.4992075e-08 ;
	setAttr ".pt[24]" -type "float3" -3.1350112 0 -0.45462912 ;
	setAttr ".pt[25]" -type "float3" -3.1707911 0 7.7950247e-08 ;
	setAttr ".pt[26]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[27]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[28]" -type "float3" -0.26459312 0 -2.9061961 ;
	setAttr ".pt[29]" -type "float3" -0.71922219 0 -2.8704171 ;
	setAttr ".pt[30]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[31]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[32]" -type "float3" -2.1683502 0 2.620296 ;
	setAttr ".pt[33]" -type "float3" -1.7350075 0 2.8858488 ;
	setAttr ".pt[34]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[35]" -type "float3" 2.6212564 0 -1.4704124 ;
	setAttr ".pt[36]" -type "float3" 2.3557036 0 -1.9037557 ;
	setAttr ".pt[37]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[38]" -type "float3" 0.94702315 0 -0.47365683 ;
	setAttr ".pt[39]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[40]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[41]" -type "float3" -1.8357078 0 0.15390025 ;
	setAttr ".pt[42]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[43]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[44]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[45]" -type "float3" -1.265458 0 3.0803428 ;
	setAttr ".pt[46]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[47]" -type "float3" 2.8157501 0 -1.0008628 ;
	setAttr ".pt[48]" -type "float3" -0.26459432 0.27224904 0.9343369 ;
	setAttr ".pt[49]" -type "float3" 0.85186267 -0.045207139 -0.72694248 ;
	setAttr ".pt[50]" -type "float3" -0.26459432 0.27224904 1.2860044 ;
	setAttr ".pt[51]" -type "float3" 0.54655927 -0.045207139 -1.0005505 ;
	setAttr ".pt[52]" -type "float3" -0.90968275 0 0.61563808 ;
	setAttr ".pt[53]" -type "float3" -1.0885785 0 0.51175153 ;
	setAttr ".pt[54]" -type "float3" 1.1406077 0.43556577 0 ;
	setAttr ".pt[55]" -type "float3" 1.0386974 0.43556577 0 ;
	setAttr ".pt[56]" -type "float3" -0.70022243 0 0.70436549 ;
	setAttr ".pt[57]" -type "float3" 0.91937584 0.43556577 0 ;
	setAttr ".pt[58]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[59]" -type "float3" -1.402223 0 0.13619864 ;
	setAttr ".pt[60]" -type "float3" -1.3379186 0 0.26904368 ;
	setAttr ".pt[61]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[62]" -type "float3" -0.21086523 0 0.8280313 ;
	setAttr ".pt[63]" -type "float3" 0.056982175 0 0.85992455 ;
	setAttr ".pt[64]" -type "float3" 1.0984321 -0.045207139 -0.19347021 ;
	setAttr ".pt[65]" -type "float3" 2.1825216 0 -0.085465841 ;
	setAttr ".pt[66]" -type "float3" 2.1113882 0 -0.16882715 ;
	setAttr ".pt[67]" -type "float3" -0.048712052 -0.045207139 -1.2215219 ;
	setAttr ".pt[68]" -type "float3" 0.86465442 0 -0.5195964 ;
	setAttr ".pt[69]" -type "float3" 0.56836432 0 -0.53960967 ;
	setAttr ".pt[70]" -type "float3" -0.26459432 0.27224904 1.1240089 ;
	setAttr ".pt[71]" -type "float3" 0.71122468 -0.045207139 -0.87451339 ;
	setAttr ".pt[72]" -type "float3" -1.232505 0 0.39526391 ;
	setAttr ".pt[73]" -type "float3" 1.2225972 0.43556577 0 ;
	setAttr ".pt[74]" -type "float3" -0.46535549 0 0.77574909 ;
	setAttr ".pt[75]" -type "float3" 0.78558087 0.43556577 0 ;
	setAttr ".pt[76]" -type "float3" 1.6495712 -0.83654004 0.63446248 ;
	setAttr ".pt[77]" -type "float3" 1.5663007 -0.83654004 0.93211693 ;
	setAttr ".pt[78]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[79]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[80]" -type "float3" -0.29880601 -0.83654004 -1.8293821 ;
	setAttr ".pt[81]" -type "float3" -0.097771347 -0.83654004 -1.9526746 ;
	setAttr ".pt[82]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[83]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[84]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[85]" -type "float3" -3.0285516 0 0.89806402 ;
	setAttr ".pt[86]" -type "float3" -3.1350112 0 0.45462942 ;
	setAttr ".pt[87]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[88]" -type "float3" 0.19003648 0 -2.8704171 ;
	setAttr ".pt[89]" -type "float3" 0.63347125 0 -2.7639568 ;
	setAttr ".pt[90]" -type "float3" 1.939034 0 0.13277334 ;
	setAttr ".pt[91]" -type "float3" 1.7910407 0 0.17190287 ;
	setAttr ".pt[92]" -type "float3" 1.4526057 -0.83654004 1.2068194 ;
	setAttr ".pt[93]" -type "float3" -0.7302267 0 -0.2366038 ;
	setAttr ".pt[94]" -type "float3" -0.48872352 0 -0.26058286 ;
	setAttr ".pt[95]" -type "float3" -0.48433912 -0.83654004 -1.6610436 ;
	setAttr ".pt[96]" -type "float3" -1.9166036 0 -0.077909485 ;
	setAttr ".pt[97]" -type "float3" -0.080873728 0 -0.49190068 ;
	setAttr ".pt[98]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[99]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[100]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[101]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[102]" -type "float3" 1.6070901 0 0.20679909 ;
	setAttr ".pt[103]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[104]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[105]" -type "float3" -0.94560546 0 -0.20679951 ;
	setAttr ".pt[106]" -type "float3" 1.2826473 0.43556577 0 ;
	setAttr ".pt[107]" -type "float3" 0.64060742 0.43556577 0 ;
	setAttr ".pt[108]" -type "float3" 1.9947815 0 -0.24803135 ;
	setAttr ".pt[109]" -type "float3" 1.1461687 0 -0.48678893 ;
	setAttr ".pt[110]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[111]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[112]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[113]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[114]" -type "float3" -1.4238352 0 -2.3352468e-08 ;
	setAttr ".pt[115]" -type "float3" 0.33159131 0 0.8706435 ;
	setAttr ".pt[116]" -type "float3" -2.4018929 0.54929191 0.18413661 ;
	setAttr ".pt[117]" -type "float3" 3.9300935 0.54929191 -0.093822166 ;
	setAttr ".pt[118]" -type "float3" -0.7712639 0 3.1989882 ;
	setAttr ".pt[119]" -type "float3" 2.9343956 0 -0.50666875 ;
	setAttr ".pt[120]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[121]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[122]" -type "float3" 1.8355715 0 -0.32112822 ;
	setAttr ".pt[123]" -type "float3" 1.4059763 0 -0.44199517 ;
	setAttr ".pt[124]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[125]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[126]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[127]" -type "float3" 1.3192791 0.43556577 0 ;
	setAttr ".pt[128]" -type "float3" 0.48802486 0.43556577 0 ;
	setAttr ".pt[129]" -type "float3" -1.8357062 0 -0.15390049 ;
	setAttr ".pt[130]" -type "float3" -0.41783491 0 -0.47365683 ;
	setAttr ".pt[131]" -type "float3" 1.6376789 0 -0.38631791 ;
	setAttr ".pt[132]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[133]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[134]" -type "float3" -2.8540342 0 1.3193852 ;
	setAttr ".pt[135]" -type "float3" 1.0547926 0 -2.5894401 ;
	setAttr ".pt[136]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[137]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[138]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[139]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[140]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[141]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[142]" -type "float3" -1.402223 0 -0.13619867 ;
	setAttr ".pt[143]" -type "float3" 0.6062007 0 0.85992455 ;
	setAttr ".pt[144]" -type "float3" 1.3112869 -0.83654004 1.4518057 ;
	setAttr ".pt[145]" -type "float3" -0.64980209 -0.83654004 -1.4518054 ;
	setAttr ".pt[146]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[147]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[148]" -type "float3" -1.7030926 0 -0.22610195 ;
	setAttr ".pt[149]" -type "float3" -0.73799258 0 -0.44375002 ;
	setAttr ".pt[150]" -type "float3" 1.3917111 0 0.23660408 ;
	setAttr ".pt[151]" -type "float3" -1.1295562 0 -0.17190275 ;
	setAttr ".pt[152]" -type "float3" -0.26459399 0 3.2388635 ;
	setAttr ".pt[153]" -type "float3" 2.9742687 0 6.1776399e-07 ;
	setAttr ".pt[154]" -type "float3" 1.3315909 0.43556577 0 ;
	setAttr ".pt[155]" -type "float3" 0.33159012 0.43556577 0 ;
	setAttr ".pt[156]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[157]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[158]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[159]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[160]" -type "float3" -1.719386 0.54929191 0.19654658 ;
	setAttr ".pt[161]" -type "float3" 4.2127991 0.54929191 -0.063861832 ;
	setAttr ".pt[162]" -type "float3" -1.5220284 0 -0.29273602 ;
	setAttr ".pt[163]" -type "float3" -1.033463 0 -0.40291661 ;
	setAttr ".pt[173]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[174]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[175]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[176]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[177]" -type "float3" -1.2969714 0 -0.35216203 ;
	setAttr ".pt[184]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[185]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[186]" -type "float3" -1.3379186 0 -0.26904371 ;
	setAttr ".pt[187]" -type "float3" 0.87404799 0 0.8280313 ;
	setAttr ".pt[188]" -type "float3" -2.6157565 0 1.7082183 ;
	setAttr ".pt[189]" -type "float3" 1.4436276 0 -2.3511617 ;
	setAttr ".pt[190]" -type "float3" 2.9343934 0 0.50667048 ;
	setAttr ".pt[191]" -type "float3" 0.2420758 0 3.1989882 ;
	setAttr ".pt[192]" -type "float3" 1.3192791 0.43556577 0 ;
	setAttr ".pt[193]" -type "float3" 0.17515564 0.43556577 0 ;
	setAttr ".pt[194]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[195]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[202]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[203]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[204]" -type "float3" 1.145824 -0.83654004 1.6610439 ;
	setAttr ".pt[205]" -type "float3" -0.79112095 -0.83654004 -1.2068186 ;
	setAttr ".pt[206]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[207]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[208]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[209]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[210]" -type "float3" 1.150208 0 0.26058263 ;
	setAttr ".pt[211]" -type "float3" -1.2775496 0 -0.13277349 ;
	setAttr ".pt[212]" -type "float3" -1.0010576 0.54929191 0.20411716 ;
	setAttr ".pt[213]" -type "float3" 4.3852534 0.54929191 -0.032328874 ;
	setAttr ".pt[214]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[215]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[216]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[217]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[224]" -type "float3" 2.815748 0 1.0008643 ;
	setAttr ".pt[225]" -type "float3" 0.73626977 0 3.0803428 ;
	setAttr ".pt[226]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[227]" -type "float3" -1.232505 0 -0.39526391 ;
	setAttr ".pt[228]" -type "float3" 1.1285385 0 0.77574909 ;
	setAttr ".pt[229]" -type "float3" 1.2826473 0.43556577 0 ;
	setAttr ".pt[230]" -type "float3" 0.022573076 0.43556577 0 ;
	setAttr ".pt[231]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[232]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[233]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[234]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[235]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[236]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[237]" -type "float3" -2.3195872 0 2.0549924 ;
	setAttr ".pt[238]" -type "float3" 1.7903981 0 -2.0549924 ;
	setAttr ".pt[239]" -type "float3" 2.6212544 0 1.4704137 ;
	setAttr ".pt[240]" -type "float3" 1.2058192 0 2.885849 ;
	setAttr ".pt[241]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[242]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[243]" -type "float3" -0.26459429 0.54929191 0.20666152 ;
	setAttr ".pt[244]" -type "float3" 4.4432106 0.54929191 3.9417529e-08 ;
	setAttr ".pt[247]" -type "float3" 0.96029061 -0.83654004 1.8293821 ;
	setAttr ".pt[248]" -type "float3" -0.90481567 -0.83654004 -0.93211609 ;
	setAttr ".pt[249]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[250]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[251]" -type "float3" 0.88852674 0 0.27814484 ;
	setAttr ".pt[252]" -type "float3" -1.3859416 0 -0.090374663 ;
	setAttr ".pt[257]" -type "float3" 2.3557017 0 1.9037567 ;
	setAttr ".pt[258]" -type "float3" 1.6391621 0 2.6202965 ;
	setAttr ".pt[259]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[260]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[261]" -type "float3" 2.0256279 0 2.2902231 ;
	setAttr ".pt[262]" -type "float3" 1.2225972 0.43556577 0 ;
	setAttr ".pt[263]" -type "float3" -0.12240057 0.43556577 0 ;
	setAttr ".pt[264]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[265]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[266]" -type "float3" -1.0885787 0 -0.51175147 ;
	setAttr ".pt[267]" -type "float3" 1.3634051 0 0.70436543 ;
	setAttr ".pt[268]" -type "float3" 4.3852506 0.54929191 0.032329012 ;
	setAttr ".pt[269]" -type "float3" 0.47186905 0.54929191 0.20411716 ;
	setAttr ".pt[272]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[273]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[274]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[275]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[280]" -type "float3" -1.9728134 0 2.3511617 ;
	setAttr ".pt[281]" -type "float3" 2.0865679 0 -1.7082181 ;
	setAttr ".pt[282]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[283]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[284]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[285]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[286]" -type "float3" 0.61311126 0 0.28885785 ;
	setAttr ".pt[287]" -type "float3" -1.4520627 0 -0.045750469 ;
	setAttr ".pt[288]" -type "float3" 0.75925648 -0.83654004 1.9526747 ;
	setAttr ".pt[289]" -type "float3" -0.98808694 -0.83654004 -0.6344617 ;
	setAttr ".pt[290]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[291]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[292]" -type "float3" 4.2127967 0.54929191 0.063861862 ;
	setAttr ".pt[293]" -type "float3" 1.1901978 0.54929191 0.19654658 ;
	setAttr ".pt[294]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[295]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[296]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[299]" -type "float3" 1.1406077 0.43556577 0 ;
	setAttr ".pt[300]" -type "float3" -0.25619534 0.43556577 0 ;
	setAttr ".pt[301]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[302]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[303]" -type "float3" -0.90968281 0 -0.61563802 ;
	setAttr ".pt[304]" -type "float3" 1.5728656 0 0.6156379 ;
	setAttr ".pt[305]" -type "float3" 3.9300916 0.54929191 0.093822367 ;
	setAttr ".pt[306]" -type "float3" 1.872705 0.54929191 0.18413654 ;
	setAttr ".pt[311]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[312]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[313]" -type "float3" 0.33074275 0 0.29245874 ;
	setAttr ".pt[314]" -type "float3" -1.4742846 0 5.5782063e-08 ;
	setAttr ".pt[315]" -type "float3" -1.5839785 0 2.5894401 ;
	setAttr ".pt[316]" -type "float3" 2.3248463 0 -1.3193846 ;
	setAttr ".pt[317]" -type "float3" 0.5476703 -0.83654004 2.0278859 ;
	setAttr ".pt[318]" -type "float3" -1.0388842 -0.83654004 -0.32118469 ;
	setAttr ".pt[319]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[320]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[321]" -type "float3" 3.5441003 0.54929191 0.1214724 ;
	setAttr ".pt[322]" -type "float3" 2.5025842 0.54929191 0.1671928 ;
	setAttr ".pt[323]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[324]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[325]" -type "float3" 3.064327 0.54929191 0.14613162 ;
	setAttr ".pt[328]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[329]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[330]" -type "float3" -1.4520614 0 0.045750678 ;
	setAttr ".pt[331]" -type "float3" 0.048374277 0 0.28885785 ;
	setAttr ".pt[332]" -type "float3" 1.0386976 0.43556577 0 ;
	setAttr ".pt[333]" -type "float3" -0.37551692 0.43556577 0 ;
	setAttr ".pt[334]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[335]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[336]" -type "float3" -0.70022273 0 -0.70436543 ;
	setAttr ".pt[337]" -type "float3" 1.7517612 0 0.51175123 ;
	setAttr ".pt[338]" -type "float3" 0.33074284 -0.83654004 2.0531638 ;
	setAttr ".pt[339]" -type "float3" -1.0559559 -0.83654004 3.9160966e-07 ;
	setAttr ".pt[344]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[345]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[346]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[347]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[348]" -type "float3" -1.1626577 0 2.763957 ;
	setAttr ".pt[349]" -type "float3" 2.4993625 0 -0.89806366 ;
	setAttr ".pt[350]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[351]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[352]" -type "float3" -1.3859401 0 0.090374649 ;
	setAttr ".pt[353]" -type "float3" -0.22704142 0 0.27814484 ;
	setAttr ".pt[354]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[357]" -type "float3" -1.0388834 -0.83654004 0.32118583 ;
	setAttr ".pt[358]" -type "float3" 0.11381529 -0.83654004 2.0278859 ;
	setAttr ".pt[359]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[360]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[361]" -type "float3" -1.2775483 0 0.13277343 ;
	setAttr ".pt[362]" -type "float3" -0.48872247 0 0.26058275 ;
	setAttr ".pt[363]" -type "float3" 0.91937584 0.43556577 0 ;
	setAttr ".pt[364]" -type "float3" -0.47742715 0.43556577 0 ;
	setAttr ".pt[365]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[366]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[367]" -type "float3" -0.46535584 0 -0.77574909 ;
	setAttr ".pt[368]" -type "float3" 1.8956876 0 0.39526361 ;
	setAttr ".pt[369]" -type "float3" -0.7192235 0 2.8704176 ;
	setAttr ".pt[370]" -type "float3" 2.6058226 0 -0.45462817 ;
	setAttr ".pt[371]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[372]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[377]" -type "float3" -1.1295551 0 0.17190279 ;
	setAttr ".pt[378]" -type "float3" -0.73022574 0 0.23660415 ;
	setAttr ".pt[379]" -type "float3" -0.98808622 -0.83654004 0.63446259 ;
	setAttr ".pt[380]" -type "float3" -0.097770721 -0.83654004 1.9526747 ;
	setAttr ".pt[381]" -type "float3" -0.94560432 0 0.20679949 ;
	setAttr ".pt[382]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[383]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[386]" -type "float3" -0.90481502 -0.83654004 0.93211704 ;
	setAttr ".pt[387]" -type "float3" -0.2988053 -0.83654004 1.8293822 ;
	setAttr ".pt[388]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[389]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[390]" -type "float3" -0.26459414 0 2.9061961 ;
	setAttr ".pt[391]" -type "float3" 2.6416016 0 5.5431292e-07 ;
	setAttr ".pt[392]" -type "float3" 0.78558123 0.43556577 0 ;
	setAttr ".pt[393]" -type "float3" -0.55941671 0.43556577 0 ;
	setAttr ".pt[394]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[395]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[396]" -type "float3" -0.2108656 0 -0.82803136 ;
	setAttr ".pt[397]" -type "float3" 2.001101 0 0.26904336 ;
	setAttr ".pt[398]" -type "float3" -0.79111993 -0.83654004 1.2068195 ;
	setAttr ".pt[399]" -type "float3" -0.48433846 -0.83654004 1.6610444 ;
	setAttr ".pt[404]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[405]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[406]" -type "float3" -0.64980137 -0.83654004 1.4518058 ;
	setAttr ".pt[407]" -type "float3" 2.6058216 0 0.45462978 ;
	setAttr ".pt[408]" -type "float3" 0.19003534 0 2.8704176 ;
	setAttr ".pt[409]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[410]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[413]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[414]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[415]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[416]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[417]" -type "float3" 0.056981593 0 -0.85992455 ;
	setAttr ".pt[418]" -type "float3" 2.0654054 0 0.13619833 ;
	setAttr ".pt[419]" -type "float3" 0.6406076 0.43556577 0 ;
	setAttr ".pt[420]" -type "float3" -0.61946678 0.43556577 0 ;
	setAttr ".pt[421]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[422]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[423]" -type "float3" 2.4993629 0 0.89806485 ;
	setAttr ".pt[424]" -type "float3" 0.6334703 0 2.763957 ;
	setAttr ".pt[425]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[430]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[431]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[432]" -type "float3" 2.324846 0 1.3193852 ;
	setAttr ".pt[433]" -type "float3" 1.0547905 0 2.5894406 ;
	setAttr ".pt[434]" -type "float3" 0.3315908 0 -0.8706435 ;
	setAttr ".pt[435]" -type "float3" 2.0870166 0 -1.6606201e-07 ;
	setAttr ".pt[438]" -type "float3" 0.48802513 0.43556577 0 ;
	setAttr ".pt[439]" -type "float3" -0.65609854 0.43556577 0 ;
	setAttr ".pt[440]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[441]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[442]" -type "float3" 2.0865672 0 1.7082186 ;
	setAttr ".pt[443]" -type "float3" 1.4436243 0 2.351162 ;
	setAttr ".pt[444]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[445]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[446]" -type "float3" 1.7903981 0 2.0549924 ;
	setAttr ".pt[451]" -type "float3" 2.0654044 0 -0.1361988 ;
	setAttr ".pt[452]" -type "float3" 0.60619992 0 -0.85992455 ;
	setAttr ".pt[453]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[454]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[455]" -type "float3" 0.33159065 0.43556577 0 ;
	setAttr ".pt[456]" -type "float3" -0.66840953 0.43556577 0 ;
	setAttr ".pt[457]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[458]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[461]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[462]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[463]" -type "float3" 2.0011001 0 -0.26904377 ;
	setAttr ".pt[464]" -type "float3" 0.87404722 0 -0.82803136 ;
	setAttr ".pt[465]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[468]" -type "float3" -0.65609789 0.43556577 0 ;
	setAttr ".pt[469]" -type "float3" 0.1751561 0.43556577 0 ;
	setAttr ".pt[472]" -type "float3" 1.8956866 0 -0.395264 ;
	setAttr ".pt[473]" -type "float3" 1.1285377 0 -0.77574921 ;
	setAttr ".pt[474]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[475]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[478]" -type "float3" 1.7517601 0 -0.51175153 ;
	setAttr ".pt[479]" -type "float3" 1.3634044 0 -0.70436555 ;
	setAttr ".pt[480]" -type "float3" -0.61946607 0.43556577 0 ;
	setAttr ".pt[481]" -type "float3" 0.022573519 0.43556577 0 ;
	setAttr ".pt[482]" -type "float3" 1.5728642 0 -0.61563808 ;
	setAttr ".pt[485]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[486]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[489]" -type "float3" -0.55941612 0.43556577 0 ;
	setAttr ".pt[490]" -type "float3" -0.12240005 0.43556577 0 ;
	setAttr ".pt[493]" -type "float3" -0.47742662 0.43556577 0 ;
	setAttr ".pt[494]" -type "float3" -0.2561948 0.43556577 0 ;
	setAttr ".pt[495]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[496]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[497]" -type "float3" -0.37551636 0.43556577 0 ;
	setAttr ".pt[504]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[505]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[506]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[507]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[510]" -type "float3" 0.33159065 0 0 ;
	setAttr ".pt[534]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[535]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[536]" -type "float3" 2.5025873 0.54929191 -0.16719244 ;
	setAttr ".pt[537]" -type "float3" -4.0732884 0.54929191 0.12147245 ;
	setAttr ".pt[538]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[539]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[540]" -type "float3" -0.26459432 -0.045207139 -1.2367482 ;
	setAttr ".pt[541]" -type "float3" 1.115422 -0.045207139 -2.3589091e-07 ;
	setAttr ".pt[542]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[543]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[544]" -type "float3" 0.11381468 -0.83654004 -2.0278859 ;
	setAttr ".pt[545]" -type "float3" 1.7003692 -0.83654004 0.32118562 ;
	setAttr ".pt[546]" -type "float3" -0.22704223 0 -0.27814451 ;
	setAttr ".pt[547]" -type "float3" 2.0474255 0 0.090374738 ;
	setAttr ".pt[548]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[549]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[550]" -type "float3" 0.16842222 0.89453268 0.55205357 ;
	setAttr ".pt[551]" -type "float3" 0.25632542 0.89453268 0.4825125 ;
	setAttr ".pt[552]" -type "float3" 0.33140177 0.89453268 0.4010902 ;
	setAttr ".pt[553]" -type "float3" -0.26459432 0.27224904 1.5700184 ;
	setAttr ".pt[554]" -type "float3" -0.26459432 0.27224904 0.24866667 ;
	setAttr ".pt[555]" -type "float3" -1.1626565 0 -2.7639568 ;
	setAttr ".pt[556]" -type "float3" -3.0285516 0 -0.89806402 ;
	setAttr ".pt[557]" -type "float3" -0.26459432 -0.67756528 0 ;
	setAttr ".pt[558]" -type "float3" 0.069856822 0.89453268 0.60800093 ;
	setAttr ".pt[559]" -type "float3" 0.39180261 0.89453268 0.30979192 ;
	setAttr ".pt[560]" -type "float3" -1.7030941 0 0.22610174 ;
	setAttr ".pt[561]" -type "float3" 1.2671809 0 -0.44374996 ;
	setAttr ".pt[562]" -type "float3" 2.1113896 0 0.16882691 ;
	setAttr ".pt[563]" -type "float3" -0.33546522 0 -0.5195964 ;
	setAttr ".pt[564]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[565]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[566]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[567]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[568]" -type "float3" 1.0984329 -0.045207139 0.19346954 ;
	setAttr ".pt[569]" -type "float3" -0.48047641 -0.045207139 -1.2215219 ;
	setAttr ".pt[570]" -type "float3" 2.0256302 0 -2.2902219 ;
	setAttr ".pt[571]" -type "float3" -2.5548162 0 2.2902224 ;
	setAttr ".pt[572]" -type "float3" -0.036944062 0.89453268 0.6489777 ;
	setAttr ".pt[573]" -type "float3" 0.43604085 0.89453268 0.21086565 ;
	setAttr ".pt[574]" -type "float3" -1.5839789 0 -2.5894401 ;
	setAttr ".pt[575]" -type "float3" -2.8540342 0 -1.3193852 ;
	setAttr ".pt[576]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[577]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[578]" -type "float3" 0.33074242 -0.83654004 -2.0531638 ;
	setAttr ".pt[579]" -type "float3" 1.7174418 -0.83654004 5.5070128e-08 ;
	setAttr ".pt[580]" -type "float3" -0.26459432 0.27224904 1.5895885 ;
	setAttr ".pt[581]" -type "float3" -0.26459432 0.27224904 3.0318984e-07 ;
	setAttr ".pt[582]" -type "float3" 1.8727078 0.54929191 -0.18413667 ;
	setAttr ".pt[583]" -type "float3" -4.45928 0.54929191 0.093822412 ;
	setAttr ".pt[584]" -type "float3" 0.048373543 0 -0.28885788 ;
	setAttr ".pt[585]" -type "float3" 2.1135473 0 0.045750588 ;
	setAttr ".pt[586]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[587]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[588]" -type "float3" -1.9728131 0 -2.3511615 ;
	setAttr ".pt[589]" -type "float3" -2.6157565 0 -1.7082186 ;
	setAttr ".pt[590]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[591]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[592]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[593]" -type "float3" -0.14935026 0.89453268 0.67397416 ;
	setAttr ".pt[594]" -type "float3" 0.4630273 0.89453268 0.10674714 ;
	setAttr ".pt[595]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[596]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[597]" -type "float3" -2.319586 0 -2.0549924 ;
	setAttr ".pt[598]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[599]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[600]" -type "float3" 1.9947823 0 0.24803115 ;
	setAttr ".pt[601]" -type "float3" -0.61697978 0 -0.4867889 ;
	setAttr ".pt[602]" -type "float3" 1.0478805 -0.045207139 0.38217586 ;
	setAttr ".pt[603]" -type "float3" -0.6910429 -0.045207139 -1.1762177 ;
	setAttr ".pt[604]" -type "float3" -1.5220296 0 0.2927359 ;
	setAttr ".pt[605]" -type "float3" 1.562651 0 -0.40291649 ;
	setAttr ".pt[606]" -type "float3" 0.54766995 -0.83654004 -2.0278859 ;
	setAttr ".pt[607]" -type "float3" 1.7003692 -0.83654004 -0.32118556 ;
	setAttr ".pt[608]" -type "float3" -0.26459432 0.27224904 -0.24866585 ;
	setAttr ".pt[609]" -type "float3" -0.26459432 0.27224904 1.5700184 ;
	setAttr ".pt[610]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[611]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[612]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[613]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[614]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[615]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[616]" -type "float3" 1.639164 0 -2.620296 ;
	setAttr ".pt[617]" -type "float3" -2.8848903 0 1.9037565 ;
	setAttr ".pt[618]" -type "float3" 0.330742 0 -0.29245874 ;
	setAttr ".pt[619]" -type "float3" 2.1357703 0 7.8443403e-09 ;
	setAttr ".pt[620]" -type "float3" -0.26459435 0.89453268 0.68237531 ;
	setAttr ".pt[621]" -type "float3" 0.47209719 0.89453268 1.3015276e-07 ;
	setAttr ".pt[622]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[623]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[624]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[625]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[626]" -type "float3" 1.1902001 0.54929191 -0.19654664 ;
	setAttr ".pt[627]" -type "float3" -4.7419863 0.54929191 0.063861847 ;
	setAttr ".pt[628]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[629]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[630]" -type "float3" 0.82630444 0 1.1373106 ;
	setAttr ".pt[631]" -type "float3" 0.99404562 0 0.99404573 ;
	setAttr ".pt[632]" -type "float3" 1.1373103 0 0.82630444 ;
	setAttr ".pt[633]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[634]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[635]" -type "float3" 0.75925624 -0.83654004 -1.9526746 ;
	setAttr ".pt[636]" -type "float3" 1.6495712 -0.83654004 -0.63446248 ;
	setAttr ".pt[637]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[638]" -type "float3" 0.63821661 0 1.2525707 ;
	setAttr ".pt[639]" -type "float3" 1.2525704 0 0.63821667 ;
	setAttr ".pt[640]" -type "float3" 0.96501046 -0.045207139 0.56147158 ;
	setAttr ".pt[641]" -type "float3" -0.89110863 -0.045207139 -1.1019508 ;
	setAttr ".pt[642]" -type "float3" -0.26459432 0.27224904 -0.49120939 ;
	setAttr ".pt[643]" -type "float3" -0.26459432 0.27224904 1.5117888 ;
	setAttr ".pt[644]" -type "float3" 1.8355721 0 0.32112807 ;
	setAttr ".pt[645]" -type "float3" -0.87678701 0 -0.44199502 ;
	setAttr ".pt[646]" -type "float3" 0.6131106 0 -0.28885788 ;
	setAttr ".pt[647]" -type "float3" 2.1135473 0 -0.045750577 ;
	setAttr ".pt[648]" -type "float3" 0.46302769 0.89453268 -0.10674676 ;
	setAttr ".pt[649]" -type "float3" -0.37983829 0.89453268 0.67397416 ;
	setAttr ".pt[650]" -type "float3" -1.2969725 0 0.35216188 ;
	setAttr ".pt[651]" -type "float3" 1.8261597 0 -0.35216194 ;
	setAttr ".pt[652]" -type "float3" 0.82630444 0 1.1373106 ;
	setAttr ".pt[653]" -type "float3" 0.99404562 0 0.99404573 ;
	setAttr ".pt[654]" -type "float3" 1.1373103 0 0.82630444 ;
	setAttr ".pt[655]" -type "float3" 0.43441397 0 1.3369887 ;
	setAttr ".pt[656]" -type "float3" 1.3369883 0 0.43441409 ;
	setAttr ".pt[657]" -type "float3" 0.96029043 -0.83654004 -1.8293821 ;
	setAttr ".pt[658]" -type "float3" 1.5663007 -0.83654004 -0.93211693 ;
	setAttr ".pt[659]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[660]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[661]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[662]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[663]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[664]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[665]" -type "float3" 0.63821661 0 1.2525707 ;
	setAttr ".pt[666]" -type "float3" 1.2525704 0 0.63821667 ;
	setAttr ".pt[667]" -type "float3" 1.2058208 0 -2.8858488 ;
	setAttr ".pt[668]" -type "float3" -3.1504431 0 1.4704137 ;
	setAttr ".pt[669]" -type "float3" 0.47187099 0.54929191 -0.204117 ;
	setAttr ".pt[670]" -type "float3" -4.9144387 0.54929191 0.032328963 ;
	setAttr ".pt[671]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[672]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[673]" -type "float3" 1.145824 -0.83654004 -1.661044 ;
	setAttr ".pt[674]" -type "float3" 1.4526055 -0.83654004 -1.2068195 ;
	setAttr ".pt[675]" -type "float3" 0.43441397 0 1.3369887 ;
	setAttr ".pt[676]" -type "float3" 1.3369883 0 0.43441409 ;
	setAttr ".pt[677]" -type "float3" 0.2199146 0 1.3884854 ;
	setAttr ".pt[678]" -type "float3" 1.3884851 0 0.21991464 ;
	setAttr ".pt[679]" -type "float3" 0.88852632 0 -0.27814451 ;
	setAttr ".pt[680]" -type "float3" 2.0474255 0 -0.09037476 ;
	setAttr ".pt[681]" -type "float3" 1.3112868 -0.83654004 -1.4518057 ;
	setAttr ".pt[682]" -type "float3" -0.26459432 0.27224904 -0.72165769 ;
	setAttr ".pt[683]" -type "float3" -0.26459432 0.27224904 1.4163337 ;
	setAttr ".pt[684]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[685]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[686]" -type "float3" 0.43604144 0.89453268 -0.21086538 ;
	setAttr ".pt[687]" -type "float3" -0.49224454 0.89453268 0.6489777 ;
	setAttr ".pt[688]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[689]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[690]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[691]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[692]" -type "float3" 0.85186327 -0.045207139 0.72694206 ;
	setAttr ".pt[693]" -type "float3" -1.0757478 -0.045207139 -1.0005507 ;
	setAttr ".pt[694]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[695]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[696]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[697]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[698]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[699]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[700]" -type "float3" 1.6376798 0 0.38631779 ;
	setAttr ".pt[701]" -type "float3" -1.1084895 0 -0.38631782 ;
	setAttr ".pt[702]" -type "float3" 0.2199146 0 1.3884854 ;
	setAttr ".pt[703]" -type "float3" 1.3884851 0 0.21991464 ;
	setAttr ".pt[704]" -type "float3" 1.1502078 0 -0.26058263 ;
	setAttr ".pt[705]" -type "float3" 1.939034 0 -0.13277334 ;
	setAttr ".pt[706]" -type "float3" -1.0334643 0 0.40291646 ;
	setAttr ".pt[707]" -type "float3" 2.0512166 0 -0.29273599 ;
	setAttr ".pt[708]" -type "float3" -0.26459289 0.54929191 -0.20666152 ;
	setAttr ".pt[709]" -type "float3" -4.9723988 0.54929191 5.5430944e-09 ;
	setAttr ".pt[710]" -type "float3" 1.2149805e-07 0 1.4057928 ;
	setAttr ".pt[711]" -type "float3" 1.4057927 0 2.6813362e-07 ;
	setAttr ".pt[712]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[713]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[714]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[715]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[716]" -type "float3" 0.73627126 0 -3.0803425 ;
	setAttr ".pt[717]" -type "float3" -3.3449366 0 1.000864 ;
	setAttr ".pt[718]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[719]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[720]" -type "float3" 1.3917111 0 -0.23660423 ;
	setAttr ".pt[721]" -type "float3" 1.7910402 0 -0.17190279 ;
	setAttr ".pt[722]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[723]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[724]" -type "float3" 1.60709 0 -0.20679916 ;
	setAttr ".pt[725]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[726]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[727]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[728]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[729]" -type "float3" 1.2149805e-07 0 1.4057928 ;
	setAttr ".pt[730]" -type "float3" 1.4057927 0 2.6813362e-07 ;
	setAttr ".pt[731]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[732]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[733]" -type "float3" 0.39180297 0.89453268 -0.30979177 ;
	setAttr ".pt[734]" -type "float3" -0.59904522 0.89453268 0.60800099 ;
	setAttr ".pt[735]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[736]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[737]" -type "float3" -0.26459432 0.27224904 -0.93433642 ;
	setAttr ".pt[738]" -type "float3" -0.26459432 0.27224904 1.2860043 ;
	setAttr ".pt[739]" -type "float3" -1.0010554 0.54929191 -0.204117 ;
	setAttr ".pt[740]" -type "float3" -4.9144387 0.54929191 -0.032328978 ;
	setAttr ".pt[741]" -type "float3" 1.3884859 0 -0.21991393 ;
	setAttr ".pt[742]" -type "float3" -0.21991435 0 1.3884854 ;
	setAttr ".pt[743]" -type "float3" 0.71122545 -0.045207139 0.87451291 ;
	setAttr ".pt[744]" -type "float3" -1.2404132 -0.045207139 -0.87451303 ;
	setAttr ".pt[745]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[746]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[747]" -type "float3" 1.4059768 0 0.44199502 ;
	setAttr ".pt[748]" -type "float3" -1.3063825 0 -0.32112822 ;
	setAttr ".pt[749]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[750]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[751]" -type "float3" -0.7379939 0 0.44374987 ;
	setAttr ".pt[752]" -type "float3" 2.2322812 0 -0.22610191 ;
	setAttr ".pt[753]" -type "float3" 0.24207701 0 -3.198988 ;
	setAttr ".pt[754]" -type "float3" -3.463582 0 0.50667012 ;
	setAttr ".pt[755]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[756]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[757]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[758]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[759]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[760]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[761]" -type "float3" 1.3884859 0 -0.21991393 ;
	setAttr ".pt[762]" -type "float3" -0.21991435 0 1.3884854 ;
	setAttr ".pt[763]" -type "float3" -1.7193856 0.54929191 -0.19654664 ;
	setAttr ".pt[764]" -type "float3" -4.7419863 0.54929191 -0.063861862 ;
	setAttr ".pt[765]" -type "float3" 0.3307429 0 0 ;
	setAttr ".pt[766]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[767]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[768]" -type "float3" 1.3369894 0 -0.43441334 ;
	setAttr ".pt[769]" -type "float3" -0.43441388 0 1.3369887 ;
	setAttr ".pt[770]" -type "float3" 0.33140209 0.89453268 -0.40109017 ;
	setAttr ".pt[771]" -type "float3" -0.69761062 0.89453268 0.55205345 ;
	setAttr ".pt[772]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[773]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[774]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[775]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[776]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[777]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[778]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[779]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[780]" -type "float3" -0.26459432 0.27224904 -1.1240085 ;
	setAttr ".pt[781]" -type "float3" -0.26459432 0.27224904 1.1240088 ;
	setAttr ".pt[782]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[783]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[784]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[785]" -type "float3" -2.4018917 0.54929191 -0.18413661 ;
	setAttr ".pt[786]" -type "float3" -4.45928 0.54929191 -0.093822412 ;
	setAttr ".pt[787]" -type "float3" 0.54655981 -0.045207139 1.0005505 ;
	setAttr ".pt[788]" -type "float3" -1.3810515 -0.045207139 -0.72694254 ;
	setAttr ".pt[789]" -type "float3" -0.26459303 0 -3.2388635 ;
	setAttr ".pt[790]" -type "float3" -3.5034573 0 8.6873044e-08 ;
	setAttr ".pt[791]" -type "float3" 1.14617 0 0.48678884 ;
	setAttr ".pt[792]" -type "float3" -1.4655921 0 -0.24803133 ;
	setAttr ".pt[793]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[794]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[795]" -type "float3" -0.41783601 0 0.4736568 ;
	setAttr ".pt[796]" -type "float3" 2.3648949 0 -0.15390043 ;
	setAttr ".pt[797]" -type "float3" 1.3369894 0 -0.43441334 ;
	setAttr ".pt[798]" -type "float3" -0.43441388 0 1.3369887 ;
	setAttr ".pt[799]" -type "float3" -3.0317719 0.54929191 -0.16719271 ;
	setAttr ".pt[800]" -type "float3" -4.0732884 0.54929191 -0.1214724 ;
	setAttr ".pt[801]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[802]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[803]" -type "float3" -3.593514 0.54929191 -0.14613159 ;
	setAttr ".pt[804]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[805]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[806]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[807]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[808]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[809]" -type "float3" 1.2525715 0 -0.6382162 ;
	setAttr ".pt[810]" -type "float3" -0.63821644 0 1.2525706 ;
	setAttr ".pt[811]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[812]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[813]" -type "float3" -0.77126306 0 -3.198988 ;
	setAttr ".pt[814]" -type "float3" -3.463582 0 -0.50666988 ;
	setAttr ".pt[815]" -type "float3" 0.2563259 0.89453268 -0.48251227 ;
	setAttr ".pt[816]" -type "float3" -0.78551382 0.89453268 0.48251235 ;
	setAttr ".pt[817]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[818]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[819]" -type "float3" -0.26459432 0.27224904 -1.2860042 ;
	setAttr ".pt[820]" -type "float3" -0.26459432 0.27224904 0.9343369 ;
	setAttr ".pt[821]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[822]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[823]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[824]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[825]" -type "float3" 0.36192077 -0.045207139 1.1019506 ;
	setAttr ".pt[826]" -type "float3" -1.4941982 -0.045207139 -0.56147206 ;
	setAttr ".pt[827]" -type "float3" -0.0808746 0 0.49190062 ;
	setAttr ".pt[828]" -type "float3" 2.4457922 0 -0.077909417 ;
	setAttr ".pt[829]" -type "float3" 0.86465526 0 0.51959634 ;
	setAttr ".pt[830]" -type "float3" -1.5821995 0 -0.16882709 ;
	setAttr ".pt[831]" -type "float3" 1.2525715 0 -0.6382162 ;
	setAttr ".pt[832]" -type "float3" -0.63821644 0 1.2525706 ;
	setAttr ".pt[833]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[834]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[835]" -type "float3" -1.2654574 0 -3.0803425 ;
	setAttr ".pt[836]" -type "float3" -3.3449366 0 -1.0008641 ;
	setAttr ".pt[837]" -type "float3" -0.26459432 0 0 ;
	setAttr ".pt[838]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[839]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[840]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[841]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[842]" -type "float3" 1.1373111 0 -0.82630408 ;
	setAttr ".pt[843]" -type "float3" -0.82630432 0 1.1373103 ;
	setAttr ".pt[844]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[845]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[846]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[847]" -type "float3" 0 -3.0615976 0 ;
	setAttr ".pt[848]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[849]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[850]" -type "float3" -1.7350067 0 -2.8858488 ;
	setAttr ".pt[851]" -type "float3" -3.1504431 0 -1.4704137 ;
	setAttr ".pt[852]" -type "float3" 0.16842246 0.89453268 -0.55205321 ;
	setAttr ".pt[853]" -type "float3" -0.86059022 0.89453268 0.40109017 ;
	setAttr ".pt[854]" -type "float3" 0.26459336 0 0.49803221 ;
	setAttr ".pt[855]" -type "float3" 2.472981 0 -1.335826e-08 ;
	setAttr ".pt[856]" -type "float3" -0.26459432 0.27224904 -1.4163337 ;
	setAttr ".pt[857]" -type "float3" -0.26459432 0.27224904 0.72165811 ;
	setAttr ".pt[858]" -type "float3" 0.5683651 0 0.53960967 ;
	setAttr ".pt[859]" -type "float3" -1.6533327 0 -0.085465789 ;
	setAttr ".pt[860]" -type "float3" 0.16185516 -0.045207139 1.1762176 ;
	setAttr ".pt[861]" -type "float3" -1.5770682 -0.045207139 -0.38217631 ;
	setAttr ".pt[862]" -type "float3" 1.1373111 0 -0.82630408 ;
	setAttr ".pt[863]" -type "float3" -0.82630432 0 1.1373103 ;
	setAttr ".pt[864]" -type "float3" -2.1683497 0 -2.6202962 ;
	setAttr ".pt[865]" -type "float3" -2.8848898 0 -1.9037567 ;
	setAttr ".pt[866]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[867]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[868]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[869]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[870]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[871]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[872]" -type "float3" -2.554816 0 -2.2902229 ;
	setAttr ".pt[873]" -type "float3" 0.61006135 0 0.49190062 ;
	setAttr ".pt[874]" -type "float3" 2.4457922 0 0.077909403 ;
	setAttr ".pt[875]" -type "float3" 0.99404645 0 -0.9940455 ;
	setAttr ".pt[876]" -type "float3" -0.9940455 0 0.99404562 ;
	setAttr ".pt[877]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[878]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[879]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[880]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[881]" -type "float3" 0.26459521 0 0.54633594 ;
	setAttr ".pt[882]" -type "float3" -1.6772397 0 -1.4653867e-08 ;
	setAttr ".pt[883]" -type "float3" 0.06985724 0.89453268 -0.60800099 ;
	setAttr ".pt[884]" -type "float3" -0.9209913 0.89453268 0.30979192 ;
	setAttr ".pt[885]" -type "float3" -0.048711691 -0.045207139 1.2215219 ;
	setAttr ".pt[886]" -type "float3" -1.6276208 -0.045207139 -0.19347009 ;
	setAttr ".pt[887]" -type "float3" -0.26459432 0.27224904 -1.5117887 ;
	setAttr ".pt[888]" -type "float3" -0.26459432 0.27224904 0.49120989 ;
	setAttr ".pt[889]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[890]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[891]" -type "float3" 0.99404645 0 -0.9940455 ;
	setAttr ".pt[892]" -type "float3" -0.9940455 0 0.99404562 ;
	setAttr ".pt[893]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[894]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[895]" -type "float3" 0.94702297 0 0.4736568 ;
	setAttr ".pt[896]" -type "float3" 2.3648949 0 0.15390043 ;
	setAttr ".pt[897]" -type "float3" 0.26459432 0 0 ;
	setAttr ".pt[898]" -type "float3" -0.039174654 0 0.53960967 ;
	setAttr ".pt[899]" -type "float3" -1.6533327 0 0.085465759 ;
	setAttr ".pt[900]" -type "float3" 1.2671804 0 0.44374996 ;
	setAttr ".pt[901]" -type "float3" 2.2322812 0 0.22610191 ;
	setAttr ".pt[902]" -type "float3" 0.82630515 0 -1.1373101 ;
	setAttr ".pt[903]" -type "float3" -1.1373101 0 0.82630444 ;
	setAttr ".pt[904]" -type "float3" -0.26459399 -0.045207139 1.2367482 ;
	setAttr ".pt[905]" -type "float3" -1.6446111 -0.045207139 -3.3172167e-08 ;
	setAttr ".pt[906]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[907]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[908]" -type "float3" -0.26459432 0.27224904 -1.5700182 ;
	setAttr ".pt[909]" -type "float3" -0.26459432 0.27224904 0.24866645 ;
	setAttr ".pt[910]" -type "float3" -0.036943711 0.89453268 -0.64897746 ;
	setAttr ".pt[911]" -type "float3" -0.96522951 0.89453268 0.21086562 ;
	setAttr ".pt[912]" -type "float3" 1.5626508 0 0.40291658 ;
	setAttr ".pt[913]" -type "float3" 2.0512164 0 0.29273602 ;
	setAttr ".pt[914]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[915]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[916]" -type "float3" -0.33546478 0 0.51959634 ;
	setAttr ".pt[917]" -type "float3" -1.5821995 0 0.16882709 ;
	setAttr ".pt[918]" -type "float3" 1.826159 0 0.352162 ;
	setAttr ".pt[919]" -type "float3" 0.82630515 0 -1.1373101 ;
	setAttr ".pt[920]" -type "float3" -1.1373101 0 0.82630444 ;
	setAttr ".pt[921]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[922]" -type "float3" -0.48047596 -0.045207139 1.2215219 ;
	setAttr ".pt[923]" -type "float3" -1.6276208 -0.045207139 0.19347003 ;
	setAttr ".pt[924]" -type "float3" -0.61697924 0 0.4867889 ;
	setAttr ".pt[925]" -type "float3" -1.4655921 0 0.24803133 ;
	setAttr ".pt[926]" -type "float3" -0.26459432 0.27224904 -1.5895885 ;
	setAttr ".pt[927]" -type "float3" -0.26459432 0.27224904 4.2636067e-08 ;
	setAttr ".pt[928]" -type "float3" 0.63821739 0 -1.2525704 ;
	setAttr ".pt[929]" -type "float3" -1.2525703 0 0.63821661 ;
	setAttr ".pt[930]" -type "float3" -0.14935 0.89453268 -0.67397428 ;
	setAttr ".pt[931]" -type "float3" -0.99221581 0.89453268 0.10674706 ;
	setAttr ".pt[932]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[933]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[934]" -type "float3" -0.87678653 0 0.44199508 ;
	setAttr ".pt[935]" -type "float3" -1.3063823 0 0.32112822 ;
	setAttr ".pt[936]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[937]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[938]" -type "float3" -0.6910426 -0.045207139 1.1762176 ;
	setAttr ".pt[939]" -type "float3" -1.5770682 -0.045207139 0.38217631 ;
	setAttr ".pt[940]" -type "float3" -1.1084894 0 0.38631788 ;
	setAttr ".pt[941]" -type "float3" 0.63821739 0 -1.2525704 ;
	setAttr ".pt[942]" -type "float3" -1.2525703 0 0.63821661 ;
	setAttr ".pt[943]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[944]" -type "float3" -0.26459432 0.27224904 -1.5700182 ;
	setAttr ".pt[945]" -type "float3" -0.26459432 0.27224904 -0.24866645 ;
	setAttr ".pt[946]" -type "float3" -0.89110845 -0.045207139 1.1019508 ;
	setAttr ".pt[947]" -type "float3" -1.4941982 -0.045207139 0.56147206 ;
	setAttr ".pt[948]" -type "float3" -0.2645942 0.89453268 -0.68237531 ;
	setAttr ".pt[949]" -type "float3" -1.0012857 0.89453268 1.8302732e-08 ;
	setAttr ".pt[950]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[951]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[952]" -type "float3" 0.43441463 0 -1.3369884 ;
	setAttr ".pt[953]" -type "float3" -1.3369882 0 0.43441397 ;
	setAttr ".pt[954]" -type "float3" -1.0757474 -0.045207139 1.0005504 ;
	setAttr ".pt[955]" -type "float3" -1.3810511 -0.045207139 0.72694248 ;
	setAttr ".pt[956]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[957]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[958]" -type "float3" -0.26459432 0.27224904 -1.5117887 ;
	setAttr ".pt[959]" -type "float3" -0.26459432 0.27224904 -0.49120978 ;
	setAttr ".pt[960]" -type "float3" -1.2404131 -0.045207139 0.87451315 ;
	setAttr ".pt[961]" -type "float3" 0.43441463 0 -1.3369884 ;
	setAttr ".pt[962]" -type "float3" -1.3369882 0 0.43441397 ;
	setAttr ".pt[963]" -type "float3" -0.37983793 0.89453268 -0.67397428 ;
	setAttr ".pt[964]" -type "float3" -0.99221581 0.89453268 -0.10674702 ;
	setAttr ".pt[965]" -type "float3" -0.26459432 0.27224904 -1.4163337 ;
	setAttr ".pt[966]" -type "float3" -0.26459432 0.27224904 -0.72165811 ;
	setAttr ".pt[967]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[968]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[969]" -type "float3" 0.21991505 0 -1.3884853 ;
	setAttr ".pt[970]" -type "float3" -1.3884851 0 0.21991453 ;
	setAttr ".pt[971]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[972]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[973]" -type "float3" -0.26459432 0.27224904 -1.2860044 ;
	setAttr ".pt[974]" -type "float3" -0.26459432 0.27224904 -0.9343369 ;
	setAttr ".pt[975]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[976]" -type "float3" -0.4922443 0.89453268 -0.64897746 ;
	setAttr ".pt[977]" -type "float3" -0.96522951 0.89453268 -0.21086562 ;
	setAttr ".pt[978]" -type "float3" -0.26459432 0.27224904 -1.1240089 ;
	setAttr ".pt[979]" -type "float3" 0.21991505 0 -1.3884853 ;
	setAttr ".pt[980]" -type "float3" -1.3884851 0 0.21991453 ;
	setAttr ".pt[981]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[982]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[983]" -type "float3" -0.59904498 0.89453268 -0.60800099 ;
	setAttr ".pt[984]" -type "float3" -0.9209913 0.89453268 -0.30979192 ;
	setAttr ".pt[985]" -type "float3" 5.8235275e-07 0 -1.4057928 ;
	setAttr ".pt[986]" -type "float3" -1.4057927 0 3.770629e-08 ;
	setAttr ".pt[987]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[988]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[989]" -type "float3" -0.69761044 0.89453268 -0.55205339 ;
	setAttr ".pt[990]" -type "float3" -0.86059022 0.89453268 -0.4010902 ;
	setAttr ".pt[991]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[992]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[993]" -type "float3" -0.78551352 0.89453268 -0.48251238 ;
	setAttr ".pt[994]" -type "float3" 5.8235275e-07 0 -1.4057928 ;
	setAttr ".pt[995]" -type "float3" -1.4057927 0 3.770629e-08 ;
	setAttr ".pt[996]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[997]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[998]" -type "float3" -1.3884851 0 -0.21991447 ;
	setAttr ".pt[999]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1000]" -type "float3" -0.21991393 0 -1.3884853 ;
	setAttr ".pt[1001]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[1002]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[1003]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[1004]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[1005]" -type "float3" -0.21991393 0 -1.3884853 ;
	setAttr ".pt[1006]" -type "float3" -1.3884851 0 -0.21991447 ;
	setAttr ".pt[1007]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1008]" -type "float3" -0.43441334 0 -1.3369884 ;
	setAttr ".pt[1009]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1010]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1011]" -type "float3" -1.3369882 0 -0.43441391 ;
	setAttr ".pt[1012]" -type "float3" 0.27145937 0.31114173 0 ;
	setAttr ".pt[1013]" -type "float3" -0.43441334 0 -1.3369884 ;
	setAttr ".pt[1014]" -type "float3" -1.3369882 0 -0.43441391 ;
	setAttr ".pt[1015]" -type "float3" -0.63821614 0 -1.2525706 ;
	setAttr ".pt[1016]" -type "float3" -1.2525703 0 -0.63821661 ;
	setAttr ".pt[1017]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1018]" -type "float3" -0.82630372 0 -1.1373104 ;
	setAttr ".pt[1019]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1020]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1021]" -type "float3" -0.99404544 0 -0.99404573 ;
	setAttr ".pt[1022]" -type "float3" -1.1373101 0 -0.82630444 ;
	setAttr ".pt[1023]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1024]" -type "float3" -0.63821614 0 -1.2525706 ;
	setAttr ".pt[1025]" -type "float3" -1.2525703 0 -0.63821661 ;
	setAttr ".pt[1026]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1027]" -type "float3" -0.82630372 0 -1.1373104 ;
	setAttr ".pt[1028]" -type "float3" -1.1373101 0 -0.82630444 ;
	setAttr ".pt[1029]" -type "float3" -0.99404544 0 -0.99404573 ;
	setAttr ".pt[1030]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1031]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1032]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1033]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1034]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1035]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1036]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1037]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1038]" -type "float3" 0 -4.0616002 0 ;
	setAttr ".pt[1039]" -type "float3" 0 -4.0616002 0 ;
	setAttr -s 1040 ".vt";
	setAttr ".vt[0:165]"  8.91006565 -14 4.53990555 9.51056576 -14 3.090170383
		 9.51056576 -12 3.090170383 8.91006565 -12 4.53990555 3.090169668 -14 9.51056767 4.53990507 -14 8.91006756
		 4.53990507 -12 8.91006756 3.090169668 -12 9.51056767 -7.071071148 -2 7.071068287
		 -5.8778553 -2 8.09017086 -5.8778553 0 8.09017086 -7.071071148 0 7.071068287 8.090176582 -2 -5.87785435
		 7.071074486 -2 -7.071070671 7.071074486 0 -7.071070671 8.090176582 0 -5.87785435
		 -1.56434631 -10 9.87688637 -1.0430813e-06 -10 10.000001907349 -1.0430813e-06 -8 10.000001907349
		 -1.56434631 -8 9.87688637 10 -10 0 9.87689018 -10 -1.56434321 9.87689018 -8 -1.56434321
		 10 -8 0 -9.87688732 10 -1.56434703 -10.000003814697 10 -1.6391277e-06 -10.000003814697 12 -1.6391277e-06
		 -9.87688732 12 -1.56434703 2.2351742e-06 10 -10.000005722046 -1.56434321 10 -9.87688923
		 -1.56434321 12 -9.87688923 2.2351742e-06 12 -10.000005722046 -5.8778553 -4 8.09017086
		 -4.53990746 -4 8.9100666 -4.53990746 -2 8.9100666 8.91007233 -4 -4.53990555 8.090176582 -4 -5.87785435
		 8.91007233 -2 -4.53990555 -3.090172052 -8 9.51056767 -1.56434631 -6 9.87688637 -3.090172052 -6 9.51056767
		 9.51057243 -8 -3.090169668 9.51057243 -6 -3.090169668 9.87689018 -6 -1.56434321 -4.53990746 -6 8.9100666
		 -3.090172052 -4 9.51056767 8.91007233 -6 -4.53990555 9.51057243 -4 -3.090169668 8.09017086 -14 5.87785339
		 8.09017086 -12 5.87785339 5.87785292 -14 8.090172768 5.87785292 -12 8.090172768 -7.071069717 14 -7.071073055
		 -8.090172768 14 -5.87785721 -8.090172768 16 -5.87785721 -7.071069717 16 -7.071073055
		 -5.87785339 14 -8.090175629 -5.87785339 16 -8.090175629 -9.51056862 12 -3.090173006
		 -9.87688732 14 -1.56434703 -9.51056862 14 -3.090173006 -3.09016943 12 -9.51057053
		 -3.09016943 14 -9.51057053 -1.56434321 14 -9.87688923 9.87688351 -12 1.56434476 9.87688351 -10 1.56434476
		 9.51056576 -10 3.090170383 1.56434405 -12 9.87688637 3.090169668 -10 9.51056767 1.56434405 -10 9.87688637
		 7.071068287 -14 7.071069717 7.071068287 -12 7.071069717 -8.91006851 14 -4.53990889
		 -8.91006851 16 -4.53990889 -4.53990507 14 -8.91007042 -4.53990507 16 -8.91007042
		 -9.51056862 6 3.09016943 -8.91006851 6 4.53990507 -8.91006851 8 4.53990507 -9.51056862 8 3.09016943
		 4.53991032 6 -8.91006947 3.090174198 6 -9.51057053 3.090174198 8 -9.51057053 4.53991032 8 -8.91006947
		 -9.87688732 8 1.56434357 -9.51056862 10 3.09016943 -9.87688732 10 1.56434357 1.56434786 8 -9.87688923
		 1.56434786 10 -9.87688923 3.090174198 10 -9.51057053 -8.91006851 4 4.53990507 -8.090173721 4 5.87785292
		 -8.090173721 6 5.87785292 5.87785816 4 -8.090173721 4.53991032 4 -8.91006947 5.87785816 6 -8.090173721
		 9.87688351 -8 1.56434476 1.56434405 -8 9.87688637 -9.87688732 12 1.56434357 1.56434786 12 -9.87688923
		 -8.090173721 2 5.87785292 -7.071071148 2 7.071068287 -7.071071148 4 7.071068287 7.071074486 2 -7.071070671
		 5.87785816 2 -8.090173721 7.071074486 4 -7.071070671 -9.51056862 16 -3.090173006
		 -3.09016943 16 -9.51057053 8.91006565 -10 4.53990555 4.53990507 -10 8.91006756 -5.8778553 2 8.09017086
		 8.090176582 2 -5.87785435 -1.0430813e-06 -6 10.000001907349 10 -6 0 -10.000003814697 14 -1.6391277e-06
		 2.2351742e-06 14 -10.000005722046 -4.53990746 0 8.9100666 8.91007233 0 -4.53990555
		 -1.56434631 -4 9.87688637 9.87689018 -4 -1.56434321 -3.090172052 -2 9.51056767 9.51057243 -2 -3.090169668
		 8.09017086 -10 5.87785339 5.87785292 -10 8.090172768 -8.090172768 18 -5.87785721
		 -7.071069717 18 -7.071073055 -5.87785339 18 -8.090175629 -9.87688732 16 -1.56434703
		 -1.56434321 16 -9.87688923 9.51056576 -8 3.090170383 3.090169668 -8 9.51056767 7.071068287 -10 7.071069717
		 -8.91006851 18 -4.53990889 -4.53990507 18 -8.91007042 -8.91006851 10 4.53990507 4.53991032 10 -8.91006947
		 -9.51056862 12 3.09016943 3.090174198 12 -9.51057053 -8.090173721 8 5.87785292 5.87785816 8 -8.090173721
		 9.87688351 -6 1.56434476 1.56434405 -6 9.87688637 -9.87688732 14 1.56434357 1.56434786 14 -9.87688923
		 -7.071071148 6 7.071068287 7.071074486 6 -7.071070671 -9.51056862 18 -3.090173006
		 -3.09016943 18 -9.51057053 8.91006565 -8 4.53990555 4.53990507 -8 8.91006756 -5.8778553 4 8.09017086
		 8.090176582 4 -5.87785435 -1.0430813e-06 -4 10.000001907349 10 -4 0 -10.000003814697 16 -1.6391277e-06
		 2.2351742e-06 16 -10.000005722046 -4.53990746 2 8.9100666 8.91007233 2 -4.53990555
		 -1.56434631 -2 9.87688637 9.87689018 -2 -1.56434321 -3.090172052 0 9.51056767 9.51057243 0 -3.090169668
		 8.09017086 -8 5.87785339 5.87785292 -8 8.090172768 -7.64905834 20 -7.64906168 -8.75146198 20 -6.35831261;
	setAttr ".vt[166:331]" -8.75146198 20 -6.35831261 -7.64905834 20 -7.64906168
		 -6.35830832 20 -8.7514658 -6.35830832 20 -8.7514658 -8.090172768 20 -5.87785721 -7.071069717 20 -7.071073055
		 -5.87785339 20 -8.090175629 -9.87688732 18 -1.56434703 -1.56434321 18 -9.87688923
		 9.51056576 -6 3.090170383 3.090169668 -6 9.51056767 7.071068287 -8 7.071069717 -9.63837624 20 -4.91100025
		 -9.63837624 20 -4.91100025 -4.91099644 20 -9.63837814 -4.91099644 20 -9.63837814
		 -8.91006851 20 -4.53990889 -4.53990507 20 -8.91007042 -8.91006851 12 4.53990507 4.53991032 12 -8.91006947
		 -9.51056862 14 3.09016943 3.090174198 14 -9.51057053 -8.090173721 10 5.87785292 5.87785816 10 -8.090173721
		 9.87688351 -4 1.56434476 1.56434405 -4 9.87688637 -9.87688732 16 1.56434357 1.56434786 16 -9.87688923
		 -7.071071148 8 7.071068287 7.071074486 8 -7.071070671 -10.28796196 20 -3.34276319
		 -10.28796196 20 -3.34276319 -3.34275937 20 -10.28796387 -3.34275937 20 -10.28796387
		 -9.51056862 20 -3.090173006 -3.09016943 20 -9.51057053 8.91006565 -6 4.53990555 4.53990507 -6 8.91006756
		 -5.8778553 6 8.09017086 8.090176582 6 -5.87785435 -1.0430813e-06 -2 10.000001907349
		 10 -2 0 -10.000003814697 18 -1.6391277e-06 2.2351742e-06 18 -10.000005722046 -4.53990746 4 8.9100666
		 8.91007233 4 -4.53990555 -1.56434631 0 9.87688637 9.87689018 0 -1.56434321 -3.090172052 2 9.51056767
		 9.51057243 2 -3.090169668 8.09017086 -6 5.87785339 5.87785292 -6 8.090172768 -9.87688732 20 -1.56434703
		 -1.56434321 20 -9.87688923 -10.68422318 20 -1.6922164 -10.68422318 20 -1.6922164
		 -1.69221222 20 -10.68422508 -1.69221222 20 -10.68422508 9.51056576 -4 3.090170383
		 3.090169668 -4 9.51056767 7.071068287 -6 7.071069717 -8.91006851 14 4.53990507 4.53991032 14 -8.91006947
		 -9.51056862 16 3.09016943 3.090174198 16 -9.51057053 -8.090173721 12 5.87785292 5.87785816 12 -8.090173721
		 9.87688351 -2 1.56434476 1.56434405 -2 9.87688637 -9.87688732 18 1.56434357 1.56434786 18 -9.87688923
		 -7.071071148 10 7.071068287 7.071074486 10 -7.071070671 8.91006565 -4 4.53990555
		 4.53990507 -4 8.91006756 -5.8778553 8 8.09017086 8.090176582 8 -5.87785435 -1.0430813e-06 0 10.000001907349
		 10 0 0 -10.000003814697 20 -1.6391277e-06 2.2351742e-06 20 -10.000005722046 -4.53990746 6 8.9100666
		 8.91007233 6 -4.53990555 -1.56434631 2 9.87688637 9.87689018 2 -1.56434321 -3.090172052 4 9.51056767
		 9.51057243 4 -3.090169668 -10.81740284 20 -1.6172033e-06 -10.81740284 20 -1.6172033e-06
		 2.5737836e-06 20 -10.81740475 2.5737836e-06 20 -10.81740475 8.09017086 -4 5.87785339
		 5.87785292 -4 8.090172768 9.51056576 -2 3.090170383 3.090169668 -2 9.51056767 7.071068287 -4 7.071069717
		 -8.91006851 16 4.53990507 4.53991032 16 -8.91006947 -9.51056862 18 3.09016943 3.090174198 18 -9.51057053
		 -8.090173721 14 5.87785292 5.87785816 14 -8.090173721 9.87688351 0 1.56434476 1.56434405 0 9.87688637
		 -9.87688732 20 1.56434357 1.56434786 20 -9.87688923 -7.071071148 12 7.071068287 7.071074486 12 -7.071070671
		 8.91006565 -2 4.53990555 4.53990507 -2 8.91006756 -10.68422318 20 1.69221294 -10.68422318 20 1.69221294
		 1.69221759 20 -10.68422508 1.69221759 20 -10.68422508 -5.8778553 10 8.09017086 8.090176582 10 -5.87785435
		 -1.0430813e-06 2 10.000001907349 10 2 0 -4.53990746 8 8.9100666 8.91007233 8 -4.53990555
		 -1.56434631 4 9.87688637 9.87689018 4 -1.56434321 -3.090172052 6 9.51056767 9.51057243 6 -3.090169668
		 8.09017086 -2 5.87785339 5.87785292 -2 8.090172768 9.51056576 0 3.090170383 3.090169668 0 9.51056767
		 7.071068287 -2 7.071069717 -8.91006851 18 4.53990507 4.53991032 18 -8.91006947 -9.51056862 20 3.09016943
		 3.090174198 20 -9.51057053 -8.090173721 16 5.87785292 5.87785816 16 -8.090173721
		 9.87688351 2 1.56434476 1.56434405 2 9.87688637 -7.071071148 14 7.071068287 7.071074486 14 -7.071070671
		 8.91006565 0 4.53990555 4.53990507 0 8.91006756 -10.28796196 20 3.34275961 -10.28796196 20 3.34275961
		 3.34276485 20 -10.28796387 3.34276485 20 -10.28796387 -5.8778553 12 8.09017086 8.090176582 12 -5.87785435
		 -1.0430813e-06 4 10.000001907349 10 4 0 -4.53990746 10 8.9100666 8.91007233 10 -4.53990555
		 -1.56434631 6 9.87688637 9.87689018 6 -1.56434321 -3.090172052 8 9.51056767 9.51057243 8 -3.090169668
		 8.09017086 0 5.87785339 5.87785292 0 8.090172768 9.51056576 2 3.090170383 3.090169668 2 9.51056767
		 7.071068287 0 7.071069717 -8.91006851 20 4.53990507 4.53991032 20 -8.91006947 -8.090173721 18 5.87785292
		 5.87785816 18 -8.090173721 9.87688351 4 1.56434476 1.56434405 4 9.87688637;
	setAttr ".vt[332:497]" -7.071071148 16 7.071068287 7.071074486 16 -7.071070671
		 8.91006565 2 4.53990555 4.53990507 2 8.91006756 -5.8778553 14 8.09017086 8.090176582 14 -5.87785435
		 -1.0430813e-06 6 10.000001907349 10 6 0 -9.63837624 20 4.91099644 -9.63837624 20 4.91099644
		 4.91100216 20 -9.63837719 4.91100216 20 -9.63837719 -4.53990746 12 8.9100666 8.91007233 12 -4.53990555
		 -1.56434631 8 9.87688637 9.87689018 8 -1.56434321 -3.090172052 10 9.51056767 9.51057243 10 -3.090169668
		 8.09017086 2 5.87785339 5.87785292 2 8.090172768 9.51056576 4 3.090170383 3.090169668 4 9.51056767
		 7.071068287 2 7.071069717 -8.090173721 20 5.87785292 5.87785816 20 -8.090173721 9.87688351 6 1.56434476
		 1.56434405 6 9.87688637 -7.071071148 18 7.071068287 7.071074486 18 -7.071070671 8.91006565 4 4.53990555
		 4.53990507 4 8.91006756 -5.8778553 16 8.09017086 8.090176582 16 -5.87785435 -1.0430813e-06 8 10.000001907349
		 10 8 0 -4.53990746 14 8.9100666 8.91007233 14 -4.53990555 -1.56434631 10 9.87688637
		 9.87689018 10 -1.56434321 -3.090172052 12 9.51056767 9.51057243 12 -3.090169668 -8.75146294 20 6.35830832
		 -8.75146294 20 6.35830832 6.35831404 20 -8.75146294 6.35831404 20 -8.75146294 8.09017086 4 5.87785339
		 5.87785292 4 8.090172768 9.51056576 6 3.090170383 3.090169668 6 9.51056767 7.071068287 4 7.071069717
		 9.87688351 8 1.56434476 1.56434405 8 9.87688637 -7.071071148 20 7.071068287 7.071074486 20 -7.071070671
		 8.91006565 6 4.53990555 4.53990507 6 8.91006756 -5.8778553 18 8.09017086 8.090176582 18 -5.87785435
		 -1.0430813e-06 10 10.000001907349 10 10 0 -4.53990746 16 8.9100666 8.91007233 16 -4.53990555
		 -1.56434631 12 9.87688637 9.87689018 12 -1.56434321 -3.090172052 14 9.51056767 9.51057243 14 -3.090169668
		 8.09017086 6 5.87785339 5.87785292 6 8.090172768 -7.6490593 20 7.64905643 -7.6490593 20 7.64905643
		 7.64906311 20 -7.6490593 7.64906311 20 -7.6490593 9.51056576 8 3.090170383 3.090169668 8 9.51056767
		 7.071068287 6 7.071069717 9.87688351 10 1.56434476 1.56434405 10 9.87688637 8.91006565 8 4.53990555
		 4.53990507 8 8.91006756 -5.8778553 20 8.09017086 8.090176582 20 -5.87785435 -1.0430813e-06 12 10.000001907349
		 10 12 0 -4.53990746 18 8.9100666 8.91007233 18 -4.53990555 -1.56434631 14 9.87688637
		 9.87689018 14 -1.56434321 -3.090172052 16 9.51056767 9.51057243 16 -3.090169668 8.09017086 8 5.87785339
		 5.87785292 8 8.090172768 9.51056576 10 3.090170383 3.090169668 10 9.51056767 7.071068287 8 7.071069717
		 -6.35831022 20 8.75146103 8.75146675 20 -6.35830927 -6.35831022 20 8.75146103 8.75146675 20 -6.35830927
		 9.87688351 12 1.56434476 1.56434405 12 9.87688637 8.91006565 10 4.53990555 4.53990507 10 8.91006756
		 -1.0430813e-06 14 10.000001907349 10 14 0 -4.53990746 20 8.9100666 8.91007233 20 -4.53990555
		 -1.56434631 16 9.87688637 9.87689018 16 -1.56434321 -3.090172052 18 9.51056767 9.51057243 18 -3.090169668
		 8.09017086 10 5.87785339 5.87785292 10 8.090172768 9.51056576 12 3.090170383 3.090169668 12 9.51056767
		 7.071068287 10 7.071069717 -4.91099882 20 9.63837433 9.638381 20 -4.91099691 -4.91099882 20 9.63837433
		 9.638381 20 -4.91099691 9.87688351 14 1.56434476 1.56434405 14 9.87688637 8.91006565 12 4.53990555
		 4.53990507 12 8.91006756 -1.0430813e-06 16 10.000001907349 10 16 0 -1.56434631 18 9.87688637
		 9.87689018 18 -1.56434321 -3.090172052 20 9.51056767 9.51057243 20 -3.090169668 8.09017086 12 5.87785339
		 5.87785292 12 8.090172768 9.51056576 14 3.090170383 3.090169668 14 9.51056767 7.071068287 12 7.071069717
		 -3.34276223 20 10.28796005 10.28796577 20 -3.34275961 9.87688351 16 1.56434476 1.56434405 16 9.87688637
		 -3.34276223 20 10.28796005 10.28796577 20 -3.34275961 8.91006565 14 4.53990555 4.53990507 14 8.91006756
		 -1.0430813e-06 18 10.000001907349 10 18 0 -1.56434631 20 9.87688637 9.87689018 20 -1.56434321
		 8.09017086 14 5.87785339 5.87785292 14 8.090172768 9.51056576 16 3.090170383 3.090169668 16 9.51056767
		 7.071068287 14 7.071069717 -1.69221568 20 10.68422222 10.68422604 20 -1.69221234
		 9.87688351 18 1.56434476 1.56434405 18 9.87688637 -1.69221568 20 10.68422222 10.68422604 20 -1.69221234
		 8.91006565 16 4.53990555 4.53990507 16 8.91006756 -1.0430813e-06 20 10.000001907349
		 10 20 0 8.09017086 16 5.87785339 5.87785292 16 8.090172768 9.51056576 18 3.090170383
		 3.090169668 18 9.51056767 7.071068287 16 7.071069717;
	setAttr ".vt[498:663]" -9.7243617e-07 20 10.81740093 10.81739902 20 1.5590649e-07
		 9.87688351 20 1.56434476 1.56434405 20 9.87688637 -9.7243617e-07 20 10.81740093 10.81739902 20 1.5590649e-07
		 8.91006565 18 4.53990555 4.53990507 18 8.91006756 8.09017086 18 5.87785339 5.87785292 18 8.090172768
		 9.51056576 20 3.090170383 3.090169668 20 9.51056767 7.071068287 18 7.071069717 10.68421936 20 1.69221425
		 1.69221354 20 10.68422222 10.68421936 20 1.69221425 1.69221354 20 10.68422222 8.91006565 20 4.53990555
		 4.53990507 20 8.91006756 10.2879591 20 3.3427608 3.34275985 20 10.28796005 8.09017086 20 5.87785339
		 5.87785292 20 8.090172768 10.2879591 20 3.3427608 3.34275985 20 10.28796005 7.071068287 20 7.071069717
		 9.63837337 20 4.91099691 4.91099644 20 9.63837528 9.63837337 20 4.91099691 4.91099644 20 9.63837528
		 8.75146103 20 6.35830879 6.35830832 20 8.75146294 7.64905643 20 7.64905834 8.75146103 20 6.35830879
		 6.35830832 20 8.75146294 7.64905643 20 7.64905834 -4.53990507 12 -8.91007042 -8.91006851 12 -4.53990889
		 5.87785816 0 -8.090173721 -8.090173721 0 5.87785292 2.2351742e-06 8 -10.000005722046
		 -10.000003814697 8 -1.6391277e-06 -1.0430813e-06 -12 10.000001907349 10 -12 0 4.53991032 2 -8.91006947
		 -8.91006851 2 4.53990507 1.56434786 6 -9.87688923 -9.87688732 6 1.56434357 3.090174198 4 -9.51057053
		 -9.51056862 4 3.09016943 -5.87785339 12 -8.090175629 -8.090172768 12 -5.87785721
		 5.87785292 -16 8.090172768 7.071068287 -16 7.071069717 8.09017086 -16 5.87785339
		 1.56434405 -14 9.87688637 9.87688351 -14 1.56434476 -3.09016943 10 -9.51057053 -9.51056862 10 -3.090173006
		 -7.071069717 12 -7.071073055 4.53990507 -16 8.91006756 8.91006565 -16 4.53990555
		 8.91007233 -8 -4.53990555 -4.53990746 -8 8.9100666 9.51057243 -10 -3.090169668 -3.090172052 -10 9.51056767
		 8.090176582 -6 -5.87785435 -5.8778553 -6 8.09017086 -1.56434321 8 -9.87688923 -9.87688732 8 -1.56434703
		 9.87689018 -12 -1.56434321 -1.56434631 -12 9.87688637 7.071074486 -4 -7.071070671
		 -7.071071148 -4 7.071068287 3.090169668 -16 9.51056767 9.51056576 -16 3.090170383
		 -4.53990507 10 -8.91007042 -8.91006851 10 -4.53990889 5.87785816 -2 -8.090173721
		 -8.090173721 -2 5.87785292 2.2351742e-06 6 -10.000005722046 -10.000003814697 6 -1.6391277e-06
		 -1.0430813e-06 -14 10.000001907349 10 -14 0 4.53991032 0 -8.91006947 -8.91006851 0 4.53990507
		 1.56434786 4 -9.87688923 -9.87688732 4 1.56434357 3.090174198 2 -9.51057053 -9.51056862 2 3.09016943
		 -5.87785339 10 -8.090175629 -8.090172768 10 -5.87785721 5.87785292 -18 8.090172768
		 7.071068287 -18 7.071069717 8.09017086 -18 5.87785339 1.56434405 -16 9.87688637 9.87688351 -16 1.56434476
		 -3.09016943 8 -9.51057053 -9.51056862 8 -3.090173006 -7.071069717 10 -7.071073055
		 4.53990507 -18 8.91006756 8.91006565 -18 4.53990555 8.91007233 -10 -4.53990555 -4.53990746 -10 8.9100666
		 9.51057243 -12 -3.090169668 -3.090172052 -12 9.51056767 8.090176582 -8 -5.87785435
		 -5.8778553 -8 8.09017086 -1.56434321 6 -9.87688923 -9.87688732 6 -1.56434703 9.87689018 -14 -1.56434321
		 -1.56434631 -14 9.87688637 7.071074486 -6 -7.071070671 -7.071071148 -6 7.071068287
		 3.090169668 -18 9.51056767 9.51056576 -18 3.090170383 -4.53990507 8 -8.91007042 -8.91006851 8 -4.53990889
		 5.87785816 -4 -8.090173721 -8.090173721 -4 5.87785292 2.2351742e-06 4 -10.000005722046
		 -10.000003814697 4 -1.6391277e-06 -1.0430813e-06 -16 10.000001907349 10 -16 0 4.53991032 -2 -8.91006947
		 -8.91006851 -2 4.53990507 1.56434786 2 -9.87688923 -9.87688732 2 1.56434357 3.090174198 0 -9.51057053
		 -9.51056862 0 3.09016943 -5.87785339 8 -8.090175629 -8.090172768 8 -5.87785721 5.87785292 -20 8.090172768
		 7.071068287 -20 7.071069717 8.09017086 -20 5.87785339 1.56434405 -18 9.87688637 9.87688351 -18 1.56434476
		 -3.09016943 6 -9.51057053 -9.51056862 6 -3.090173006 -7.071069717 8 -7.071073055
		 4.53990507 -20 8.91006756 8.91006565 -20 4.53990555 8.91007233 -12 -4.53990555 -4.53990746 -12 8.9100666
		 9.51057243 -14 -3.090169668 -3.090172052 -14 9.51056767 8.090176582 -10 -5.87785435
		 -5.8778553 -10 8.09017086 -1.56434321 4 -9.87688923 -9.87688732 4 -1.56434703 9.87689018 -16 -1.56434321
		 -1.56434631 -16 9.87688637 7.071074486 -8 -7.071070671 -7.071071148 -8 7.071068287
		 5.87785292 -20 8.090172768 7.071068287 -20 7.071069717 8.09017086 -20 5.87785339
		 3.090169668 -20 9.51056767 9.51056576 -20 3.090170383 -4.53990507 6 -8.91007042 -8.91006851 6 -4.53990889
		 5.87785816 -6 -8.090173721 -8.090173721 -6 5.87785292 2.2351742e-06 2 -10.000005722046
		 -10.000003814697 2 -1.6391277e-06 -1.0430813e-06 -18 10.000001907349;
	setAttr ".vt[664:829]" 10 -18 0 4.53990507 -20 8.91006756 8.91006565 -20 4.53990555
		 4.53991032 -4 -8.91006947 -8.91006851 -4 4.53990507 1.56434786 0 -9.87688923 -9.87688732 0 1.56434357
		 3.090174198 -2 -9.51057053 -9.51056862 -2 3.09016943 -5.87785339 6 -8.090175629 -8.090172768 6 -5.87785721
		 3.090169668 -20 9.51056767 9.51056576 -20 3.090170383 1.56434405 -20 9.87688637 9.87688351 -20 1.56434476
		 -3.09016943 4 -9.51057053 -9.51056862 4 -3.090173006 -7.071069717 6 -7.071073055
		 8.91007233 -14 -4.53990555 -4.53990746 -14 8.9100666 6.47213602 -21 4.70228243 4.70228195 -21 6.4721365
		 9.51057243 -16 -3.090169668 -3.090172052 -16 9.51056767 3.5872097 -20 7.04029417
		 4.70228004 -20 6.47213745 6.4721365 -20 4.70228148 7.040293694 -20 3.58721018 8.090176582 -12 -5.87785435
		 -5.8778553 -12 8.09017086 -1.56434321 2 -9.87688923 -9.87688732 2 -1.56434703 2.47213578 -21 7.6084528
		 7.60845232 -21 2.47213602 9.87689018 -18 -1.56434321 -1.56434631 -18 9.87688637 7.071074486 -10 -7.071070671
		 -7.071071148 -10 7.071068287 1.56434405 -20 9.87688637 9.87688351 -20 1.56434476
		 -4.53990507 4 -8.91007042 -8.91006851 4 -4.53990889 5.87785816 -8 -8.090173721 -8.090173721 -8 5.87785292
		 2.2351742e-06 0 -10.000005722046 -10.000003814697 0 -1.6391277e-06 -1.0430813e-06 -20 10.000001907349
		 10 -20 0 4.53991032 -6 -8.91006947 -8.91006851 -6 4.53990507 1.56434786 -2 -9.87688923
		 -9.87688732 -2 1.56434357 3.090174198 -4 -9.51057053 -9.51056862 -4 3.09016943 -2.3841858e-07 -21 8.000000953674
		 8 -21 0 -5.87785339 4 -8.090175629 -8.090172768 4 -5.87785721 -3.09016943 2 -9.51057053
		 -9.51056862 2 -3.090173006 -7.071069717 4 -7.071073055 1.2360667 -20 7.80422688 2.47213483 -20 7.6084528
		 7.60845232 -20 2.47213531 7.8042264 -20 1.23606777 -1.0430813e-06 -20 10.000001907349
		 10 -20 0 7.60845709 -21 -2.47213745 -2.4721365 -21 7.60845327 8.91007233 -16 -4.53990555
		 -4.53990746 -16 8.9100666 9.51057243 -18 -3.090169668 -3.090172052 -18 9.51056767
		 8.090176582 -14 -5.87785435 -5.8778553 -14 8.09017086 -1.56434321 0 -9.87688923 -9.87688732 0 -1.56434703
		 9.87689018 -20 -1.56434321 -1.56434631 -20 9.87688637 7.071074486 -12 -7.071070671
		 -7.071071148 -12 7.071068287 -4.53990507 2 -8.91007042 -8.91006851 2 -4.53990889
		 5.87785816 -10 -8.090173721 -8.090173721 -10 5.87785292 2.2351742e-06 -2 -10.000005722046
		 -10.000003814697 -2 -1.6391277e-06 4.53991032 -8 -8.91006947 -8.91006851 -8 4.53990507
		 1.56434786 -4 -9.87688923 -9.87688732 -4 1.56434357 3.090174198 -6 -9.51057053 -9.51056862 -6 3.09016943
		 6.47214031 -21 -4.70228481 -4.70228291 -21 6.47213697 -5.87785339 2 -8.090175629
		 -8.090172768 2 -5.87785721 9.87689018 -20 -1.56434321 -1.56434631 -20 9.87688637
		 -3.09016943 0 -9.51057053 -9.51056862 0 -3.090173006 -7.071069717 2 -7.071073055
		 8.91007233 -18 -4.53990555 -4.53990746 -18 8.9100666 9.51057243 -20 -3.090169668
		 -3.090172052 -20 9.51056767 8.090176582 -16 -5.87785435 -5.8778553 -16 8.09017086
		 -1.56434321 -2 -9.87688923 -9.87688732 -2 -1.56434703 8 -20 -3.8147061e-07 7.80422878 -20 -1.23606682
		 -1.2360698 -20 7.80422688 -1.2159348e-06 -20 8.000000953674 4.70228481 -21 -6.47213984
		 -6.47213745 -21 4.70228291 7.071074486 -14 -7.071070671 -7.071071148 -14 7.071068287
		 5.58720875 -20 5.5872097 4.70228195 -20 6.4721365 6.47213602 -20 4.70228243 -4.53990507 0 -8.91007042
		 -8.91006851 0 -4.53990889 5.87785816 -12 -8.090173721 -8.090173721 -12 5.87785292
		 2.2351742e-06 -4 -10.000005722046 -10.000003814697 -4 -1.6391277e-06 4.53991032 -10 -8.91006947
		 -8.91006851 -10 4.53990507 1.56434786 -6 -9.87688923 -9.87688732 -6 1.56434357 3.090174198 -8 -9.51057053
		 -9.51056862 -8 3.09016943 9.51057243 -20 -3.090169668 -3.090172052 -20 9.51056767
		 -5.87785339 0 -8.090175629 -8.090172768 0 -5.87785721 -3.09016943 -2 -9.51057053
		 -9.51056862 -2 -3.090173006 -7.071069717 0 -7.071073055 -7.60845423 -21 2.4721365
		 2.47213721 -21 -7.60845613 7.60845757 -20 -2.47213578 2.47213578 -20 7.6084528 7.60845232 -20 2.47213602
		 8.91007233 -20 -4.53990555 -4.53990746 -20 8.9100666 8.090176582 -18 -5.87785435
		 -5.8778553 -18 8.09017086 -1.56434321 -4 -9.87688923 -9.87688732 -4 -1.56434703 7.071074486 -16 -7.071070671
		 -7.071071148 -16 7.071068287 -4.53990507 -2 -8.91007042 -8.91006851 -2 -4.53990889
		 5.87785816 -14 -8.090173721 -8.090173721 -14 5.87785292 2.2351742e-06 -6 -10.000005722046
		 -10.000003814697 -6 -1.6391277e-06 -8.000001907349 -21 0 0 -21 -8.000003814697 4.53991032 -12 -8.91006947
		 -8.91006851 -12 4.53990507 1.56434786 -8 -9.87688923 -9.87688732 -8 1.56434357 3.090174198 -10 -9.51057053;
	setAttr ".vt[830:995]" -9.51056862 -10 3.09016943 8.91007233 -20 -4.53990555
		 -4.53990746 -20 8.9100666 -5.87785339 -2 -8.090175629 -8.090172768 -2 -5.87785721
		 -3.09016943 -4 -9.51057053 -9.51056862 -4 -3.090173006 -7.071069717 -2 -7.071073055
		 -3.58721209 -20 7.040293694 -2.47213697 -20 7.60845327 -7.60845423 -21 -2.4721365
		 -2.47213721 -21 -7.60845566 8.090176582 -20 -5.87785435 -5.8778553 -20 8.09017086
		 -1.56434321 -6 -9.87688923 -9.87688732 -6 -1.56434703 -6.47213793 -21 -4.70228338
		 -4.70228386 -21 -6.4721384 7.071074486 -18 -7.071070671 -7.071071148 -18 7.071068287
		 -4.53990507 -4 -8.91007042 -8.91006851 -4 -4.53990889 5.87785816 -16 -8.090173721
		 -8.090173721 -16 5.87785292 2.2351742e-06 -8 -10.000005722046 -10.000003814697 -8 -1.6391277e-06
		 4.53991032 -14 -8.91006947 -8.91006851 -14 4.53990507 1.56434786 -10 -9.87688923
		 -9.87688732 -10 1.56434357 3.090174198 -12 -9.51057053 -9.51056862 -12 3.09016943
		 8.090176582 -20 -5.87785435 -5.8778553 -20 8.09017086 -5.87785339 -4 -8.090175629
		 -8.090172768 -4 -5.87785721 -2.3841858e-07 -20 8.000000953674 8 -20 0 -3.09016943 -6 -9.51057053
		 -9.51056862 -6 -3.090173006 7.040298939 -20 -3.58721089 6.47214127 -20 -4.70228243
		 -7.071069717 -4 -7.071073055 -1.56434321 -8 -9.87688923 -9.87688732 -8 -1.56434703
		 7.071074486 -20 -7.071070671 -7.071071148 -20 7.071068287 -4.53990507 -6 -8.91007042
		 -8.91006851 -6 -4.53990889 5.87785816 -18 -8.090173721 -8.090173721 -18 5.87785292
		 2.2351742e-06 -10 -10.000005722046 -10.000003814697 -10 -1.6391277e-06 4.53991032 -16 -8.91006947
		 -8.91006851 -16 4.53990507 1.56434786 -12 -9.87688923 -9.87688732 -12 1.56434357
		 3.090174198 -14 -9.51057053 -9.51056862 -14 3.09016943 -5.87785339 -6 -8.090175629
		 -8.090172768 -6 -5.87785721 7.071074486 -20 -7.071070671 -7.071071148 -20 7.071068287
		 -5.58721113 -20 5.58720922 -4.70228338 -20 6.4721365 -3.09016943 -8 -9.51057053 -9.51056862 -8 -3.090173006
		 -7.071069717 -6 -7.071073055 -1.56434321 -10 -9.87688923 -9.87688732 -10 -1.56434703
		 -4.53990507 -8 -8.91007042 -8.91006851 -8 -4.53990889 5.87785816 -20 -8.090173721
		 -8.090173721 -20 5.87785292 2.2351742e-06 -12 -10.000005722046 -10.000003814697 -12 -1.6391277e-06
		 4.53991032 -18 -8.91006947 -8.91006851 -18 4.53990507 1.56434786 -14 -9.87688923
		 -9.87688732 -14 1.56434357 3.090174198 -16 -9.51057053 -9.51056862 -16 3.09016943
		 -5.87785339 -8 -8.090175629 -8.090172768 -8 -5.87785721 5.58721352 -20 -5.58721161
		 4.70228529 -20 -6.47213936 -3.09016943 -10 -9.51057053 -9.51056862 -10 -3.090173006
		 -7.071069717 -8 -7.071073055 5.87785816 -20 -8.090173721 -8.090173721 -20 5.87785292
		 -2.4721365 -20 7.60845327 -1.56434321 -12 -9.87688923 -9.87688732 -12 -1.56434703
		 -4.53990507 -10 -8.91007042 -8.91006851 -10 -4.53990889 2.2351742e-06 -14 -10.000005722046
		 -10.000003814697 -14 -1.6391277e-06 4.53991032 -20 -8.91006947 -8.91006851 -20 4.53990507
		 1.56434786 -16 -9.87688923 -9.87688732 -16 1.56434357 3.090174198 -18 -9.51057053
		 -9.51056862 -18 3.09016943 -5.87785339 -10 -8.090175629 -8.090172768 -10 -5.87785721
		 -7.040296078 -20 3.58720899 -6.47213888 -20 4.70228004 -3.09016943 -12 -9.51057053
		 -9.51056862 -12 -3.090173006 -7.071069717 -10 -7.071073055 4.53991032 -20 -8.91006947
		 -8.91006851 -20 4.53990507 7.60845709 -20 -2.47213745 -1.56434321 -14 -9.87688923
		 -9.87688732 -14 -1.56434703 -4.53990507 -12 -8.91007042 -8.91006851 -12 -4.53990889
		 2.2351742e-06 -16 -10.000005722046 -10.000003814697 -16 -1.6391277e-06 1.56434786 -18 -9.87688923
		 -9.87688732 -18 1.56434357 3.090174198 -20 -9.51057053 -9.51056862 -20 3.09016943
		 -5.87785339 -12 -8.090175629 -8.090172768 -12 -5.87785721 3.58721399 -20 -7.040296555
		 2.4721384 -20 -7.60845566 -3.09016943 -14 -9.51057053 -9.51056862 -14 -3.090173006
		 -7.071069717 -12 -7.071073055 3.090174198 -20 -9.51057053 -9.51056862 -20 3.09016943
		 -1.56434321 -16 -9.87688923 -9.87688732 -16 -1.56434703 -4.53990507 -14 -8.91007042
		 -8.91006851 -14 -4.53990889 2.2351742e-06 -18 -10.000005722046 -10.000003814697 -18 -1.6391277e-06
		 1.56434786 -20 -9.87688923 -9.87688732 -20 1.56434357 -7.80422831 -20 1.23606622
		 -7.6084547 -20 2.47213292 -5.87785339 -14 -8.090175629 -8.090172768 -14 -5.87785721
		 -4.70228291 -20 6.47213697 -3.09016943 -16 -9.51057053 -9.51056862 -16 -3.090173006
		 -7.071069717 -14 -7.071073055 1.56434786 -20 -9.87688923 -9.87688732 -20 1.56434357
		 -1.56434321 -18 -9.87688923 -9.87688732 -18 -1.56434703 -4.53990507 -16 -8.91007042
		 -8.91006851 -16 -4.53990889 2.2351742e-06 -20 -10.000005722046 -10.000003814697 -20 -1.6391277e-06
		 1.23607004 -20 -7.80422974 1.4066692e-06 -20 -8.000003814697 -5.87785339 -16 -8.090175629
		 -8.090172768 -16 -5.87785721 -3.09016943 -18 -9.51057053 -9.51056862 -18 -3.090173006
		 -7.071069717 -16 -7.071073055 2.2351742e-06 -20 -10.000005722046 -10.000003814697 -20 -1.6391277e-06;
	setAttr ".vt[996:1039]" 6.47214031 -20 -4.70228481 -7.80422783 -20 -1.23606992
		 -9.87688732 -20 -1.56434703 -8.000001907349 -20 -1.6927721e-06 -1.56434321 -20 -9.87688923
		 -4.53990507 -18 -8.91007042 -8.91006851 -18 -4.53990889 -5.87785339 -18 -8.090175629
		 -8.090172768 -18 -5.87785721 -1.56434321 -20 -9.87688923 -9.87688732 -20 -1.56434703
		 -1.2360673 -20 -7.80422974 -3.09016943 -20 -9.51057053 -2.47213507 -20 -7.60845613
		 -7.60845423 -20 -2.4721365 -9.51056862 -20 -3.090173006 -7.071069717 -18 -7.071073055
		 -3.09016943 -20 -9.51057053 -9.51056862 -20 -3.090173006 -4.53990507 -20 -8.91007042
		 -8.91006851 -20 -4.53990889 -3.58721113 -20 -7.040296555 -5.87785339 -20 -8.090175629
		 -4.70228195 -20 -6.47213936 -5.5872097 -20 -5.58721209 -7.071069717 -20 -7.071073055
		 -8.090172768 -20 -5.87785721 -6.47213745 -20 -4.70228386 -4.53990507 -20 -8.91007042
		 -8.91006851 -20 -4.53990889 -6.47213745 -20 4.70228291 -5.87785339 -20 -8.090175629
		 -8.090172768 -20 -5.87785721 -7.071069717 -20 -7.071073055 4.70228481 -20 -6.47213984
		 -7.60845423 -20 2.4721365 2.47213721 -20 -7.60845613 -8.000001907349 -20 0 0 -20 -8.000003814697
		 -6.47213793 -20 -4.70228338 -7.040295124 -20 -3.58721209 -7.60845423 -20 -2.4721365
		 -2.47213721 -20 -7.60845566 -4.70228386 -20 -6.4721384;
	setAttr -s 2100 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 1 3 0 1 4 5 1 5 6 1 6 7 1 7 4 1 8 9 1
		 9 10 1 10 11 1 11 8 1 12 13 1 13 14 1 14 15 1 15 12 1 16 17 1 17 18 1 18 19 1 19 16 1
		 20 21 1 21 22 1 22 23 1 23 20 1 24 25 1 25 26 1 26 27 1 27 24 1 28 29 1 29 30 1 30 31 1
		 31 28 1 32 33 1 33 34 1 34 9 1 9 32 1 35 36 1 36 12 1 12 37 1 37 35 1 38 19 1 19 39 1
		 39 40 1 40 38 1 22 41 1 41 42 1 42 43 1 43 22 1 44 40 1 40 45 1 45 33 1 33 44 1 42 46 1
		 46 35 1 35 47 1 47 42 1 48 0 1 3 49 1 49 48 1 5 50 1 50 51 1 51 6 1 52 53 1 53 54 1
		 54 55 1 55 52 1 56 52 1 55 57 1 57 56 1 58 27 1 27 59 1 59 60 1 60 58 1 30 61 1 61 62 1
		 62 63 1 63 30 1 2 64 1 64 65 1 65 66 1 66 2 1 67 7 1 7 68 1 68 69 1 69 67 1 70 48 1
		 49 71 1 71 70 1 50 70 1 71 51 1 53 72 1 72 73 1 73 54 1 74 56 1 57 75 1 75 74 1 76 77 1
		 77 78 1 78 79 1 79 76 1 80 81 1 81 82 1 82 83 1 83 80 1 84 79 1 79 85 1 85 86 1 86 84 1
		 82 87 1 87 88 1 88 89 1 89 82 1 90 91 1 91 92 1 92 77 1 77 90 1 93 94 1 94 80 1 80 95 1
		 95 93 1 65 20 1 23 96 1 96 65 1 17 69 1 69 97 1 97 18 1 25 86 1 86 98 1 98 26 1 88 28 1
		 31 99 1 99 88 1 100 101 1 101 102 1 102 91 1 91 100 1 103 104 1 104 93 1 93 105 1
		 105 103 1 72 60 1 60 106 1 106 73 1 62 74 1 75 107 1 107 62 1 66 108 1 108 3 1 6 109 1
		 109 68 1 10 110 1 110 101 1 101 11 1 14 103 1 103 111 1 111 15 1 18 112 1 112 39 1
		 43 113 1 113 23 1 26 114 1 114 59 1 63 115 1 115 31 1 34 116 1 116 10 1;
	setAttr ".ed[166:331]" 15 117 1 117 37 1 39 118 1 118 45 1 47 119 1 119 43 1
		 45 120 1 120 34 1 37 121 1 121 47 1 108 122 1 122 49 1 51 123 1 123 109 1 54 124 1
		 124 125 1 125 55 1 125 126 1 126 57 1 59 127 1 127 106 1 107 128 1 128 63 1 96 129 1
		 129 66 1 68 130 1 130 97 1 122 131 1 131 71 1 131 123 1 73 132 1 132 124 1 126 133 1
		 133 75 1 78 134 1 134 85 1 89 135 1 135 83 1 85 136 1 136 98 1 99 137 1 137 89 1
		 92 138 1 138 78 1 83 139 1 139 95 1 113 140 1 140 96 1 97 141 1 141 112 1 98 142 1
		 142 114 1 115 143 1 143 99 1 102 144 1 144 92 1 95 145 1 145 105 1 106 146 1 146 132 1
		 133 147 1 147 107 1 129 148 1 148 108 1 109 149 1 149 130 1 110 150 1 150 102 1 105 151 1
		 151 111 1 112 152 1 152 118 1 119 153 1 153 113 1 114 154 1 154 127 1 128 155 1 155 115 1
		 116 156 1 156 110 1 111 157 1 157 117 1 118 158 1 158 120 1 121 159 1 159 119 1 120 160 1
		 160 116 1 117 161 1 161 121 1 148 162 1 162 122 1 123 163 1 163 149 1 164 165 0 165 166 0
		 166 167 0 167 164 0 168 164 0 167 169 0 169 168 0 124 170 1 170 171 0 171 125 1 171 172 0
		 172 126 1 127 173 1 173 146 1 147 174 1 174 128 1 140 175 1 175 129 1 130 176 1 176 141 1
		 162 177 1 177 131 1 177 163 1 165 178 0 178 179 0 179 166 0 180 168 0 169 181 0 181 180 0
		 132 182 1 182 170 0 172 183 0 183 133 1 170 165 0 164 171 0 168 172 0 134 184 1 184 136 1
		 137 185 1 185 135 1 136 186 1 186 142 1 143 187 1 187 137 1 138 188 1 188 134 1 135 189 1
		 189 139 1 153 190 1 190 140 1 141 191 1 191 152 1 182 178 0 180 183 0 142 192 1 192 154 1
		 155 193 1 193 143 1 144 194 1 194 138 1 139 195 1 195 145 1 178 196 0 196 197 0 197 179 0
		 198 180 0 181 199 0 199 198 0 146 200 1 200 182 0 183 201 0 201 147 1;
	setAttr ".ed[332:497]" 175 202 1 202 148 1 149 203 1 203 176 1 150 204 1 204 144 1
		 145 205 1 205 151 1 152 206 1 206 158 1 159 207 1 207 153 1 154 208 1 208 173 1 174 209 1
		 209 155 1 156 210 1 210 150 1 151 211 1 211 157 1 158 212 1 212 160 1 161 213 1 213 159 1
		 160 214 1 214 156 1 157 215 1 215 161 1 200 196 0 198 201 0 202 216 1 216 162 1 163 217 1
		 217 203 1 173 218 1 218 200 0 201 219 0 219 174 1 196 220 0 220 221 0 221 197 0 222 198 0
		 199 223 0 223 222 0 190 224 1 224 175 1 176 225 1 225 191 1 216 226 1 226 177 1 226 217 1
		 218 220 0 222 219 0 184 227 1 227 186 1 187 228 1 228 185 1 186 229 1 229 192 1 193 230 1
		 230 187 1 188 231 1 231 184 1 185 232 1 232 189 1 207 233 1 233 190 1 191 234 1 234 206 1
		 192 235 1 235 208 1 209 236 1 236 193 1 194 237 1 237 188 1 189 238 1 238 195 1 224 239 1
		 239 202 1 203 240 1 240 225 1 204 241 1 241 194 1 195 242 1 242 205 1 206 243 1 243 212 1
		 213 244 1 244 207 1 208 245 1 245 218 0 219 246 0 246 209 1 210 247 1 247 204 1 205 248 1
		 248 211 1 212 249 1 249 214 1 215 250 1 250 213 1 214 251 1 251 210 1 211 252 1 252 215 1
		 220 253 0 253 254 0 254 221 0 255 222 0 223 256 0 256 255 0 239 257 1 257 216 1 217 258 1
		 258 240 1 245 253 0 255 246 0 233 259 1 259 224 1 225 260 1 260 234 1 257 261 1 261 226 1
		 261 258 1 227 262 1 262 229 1 230 263 1 263 228 1 229 264 1 264 235 1 236 265 1 265 230 1
		 231 266 1 266 227 1 228 267 1 267 232 1 244 268 1 268 233 1 234 269 1 269 243 1 235 270 1
		 270 245 0 246 271 0 271 236 1 237 272 1 272 231 1 232 273 1 273 238 1 259 274 1 274 239 1
		 240 275 1 275 260 1 253 276 0 276 277 0 277 254 0 278 255 0 256 279 0 279 278 0 241 280 1
		 280 237 1 238 281 1 281 242 1 243 282 1 282 249 1 250 283 1 283 244 1;
	setAttr ".ed[498:663]" 247 284 1 284 241 1 242 285 1 285 248 1 249 286 1 286 251 1
		 252 287 1 287 250 1 251 288 1 288 247 1 248 289 1 289 252 1 270 276 0 278 271 0 274 290 1
		 290 257 1 258 291 1 291 275 1 268 292 1 292 259 1 260 293 1 293 269 1 290 294 1 294 261 1
		 294 291 1 262 295 1 295 264 1 265 296 1 296 263 1 264 297 1 297 270 0 271 298 0 298 265 1
		 266 299 1 299 262 1 263 300 1 300 267 1 283 301 1 301 268 1 269 302 1 302 282 1 272 303 1
		 303 266 1 267 304 1 304 273 1 292 305 1 305 274 1 275 306 1 306 293 1 276 307 0 307 308 0
		 308 277 0 309 278 0 279 310 0 310 309 0 280 311 1 311 272 1 273 312 1 312 281 1 282 313 1
		 313 286 1 287 314 1 314 283 1 284 315 1 315 280 1 281 316 1 316 285 1 297 307 0 309 298 0
		 286 317 1 317 288 1 289 318 1 318 287 1 288 319 1 319 284 1 285 320 1 320 289 1 305 321 1
		 321 290 1 291 322 1 322 306 1 301 323 1 323 292 1 293 324 1 324 302 1 321 325 1 325 294 1
		 325 322 1 295 326 1 326 297 0 298 327 0 327 296 1 299 328 1 328 295 1 296 329 1 329 300 1
		 314 330 1 330 301 1 302 331 1 331 313 1 303 332 1 332 299 1 300 333 1 333 304 1 323 334 1
		 334 305 1 306 335 1 335 324 1 311 336 1 336 303 1 304 337 1 337 312 1 313 338 1 338 317 1
		 318 339 1 339 314 1 307 340 0 340 341 0 341 308 0 342 309 0 310 343 0 343 342 0 315 344 1
		 344 311 1 312 345 1 345 316 1 317 346 1 346 319 1 320 347 1 347 318 1 326 340 0 342 327 0
		 319 348 1 348 315 1 316 349 1 349 320 1 334 350 1 350 321 1 322 351 1 351 335 1 330 352 1
		 352 323 1 324 353 1 353 331 1 350 354 1 354 325 1 354 351 1 328 355 1 355 326 0 327 356 0
		 356 329 1 339 357 1 357 330 1 331 358 1 358 338 1 332 359 1 359 328 1 329 360 1 360 333 1
		 352 361 1 361 334 1 335 362 1 362 353 1 336 363 1 363 332 1 333 364 1;
	setAttr ".ed[664:829]" 364 337 1 338 365 1 365 346 1 347 366 1 366 339 1 344 367 1
		 367 336 1 337 368 1 368 345 1 346 369 1 369 348 1 349 370 1 370 347 1 348 371 1 371 344 1
		 345 372 1 372 349 1 340 373 0 373 374 0 374 341 0 375 342 0 343 376 0 376 375 0 355 373 0
		 375 356 0 361 377 1 377 350 1 351 378 1 378 362 1 357 379 1 379 352 1 353 380 1 380 358 1
		 377 381 1 381 354 1 381 378 1 366 382 1 382 357 1 358 383 1 383 365 1 359 384 1 384 355 0
		 356 385 0 385 360 1 379 386 1 386 361 1 362 387 1 387 380 1 363 388 1 388 359 1 360 389 1
		 389 364 1 365 390 1 390 369 1 370 391 1 391 366 1 367 392 1 392 363 1 364 393 1 393 368 1
		 369 394 1 394 371 1 372 395 1 395 370 1 371 396 1 396 367 1 368 397 1 397 372 1 386 398 1
		 398 377 1 378 399 1 399 387 1 373 400 0 400 401 0 401 374 0 402 375 0 376 403 0 403 402 0
		 384 400 0 402 385 0 382 404 1 404 379 1 380 405 1 405 383 1 398 406 1 406 381 1 406 399 1
		 391 407 1 407 382 1 383 408 1 408 390 1 404 409 1 409 386 1 387 410 1 410 405 1 388 411 1
		 411 384 0 385 412 0 412 389 1 390 413 1 413 394 1 395 414 1 414 391 1 392 415 1 415 388 1
		 389 416 1 416 393 1 394 417 1 417 396 1 397 418 1 418 395 1 396 419 1 419 392 1 393 420 1
		 420 397 1 409 421 1 421 398 1 399 422 1 422 410 1 407 423 1 423 404 1 405 424 1 424 408 1
		 421 425 1 425 406 1 425 422 1 411 426 0 426 400 0 402 427 0 427 412 0 426 428 0 428 401 0
		 403 429 0 429 427 0 414 430 1 430 407 1 408 431 1 431 413 1 423 432 1 432 409 1 410 433 1
		 433 424 1 413 434 1 434 417 1 418 435 1 435 414 1 415 436 1 436 411 0 412 437 0 437 416 1
		 417 438 1 438 419 1 420 439 1 439 418 1 419 440 1 440 415 1 416 441 1 441 420 1 432 442 1
		 442 421 1 422 443 1 443 433 1 430 444 1 444 423 1 424 445 1 445 431 1;
	setAttr ".ed[830:995]" 442 446 1 446 425 1 446 443 1 436 447 0 447 426 0 427 448 0
		 448 437 0 447 449 0 449 428 0 429 450 0 450 448 0 435 451 1 451 430 1 431 452 1 452 434 1
		 444 453 1 453 432 1 433 454 1 454 445 1 434 455 1 455 438 1 439 456 1 456 435 1 438 457 1
		 457 440 1 441 458 1 458 439 1 440 459 1 459 436 0 437 460 0 460 441 1 453 461 1 461 442 1
		 443 462 1 462 454 1 451 463 1 463 444 1 445 464 1 464 452 1 461 465 1 465 446 1 465 462 1
		 459 466 0 466 447 0 448 467 0 467 460 0 456 468 1 468 451 1 452 469 1 469 455 1 466 470 0
		 470 449 0 450 471 0 471 467 0 463 472 1 472 453 1 454 473 1 473 464 1 455 474 1 474 457 1
		 458 475 1 475 456 1 457 476 1 476 459 0 460 477 0 477 458 1 472 478 1 478 461 1 462 479 1
		 479 473 1 468 480 1 480 463 1 464 481 1 481 469 1 478 482 1 482 465 1 482 479 1 476 483 0
		 483 466 0 467 484 0 484 477 0 475 485 1 485 468 1 469 486 1 486 474 1 483 487 0 487 470 0
		 471 488 0 488 484 0 480 489 1 489 472 1 473 490 1 490 481 1 474 491 1 491 476 0 477 492 0
		 492 475 1 489 493 1 493 478 1 479 494 1 494 490 1 485 495 1 495 480 1 481 496 1 496 486 1
		 493 497 1 497 482 1 497 494 1 491 498 0 498 483 0 484 499 0 499 492 0 492 500 0 500 485 1
		 486 501 1 501 491 0 498 502 0 502 487 0 488 503 0 503 499 0 495 504 1 504 489 1 490 505 1
		 505 496 1 504 506 1 506 493 1 494 507 1 507 505 1 500 508 0 508 495 1 496 509 1 509 501 0
		 506 510 1 510 497 1 510 507 1 499 511 0 511 500 0 501 512 0 512 498 0 503 513 0 513 511 0
		 512 514 0 514 502 0 508 515 0 515 504 1 505 516 1 516 509 0 511 517 0 517 508 0 509 518 0
		 518 512 0 515 519 0 519 506 1 507 520 1 520 516 0 513 521 0 521 517 0 518 522 0 522 514 0
		 519 523 0 523 510 1 523 520 0 517 524 0 524 515 0 516 525 0 525 518 0;
	setAttr ".ed[996:1161]" 521 526 0 526 524 0 525 527 0 527 522 0 524 528 0 528 519 0
		 520 529 0 529 525 0 528 530 0 530 523 0 530 529 0 526 531 0 531 528 0 529 532 0 532 527 0
		 531 533 0 533 530 0 533 532 0 61 534 1 534 74 1 535 58 1 72 535 1 14 536 1 536 104 1
		 537 11 1 100 537 1 87 538 1 538 28 1 539 84 1 25 539 1 540 67 1 17 540 1 64 541 1
		 541 20 1 104 542 1 542 94 1 543 100 1 90 543 1 81 544 1 544 87 1 545 76 1 84 545 1
		 94 546 1 546 81 1 547 90 1 76 547 1 534 548 1 548 56 1 549 535 1 53 549 1 550 551 1
		 551 70 1 50 550 1 551 552 1 552 48 1 553 4 1 67 553 1 1 554 1 554 64 1 29 555 1 555 61 1
		 556 24 1 58 556 1 548 557 1 557 52 1 557 549 1 558 550 1 5 558 1 552 559 1 559 0 1
		 41 560 1 560 46 1 561 38 1 44 561 1 21 562 1 562 41 1 563 16 1 38 563 1 46 564 1
		 564 36 1 565 44 1 32 565 1 538 566 1 566 29 1 567 539 1 24 567 1 541 568 1 568 21 1
		 569 540 1 16 569 1 36 570 1 570 13 1 571 32 1 8 571 1 572 558 1 4 572 1 559 573 1
		 573 1 1 555 574 1 574 534 1 575 556 1 535 575 1 13 576 1 576 536 1 577 8 1 537 577 1
		 544 578 1 578 538 1 579 545 1 539 579 1 580 553 1 540 580 1 554 581 1 581 541 1 536 582 1
		 582 542 1 583 537 1 543 583 1 546 584 1 584 544 1 585 547 1 545 585 1 542 586 1 586 546 1
		 587 543 1 547 587 1 574 588 1 588 548 1 589 575 1 549 589 1 590 591 1 591 551 1 550 590 1
		 591 592 1 592 552 1 593 572 1 553 593 1 573 594 1 594 554 1 566 595 1 595 555 1 596 567 1
		 556 596 1 588 597 1 597 557 1 597 589 1 598 590 1 558 598 1 592 599 1 599 559 1 562 600 1
		 600 560 1 601 563 1 561 601 1 568 602 1 602 562 1 603 569 1 563 603 1 560 604 1 604 564 1
		 605 561 1 565 605 1 578 606 1 606 566 1 607 579 1 567 607 1;
	setAttr ".ed[1162:1327]" 581 608 1 608 568 1 609 580 1 569 609 1 564 610 1 610 570 1
		 611 565 1 571 611 1 612 598 1 572 612 1 599 613 1 613 573 1 595 614 1 614 574 1 615 596 1
		 575 615 1 570 616 1 616 576 1 617 571 1 577 617 1 584 618 1 618 578 1 619 585 1 579 619 1
		 620 593 1 580 620 1 594 621 1 621 581 1 576 622 1 622 582 1 623 577 1 583 623 1 586 624 1
		 624 584 1 625 587 1 585 625 1 582 626 1 626 586 1 627 583 1 587 627 1 614 628 1 628 588 1
		 629 615 1 589 629 1 630 631 0 631 591 1 590 630 1 631 632 0 632 592 1 633 612 1 593 633 1
		 613 634 1 634 594 1 606 635 1 635 595 1 636 607 1 596 636 1 628 637 1 637 597 1 637 629 1
		 638 630 0 598 638 1 632 639 0 639 599 1 602 640 1 640 600 1 641 603 1 601 641 1 608 642 1
		 642 602 1 643 609 1 603 643 1 600 644 1 644 604 1 645 601 1 605 645 1 618 646 1 646 606 1
		 647 619 1 607 647 1 621 648 1 648 608 1 649 620 1 609 649 1 604 650 1 650 610 1 651 605 1
		 611 651 1 631 630 0 630 652 0 652 653 0 653 631 0 632 631 0 653 654 0 654 632 0 655 638 0
		 612 655 1 639 656 0 656 613 1 635 657 1 657 614 1 658 636 1 615 658 1 610 659 1 659 616 1
		 660 611 1 617 660 1 624 661 1 661 618 1 662 625 1 619 662 1 663 633 1 620 663 1 634 664 1
		 664 621 1 630 638 0 638 665 0 665 652 0 639 632 0 654 666 0 666 639 0 616 667 1 667 622 1
		 668 617 1 623 668 1 626 669 1 669 624 1 670 627 1 625 670 1 622 671 1 671 626 1 672 623 1
		 627 672 1 657 673 1 673 628 1 674 658 1 629 674 1 638 655 0 655 675 0 675 665 0 656 639 0
		 666 676 0 676 656 0 677 655 0 633 677 1 656 678 0 678 634 1 646 679 1 679 635 1 680 647 1
		 636 680 1 673 681 1 681 637 1 681 674 1 642 682 1 682 640 1 683 643 1 641 683 1 685 684 0
		 648 686 1 686 642 1 687 649 1 643 687 1 688 638 1 638 630 0 630 689 1;
	setAttr ".ed[1328:1493]" 689 688 0 690 632 1 632 639 0 639 691 1 691 690 0 640 692 1
		 692 644 1 693 641 1 645 693 1 661 694 1 694 646 1 695 662 1 647 695 1 696 685 0 684 697 0
		 664 698 1 698 648 1 699 663 1 649 699 1 644 700 1 700 650 1 701 645 1 651 701 1 655 677 0
		 677 702 0 702 675 0 678 656 0 676 703 0 703 678 0 679 704 1 704 657 1 705 680 1 658 705 1
		 650 706 1 706 659 1 707 651 1 660 707 1 669 708 1 708 661 1 709 670 1 662 709 1 710 677 0
		 663 710 1 678 711 0 711 664 1 659 712 1 712 667 1 713 660 1 668 713 1 671 714 1 714 669 1
		 715 672 1 670 715 1 667 716 1 716 671 1 717 668 1 672 717 1 718 696 0 697 719 0 704 720 1
		 720 673 1 721 705 1 674 721 1 694 722 1 722 679 1 723 695 1 680 723 1 720 724 1 724 681 1
		 724 721 1 725 677 1 677 655 0 655 726 1 726 725 0 727 656 1 656 678 0 678 728 1 728 727 0
		 677 710 0 710 729 0 729 702 0 711 678 0 703 730 0 730 711 0 719 731 0 732 718 0 686 733 1
		 733 682 1 734 687 1 683 734 1 698 735 1 735 686 1 736 699 1 687 736 1 682 737 1 737 692 1
		 738 683 1 693 738 1 708 739 1 739 694 1 740 709 1 695 740 1 711 741 0 741 698 1 742 710 0
		 699 742 1 692 743 1 743 700 1 744 693 1 701 744 1 722 745 1 745 704 1 746 723 1 705 746 1
		 700 747 1 747 706 1 748 701 1 707 748 1 714 749 1 749 708 1 750 715 1 709 750 1 706 751 1
		 751 712 1 752 707 1 713 752 1 716 753 1 753 714 1 754 717 1 715 754 1 712 755 1 755 716 1
		 756 713 1 717 756 1 731 757 0 758 732 0 745 759 1 759 720 1 760 746 1 721 760 1 741 711 0
		 730 761 0 761 741 0 710 742 0 742 762 0 762 729 0 739 763 1 763 722 1 764 740 1 723 764 1
		 759 765 1 765 724 1 765 760 1 735 766 1 766 733 1 767 736 1 734 767 1 741 768 0 768 735 1
		 769 742 0 736 769 1 733 770 1 770 737 1 771 734 1 738 771 1 749 772 1;
	setAttr ".ed[1494:1659]" 772 739 1 773 750 1 740 773 1 774 711 1 711 741 0 741 775 1
		 775 774 0 776 742 1 742 710 0 710 777 1 777 776 0 757 778 0 779 758 0 737 780 1 780 743 1
		 781 738 1 744 781 1 630 631 0 631 782 1 782 783 0 783 689 0 631 632 0 690 784 0 784 782 0
		 763 785 1 785 745 1 786 764 1 746 786 1 743 787 1 787 747 1 788 744 1 748 788 1 753 789 1
		 789 749 1 790 754 1 750 790 1 747 791 1 791 751 1 792 748 1 752 792 1 755 793 1 793 753 1
		 794 756 1 754 794 1 751 795 1 795 755 1 796 752 1 756 796 1 768 741 0 761 797 0 797 768 0
		 742 769 0 769 798 0 798 762 0 785 799 1 799 759 1 800 786 1 760 800 1 772 801 1 801 763 1
		 802 773 1 764 802 1 799 803 1 803 765 1 803 800 1 804 779 0 778 805 0 741 768 0 768 806 1
		 806 775 0 655 638 0 688 807 0 807 726 0 639 656 0 727 808 0 808 691 0 768 809 0 809 766 1
		 810 769 0 767 810 1 766 811 1 811 770 1 812 767 1 771 812 1 789 813 1 813 772 1 814 790 1
		 773 814 1 770 815 1 815 780 1 816 771 1 781 816 1 801 817 1 817 785 1 818 802 1 786 818 1
		 780 819 1 819 787 1 820 781 1 788 820 1 793 821 1 821 789 1 822 794 1 790 822 1 823 804 0
		 805 824 0 787 825 1 825 791 1 826 788 1 792 826 1 795 827 1 827 793 1 828 796 1 794 828 1
		 791 829 1 829 795 1 830 792 1 796 830 1 685 783 1 784 684 1 809 768 0 797 831 0 831 809 0
		 769 810 0 810 832 0 832 798 0 817 833 1 833 799 1 834 818 1 800 834 1 813 835 1 835 801 1
		 836 814 1 802 836 1 833 837 1 837 803 1 837 834 1 838 810 1 810 769 0 769 839 1 839 838 0
		 840 823 0 824 841 0 809 842 0 842 811 1 843 810 0 812 843 1 821 844 1 844 813 1 845 822 1
		 814 845 1 846 840 0 841 847 0 811 848 1 848 815 1 849 812 1 816 849 1 847 846 0 835 850 1
		 850 817 1 851 836 1 818 851 1 815 852 1 852 819 1 853 816 1 820 853 1;
	setAttr ".ed[1660:1825]" 827 854 1 854 821 1 855 828 1 822 855 1 819 856 1 856 825 1
		 857 820 1 826 857 1 829 858 1 858 827 1 859 830 1 828 859 1 825 860 1 860 829 1 861 826 1
		 830 861 1 842 809 0 831 862 0 862 842 0 810 843 0 843 863 0 863 832 0 850 864 1 864 833 1
		 865 851 1 834 865 1 710 677 0 725 866 0 866 777 0 678 711 0 774 867 0 867 728 0 844 868 1
		 868 835 1 869 845 1 836 869 1 870 809 1 809 842 0 842 871 1 871 870 0 864 872 1 872 837 1
		 872 865 1 854 873 1 873 844 1 874 855 1 845 874 1 842 875 0 875 848 1 876 843 0 849 876 1
		 868 877 1 877 850 1 878 869 1 851 878 1 848 879 1 879 852 1 880 849 1 853 880 1 858 881 1
		 881 854 1 882 859 1 855 882 1 852 883 1 883 856 1 884 853 1 857 884 1 860 885 1 885 858 1
		 886 861 1 859 886 1 856 887 1 887 860 1 888 857 1 861 888 1 877 889 1 889 864 1 890 878 1
		 865 890 1 875 842 0 862 891 0 891 875 0 843 876 0 876 892 0 892 863 0 893 876 1 876 843 0
		 843 894 1 894 893 0 873 895 1 895 868 1 896 874 1 869 896 1 889 897 1 897 872 1 897 890 1
		 881 898 1 898 873 1 899 882 1 874 899 1 895 900 1 900 877 1 901 896 1 878 901 1 875 902 0
		 902 879 1 903 876 0 880 903 1 885 904 1 904 881 1 905 886 1 882 905 1 879 906 1 906 883 1
		 907 880 1 884 907 1 887 908 1 908 885 1 909 888 1 886 909 1 883 910 1 910 887 1 911 884 1
		 888 911 1 900 912 1 912 889 1 913 901 1 890 913 1 914 875 1 875 902 0 902 915 1 915 914 0
		 898 916 1 916 895 1 917 899 1 896 917 1 912 918 1 918 897 1 918 913 1 902 875 0 891 919 0
		 919 902 0 876 903 0 903 920 0 920 892 0 769 742 0 776 921 0 921 839 0 696 807 1 808 697 1
		 904 922 1 922 898 1 923 905 1 899 923 1 916 924 1 924 900 1 925 917 1 901 925 1 908 926 1
		 926 904 1 927 909 1 905 927 1 902 928 0 928 906 1 929 903 0 907 929 1;
	setAttr ".ed[1826:1991]" 910 930 1 930 908 1 931 911 1 909 931 1 906 932 1 932 910 1
		 933 907 1 911 933 1 924 934 1 934 912 1 935 925 1 913 935 1 936 929 1 929 903 0 903 937 1
		 937 936 0 922 938 1 938 916 1 939 923 1 917 939 1 934 940 1 940 918 1 940 935 1 928 902 0
		 919 941 0 941 928 0 903 929 0 929 942 0 942 920 0 768 809 0 870 943 0 943 806 0 926 944 1
		 944 922 1 945 927 1 923 945 1 938 946 1 946 924 1 947 939 1 925 947 1 930 948 1 948 926 1
		 949 931 1 927 949 1 932 950 1 950 930 1 951 933 1 931 951 1 928 952 0 952 932 1 953 929 0
		 933 953 1 946 954 1 954 934 1 955 947 1 935 955 1 956 928 1 928 952 0 952 957 1 957 956 0
		 718 866 1 867 719 1 944 958 1 958 938 1 959 945 1 939 959 1 954 960 1 960 940 1 960 955 1
		 952 928 0 941 961 0 961 952 0 929 953 0 953 962 0 962 942 0 948 963 1 963 944 1 964 949 1
		 945 964 1 958 965 1 965 946 1 966 959 1 947 966 1 950 967 1 967 948 1 968 951 1 949 968 1
		 952 969 0 969 950 1 970 953 0 951 970 1 971 970 1 970 953 0 953 972 1 972 971 0 965 973 1
		 973 954 1 974 966 1 955 974 1 843 810 0 838 975 0 975 894 0 963 976 1 976 958 1 977 964 1
		 959 977 1 973 978 1 978 960 1 978 974 1 969 952 0 961 979 0 979 969 0 953 970 0 970 980 0
		 980 962 0 967 981 1 981 963 1 982 968 1 964 982 1 976 983 1 983 965 1 984 977 1 966 984 1
		 969 985 0 985 967 1 986 970 0 968 986 1 987 969 1 969 985 0 985 988 1 988 987 0 983 989 1
		 989 973 1 990 984 1 974 990 1 981 991 1 991 976 1 992 982 1 977 992 1 989 993 1 993 978 1
		 993 990 1 985 969 0 979 994 0 994 985 0 970 986 0 986 995 0 995 980 0 842 875 0 914 996 0
		 996 871 0 997 998 1 998 986 0 986 999 1 999 997 0 985 1000 0 1000 981 1 998 986 0
		 982 998 1 991 1001 1 1001 983 1 1002 992 1 984 1002 1 732 921 1 1001 1003 1 1003 989 1;
	setAttr ".ed[1992:2099]" 1004 1002 1 990 1004 1 1000 985 0 994 1005 0 1005 1000 0
		 986 998 0 998 1006 0 1006 995 0 1007 1000 1 1000 1008 0 1008 1009 1 1009 1007 0 1010 1011 1
		 1011 998 0 997 1010 0 1000 1008 0 1008 991 1 1011 998 0 992 1011 1 1003 1012 1 1012 993 1
		 1012 1004 1 1008 1000 0 1005 1013 0 1013 1008 0 998 1011 0 1011 1014 0 1014 1006 0
		 1008 1015 0 1015 1001 1 1016 1011 0 1002 1016 1 1017 1015 1 1015 1018 0 1018 1019 1
		 1019 1017 0 1020 1021 1 1021 1022 0 1022 1023 1 1023 1020 0 1015 1018 0 1018 1003 1
		 1022 1016 0 1004 1022 1 1015 1008 0 1013 1024 0 1024 1015 0 1011 1016 0 1016 1025 0
		 1025 1014 0 903 876 0 893 1026 0 1026 937 0 1018 1021 0 1021 1012 1 1021 1022 0 1018 1015 0
		 1024 1027 0 1027 1018 0 1016 1022 0 1022 1028 0 1028 1025 0 1021 1018 0 1027 1029 0
		 1029 1021 0 1022 1021 0 1029 1028 0 902 928 0 956 1030 0 1030 915 0 758 975 1 731 943 1
		 996 757 1 953 929 0 936 1031 0 1031 972 0 952 969 0 987 1032 0 1032 957 0 986 970 0
		 971 1033 0 1033 999 0 779 1026 1 985 1000 0 1007 1034 0 1034 988 0 1030 778 1 846 1035 1
		 1035 1036 0 1036 1037 0 1037 840 1 1008 1015 0 1017 1038 0 1038 1009 0 1016 1011 0
		 1010 1037 0 1036 1016 1 1022 1016 0 1035 1023 0 1018 1021 0 1020 1039 0 1039 1019 0
		 804 1031 1 1032 805 1 823 1033 1 1034 824 1 1038 841 1 1039 847 1;
	setAttr -s 980 -ch 3980 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 7
		mu 0 4 4 5 6 7
		f 4 8 9 10 11
		mu 0 4 8 9 10 11
		f 4 12 13 14 15
		mu 0 4 12 13 14 15
		f 4 16 17 18 19
		mu 0 4 16 17 18 19
		f 4 20 21 22 23
		mu 0 4 20 21 22 23
		f 4 24 25 26 27
		mu 0 4 24 25 26 27
		f 4 28 29 30 31
		mu 0 4 28 29 30 31
		f 4 32 33 34 35
		mu 0 4 32 33 34 9
		f 4 36 37 38 39
		mu 0 4 35 36 12 37
		f 4 40 41 42 43
		mu 0 4 38 19 39 40
		f 4 44 45 46 47
		mu 0 4 41 42 43 44
		f 4 48 49 50 51
		mu 0 4 45 40 46 33
		f 4 52 53 54 55
		mu 0 4 43 47 35 48
		f 4 56 -4 57 58
		mu 0 4 49 0 3 50
		f 4 59 60 61 -6
		mu 0 4 5 51 52 6
		f 4 62 63 64 65
		mu 0 4 53 54 55 56
		f 4 66 -66 67 68
		mu 0 4 57 53 56 58
		f 4 69 70 71 72
		mu 0 4 59 27 60 61
		f 4 73 74 75 76
		mu 0 4 30 62 63 64
		f 4 77 78 79 80
		mu 0 4 2 65 66 67
		f 4 81 82 83 84
		mu 0 4 68 7 69 70
		f 4 85 -59 86 87
		mu 0 4 71 49 50 72
		f 4 88 -88 89 -61
		mu 0 4 51 71 72 52
		f 4 90 91 92 -64
		mu 0 4 54 73 74 55
		f 4 93 -69 94 95
		mu 0 4 75 57 58 76
		f 4 96 97 98 99
		mu 0 4 77 78 79 80
		f 4 100 101 102 103
		mu 0 4 81 82 83 84
		f 4 104 105 106 107
		mu 0 4 85 80 86 87
		f 4 108 109 110 111
		mu 0 4 83 88 89 90
		f 4 112 113 114 115
		mu 0 4 91 92 93 78
		f 4 116 117 118 119
		mu 0 4 94 95 81 96
		f 4 120 -24 121 122
		mu 0 4 66 20 23 97
		f 4 123 124 125 -18
		mu 0 4 17 70 98 18
		f 4 126 127 128 -26
		mu 0 4 25 87 99 26
		f 4 129 -32 130 131
		mu 0 4 89 28 31 100
		f 4 132 133 134 135
		mu 0 4 101 102 103 92
		f 4 136 137 138 139
		mu 0 4 104 105 94 106
		f 4 140 141 142 -92
		mu 0 4 73 61 107 74
		f 4 143 -96 144 145
		mu 0 4 63 75 76 108
		f 4 -3 -81 146 147
		mu 0 4 3 2 67 109
		f 4 -7 148 149 -83
		mu 0 4 7 6 110 69
		f 4 -11 150 151 152
		mu 0 4 11 10 111 102
		f 4 -15 153 154 155
		mu 0 4 15 14 104 112
		f 4 -19 156 157 -42
		mu 0 4 19 18 113 39
		f 4 -23 -48 158 159
		mu 0 4 23 22 114 115
		f 4 -27 160 161 -71
		mu 0 4 27 26 116 60
		f 4 -31 -77 162 163
		mu 0 4 31 30 64 117
		f 4 -35 164 165 -10
		mu 0 4 9 34 118 10
		f 4 -39 -16 166 167
		mu 0 4 37 12 15 119
		f 4 -43 168 169 -50
		mu 0 4 40 39 120 46
		f 4 -47 -56 170 171
		mu 0 4 44 43 48 121
		f 4 -51 172 173 -34
		mu 0 4 33 46 122 34
		f 4 -55 -40 174 175
		mu 0 4 48 35 37 123
		f 4 -58 -148 176 177
		mu 0 4 50 3 109 124
		f 4 -62 178 179 -149
		mu 0 4 6 52 125 110
		f 4 -65 180 181 182
		mu 0 4 56 55 126 127
		f 4 -68 -183 183 184
		mu 0 4 58 56 127 128
		f 4 -72 185 186 -142
		mu 0 4 61 60 129 107
		f 4 -76 -146 187 188
		mu 0 4 64 63 108 130
		f 4 -80 -123 189 190
		mu 0 4 67 66 97 131
		f 4 -84 191 192 -125
		mu 0 4 70 69 132 98
		f 4 -87 -178 193 194
		mu 0 4 72 50 124 133
		f 4 -90 -195 195 -179
		mu 0 4 52 72 133 125
		f 4 -93 196 197 -181
		mu 0 4 55 74 134 126
		f 4 -95 -185 198 199
		mu 0 4 76 58 128 135
		f 4 -99 200 201 -106
		mu 0 4 80 79 136 86
		f 4 -103 -112 202 203
		mu 0 4 84 83 90 137
		f 4 -107 204 205 -128
		mu 0 4 87 86 138 99
		f 4 -111 -132 206 207
		mu 0 4 90 89 100 139
		f 4 -115 208 209 -98
		mu 0 4 78 93 140 79
		f 4 -119 -104 210 211
		mu 0 4 96 81 84 141
		f 4 -122 -160 212 213
		mu 0 4 97 23 115 142
		f 4 -126 214 215 -157
		mu 0 4 18 98 143 113
		f 4 -129 216 217 -161
		mu 0 4 26 99 144 116
		f 4 -131 -164 218 219
		mu 0 4 100 31 117 145
		f 4 -135 220 221 -114
		mu 0 4 92 103 146 93
		f 4 -139 -120 222 223
		mu 0 4 106 94 96 147
		f 4 -143 224 225 -197
		mu 0 4 74 107 148 134
		f 4 -145 -200 226 227
		mu 0 4 108 76 135 149
		f 4 -147 -191 228 229
		mu 0 4 109 67 131 150
		f 4 -150 230 231 -192
		mu 0 4 69 110 151 132
		f 4 -152 232 233 -134
		mu 0 4 102 111 152 103
		f 4 -155 -140 234 235
		mu 0 4 112 104 106 153
		f 4 -158 236 237 -169
		mu 0 4 39 113 154 120
		f 4 -159 -172 238 239
		mu 0 4 115 114 155 156
		f 4 -162 240 241 -186
		mu 0 4 60 116 157 129
		f 4 -163 -189 242 243
		mu 0 4 117 64 130 158
		f 4 -166 244 245 -151
		mu 0 4 10 118 159 111
		f 4 -167 -156 246 247
		mu 0 4 119 15 112 160
		f 4 -170 248 249 -173
		mu 0 4 46 120 161 122
		f 4 -171 -176 250 251
		mu 0 4 121 48 123 162
		f 4 -174 252 253 -165
		mu 0 4 34 122 163 118
		f 4 -175 -168 254 255
		mu 0 4 123 37 119 164
		f 4 -177 -230 256 257
		mu 0 4 124 109 150 165
		f 4 -180 258 259 -231
		mu 0 4 110 125 166 151
		f 4 260 261 262 263
		mu 0 4 167 168 169 170
		f 4 264 -264 265 266
		mu 0 4 171 172 173 174
		f 4 -182 267 268 269
		mu 0 4 127 126 175 176
		f 4 -184 -270 270 271
		mu 0 4 128 127 176 177
		f 4 -187 272 273 -225
		mu 0 4 107 129 178 148
		f 4 -188 -228 274 275
		mu 0 4 130 108 149 179
		f 4 -190 -214 276 277
		mu 0 4 131 97 142 180
		f 4 -193 278 279 -215
		mu 0 4 98 132 181 143
		f 4 -194 -258 280 281
		mu 0 4 133 124 165 182
		f 4 -196 -282 282 -259
		mu 0 4 125 133 182 166
		f 4 283 284 285 -262
		mu 0 4 183 184 185 186
		f 4 286 -267 287 288
		mu 0 4 187 188 189 190
		f 4 -198 289 290 -268
		mu 0 4 126 134 191 175
		f 4 -199 -272 291 292
		mu 0 4 135 128 177 192
		f 4 -269 293 -261 294
		mu 0 4 193 194 195 196
		f 4 -271 -295 -265 295
		mu 0 4 197 198 199 200
		f 4 -202 296 297 -205
		mu 0 4 86 136 201 138
		f 4 -203 -208 298 299
		mu 0 4 137 90 139 202
		f 4 -206 300 301 -217
		mu 0 4 99 138 203 144
		f 4 -207 -220 302 303
		mu 0 4 139 100 145 204
		f 4 -210 304 305 -201
		mu 0 4 79 140 205 136
		f 4 -211 -204 306 307
		mu 0 4 141 84 137 206
		f 4 -213 -240 308 309
		mu 0 4 142 115 156 207
		f 4 -216 310 311 -237
		mu 0 4 113 143 208 154
		f 4 -291 312 -284 -294
		mu 0 4 209 210 211 212
		f 4 -292 -296 -287 313
		mu 0 4 213 214 215 216
		f 4 -218 314 315 -241
		mu 0 4 116 144 217 157
		f 4 -219 -244 316 317
		mu 0 4 145 117 158 218
		f 4 -222 318 319 -209
		mu 0 4 93 146 219 140
		f 4 -223 -212 320 321
		mu 0 4 147 96 141 220
		f 4 322 323 324 -285
		mu 0 4 221 222 223 224
		f 4 325 -289 326 327
		mu 0 4 225 226 227 228
		f 4 -226 328 329 -290
		mu 0 4 134 148 229 191
		f 4 -227 -293 330 331
		mu 0 4 149 135 192 230
		f 4 -229 -278 332 333
		mu 0 4 150 131 180 231
		f 4 -232 334 335 -279
		mu 0 4 132 151 232 181
		f 4 -234 336 337 -221
		mu 0 4 103 152 233 146
		f 4 -235 -224 338 339
		mu 0 4 153 106 147 234
		f 4 -238 340 341 -249
		mu 0 4 120 154 235 161
		f 4 -239 -252 342 343
		mu 0 4 156 155 236 237
		f 4 -242 344 345 -273
		mu 0 4 129 157 238 178
		f 4 -243 -276 346 347
		mu 0 4 158 130 179 239
		f 4 -246 348 349 -233
		mu 0 4 111 159 240 152
		f 4 -247 -236 350 351
		mu 0 4 160 112 153 241
		f 4 -250 352 353 -253
		mu 0 4 122 161 242 163
		f 4 -251 -256 354 355
		mu 0 4 162 123 164 243
		f 4 -254 356 357 -245
		mu 0 4 118 163 244 159
		f 4 -255 -248 358 359
		mu 0 4 164 119 160 245
		f 4 -330 360 -323 -313
		mu 0 4 246 247 248 249
		f 4 -331 -314 -326 361
		mu 0 4 250 251 252 253
		f 4 -257 -334 362 363
		mu 0 4 165 150 231 254
		f 4 -260 364 365 -335
		mu 0 4 151 166 255 232
		f 4 -274 366 367 -329
		mu 0 4 148 178 256 229
		f 4 -275 -332 368 369
		mu 0 4 179 149 230 257
		f 4 370 371 372 -324
		mu 0 4 258 259 260 261
		f 4 373 -328 374 375
		mu 0 4 262 263 264 265
		f 4 -277 -310 376 377
		mu 0 4 180 142 207 266
		f 4 -280 378 379 -311
		mu 0 4 143 181 267 208
		f 4 -281 -364 380 381
		mu 0 4 182 165 254 268
		f 4 -283 -382 382 -365
		mu 0 4 166 182 268 255
		f 4 -368 383 -371 -361
		mu 0 4 269 270 271 272
		f 4 -369 -362 -374 384
		mu 0 4 273 274 275 276
		f 4 -298 385 386 -301
		mu 0 4 138 201 277 203
		f 4 -299 -304 387 388
		mu 0 4 202 139 204 278
		f 4 -302 389 390 -315
		mu 0 4 144 203 279 217
		f 4 -303 -318 391 392
		mu 0 4 204 145 218 280
		f 4 -306 393 394 -297
		mu 0 4 136 205 281 201
		f 4 -307 -300 395 396
		mu 0 4 206 137 202 282
		f 4 -309 -344 397 398
		mu 0 4 207 156 237 283
		f 4 -312 399 400 -341
		mu 0 4 154 208 284 235
		f 4 -316 401 402 -345
		mu 0 4 157 217 285 238
		f 4 -317 -348 403 404
		mu 0 4 218 158 239 286
		f 4 -320 405 406 -305
		mu 0 4 140 219 287 205
		f 4 -321 -308 407 408
		mu 0 4 220 141 206 288
		f 4 -333 -378 409 410
		mu 0 4 231 180 266 289
		f 4 -336 411 412 -379
		mu 0 4 181 232 290 267
		f 4 -338 413 414 -319
		mu 0 4 146 233 291 219
		f 4 -339 -322 415 416
		mu 0 4 234 147 220 292
		f 4 -342 417 418 -353
		mu 0 4 161 235 293 242
		f 4 -343 -356 419 420
		mu 0 4 237 236 294 295
		f 4 -346 421 422 -367
		mu 0 4 178 238 296 256
		f 4 -347 -370 423 424
		mu 0 4 239 179 257 297
		f 4 -350 425 426 -337
		mu 0 4 152 240 298 233
		f 4 -351 -340 427 428
		mu 0 4 241 153 234 299
		f 4 -354 429 430 -357
		mu 0 4 163 242 300 244
		f 4 -355 -360 431 432
		mu 0 4 243 164 245 301
		f 4 -358 433 434 -349
		mu 0 4 159 244 302 240
		f 4 -359 -352 435 436
		mu 0 4 245 160 241 303
		f 4 437 438 439 -372
		mu 0 4 304 305 306 307
		f 4 440 -376 441 442
		mu 0 4 308 309 310 311
		f 4 -363 -411 443 444
		mu 0 4 254 231 289 312
		f 4 -366 445 446 -412
		mu 0 4 232 255 313 290
		f 4 -423 447 -438 -384
		mu 0 4 314 315 316 317
		f 4 -424 -385 -441 448
		mu 0 4 318 319 320 321
		f 4 -377 -399 449 450
		mu 0 4 266 207 283 322
		f 4 -380 451 452 -400
		mu 0 4 208 267 323 284
		f 4 -381 -445 453 454
		mu 0 4 268 254 312 324
		f 4 -383 -455 455 -446
		mu 0 4 255 268 324 313
		f 4 -387 456 457 -390
		mu 0 4 203 277 325 279
		f 4 -388 -393 458 459
		mu 0 4 278 204 280 326
		f 4 -391 460 461 -402
		mu 0 4 217 279 327 285
		f 4 -392 -405 462 463
		mu 0 4 280 218 286 328
		f 4 -395 464 465 -386
		mu 0 4 201 281 329 277
		f 4 -396 -389 466 467
		mu 0 4 282 202 278 330
		f 4 -398 -421 468 469
		mu 0 4 283 237 295 331
		f 4 -401 470 471 -418
		mu 0 4 235 284 332 293
		f 4 -403 472 473 -422
		mu 0 4 238 285 333 296
		f 4 -404 -425 474 475
		mu 0 4 286 239 297 334
		f 4 -407 476 477 -394
		mu 0 4 205 287 335 281
		f 4 -408 -397 478 479
		mu 0 4 288 206 282 336
		f 4 -410 -451 480 481
		mu 0 4 289 266 322 337
		f 4 -413 482 483 -452
		mu 0 4 267 290 338 323
		f 4 484 485 486 -439
		mu 0 4 339 340 341 342
		f 4 487 -443 488 489
		mu 0 4 343 344 345 346
		f 4 -415 490 491 -406
		mu 0 4 219 291 347 287
		f 4 -416 -409 492 493
		mu 0 4 292 220 288 348
		f 4 -419 494 495 -430
		mu 0 4 242 293 349 300
		f 4 -420 -433 496 497
		mu 0 4 295 294 350 351
		f 4 -427 498 499 -414
		mu 0 4 233 298 352 291
		f 4 -428 -417 500 501
		mu 0 4 299 234 292 353
		f 4 -431 502 503 -434
		mu 0 4 244 300 354 302
		f 4 -432 -437 504 505
		mu 0 4 301 245 303 355
		f 4 -435 506 507 -426
		mu 0 4 240 302 356 298
		f 4 -436 -429 508 509
		mu 0 4 303 241 299 357
		f 4 -474 510 -485 -448
		mu 0 4 358 359 360 361
		f 4 -475 -449 -488 511
		mu 0 4 362 363 364 365
		f 4 -444 -482 512 513
		mu 0 4 312 289 337 366
		f 4 -447 514 515 -483
		mu 0 4 290 313 367 338
		f 4 -450 -470 516 517
		mu 0 4 322 283 331 368
		f 4 -453 518 519 -471
		mu 0 4 284 323 369 332
		f 4 -454 -514 520 521
		mu 0 4 324 312 366 370
		f 4 -456 -522 522 -515
		mu 0 4 313 324 370 367
		f 4 -458 523 524 -461
		mu 0 4 279 325 371 327
		f 4 -459 -464 525 526
		mu 0 4 326 280 328 372
		f 4 -462 527 528 -473
		mu 0 4 285 327 373 333
		f 4 -463 -476 529 530
		mu 0 4 328 286 334 374
		f 4 -466 531 532 -457
		mu 0 4 277 329 375 325
		f 4 -467 -460 533 534
		mu 0 4 330 278 326 376
		f 4 -469 -498 535 536
		mu 0 4 331 295 351 377
		f 4 -472 537 538 -495
		mu 0 4 293 332 378 349
		f 4 -478 539 540 -465
		mu 0 4 281 335 379 329
		f 4 -479 -468 541 542
		mu 0 4 336 282 330 380
		f 4 -481 -518 543 544
		mu 0 4 337 322 368 381
		f 4 -484 545 546 -519
		mu 0 4 323 338 382 369
		f 4 547 548 549 -486
		mu 0 4 383 384 385 386
		f 4 550 -490 551 552
		mu 0 4 387 388 389 390
		f 4 -492 553 554 -477
		mu 0 4 287 347 391 335
		f 4 -493 -480 555 556
		mu 0 4 348 288 336 392
		f 4 -496 557 558 -503
		mu 0 4 300 349 393 354
		f 4 -497 -506 559 560
		mu 0 4 351 350 394 395
		f 4 -500 561 562 -491
		mu 0 4 291 352 396 347
		f 4 -501 -494 563 564
		mu 0 4 353 292 348 397
		f 4 -529 565 -548 -511
		mu 0 4 398 399 400 401
		f 4 -530 -512 -551 566
		mu 0 4 402 403 404 405
		f 4 -504 567 568 -507
		mu 0 4 302 354 406 356
		f 4 -505 -510 569 570
		mu 0 4 355 303 357 407
		f 4 -508 571 572 -499
		mu 0 4 298 356 408 352
		f 4 -509 -502 573 574
		mu 0 4 357 299 353 409
		f 4 -513 -545 575 576
		mu 0 4 366 337 381 410
		f 4 -516 577 578 -546
		mu 0 4 338 367 411 382
		f 4 -517 -537 579 580
		mu 0 4 368 331 377 412
		f 4 -520 581 582 -538
		mu 0 4 332 369 413 378
		f 4 -521 -577 583 584
		mu 0 4 370 366 410 414
		f 4 -523 -585 585 -578
		mu 0 4 367 370 414 411
		f 4 -525 586 587 -528
		mu 0 4 327 371 415 373
		f 4 -526 -531 588 589
		mu 0 4 372 328 374 416
		f 4 -533 590 591 -524
		mu 0 4 325 375 417 371
		f 4 -534 -527 592 593
		mu 0 4 376 326 372 418
		f 4 -536 -561 594 595
		mu 0 4 377 351 395 419
		f 4 -539 596 597 -558
		mu 0 4 349 378 420 393
		f 4 -541 598 599 -532
		mu 0 4 329 379 421 375
		f 4 -542 -535 600 601
		mu 0 4 380 330 376 422
		f 4 -544 -581 602 603
		mu 0 4 381 368 412 423
		f 4 -547 604 605 -582
		mu 0 4 369 382 424 413
		f 4 -555 606 607 -540
		mu 0 4 335 391 425 379
		f 4 -556 -543 608 609
		mu 0 4 392 336 380 426
		f 4 -559 610 611 -568
		mu 0 4 354 393 427 406
		f 4 -560 -571 612 613
		mu 0 4 395 394 428 429
		f 4 614 615 616 -549
		mu 0 4 430 431 432 433
		f 4 617 -553 618 619
		mu 0 4 434 435 436 437
		f 4 -563 620 621 -554
		mu 0 4 347 396 438 391
		f 4 -564 -557 622 623
		mu 0 4 397 348 392 439
		f 4 -569 624 625 -572
		mu 0 4 356 406 440 408
		f 4 -570 -575 626 627
		mu 0 4 407 357 409 441
		f 4 -588 628 -615 -566
		mu 0 4 442 443 444 445
		f 4 -589 -567 -618 629
		mu 0 4 446 447 448 449
		f 4 -573 630 631 -562
		mu 0 4 352 408 450 396
		f 4 -574 -565 632 633
		mu 0 4 409 353 397 451
		f 4 -576 -604 634 635
		mu 0 4 410 381 423 452
		f 4 -579 636 637 -605
		mu 0 4 382 411 453 424
		f 4 -580 -596 638 639
		mu 0 4 412 377 419 454
		f 4 -583 640 641 -597
		mu 0 4 378 413 455 420
		f 4 -584 -636 642 643
		mu 0 4 414 410 452 456
		f 4 -586 -644 644 -637
		mu 0 4 411 414 456 453
		f 4 -592 645 646 -587
		mu 0 4 371 417 457 415
		f 4 -593 -590 647 648
		mu 0 4 418 372 416 458
		f 4 -595 -614 649 650
		mu 0 4 419 395 429 459
		f 4 -598 651 652 -611
		mu 0 4 393 420 460 427
		f 4 -600 653 654 -591
		mu 0 4 375 421 461 417
		f 4 -601 -594 655 656
		mu 0 4 422 376 418 462
		f 4 -603 -640 657 658
		mu 0 4 423 412 454 463
		f 4 -606 659 660 -641
		mu 0 4 413 424 464 455
		f 4 -608 661 662 -599
		mu 0 4 379 425 465 421
		f 4 -609 -602 663 664
		mu 0 4 426 380 422 466
		f 4 -612 665 666 -625
		mu 0 4 406 427 467 440
		f 4 -613 -628 667 668
		mu 0 4 429 428 468 469
		f 4 -622 669 670 -607
		mu 0 4 391 438 470 425
		f 4 -623 -610 671 672
		mu 0 4 439 392 426 471
		f 4 -626 673 674 -631
		mu 0 4 408 440 472 450
		f 4 -627 -634 675 676
		mu 0 4 441 409 451 473
		f 4 -632 677 678 -621
		mu 0 4 396 450 474 438
		f 4 -633 -624 679 680
		mu 0 4 451 397 439 475
		f 4 681 682 683 -616
		mu 0 4 476 477 478 479
		f 4 684 -620 685 686
		mu 0 4 480 481 482 483
		f 4 -647 687 -682 -629
		mu 0 4 484 485 486 487
		f 4 -648 -630 -685 688
		mu 0 4 488 489 490 491
		f 4 -635 -659 689 690
		mu 0 4 452 423 463 492
		f 4 -638 691 692 -660
		mu 0 4 424 453 493 464
		f 4 -639 -651 693 694
		mu 0 4 454 419 459 494
		f 4 -642 695 696 -652
		mu 0 4 420 455 495 460
		f 4 -643 -691 697 698
		mu 0 4 456 452 492 496
		f 4 -645 -699 699 -692
		mu 0 4 453 456 496 493
		f 4 -650 -669 700 701
		mu 0 4 459 429 469 497
		f 4 -653 702 703 -666
		mu 0 4 427 460 498 467
		f 4 -655 704 705 -646
		mu 0 4 417 461 499 457
		f 4 -656 -649 706 707
		mu 0 4 462 418 458 500
		f 4 -658 -695 708 709
		mu 0 4 463 454 494 501
		f 4 -661 710 711 -696
		mu 0 4 455 464 502 495
		f 4 -663 712 713 -654
		mu 0 4 421 465 503 461
		f 4 -664 -657 714 715
		mu 0 4 466 422 462 504
		f 4 -667 716 717 -674
		mu 0 4 440 467 505 472
		f 4 -668 -677 718 719
		mu 0 4 469 468 506 507
		f 4 -671 720 721 -662
		mu 0 4 425 470 508 465
		f 4 -672 -665 722 723
		mu 0 4 471 426 466 509
		f 4 -675 724 725 -678
		mu 0 4 450 472 510 474
		f 4 -676 -681 726 727
		mu 0 4 473 451 475 511
		f 4 -679 728 729 -670
		mu 0 4 438 474 512 470
		f 4 -680 -673 730 731
		mu 0 4 475 439 471 513
		f 4 -690 -710 732 733
		mu 0 4 492 463 501 514
		f 4 -693 734 735 -711
		mu 0 4 464 493 515 502
		f 4 736 737 738 -683
		mu 0 4 516 517 518 519
		f 4 739 -687 740 741
		mu 0 4 520 521 522 523
		f 4 -706 742 -737 -688
		mu 0 4 524 525 526 527
		f 4 -707 -689 -740 743
		mu 0 4 528 529 530 531
		f 4 -694 -702 744 745
		mu 0 4 494 459 497 532
		f 4 -697 746 747 -703
		mu 0 4 460 495 533 498
		f 4 -698 -734 748 749
		mu 0 4 496 492 514 534
		f 4 -700 -750 750 -735
		mu 0 4 493 496 534 515
		f 4 -701 -720 751 752
		mu 0 4 497 469 507 535
		f 4 -704 753 754 -717
		mu 0 4 467 498 536 505
		f 4 -709 -746 755 756
		mu 0 4 501 494 532 537
		f 4 -712 757 758 -747
		mu 0 4 495 502 538 533
		f 4 -714 759 760 -705
		mu 0 4 461 503 539 499
		f 4 -715 -708 761 762
		mu 0 4 504 462 500 540
		f 4 -718 763 764 -725
		mu 0 4 472 505 541 510
		f 4 -719 -728 765 766
		mu 0 4 507 506 542 543
		f 4 -722 767 768 -713
		mu 0 4 465 508 544 503
		f 4 -723 -716 769 770
		mu 0 4 509 466 504 545
		f 4 -726 771 772 -729
		mu 0 4 474 510 546 512
		f 4 -727 -732 773 774
		mu 0 4 511 475 513 547
		f 4 -730 775 776 -721
		mu 0 4 470 512 548 508
		f 4 -731 -724 777 778
		mu 0 4 513 471 509 549
		f 4 -733 -757 779 780
		mu 0 4 514 501 537 550
		f 4 -736 781 782 -758
		mu 0 4 502 515 551 538
		f 4 -745 -753 783 784
		mu 0 4 532 497 535 552
		f 4 -748 785 786 -754
		mu 0 4 498 533 553 536
		f 4 -749 -781 787 788
		mu 0 4 534 514 550 554
		f 4 -751 -789 789 -782
		mu 0 4 515 534 554 551
		f 4 -761 790 791 -743
		mu 0 4 555 556 557 558
		f 4 -762 -744 792 793
		mu 0 4 559 560 561 562
		f 4 -792 794 795 -738
		mu 0 4 563 564 565 566
		f 4 -793 -742 796 797
		mu 0 4 567 568 569 570
		f 4 -752 -767 798 799
		mu 0 4 535 507 543 571
		f 4 -755 800 801 -764
		mu 0 4 505 536 572 541
		f 4 -756 -785 802 803
		mu 0 4 537 532 552 573
		f 4 -759 804 805 -786
		mu 0 4 533 538 574 553
		f 4 -765 806 807 -772
		mu 0 4 510 541 575 546
		f 4 -766 -775 808 809
		mu 0 4 543 542 576 577
		f 4 -769 810 811 -760
		mu 0 4 503 544 578 539
		f 4 -770 -763 812 813
		mu 0 4 545 504 540 579
		f 4 -773 814 815 -776
		mu 0 4 512 546 580 548
		f 4 -774 -779 816 817
		mu 0 4 547 513 549 581
		f 4 -777 818 819 -768
		mu 0 4 508 548 582 544
		f 4 -778 -771 820 821
		mu 0 4 549 509 545 583
		f 4 -780 -804 822 823
		mu 0 4 550 537 573 584
		f 4 -783 824 825 -805
		mu 0 4 538 551 585 574
		f 4 -784 -800 826 827
		mu 0 4 552 535 571 586
		f 4 -787 828 829 -801
		mu 0 4 536 553 587 572
		f 4 -788 -824 830 831
		mu 0 4 554 550 584 588
		f 4 -790 -832 832 -825
		mu 0 4 551 554 588 585
		f 4 -812 833 834 -791
		mu 0 4 589 590 591 592
		f 4 -813 -794 835 836
		mu 0 4 593 594 595 596
		f 4 -835 837 838 -795
		mu 0 4 597 598 599 600
		f 4 -836 -798 839 840
		mu 0 4 601 602 603 604
		f 4 -799 -810 841 842
		mu 0 4 571 543 577 605
		f 4 -802 843 844 -807
		mu 0 4 541 572 606 575
		f 4 -803 -828 845 846
		mu 0 4 573 552 586 607
		f 4 -806 847 848 -829
		mu 0 4 553 574 608 587
		f 4 -808 849 850 -815
		mu 0 4 546 575 609 580
		f 4 -809 -818 851 852
		mu 0 4 577 576 610 611
		f 4 -816 853 854 -819
		mu 0 4 548 580 612 582
		f 4 -817 -822 855 856
		mu 0 4 581 549 583 613
		f 4 -820 857 858 -811
		mu 0 4 544 582 614 578
		f 4 -821 -814 859 860
		mu 0 4 583 545 579 615
		f 4 -823 -847 861 862
		mu 0 4 584 573 607 616
		f 4 -826 863 864 -848
		mu 0 4 574 585 617 608
		f 4 -827 -843 865 866
		mu 0 4 586 571 605 618
		f 4 -830 867 868 -844
		mu 0 4 572 587 619 606
		f 4 -831 -863 869 870
		mu 0 4 588 584 616 620
		f 4 -833 -871 871 -864
		mu 0 4 585 588 620 617
		f 4 -859 872 873 -834
		mu 0 4 621 622 623 624
		f 4 -860 -837 874 875
		mu 0 4 625 626 627 628
		f 4 -842 -853 876 877
		mu 0 4 605 577 611 629
		f 4 -845 878 879 -850
		mu 0 4 575 606 630 609
		f 4 -874 880 881 -838
		mu 0 4 631 632 633 634
		f 4 -875 -841 882 883
		mu 0 4 635 636 637 638
		f 4 -846 -867 884 885
		mu 0 4 607 586 618 639
		f 4 -849 886 887 -868
		mu 0 4 587 608 640 619
		f 4 -851 888 889 -854
		mu 0 4 580 609 641 612
		f 4 -852 -857 890 891
		mu 0 4 611 610 642 643
		f 4 -855 892 893 -858
		mu 0 4 582 612 644 614
		f 4 -856 -861 894 895
		mu 0 4 613 583 615 645
		f 4 -862 -886 896 897
		mu 0 4 616 607 639 646
		f 4 -865 898 899 -887
		mu 0 4 608 617 647 640
		f 4 -866 -878 900 901
		mu 0 4 618 605 629 648
		f 4 -869 902 903 -879
		mu 0 4 606 619 649 630
		f 4 -870 -898 904 905
		mu 0 4 620 616 646 650
		f 4 -872 -906 906 -899
		mu 0 4 617 620 650 647
		f 4 -894 907 908 -873
		mu 0 4 651 652 653 654
		f 4 -895 -876 909 910
		mu 0 4 655 656 657 658
		f 4 -877 -892 911 912
		mu 0 4 629 611 643 659
		f 4 -880 913 914 -889
		mu 0 4 609 630 660 641
		f 4 -909 915 916 -881
		mu 0 4 661 662 663 664
		f 4 -910 -884 917 918
		mu 0 4 665 666 667 668
		f 4 -885 -902 919 920
		mu 0 4 639 618 648 669
		f 4 -888 921 922 -903
		mu 0 4 619 640 670 649
		f 4 -890 923 924 -893
		mu 0 4 612 641 671 644
		f 4 -891 -896 925 926
		mu 0 4 643 642 672 673
		f 4 -897 -921 927 928
		mu 0 4 646 639 669 674
		f 4 -900 929 930 -922
		mu 0 4 640 647 675 670
		f 4 -901 -913 931 932
		mu 0 4 648 629 659 676
		f 4 -904 933 934 -914
		mu 0 4 630 649 677 660
		f 4 -905 -929 935 936
		mu 0 4 650 646 674 678
		f 4 -907 -937 937 -930
		mu 0 4 647 650 678 675
		f 4 -925 938 939 -908
		mu 0 4 679 680 681 682
		f 4 -926 -911 940 941
		mu 0 4 683 684 685 686
		f 4 -912 -927 942 943
		mu 0 4 659 643 673 687
		f 4 -915 944 945 -924
		mu 0 4 641 660 688 671
		f 4 -940 946 947 -916
		mu 0 4 689 690 691 692
		f 4 -941 -919 948 949
		mu 0 4 693 694 695 696
		f 4 -920 -933 950 951
		mu 0 4 669 648 676 697
		f 4 -923 952 953 -934
		mu 0 4 649 670 698 677
		f 4 -928 -952 954 955
		mu 0 4 674 669 697 699
		f 4 -931 956 957 -953
		mu 0 4 670 675 700 698
		f 4 -932 -944 958 959
		mu 0 4 676 659 687 701
		f 4 -935 960 961 -945
		mu 0 4 660 677 702 688
		f 4 -936 -956 962 963
		mu 0 4 678 674 699 703
		f 4 -938 -964 964 -957
		mu 0 4 675 678 703 700
		f 4 -943 -942 965 966
		mu 0 4 704 705 706 707
		f 4 -946 967 968 -939
		mu 0 4 708 709 710 711
		f 4 -966 -950 969 970
		mu 0 4 712 713 714 715
		f 4 -969 971 972 -947
		mu 0 4 716 717 718 719
		f 4 -951 -960 973 974
		mu 0 4 697 676 701 720
		f 4 -954 975 976 -961
		mu 0 4 677 698 721 702
		f 4 -959 -967 977 978
		mu 0 4 722 723 724 725
		f 4 -962 979 980 -968
		mu 0 4 726 727 728 729
		f 4 -955 -975 981 982
		mu 0 4 699 697 720 730
		f 4 -958 983 984 -976
		mu 0 4 698 700 731 721
		f 4 -978 -971 985 986
		mu 0 4 732 733 734 735
		f 4 -981 987 988 -972
		mu 0 4 736 737 738 739
		f 4 -963 -983 989 990
		mu 0 4 703 699 730 740
		f 4 -965 -991 991 -984
		mu 0 4 700 703 740 731
		f 4 -974 -979 992 993
		mu 0 4 741 742 743 744
		f 4 -977 994 995 -980
		mu 0 4 745 746 747 748
		f 4 -993 -987 996 997
		mu 0 4 749 750 751 752
		f 4 -996 998 999 -988
		mu 0 4 753 754 755 756
		f 4 -982 -994 1000 1001
		mu 0 4 757 758 759 760
		f 4 -985 1002 1003 -995
		mu 0 4 761 762 763 764
		f 4 -990 -1002 1004 1005
		mu 0 4 765 766 767 768
		f 4 -992 -1006 1006 -1003
		mu 0 4 769 770 771 772
		f 4 -1001 -998 1007 1008
		mu 0 4 773 774 775 776
		f 4 -1004 1009 1010 -999
		mu 0 4 777 778 779 780
		f 4 -1005 -1009 1011 1012
		mu 0 4 781 782 783 784
		f 4 -1007 -1013 1013 -1010
		mu 0 4 785 786 787 788
		f 4 1014 1015 -144 -75
		mu 0 4 62 789 75 63
		f 4 1016 -73 -141 1017
		mu 0 4 790 59 61 73
		f 4 1018 1019 -137 -154
		mu 0 4 14 791 105 104
		f 4 1020 -153 -133 1021
		mu 0 4 792 11 102 101
		f 4 1022 1023 -130 -110
		mu 0 4 88 793 28 89
		f 4 1024 -108 -127 1025
		mu 0 4 794 85 87 25
		f 4 1026 -85 -124 1027
		mu 0 4 795 68 70 17
		f 4 1028 1029 -121 -79
		mu 0 4 65 796 20 66
		f 4 1030 1031 -117 -138
		mu 0 4 105 797 95 94
		f 4 1032 -136 -113 1033
		mu 0 4 798 101 92 91
		f 4 1034 1035 -109 -102
		mu 0 4 82 799 88 83
		f 4 1036 -100 -105 1037
		mu 0 4 800 77 80 85
		f 4 1038 1039 -101 -118
		mu 0 4 95 801 82 81
		f 4 1040 -116 -97 1041
		mu 0 4 802 91 78 77
		f 4 1042 1043 -94 -1016
		mu 0 4 789 803 57 75
		f 4 1044 -1018 -91 1045
		mu 0 4 804 790 73 54
		f 4 1046 1047 -89 1048
		mu 0 4 805 806 71 51
		f 4 1049 1050 -86 -1048
		mu 0 4 806 807 49 71
		f 4 1051 -8 -82 1052
		mu 0 4 808 4 7 68
		f 4 1053 1054 -78 -2
		mu 0 4 1 809 65 2;
	setAttr ".fc[500:979]"
		f 4 1055 1056 -74 -30
		mu 0 4 29 810 62 30
		f 4 1057 -28 -70 1058
		mu 0 4 811 24 27 59
		f 4 1059 1060 -67 -1044
		mu 0 4 803 812 53 57
		f 4 1061 -1046 -63 -1061
		mu 0 4 812 804 54 53
		f 4 1062 -1049 -60 1063
		mu 0 4 813 805 51 5
		f 4 1064 1065 -57 -1051
		mu 0 4 807 814 0 49
		f 4 1066 1067 -53 -46
		mu 0 4 42 815 47 43
		f 4 1068 -44 -49 1069
		mu 0 4 816 38 40 45
		f 4 1070 1071 -45 -22
		mu 0 4 817 818 42 41
		f 4 1072 -20 -41 1073
		mu 0 4 819 16 19 38
		f 4 1074 1075 -37 -54
		mu 0 4 47 820 36 35
		f 4 1076 -52 -33 1077
		mu 0 4 821 45 33 32
		f 4 1078 1079 -29 -1024
		mu 0 4 793 822 29 28
		f 4 1080 -1026 -25 1081
		mu 0 4 823 794 25 24
		f 4 1082 1083 -21 -1030
		mu 0 4 796 824 21 20
		f 4 1084 -1028 -17 1085
		mu 0 4 825 795 17 16
		f 4 1086 1087 -13 -38
		mu 0 4 36 826 13 12
		f 4 1088 -36 -9 1089
		mu 0 4 827 32 9 8
		f 4 1090 -1064 -5 1091
		mu 0 4 828 813 5 4
		f 4 1092 1093 -1 -1066
		mu 0 4 814 829 1 0
		f 4 1094 1095 -1015 -1057
		mu 0 4 810 830 789 62
		f 4 1096 -1059 -1017 1097
		mu 0 4 831 811 59 790
		f 4 1098 1099 -1019 -14
		mu 0 4 13 832 791 14
		f 4 1100 -12 -1021 1101
		mu 0 4 833 8 11 792
		f 4 1102 1103 -1023 -1036
		mu 0 4 799 834 793 88
		f 4 1104 -1038 -1025 1105
		mu 0 4 835 800 85 794
		f 4 1106 -1053 -1027 1107
		mu 0 4 836 808 68 795
		f 4 1108 1109 -1029 -1055
		mu 0 4 809 837 796 65
		f 4 1110 1111 -1031 -1020
		mu 0 4 791 838 797 105
		f 4 1112 -1022 -1033 1113
		mu 0 4 839 792 101 798
		f 4 1114 1115 -1035 -1040
		mu 0 4 801 840 799 82
		f 4 1116 -1042 -1037 1117
		mu 0 4 841 802 77 800
		f 4 1118 1119 -1039 -1032
		mu 0 4 797 842 801 95
		f 4 1120 -1034 -1041 1121
		mu 0 4 843 798 91 802
		f 4 1122 1123 -1043 -1096
		mu 0 4 830 844 803 789
		f 4 1124 -1098 -1045 1125
		mu 0 4 845 831 790 804
		f 4 1126 1127 -1047 1128
		mu 0 4 846 847 806 805
		f 4 1129 1130 -1050 -1128
		mu 0 4 847 848 807 806
		f 4 1131 -1092 -1052 1132
		mu 0 4 849 828 4 808
		f 4 1133 1134 -1054 -1094
		mu 0 4 829 850 809 1
		f 4 1135 1136 -1056 -1080
		mu 0 4 822 851 810 29
		f 4 1137 -1082 -1058 1138
		mu 0 4 852 823 24 811
		f 4 1139 1140 -1060 -1124
		mu 0 4 844 853 812 803
		f 4 1141 -1126 -1062 -1141
		mu 0 4 853 845 804 812
		f 4 1142 -1129 -1063 1143
		mu 0 4 854 846 805 813
		f 4 1144 1145 -1065 -1131
		mu 0 4 848 855 814 807
		f 4 1146 1147 -1067 -1072
		mu 0 4 818 856 815 42
		f 4 1148 -1074 -1069 1149
		mu 0 4 857 819 38 816
		f 4 1150 1151 -1071 -1084
		mu 0 4 858 859 818 817
		f 4 1152 -1086 -1073 1153
		mu 0 4 860 825 16 819
		f 4 1154 1155 -1075 -1068
		mu 0 4 815 861 820 47
		f 4 1156 -1070 -1077 1157
		mu 0 4 862 816 45 821
		f 4 1158 1159 -1079 -1104
		mu 0 4 834 863 822 793
		f 4 1160 -1106 -1081 1161
		mu 0 4 864 835 794 823
		f 4 1162 1163 -1083 -1110
		mu 0 4 837 865 824 796
		f 4 1164 -1108 -1085 1165
		mu 0 4 866 836 795 825
		f 4 1166 1167 -1087 -1076
		mu 0 4 820 867 826 36
		f 4 1168 -1078 -1089 1169
		mu 0 4 868 821 32 827
		f 4 1170 -1144 -1091 1171
		mu 0 4 869 854 813 828
		f 4 1172 1173 -1093 -1146
		mu 0 4 855 870 829 814
		f 4 1174 1175 -1095 -1137
		mu 0 4 851 871 830 810
		f 4 1176 -1139 -1097 1177
		mu 0 4 872 852 811 831
		f 4 1178 1179 -1099 -1088
		mu 0 4 826 873 832 13
		f 4 1180 -1090 -1101 1181
		mu 0 4 874 827 8 833
		f 4 1182 1183 -1103 -1116
		mu 0 4 840 875 834 799
		f 4 1184 -1118 -1105 1185
		mu 0 4 876 841 800 835
		f 4 1186 -1133 -1107 1187
		mu 0 4 877 849 808 836
		f 4 1188 1189 -1109 -1135
		mu 0 4 850 878 837 809
		f 4 1190 1191 -1111 -1100
		mu 0 4 832 879 838 791
		f 4 1192 -1102 -1113 1193
		mu 0 4 880 833 792 839
		f 4 1194 1195 -1115 -1120
		mu 0 4 842 881 840 801
		f 4 1196 -1122 -1117 1197
		mu 0 4 882 843 802 841
		f 4 1198 1199 -1119 -1112
		mu 0 4 838 883 842 797
		f 4 1200 -1114 -1121 1201
		mu 0 4 884 839 798 843
		f 4 1202 1203 -1123 -1176
		mu 0 4 871 885 844 830
		f 4 1204 -1178 -1125 1205
		mu 0 4 886 872 831 845
		f 4 1206 1207 -1127 1208
		mu 0 4 887 888 847 846
		f 4 1209 1210 -1130 -1208
		mu 0 4 888 889 848 847
		f 4 1211 -1172 -1132 1212
		mu 0 4 890 869 828 849
		f 4 1213 1214 -1134 -1174
		mu 0 4 870 891 850 829
		f 4 1215 1216 -1136 -1160
		mu 0 4 863 892 851 822
		f 4 1217 -1162 -1138 1218
		mu 0 4 893 864 823 852
		f 4 1219 1220 -1140 -1204
		mu 0 4 885 894 853 844
		f 4 1221 -1206 -1142 -1221
		mu 0 4 894 886 845 853
		f 4 1222 -1209 -1143 1223
		mu 0 4 895 887 846 854
		f 4 1224 1225 -1145 -1211
		mu 0 4 889 896 855 848
		f 4 1226 1227 -1147 -1152
		mu 0 4 859 897 856 818
		f 4 1228 -1154 -1149 1229
		mu 0 4 898 860 819 857
		f 4 1230 1231 -1151 -1164
		mu 0 4 899 900 859 858
		f 4 1232 -1166 -1153 1233
		mu 0 4 901 866 825 860
		f 4 1234 1235 -1155 -1148
		mu 0 4 856 902 861 815
		f 4 1236 -1150 -1157 1237
		mu 0 4 903 857 816 862
		f 4 1238 1239 -1159 -1184
		mu 0 4 875 904 863 834
		f 4 1240 -1186 -1161 1241
		mu 0 4 905 876 835 864
		f 4 1242 1243 -1163 -1190
		mu 0 4 878 906 865 837
		f 4 1244 -1188 -1165 1245
		mu 0 4 907 877 836 866
		f 4 1246 1247 -1167 -1156
		mu 0 4 861 908 867 820
		f 4 1248 -1158 -1169 1249
		mu 0 4 909 862 821 868
		f 4 1250 1251 1252 1253
		mu 0 4 910 911 912 913
		f 4 1254 -1254 1255 1256
		mu 0 4 914 915 916 917
		f 4 1257 -1224 -1171 1258
		mu 0 4 918 895 854 869
		f 4 1259 1260 -1173 -1226
		mu 0 4 896 919 870 855
		f 4 1261 1262 -1175 -1217
		mu 0 4 892 920 871 851
		f 4 1263 -1219 -1177 1264
		mu 0 4 921 893 852 872
		f 4 1265 1266 -1179 -1168
		mu 0 4 867 922 873 826
		f 4 1267 -1170 -1181 1268
		mu 0 4 923 868 827 874
		f 4 1269 1270 -1183 -1196
		mu 0 4 881 924 875 840
		f 4 1271 -1198 -1185 1272
		mu 0 4 925 882 841 876
		f 4 1273 -1213 -1187 1274
		mu 0 4 926 890 849 877
		f 4 1275 1276 -1189 -1215
		mu 0 4 891 927 878 850
		f 4 1277 1278 1279 -1252
		mu 0 4 928 929 930 931
		f 4 1280 -1257 1281 1282
		mu 0 4 932 933 934 935
		f 4 1283 1284 -1191 -1180
		mu 0 4 873 936 879 832
		f 4 1285 -1182 -1193 1286
		mu 0 4 937 874 833 880
		f 4 1287 1288 -1195 -1200
		mu 0 4 883 938 881 842
		f 4 1289 -1202 -1197 1290
		mu 0 4 939 884 843 882
		f 4 1291 1292 -1199 -1192
		mu 0 4 879 940 883 838
		f 4 1293 -1194 -1201 1294
		mu 0 4 941 880 839 884
		f 4 1295 1296 -1203 -1263
		mu 0 4 920 942 885 871
		f 4 1297 -1265 -1205 1298
		mu 0 4 943 921 872 886
		f 4 1299 1300 1301 -1279
		mu 0 4 944 945 946 947
		f 4 1302 -1283 1303 1304
		mu 0 4 948 949 950 951
		f 4 1305 -1259 -1212 1306
		mu 0 4 952 918 869 890
		f 4 1307 1308 -1214 -1261
		mu 0 4 919 953 891 870
		f 4 1309 1310 -1216 -1240
		mu 0 4 904 954 892 863
		f 4 1311 -1242 -1218 1312
		mu 0 4 955 905 864 893
		f 4 1313 1314 -1220 -1297
		mu 0 4 942 956 894 885
		f 4 1315 -1299 -1222 -1315
		mu 0 4 956 943 886 894
		f 4 1316 1317 -1227 -1232
		mu 0 4 900 957 897 859
		f 4 1318 -1234 -1229 1319
		mu 0 4 958 901 860 898
		f 4 1321 1322 -1231 -1244
		mu 0 4 959 960 900 899
		f 4 1323 -1246 -1233 1324
		mu 0 4 961 907 866 901
		f 4 1325 1326 1327 1328
		mu 0 4 962 944 911 963
		f 4 1329 1330 1331 1332
		mu 0 4 964 914 949 965
		f 4 1333 1334 -1235 -1228
		mu 0 4 897 966 902 856
		f 4 1335 -1230 -1237 1336
		mu 0 4 967 898 857 903
		f 4 1337 1338 -1239 -1271
		mu 0 4 924 968 904 875
		f 4 1339 -1273 -1241 1340
		mu 0 4 969 925 876 905
		f 4 1343 1344 -1243 -1277
		mu 0 4 927 970 906 878
		f 4 1345 -1275 -1245 1346
		mu 0 4 971 926 877 907
		f 4 1347 1348 -1247 -1236
		mu 0 4 902 972 908 861
		f 4 1349 -1238 -1249 1350
		mu 0 4 973 903 862 909
		f 4 1351 1352 1353 -1301
		mu 0 4 974 975 976 977
		f 4 1354 -1305 1355 1356
		mu 0 4 978 979 980 981
		f 4 1357 1358 -1262 -1311
		mu 0 4 954 982 920 892
		f 4 1359 -1313 -1264 1360
		mu 0 4 983 955 893 921
		f 4 1361 1362 -1266 -1248
		mu 0 4 908 984 922 867
		f 4 1363 -1250 -1268 1364
		mu 0 4 985 909 868 923
		f 4 1365 1366 -1270 -1289
		mu 0 4 938 986 924 881
		f 4 1367 -1291 -1272 1368
		mu 0 4 987 939 882 925
		f 4 1369 -1307 -1274 1370
		mu 0 4 988 952 890 926
		f 4 1371 1372 -1276 -1309
		mu 0 4 953 989 927 891
		f 4 1373 1374 -1284 -1267
		mu 0 4 922 990 936 873
		f 4 1375 -1269 -1286 1376
		mu 0 4 991 923 874 937
		f 4 1377 1378 -1288 -1293
		mu 0 4 940 992 938 883
		f 4 1379 -1295 -1290 1380
		mu 0 4 993 941 884 939
		f 4 1381 1382 -1292 -1285
		mu 0 4 936 994 940 879
		f 4 1383 -1287 -1294 1384
		mu 0 4 995 937 880 941
		f 4 1387 1388 -1296 -1359
		mu 0 4 982 996 942 920
		f 4 1389 -1361 -1298 1390
		mu 0 4 997 983 921 943
		f 4 1391 1392 -1310 -1339
		mu 0 4 968 998 954 904
		f 4 1393 -1341 -1312 1394
		mu 0 4 999 969 905 955
		f 4 1395 1396 -1314 -1389
		mu 0 4 996 1000 956 942
		f 4 1397 -1391 -1316 -1397
		mu 0 4 1000 997 943 956
		f 4 1398 1399 1400 1401
		mu 0 4 1001 1002 945 1003
		f 4 1402 1403 1404 1405
		mu 0 4 1004 948 1005 1006
		f 4 1406 1407 1408 -1353
		mu 0 4 1002 1007 1008 1009
		f 4 1409 -1357 1410 1411
		mu 0 4 1010 1005 1011 1012
		f 4 1414 1415 -1317 -1323
		mu 0 4 960 1013 957 900
		f 4 1416 -1325 -1319 1417
		mu 0 4 1014 961 901 958
		f 4 1418 1419 -1322 -1345
		mu 0 4 1015 1016 960 959
		f 4 1420 -1347 -1324 1421
		mu 0 4 1017 971 907 961
		f 4 1422 1423 -1334 -1318
		mu 0 4 957 1018 966 897
		f 4 1424 -1320 -1336 1425
		mu 0 4 1019 958 898 967
		f 4 1426 1427 -1338 -1367
		mu 0 4 986 1020 968 924
		f 4 1428 -1369 -1340 1429
		mu 0 4 1021 987 925 969
		f 4 1430 1431 -1344 -1373
		mu 0 4 989 1022 970 927
		f 4 1432 -1371 -1346 1433
		mu 0 4 1023 988 926 971
		f 4 1434 1435 -1348 -1335
		mu 0 4 966 1024 972 902
		f 4 1436 -1337 -1350 1437
		mu 0 4 1025 967 903 973
		f 4 1438 1439 -1358 -1393
		mu 0 4 998 1026 982 954
		f 4 1440 -1395 -1360 1441
		mu 0 4 1027 999 955 983
		f 4 1442 1443 -1362 -1349
		mu 0 4 972 1028 984 908
		f 4 1444 -1351 -1364 1445
		mu 0 4 1029 973 909 985
		f 4 1446 1447 -1366 -1379
		mu 0 4 992 1030 986 938
		f 4 1448 -1381 -1368 1449
		mu 0 4 1031 993 939 987
		f 4 1450 1451 -1374 -1363
		mu 0 4 984 1032 990 922
		f 4 1452 -1365 -1376 1453
		mu 0 4 1033 985 923 991
		f 4 1454 1455 -1378 -1383
		mu 0 4 994 1034 992 940
		f 4 1456 -1385 -1380 1457
		mu 0 4 1035 995 941 993
		f 4 1458 1459 -1382 -1375
		mu 0 4 990 1036 994 936
		f 4 1460 -1377 -1384 1461
		mu 0 4 1037 991 937 995
		f 4 1464 1465 -1388 -1440
		mu 0 4 1026 1038 996 982
		f 4 1466 -1442 -1390 1467
		mu 0 4 1039 1027 983 997
		f 4 1468 -1412 1469 1470
		mu 0 4 1040 1041 1042 1043
		f 4 1471 1472 1473 -1408
		mu 0 4 1044 1045 1046 1047
		f 4 1474 1475 -1392 -1428
		mu 0 4 1020 1048 998 968
		f 4 1476 -1430 -1394 1477
		mu 0 4 1049 1021 969 999
		f 4 1478 1479 -1396 -1466
		mu 0 4 1038 1050 1000 996
		f 4 1480 -1468 -1398 -1480
		mu 0 4 1050 1039 997 1000
		f 4 1481 1482 -1415 -1420
		mu 0 4 1016 1051 1013 960
		f 4 1483 -1422 -1417 1484
		mu 0 4 1052 1017 961 1014
		f 4 1485 1486 -1419 -1432
		mu 0 4 1053 1054 1016 1015
		f 4 1487 -1434 -1421 1488
		mu 0 4 1055 1023 971 1017
		f 4 1489 1490 -1423 -1416
		mu 0 4 1013 1056 1018 957
		f 4 1491 -1418 -1425 1492
		mu 0 4 1057 1014 958 1019
		f 4 1493 1494 -1427 -1448
		mu 0 4 1030 1058 1020 986
		f 4 1495 -1450 -1429 1496
		mu 0 4 1059 1031 987 1021
		f 4 1497 1498 1499 1500
		mu 0 4 1060 1010 1061 1062
		f 4 1501 1502 1503 1504
		mu 0 4 1063 1064 1007 1065
		f 4 1507 1508 -1435 -1424
		mu 0 4 1018 1066 1024 966
		f 4 1509 -1426 -1437 1510
		mu 0 4 1067 1019 967 1025
		f 5 1511 1512 1513 1514 -1328
		mu 0 5 928 915 1068 1069 1070
		f 5 1515 -1330 1516 1517 -1513
		mu 0 5 910 933 1071 1072 1073
		f 4 1518 1519 -1439 -1476
		mu 0 4 1048 1074 1026 998
		f 4 1520 -1478 -1441 1521
		mu 0 4 1075 1049 999 1027
		f 4 1522 1523 -1443 -1436
		mu 0 4 1024 1076 1028 972
		f 4 1524 -1438 -1445 1525
		mu 0 4 1077 1025 973 1029
		f 4 1526 1527 -1447 -1456
		mu 0 4 1034 1078 1030 992
		f 4 1528 -1458 -1449 1529
		mu 0 4 1079 1035 993 1031
		f 4 1530 1531 -1451 -1444
		mu 0 4 1028 1080 1032 984
		f 4 1532 -1446 -1453 1533
		mu 0 4 1081 1029 985 1033
		f 4 1534 1535 -1455 -1460
		mu 0 4 1036 1082 1034 994
		f 4 1536 -1462 -1457 1537
		mu 0 4 1083 1037 995 1035
		f 4 1538 1539 -1459 -1452
		mu 0 4 1032 1084 1036 990
		f 4 1540 -1454 -1461 1541
		mu 0 4 1085 1033 991 1037
		f 4 1542 -1471 1543 1544
		mu 0 4 1086 1061 1087 1088
		f 4 1545 1546 1547 -1473
		mu 0 4 1064 1089 1090 1091
		f 4 1548 1549 -1465 -1520
		mu 0 4 1074 1092 1038 1026
		f 4 1550 -1522 -1467 1551
		mu 0 4 1093 1075 1027 1039
		f 4 1552 1553 -1475 -1495
		mu 0 4 1058 1094 1048 1020
		f 4 1554 -1497 -1477 1555
		mu 0 4 1095 1059 1021 1049
		f 4 1556 1557 -1479 -1550
		mu 0 4 1092 1096 1050 1038
		f 4 1558 -1552 -1481 -1558
		mu 0 4 1096 1093 1039 1050
		f 4 -1500 1561 1562 1563
		mu 0 4 1097 1040 1098 1099
		f 5 1564 -1326 1565 1566 -1401
		mu 0 5 974 929 1100 1101 1102
		f 5 1567 -1403 1568 1569 -1332
		mu 0 5 932 979 1103 1104 1105
		f 4 1570 1571 -1482 -1487
		mu 0 4 1054 1106 1051 1016
		f 4 1572 -1489 -1484 1573
		mu 0 4 1107 1055 1017 1052
		f 4 1574 1575 -1490 -1483
		mu 0 4 1051 1108 1056 1013
		f 4 1576 -1485 -1492 1577
		mu 0 4 1109 1052 1014 1057
		f 4 1578 1579 -1494 -1528
		mu 0 4 1078 1110 1058 1030
		f 4 1580 -1530 -1496 1581
		mu 0 4 1111 1079 1031 1059
		f 4 1582 1583 -1508 -1491
		mu 0 4 1056 1112 1066 1018
		f 4 1584 -1493 -1510 1585
		mu 0 4 1113 1057 1019 1067
		f 4 1586 1587 -1519 -1554
		mu 0 4 1094 1114 1074 1048
		f 4 1588 -1556 -1521 1589
		mu 0 4 1115 1095 1049 1075
		f 4 1590 1591 -1523 -1509
		mu 0 4 1066 1116 1076 1024
		f 4 1592 -1511 -1525 1593
		mu 0 4 1117 1067 1025 1077
		f 4 1594 1595 -1527 -1536
		mu 0 4 1082 1118 1078 1034
		f 4 1596 -1538 -1529 1597
		mu 0 4 1119 1083 1035 1079
		f 4 1600 1601 -1531 -1524
		mu 0 4 1076 1120 1080 1028
		f 4 1602 -1526 -1533 1603
		mu 0 4 1121 1077 1029 1081
		f 4 1604 1605 -1535 -1540
		mu 0 4 1084 1122 1082 1036
		f 4 1606 -1542 -1537 1607
		mu 0 4 1123 1085 1037 1083
		f 4 1608 1609 -1539 -1532
		mu 0 4 1080 1124 1084 1032
		f 4 1610 -1534 -1541 1611
		mu 0 4 1125 1081 1033 1085
		f 5 1612 -1514 -1518 1613 -1321
		mu 0 5 1126 1127 1128 1129 1130
		f 4 1614 -1545 1615 1616
		mu 0 4 1131 1098 1132 1133
		f 4 1617 1618 1619 -1547
		mu 0 4 1134 1135 1136 1137
		f 4 1620 1621 -1549 -1588
		mu 0 4 1114 1138 1092 1074
		f 4 1622 -1590 -1551 1623
		mu 0 4 1139 1115 1075 1093
		f 4 1624 1625 -1553 -1580
		mu 0 4 1110 1140 1094 1058
		f 4 1626 -1582 -1555 1627
		mu 0 4 1141 1111 1059 1095
		f 4 1628 1629 -1557 -1622
		mu 0 4 1138 1142 1096 1092
		f 4 1630 -1624 -1559 -1630
		mu 0 4 1142 1139 1093 1096
		f 4 1631 1632 1633 1634
		mu 0 4 1143 1144 1089 1145
		f 4 1637 1638 -1575 -1572
		mu 0 4 1106 1146 1108 1051
		f 4 1639 -1574 -1577 1640
		mu 0 4 1147 1107 1052 1109
		f 4 1641 1642 -1579 -1596
		mu 0 4 1118 1148 1110 1078
		f 4 1643 -1598 -1581 1644
		mu 0 4 1149 1119 1079 1111
		f 4 1647 1648 -1583 -1576
		mu 0 4 1108 1150 1112 1056
		f 4 1649 -1578 -1585 1650
		mu 0 4 1151 1109 1057 1113
		f 4 1652 1653 -1587 -1626
		mu 0 4 1140 1152 1114 1094
		f 4 1654 -1628 -1589 1655
		mu 0 4 1153 1141 1095 1115
		f 4 1656 1657 -1591 -1584
		mu 0 4 1112 1154 1116 1066
		f 4 1658 -1586 -1593 1659
		mu 0 4 1155 1113 1067 1117
		f 4 1660 1661 -1595 -1606
		mu 0 4 1122 1156 1118 1082
		f 4 1662 -1608 -1597 1663
		mu 0 4 1157 1123 1083 1119
		f 4 1664 1665 -1601 -1592
		mu 0 4 1116 1158 1120 1076
		f 4 1666 -1594 -1603 1667
		mu 0 4 1159 1117 1077 1121
		f 4 1668 1669 -1605 -1610
		mu 0 4 1124 1160 1122 1084
		f 4 1670 -1612 -1607 1671
		mu 0 4 1161 1125 1085 1123
		f 4 1672 1673 -1609 -1602
		mu 0 4 1120 1162 1124 1080
		f 4 1674 -1604 -1611 1675
		mu 0 4 1163 1121 1081 1125
		f 4 1676 -1617 1677 1678
		mu 0 4 1164 1165 1166 1167
		f 4 1679 1680 1681 -1619
		mu 0 4 1144 1168 1169 1170
		f 4 1682 1683 -1621 -1654
		mu 0 4 1152 1171 1138 1114
		f 4 1684 -1656 -1623 1685
		mu 0 4 1172 1153 1115 1139
		f 5 1686 -1399 1687 1688 -1504
		mu 0 5 1044 975 1173 1174 1175
		f 5 1689 -1498 1690 1691 -1405
		mu 0 5 978 1041 1176 1177 1178
		f 4 1692 1693 -1625 -1643
		mu 0 4 1148 1179 1140 1110
		f 4 1694 -1645 -1627 1695
		mu 0 4 1180 1149 1111 1141
		f 4 1696 1697 1698 1699
		mu 0 4 1181 1131 1182 1183
		f 4 1700 1701 -1629 -1684
		mu 0 4 1171 1184 1142 1138
		f 4 1702 -1686 -1631 -1702
		mu 0 4 1184 1172 1139 1142
		f 4 1703 1704 -1642 -1662
		mu 0 4 1156 1185 1148 1118
		f 4 1705 -1664 -1644 1706
		mu 0 4 1186 1157 1119 1149
		f 4 1707 1708 -1648 -1639
		mu 0 4 1146 1187 1150 1108
		f 4 1709 -1641 -1650 1710
		mu 0 4 1188 1147 1109 1151
		f 4 1711 1712 -1653 -1694
		mu 0 4 1179 1189 1152 1140
		f 4 1713 -1696 -1655 1714
		mu 0 4 1190 1180 1141 1153
		f 4 1715 1716 -1657 -1649
		mu 0 4 1150 1191 1154 1112
		f 4 1717 -1651 -1659 1718
		mu 0 4 1192 1151 1113 1155
		f 4 1719 1720 -1661 -1670
		mu 0 4 1160 1193 1156 1122
		f 4 1721 -1672 -1663 1722
		mu 0 4 1194 1161 1123 1157
		f 4 1723 1724 -1665 -1658
		mu 0 4 1154 1195 1158 1116
		f 4 1725 -1660 -1667 1726
		mu 0 4 1196 1155 1117 1159
		f 4 1727 1728 -1669 -1674
		mu 0 4 1162 1197 1160 1124
		f 4 1729 -1676 -1671 1730
		mu 0 4 1198 1163 1125 1161
		f 4 1731 1732 -1673 -1666
		mu 0 4 1158 1199 1162 1120
		f 4 1733 -1668 -1675 1734
		mu 0 4 1200 1159 1121 1163
		f 4 1735 1736 -1683 -1713
		mu 0 4 1189 1201 1171 1152
		f 4 1737 -1715 -1685 1738
		mu 0 4 1202 1190 1153 1172
		f 4 1739 -1679 1740 1741
		mu 0 4 1203 1182 1204 1205
		f 4 1742 1743 1744 -1681
		mu 0 4 1206 1207 1208 1209
		f 4 1745 1746 1747 1748
		mu 0 4 1210 1211 1168 1212
		f 4 1749 1750 -1693 -1705
		mu 0 4 1185 1213 1179 1148
		f 4 1751 -1707 -1695 1752
		mu 0 4 1214 1186 1149 1180
		f 4 1753 1754 -1701 -1737
		mu 0 4 1201 1215 1184 1171
		f 4 1755 -1739 -1703 -1755
		mu 0 4 1215 1202 1172 1184
		f 4 1756 1757 -1704 -1721
		mu 0 4 1193 1216 1185 1156
		f 4 1758 -1723 -1706 1759
		mu 0 4 1217 1194 1157 1186
		f 4 1760 1761 -1712 -1751
		mu 0 4 1213 1218 1189 1179
		f 4 1762 -1753 -1714 1763
		mu 0 4 1219 1214 1180 1190
		f 4 1764 1765 -1716 -1709
		mu 0 4 1187 1220 1191 1150
		f 4 1766 -1711 -1718 1767
		mu 0 4 1221 1188 1151 1192
		f 4 1768 1769 -1720 -1729
		mu 0 4 1197 1222 1193 1160
		f 4 1770 -1731 -1722 1771
		mu 0 4 1223 1198 1161 1194
		f 4 1772 1773 -1724 -1717
		mu 0 4 1191 1224 1195 1154
		f 4 1774 -1719 -1726 1775
		mu 0 4 1225 1192 1155 1196
		f 4 1776 1777 -1728 -1733
		mu 0 4 1199 1226 1197 1162
		f 4 1778 -1735 -1730 1779
		mu 0 4 1227 1200 1163 1198
		f 4 1780 1781 -1732 -1725
		mu 0 4 1195 1228 1199 1158
		f 4 1782 -1727 -1734 1783
		mu 0 4 1229 1196 1159 1200
		f 4 1784 1785 -1736 -1762
		mu 0 4 1218 1230 1201 1189
		f 4 1786 -1764 -1738 1787
		mu 0 4 1231 1219 1190 1202
		f 4 1788 1789 1790 1791
		mu 0 4 1232 1203 1233 1234
		f 4 1792 1793 -1750 -1758
		mu 0 4 1216 1235 1213 1185
		f 4 1794 -1760 -1752 1795
		mu 0 4 1236 1217 1186 1214
		f 4 1796 1797 -1754 -1786
		mu 0 4 1230 1237 1215 1201
		f 4 1798 -1788 -1756 -1798
		mu 0 4 1237 1231 1202 1215
		f 4 1799 -1742 1800 1801
		mu 0 4 1238 1239 1240 1241
		f 4 1802 1803 1804 -1744
		mu 0 4 1211 1242 1243 1244
		f 5 1805 -1502 1806 1807 -1634
		mu 0 5 1134 1045 1245 1246 1247
		f 6 1808 -1566 -1329 -1515 -1613 -1342
		mu 0 6 1248 1249 1250 1251 1127 1126
		f 6 -1614 -1517 -1333 -1570 1809 -1343
		mu 0 6 1130 1129 1252 1253 1254 1255
		f 4 1810 1811 -1757 -1770
		mu 0 4 1222 1256 1216 1193
		f 4 1812 -1772 -1759 1813
		mu 0 4 1257 1223 1194 1217
		f 4 1814 1815 -1761 -1794
		mu 0 4 1235 1258 1218 1213
		f 4 1816 -1796 -1763 1817
		mu 0 4 1259 1236 1214 1219
		f 4 1818 1819 -1769 -1778
		mu 0 4 1226 1260 1222 1197
		f 4 1820 -1780 -1771 1821
		mu 0 4 1261 1227 1198 1223
		f 4 1822 1823 -1773 -1766
		mu 0 4 1220 1262 1224 1191
		f 4 1824 -1768 -1775 1825
		mu 0 4 1263 1221 1192 1225
		f 4 1826 1827 -1777 -1782
		mu 0 4 1228 1264 1226 1199
		f 4 1828 -1784 -1779 1829
		mu 0 4 1265 1229 1200 1227
		f 4 1830 1831 -1781 -1774
		mu 0 4 1224 1266 1228 1195
		f 4 1832 -1776 -1783 1833
		mu 0 4 1267 1225 1196 1229
		f 4 1834 1835 -1785 -1816
		mu 0 4 1258 1268 1230 1218
		f 4 1836 -1818 -1787 1837
		mu 0 4 1269 1259 1219 1231
		f 4 1838 1839 1840 1841
		mu 0 4 1270 1271 1242 1272
		f 4 1842 1843 -1793 -1812
		mu 0 4 1256 1273 1235 1216
		f 4 1844 -1814 -1795 1845
		mu 0 4 1274 1257 1217 1236
		f 4 1846 1847 -1797 -1836
		mu 0 4 1268 1275 1237 1230
		f 4 1848 -1838 -1799 -1848
		mu 0 4 1275 1269 1231 1237
		f 4 1849 -1802 1850 1851
		mu 0 4 1276 1233 1277 1278
		f 4 1852 1853 1854 -1804
		mu 0 4 1279 1280 1281 1282
		f 5 1855 -1697 1856 1857 -1563
		mu 0 5 1086 1165 1283 1284 1285
		f 4 1858 1859 -1811 -1820
		mu 0 4 1260 1286 1256 1222
		f 4 1860 -1822 -1813 1861
		mu 0 4 1287 1261 1223 1257
		f 4 1862 1863 -1815 -1844
		mu 0 4 1273 1288 1258 1235
		f 4 1864 -1846 -1817 1865
		mu 0 4 1289 1274 1236 1259
		f 4 1866 1867 -1819 -1828
		mu 0 4 1264 1290 1260 1226
		f 4 1868 -1830 -1821 1869
		mu 0 4 1291 1265 1227 1261
		f 4 1870 1871 -1827 -1832
		mu 0 4 1266 1292 1264 1228
		f 4 1872 -1834 -1829 1873
		mu 0 4 1293 1267 1229 1265
		f 4 1874 1875 -1831 -1824
		mu 0 4 1262 1294 1266 1224
		f 4 1876 -1826 -1833 1877
		mu 0 4 1295 1263 1225 1267
		f 4 1878 1879 -1835 -1864
		mu 0 4 1288 1296 1268 1258
		f 4 1880 -1866 -1837 1881
		mu 0 4 1297 1289 1259 1269
		f 4 1882 1883 1884 1885
		mu 0 4 1298 1276 1299 1300
		f 6 1886 -1688 -1402 -1567 -1809 -1386
		mu 0 6 1301 1302 1303 1304 1249 1248
		f 6 -1810 -1569 -1406 -1692 1887 -1387
		mu 0 6 1255 1254 1305 1306 1307 1308
		f 4 1888 1889 -1843 -1860
		mu 0 4 1286 1309 1273 1256
		f 4 1890 -1862 -1845 1891
		mu 0 4 1310 1287 1257 1274
		f 4 1892 1893 -1847 -1880
		mu 0 4 1296 1311 1275 1268
		f 4 1894 -1882 -1849 -1894
		mu 0 4 1311 1297 1269 1275
		f 4 1895 -1852 1896 1897
		mu 0 4 1312 1313 1314 1315
		f 4 1898 1899 1900 -1854
		mu 0 4 1271 1316 1317 1318
		f 4 1901 1902 -1859 -1868
		mu 0 4 1290 1319 1286 1260
		f 4 1903 -1870 -1861 1904
		mu 0 4 1320 1291 1261 1287
		f 4 1905 1906 -1863 -1890
		mu 0 4 1309 1321 1288 1273
		f 4 1907 -1892 -1865 1908
		mu 0 4 1322 1310 1274 1289
		f 4 1909 1910 -1867 -1872
		mu 0 4 1292 1323 1290 1264
		f 4 1911 -1874 -1869 1912
		mu 0 4 1324 1293 1265 1291
		f 4 1913 1914 -1871 -1876
		mu 0 4 1294 1325 1292 1266
		f 4 1915 -1878 -1873 1916
		mu 0 4 1326 1295 1267 1293
		f 4 1917 1918 1919 1920
		mu 0 4 1327 1328 1316 1329
		f 4 1921 1922 -1879 -1907
		mu 0 4 1321 1330 1296 1288
		f 4 1923 -1909 -1881 1924
		mu 0 4 1331 1322 1289 1297
		f 5 1925 -1632 1926 1927 -1748
		mu 0 5 1206 1135 1332 1333 1334
		f 4 1928 1929 -1889 -1903
		mu 0 4 1319 1335 1309 1286
		f 4 1930 -1905 -1891 1931
		mu 0 4 1336 1320 1287 1310
		f 4 1932 1933 -1893 -1923
		mu 0 4 1330 1337 1311 1296
		f 4 1934 -1925 -1895 -1934
		mu 0 4 1337 1331 1297 1311
		f 4 1935 -1898 1936 1937
		mu 0 4 1338 1299 1339 1340
		f 4 1938 1939 1940 -1900
		mu 0 4 1341 1342 1343 1344
		f 4 1941 1942 -1902 -1911
		mu 0 4 1323 1345 1319 1290
		f 4 1943 -1913 -1904 1944
		mu 0 4 1346 1324 1291 1320
		f 4 1945 1946 -1906 -1930
		mu 0 4 1335 1347 1321 1309
		f 4 1947 -1932 -1908 1948
		mu 0 4 1348 1336 1310 1322
		f 4 1949 1950 -1910 -1915
		mu 0 4 1325 1349 1323 1292
		f 4 1951 -1917 -1912 1952
		mu 0 4 1350 1326 1293 1324
		f 4 1953 1954 1955 1956
		mu 0 4 1351 1338 1352 1353
		f 4 1957 1958 -1922 -1947
		mu 0 4 1347 1354 1330 1321
		f 4 1959 -1949 -1924 1960
		mu 0 4 1355 1348 1322 1331
		f 4 1961 1962 -1929 -1943
		mu 0 4 1345 1356 1335 1319
		f 4 1963 -1945 -1931 1964
		mu 0 4 1357 1346 1320 1336
		f 4 1965 1966 -1933 -1959
		mu 0 4 1354 1358 1337 1330
		f 4 1967 -1961 -1935 -1967
		mu 0 4 1358 1355 1331 1337
		f 4 1968 -1938 1969 1970
		mu 0 4 1359 1360 1361 1362
		f 4 1971 1972 1973 -1940
		mu 0 4 1328 1363 1364 1365
		f 5 1974 -1789 1975 1976 -1699
		mu 0 5 1164 1239 1366 1367 1368
		f 4 1977 1978 1979 1980
		mu 0 4 1369 1370 1363 1371
		f 4 1981 1982 -1942 -1951
		mu 0 4 1349 1372 1345 1323
		f 4 1983 -1953 -1944 1984
		mu 0 4 1373 1350 1324 1346
		f 4 1985 1986 -1946 -1963
		mu 0 4 1356 1374 1347 1335
		f 4 1987 -1965 -1948 1988
		mu 0 4 1375 1357 1336 1348
		f 6 1989 -1807 -1505 -1689 -1887 -1414
		mu 0 6 1376 1377 1378 1379 1302 1301
		f 4 1990 1991 -1958 -1987
		mu 0 4 1374 1380 1354 1347
		f 4 1992 -1989 -1960 1993
		mu 0 4 1381 1375 1348 1355
		f 4 1994 -1971 1995 1996
		mu 0 4 1382 1352 1383 1384
		f 4 1997 1998 1999 -1973
		mu 0 4 1385 1386 1387 1388
		f 4 2000 2001 2002 2003
		mu 0 4 1389 1382 1390 1391
		f 4 2004 2005 -1978 2006
		mu 0 4 1392 1393 1386 1394
		f 4 2007 2008 -1962 -1983
		mu 0 4 1372 1395 1356 1345
		f 4 2009 -1985 -1964 2010
		mu 0 4 1396 1373 1346 1357
		f 4 2011 2012 -1966 -1992
		mu 0 4 1380 1397 1358 1354
		f 4 2013 -1994 -1968 -2013
		mu 0 4 1397 1381 1355 1358
		f 4 2014 -1997 2015 2016
		mu 0 4 1398 1399 1400 1401
		f 4 2017 2018 2019 -1999
		mu 0 4 1370 1402 1403 1404
		f 4 2020 2021 -1986 -2009
		mu 0 4 1395 1405 1374 1356
		f 4 2022 -2011 -1988 2023
		mu 0 4 1406 1396 1357 1375
		f 4 2024 2025 2026 2027
		mu 0 4 1407 1408 1409 1410
		f 4 2028 2029 2030 2031
		mu 0 4 1411 1412 1413 1414
		f 4 2032 2033 -1991 -2022
		mu 0 4 1405 1415 1380 1374
		f 4 2034 -2024 -1993 2035
		mu 0 4 1416 1406 1375 1381
		f 4 2036 -2017 2037 2038
		mu 0 4 1408 1390 1417 1418
		f 4 2039 2040 2041 -2019
		mu 0 4 1393 1419 1420 1421
		f 5 2042 -1746 2043 2044 -1841
		mu 0 5 1279 1207 1422 1423 1424
		f 4 2045 2046 -2012 -2034
		mu 0 4 1415 1425 1397 1380
		f 4 2047 -2036 -2014 -2047
		mu 0 4 1425 1416 1381 1397
		f 4 2048 -2039 2049 2050
		mu 0 4 1426 1427 1428 1429
		f 4 2051 2052 2053 -2041
		mu 0 4 1430 1413 1431 1432
		f 4 2054 -2051 2055 2056
		mu 0 4 1412 1409 1433 1434
		f 4 2057 -2057 2058 -2053
		mu 0 4 1435 1436 1437 1438
		f 5 2059 -1883 2060 2061 -1791
		mu 0 5 1238 1313 1439 1440 1441
		f 6 2062 -1927 -1635 -1808 -1990 -1464
		mu 0 6 1442 1443 1444 1445 1377 1376
		f 6 2063 -1857 -1700 -1977 2064 -1463
		mu 0 6 1446 1447 1448 1449 1450 1451
		f 5 2065 -1839 2066 2067 -1920
		mu 0 5 1341 1280 1452 1453 1454
		f 5 2068 -1954 2069 2070 -1885
		mu 0 5 1312 1360 1455 1456 1457
		f 7 -1888 -1691 -1501 -1564 -1858 -2064 -1413
		mu 0 7 1308 1307 1458 1459 1460 1461 1462
		f 5 2071 -1918 2072 2073 -1980
		mu 0 5 1385 1342 1463 1464 1465
		f 6 2074 -2044 -1749 -1928 -2063 -1507
		mu 0 6 1466 1467 1468 1469 1443 1442
		f 5 2075 -2001 2076 2077 -1956
		mu 0 5 1359 1399 1470 1471 1472
		f 6 -2065 -1976 -1792 -2062 2078 -1506
		mu 0 6 1451 1450 1473 1474 1475 1476
		f 5 2079 2080 2081 2082 -1646
		mu 0 5 1477 1478 1479 1480 1481
		f 5 2083 -2025 2084 2085 -2003
		mu 0 5 1398 1427 1482 1483 1484
		f 5 2086 -2005 2087 -2082 2088
		mu 0 5 1430 1402 1485 1486 1487
		f 5 2089 -2089 -2081 2090 -2031
		mu 0 5 1435 1419 1488 1489 1490
		f 5 2091 -2029 2092 2093 -2027
		mu 0 5 1426 1436 1491 1492 1493
		f 6 2094 -2067 -1842 -2045 -2075 -1560
		mu 0 6 1494 1495 1496 1497 1467 1466
		f 6 -2079 -2061 -1886 -2071 2095 -1561
		mu 0 6 1476 1475 1498 1499 1500 1501
		f 6 2096 -2073 -1921 -2068 -2095 -1599
		mu 0 6 1502 1503 1504 1505 1495 1494
		f 6 -2096 -2070 -1957 -2078 2097 -1600
		mu 0 6 1501 1500 1506 1507 1508 1509
		f 6 -2098 -2077 -2004 -2086 2098 -1637
		mu 0 6 1509 1508 1510 1511 1512 1513
		f 6 -2099 -2085 -2028 -2094 2099 -1647
		mu 0 6 1513 1512 1514 1515 1516 1517
		f 6 -2100 -2093 -2032 -2091 -2080 -1652
		mu 0 6 1517 1516 1518 1519 1478 1477
		f 7 -2083 -2088 -2007 -1981 -2074 -2097 -1636
		mu 0 7 1481 1480 1520 1521 1522 1503 1502;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "FFB2131D-4142-D2A9-5C92-C3B016AD03FC";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "48335DF9-CF4E-5519-49F3-42886B2D9295";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "C7F03077-7147-476E-17CD-79A80C82F13C";
createNode displayLayerManager -n "layerManager";
	rename -uid "65EC4EE9-0B4E-1067-56A1-4FBBE94CAACB";
createNode displayLayer -n "defaultLayer";
	rename -uid "DB3D4C32-1347-36BF-7E64-4FA9609793C4";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "488D6A18-A749-1C05-D73F-84AB681E9C48";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "D3769965-6C48-6245-8790-438BC1E3FA5E";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "5EE73600-D744-51E9-8496-3F85DAA0DF20";
	setAttr ".version" -type "string" "5.2.1.1";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "CC004E31-CE41-0E72-DC51-5AA499D27E2E";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "DC07F9A4-BC42-6932-45DD-E6A5AB4679D8";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "24D38108-5A42-1A92-3AD5-5CBC2F93F417";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "23985B52-7645-7E55-482C-9B99054F18A3";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -53.571426442691354 -605.9523568739977 ;
	setAttr ".tgi[0].vh" -type "double2" 1164.2856680211587 44.047617297323995 ;
	setAttr -s 4 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 481.42855834960938;
	setAttr ".tgi[0].ni[0].y" 30;
	setAttr ".tgi[0].ni[0].nvs" 18304;
	setAttr ".tgi[0].ni[1].x" 161.42857360839844;
	setAttr ".tgi[0].ni[1].y" -68.571426391601562;
	setAttr ".tgi[0].ni[1].nvs" 18304;
	setAttr ".tgi[0].ni[2].x" 161.42857360839844;
	setAttr ".tgi[0].ni[2].y" 128.57142639160156;
	setAttr ".tgi[0].ni[2].nvs" 18304;
	setAttr ".tgi[0].ni[3].x" 161.42857360839844;
	setAttr ".tgi[0].ni[3].y" 30;
	setAttr ".tgi[0].ni[3].nvs" 18304;
createNode groupId -n "groupId1";
	rename -uid "B78DBE37-C24E-8397-9F81-10A7D848309F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "263C7FEA-AD49-E0AF-6F7E-1EAA63678D4A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:979]";
createNode polySplit -n "polySplit1";
	rename -uid "F0D299B5-A64B-DD76-AACB-43B4D4EAF286";
	setAttr -s 41 ".e[0:40]"  0.62491602 0.37508401 0.37508401 0.37508401
		 0.37508401 0.37508401 0.37508401 0.37508401 0.37508401 0.37508401 0.37508401 0.37508401
		 0.37508401 0.37508401 0.37508401 0.37508401 0.37508401 0.37508401 0.37508401 0.37508401
		 0.37508401 0.37508401 0.62491602 0.62491602 0.62491602 0.62491602 0.62491602 0.62491602
		 0.62491602 0.62491602 0.62491602 0.62491602 0.62491602 0.62491602 0.62491602 0.62491602
		 0.62491602 0.62491602 0.62491602 0.62491602 0.62491602;
	setAttr -s 41 ".d[0:40]"  -2147483381 -2147483379 -2147483377 -2147483356 -2147483317 -2147483279 
		-2147483224 -2147483173 -2147483118 -2147483059 -2147483000 -2147482941 -2147482886 -2147482835 -2147482788 -2147482753 -2147482722 -2147482705 
		-2147482689 -2147482674 -2147482666 -2147482658 -2147482665 -2147482673 -2147482688 -2147482704 -2147482725 -2147482756 -2147482791 -2147482838 
		-2147482889 -2147482944 -2147483003 -2147483062 -2147483121 -2147483176 -2147483227 -2147483282 -2147483320 -2147483359 -2147483381;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "468D8BE7-EC42-86D9-6961-8DB9FD4D059B";
	setAttr -s 41 ".e[0:40]"  0.67443299 0.32556701 0.32556701 0.32556701
		 0.32556701 0.32556701 0.32556701 0.32556701 0.32556701 0.32556701 0.32556701 0.32556701
		 0.32556701 0.32556701 0.32556701 0.32556701 0.32556701 0.32556701 0.32556701 0.32556701
		 0.32556701 0.32556701 0.67443299 0.67443299 0.67443299 0.67443299 0.67443299 0.67443299
		 0.67443299 0.67443299 0.67443299 0.67443299 0.67443299 0.67443299 0.67443299 0.67443299
		 0.67443299 0.67443299 0.67443299 0.67443299 0.67443299;
	setAttr -s 41 ".d[0:40]"  -2147483585 -2147483583 -2147483580 -2147483553 -2147483503 -2147483460 
		-2147483405 -2147483331 -2147483256 -2147483189 -2147483114 -2147483047 -2147482984 -2147482925 -2147482870 -2147482831 -2147482796 -2147482771 
		-2147482747 -2147482728 -2147482720 -2147482712 -2147482719 -2147482727 -2147482746 -2147482770 -2147482799 -2147482834 -2147482873 -2147482928 
		-2147482987 -2147483050 -2147483117 -2147483192 -2147483259 -2147483334 -2147483408 -2147483463 -2147483507 -2147483557 -2147483585;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit3";
	rename -uid "8DA3CD42-CC49-0A93-8DAC-36BF76DF827B";
	setAttr -s 41 ".e[0:40]"  0.41560599 0.58439398 0.58439398 0.58439398
		 0.58439398 0.58439398 0.58439398 0.58439398 0.58439398 0.58439398 0.58439398 0.58439398
		 0.58439398 0.58439398 0.58439398 0.58439398 0.58439398 0.58439398 0.58439398 0.58439398
		 0.58439398 0.58439398 0.41560599 0.41560599 0.41560599 0.41560599 0.41560599 0.41560599
		 0.41560599 0.41560599 0.41560599 0.41560599 0.41560599 0.41560599 0.41560599 0.41560599
		 0.41560599 0.41560599 0.41560599 0.41560599 0.41560599;
	setAttr -s 41 ".d[0:40]"  -2147483585 -2147481467 -2147481466 -2147481465 -2147481464 -2147481463 
		-2147481462 -2147481461 -2147481460 -2147481459 -2147481458 -2147481457 -2147481456 -2147481455 -2147481454 -2147481453 -2147481452 -2147481451 
		-2147481450 -2147481449 -2147481448 -2147481447 -2147482719 -2147482727 -2147482746 -2147482770 -2147482799 -2147482834 -2147482873 -2147482928 
		-2147482987 -2147483050 -2147483117 -2147483192 -2147483259 -2147483334 -2147483408 -2147483463 -2147483507 -2147483557 -2147483585;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "0F4CA48F-B746-8E8F-B969-709225326543";
	setAttr -s 41 ".e[0:40]"  0.743559 0.256441 0.256441 0.256441 0.743559
		 0.743559 0.743559 0.743559 0.256441 0.256441 0.256441 0.256441 0.256441 0.256441
		 0.256441 0.256441 0.256441 0.256441 0.256441 0.256441 0.256441 0.256441 0.256441
		 0.256441 0.256441 0.743559 0.743559 0.743559 0.743559 0.743559 0.743559 0.743559
		 0.743559 0.743559 0.743559 0.743559 0.743559 0.743559 0.743559 0.743559 0.743559;
	setAttr -s 41 ".d[0:40]"  -2147483578 -2147483576 -2147482631 -2147482603 -2147482588 -2147482605 
		-2147482633 -2147483574 -2147483572 -2147483485 -2147483429 -2147483345 -2147483260 -2147483181 -2147483106 -2147483039 -2147482976 -2147482917 
		-2147482874 -2147482839 -2147482806 -2147482782 -2147482763 -2147482751 -2147482743 -2147482750 -2147482762 -2147482781 -2147482805 -2147482842 
		-2147482877 -2147482920 -2147482979 -2147483042 -2147483109 -2147483184 -2147483263 -2147483348 -2147483432 -2147483488 -2147483578;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "B443C6FD-2845-F2AA-9C94-89901737FF51";
	setAttr -s 41 ".e[0:40]"  0.46819699 0.53180301 0.53180301 0.53180301
		 0.46819699 0.46819699 0.46819699 0.46819699 0.53180301 0.53180301 0.53180301 0.53180301
		 0.53180301 0.53180301 0.53180301 0.53180301 0.53180301 0.53180301 0.53180301 0.53180301
		 0.53180301 0.53180301 0.53180301 0.53180301 0.53180301 0.46819699 0.46819699 0.46819699
		 0.46819699 0.46819699 0.46819699 0.46819699 0.46819699 0.46819699 0.46819699 0.46819699
		 0.46819699 0.46819699 0.46819699 0.46819699 0.46819699;
	setAttr -s 41 ".d[0:40]"  -2147483578 -2147481307 -2147481306 -2147481305 -2147482588 -2147482605 
		-2147482633 -2147483574 -2147481300 -2147481299 -2147481298 -2147481297 -2147481296 -2147481295 -2147481294 -2147481293 -2147481292 -2147481291 
		-2147481290 -2147481289 -2147481288 -2147481287 -2147481286 -2147481285 -2147481284 -2147482750 -2147482762 -2147482781 -2147482805 -2147482842 
		-2147482877 -2147482920 -2147482979 -2147483042 -2147483109 -2147483184 -2147483263 -2147483348 -2147483432 -2147483488 -2147483578;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit6";
	rename -uid "CE9F48B5-0E43-0763-C45C-B4BE224D5AC1";
	setAttr -s 41 ".e[0:40]"  0.51030499 0.48969501 0.48969501 0.48969501
		 0.48969501 0.51030499 0.51030499 0.51030499 0.51030499 0.51030499 0.48969501 0.48969501
		 0.48969501 0.48969501 0.48969501 0.48969501 0.48969501 0.48969501 0.48969501 0.48969501
		 0.48969501 0.48969501 0.48969501 0.48969501 0.48969501 0.48969501 0.51030499 0.51030499
		 0.51030499 0.51030499 0.51030499 0.51030499 0.51030499 0.51030499 0.51030499 0.51030499
		 0.51030499 0.51030499 0.51030499 0.51030499 0.51030499;
	setAttr -s 41 ".d[0:40]"  -2147483623 -2147483621 -2147482590 -2147482551 -2147482523 -2147482508 
		-2147482525 -2147482553 -2147482592 -2147483619 -2147483617 -2147483517 -2147483441 -2147483349 -2147483252 -2147483169 -2147483092 -2147483025 
		-2147482968 -2147482921 -2147482882 -2147482849 -2147482821 -2147482802 -2147482786 -2147482778 -2147482785 -2147482801 -2147482820 -2147482848 
		-2147482885 -2147482924 -2147482971 -2147483028 -2147483095 -2147483172 -2147483255 -2147483352 -2147483444 -2147483521 -2147483623;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit7";
	rename -uid "7BF386FA-884C-040A-B0FB-38A7F76EAADF";
	setAttr -s 41 ".e[0:40]"  0.67983598 0.320164 0.320164 0.320164 0.320164
		 0.320164 0.320164 0.67983598 0.67983598 0.67983598 0.67983598 0.67983598 0.67983598
		 0.67983598 0.320164 0.320164 0.320164 0.320164 0.320164 0.320164 0.320164 0.320164
		 0.320164 0.320164 0.320164 0.320164 0.320164 0.320164 0.67983598 0.67983598 0.67983598
		 0.67983598 0.67983598 0.67983598 0.67983598 0.67983598 0.67983598 0.67983598 0.67983598
		 0.67983598 0.67983598;
	setAttr -s 41 ".d[0:40]"  -2147483543 -2147483541 -2147482623 -2147482567 -2147482510 -2147482471 
		-2147482443 -2147482428 -2147482445 -2147482473 -2147482512 -2147482569 -2147482625 -2147483539 -2147483537 -2147483445 -2147483341 -2147483240 
		-2147483155 -2147483084 -2147483015 -2147482972 -2147482929 -2147482896 -2147482864 -2147482845 -2147482825 -2147482817 -2147482824 -2147482844 
		-2147482863 -2147482895 -2147482932 -2147482975 -2147483018 -2147483087 -2147483158 -2147483243 -2147483344 -2147483448 -2147483543;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit8";
	rename -uid "6C48613E-F14F-FB61-3909-2A8C38162DB1";
	setAttr -s 41 ".e[0:40]"  0.57888299 0.42111701 0.42111701 0.42111701
		 0.42111701 0.42111701 0.42111701 0.57888299 0.57888299 0.57888299 0.57888299 0.57888299
		 0.57888299 0.57888299 0.42111701 0.42111701 0.42111701 0.42111701 0.42111701 0.42111701
		 0.42111701 0.42111701 0.42111701 0.42111701 0.42111701 0.42111701 0.42111701 0.42111701
		 0.57888299 0.57888299 0.57888299 0.57888299 0.57888299 0.57888299 0.57888299 0.57888299
		 0.57888299 0.57888299 0.57888299 0.57888299 0.57888299;
	setAttr -s 41 ".d[0:40]"  -2147483543 -2147481067 -2147481066 -2147481065 -2147481064 -2147481063 
		-2147481062 -2147482428 -2147482445 -2147482473 -2147482512 -2147482569 -2147482625 -2147483539 -2147481054 -2147481053 -2147481052 -2147481051 
		-2147481050 -2147481049 -2147481048 -2147481047 -2147481046 -2147481045 -2147481044 -2147481043 -2147481042 -2147481041 -2147482824 -2147482844 
		-2147482863 -2147482895 -2147482932 -2147482975 -2147483018 -2147483087 -2147483158 -2147483243 -2147483344 -2147483448 -2147483543;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit9";
	rename -uid "F74F9CDC-5244-C7BB-7E6E-1EAC2C373199";
	setAttr -s 41 ".e[0:40]"  0.77231801 0.22768199 0.22768199 0.22768199
		 0.22768199 0.22768199 0.22768199 0.22768199 0.77231801 0.77231801 0.77231801 0.77231801
		 0.77231801 0.77231801 0.77231801 0.77231801 0.22768199 0.22768199 0.22768199 0.22768199
		 0.22768199 0.22768199 0.22768199 0.22768199 0.22768199 0.22768199 0.22768199 0.22768199
		 0.22768199 0.77231801 0.77231801 0.77231801 0.77231801 0.77231801 0.77231801 0.77231801
		 0.77231801 0.77231801 0.77231801 0.77231801 0.77231801;
	setAttr -s 41 ".d[0:40]"  -2147483551 -2147483549 -2147482611 -2147482543 -2147482487 -2147482430 
		-2147482384 -2147482350 -2147482334 -2147482352 -2147482386 -2147482432 -2147482489 -2147482545 -2147482613 -2147483547 -2147483545 -2147483437 
		-2147483327 -2147483232 -2147483147 -2147483074 -2147483021 -2147482980 -2147482947 -2147482903 -2147482892 -2147482868 -2147482860 -2147482867 
		-2147482891 -2147482902 -2147482946 -2147482983 -2147483024 -2147483077 -2147483150 -2147483235 -2147483330 -2147483440 -2147483551;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit10";
	rename -uid "C844D527-AE4B-47C1-1377-53A2F2878819";
	setAttr -s 41 ".e[0:40]"  0.45533699 0.54466301 0.54466301 0.54466301
		 0.54466301 0.54466301 0.54466301 0.54466301 0.45533699 0.45533699 0.45533699 0.45533699
		 0.45533699 0.45533699 0.45533699 0.45533699 0.54466301 0.54466301 0.54466301 0.54466301
		 0.54466301 0.54466301 0.54466301 0.54466301 0.54466301 0.54466301 0.54466301 0.54466301
		 0.54466301 0.45533699 0.45533699 0.45533699 0.45533699 0.45533699 0.45533699 0.45533699
		 0.45533699 0.45533699 0.45533699 0.45533699 0.45533699;
	setAttr -s 41 ".d[0:40]"  -2147483551 -2147480907 -2147480906 -2147480905 -2147480904 -2147480903 
		-2147480902 -2147480901 -2147482334 -2147482352 -2147482386 -2147482432 -2147482489 -2147482545 -2147482613 -2147483547 -2147480892 -2147480891 
		-2147480890 -2147480889 -2147480888 -2147480887 -2147480886 -2147480885 -2147480884 -2147480883 -2147480882 -2147480881 -2147480880 -2147482867 
		-2147482891 -2147482902 -2147482946 -2147482983 -2147483024 -2147483077 -2147483150 -2147483235 -2147483330 -2147483440 -2147483551;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit11";
	rename -uid "D068FC98-944E-E85F-7A55-E78BFECDFEB1";
	setAttr -s 41 ".e[0:40]"  0.73123097 0.268769 0.268769 0.268769 0.268769
		 0.268769 0.268769 0.268769 0.268769 0.268769 0.73123097 0.73123097 0.73123097 0.73123097
		 0.73123097 0.73123097 0.73123097 0.73123097 0.73123097 0.73123097 0.268769 0.268769
		 0.268769 0.268769 0.268769 0.268769 0.268769 0.268769 0.268769 0.268769 0.268769
		 0.73123097 0.73123097 0.73123097 0.73123097 0.73123097 0.73123097 0.73123097 0.73123097
		 0.73123097 0.73123097;
	setAttr -s 41 ".d[0:40]"  -2147483515 -2147483513 -2147482615 -2147482527 -2147482451 -2147482376 
		-2147482308 -2147482254 -2147482207 -2147482181 -2147482169 -2147482183 -2147482209 -2147482256 -2147482310 -2147482378 -2147482453 -2147482529 
		-2147482617 -2147483511 -2147483509 -2147483413 -2147483297 -2147483212 -2147483143 -2147483088 -2147483053 -2147483009 -2147482990 -2147482958 
		-2147482950 -2147482957 -2147482989 -2147483008 -2147483052 -2147483091 -2147483146 -2147483215 -2147483300 -2147483416 -2147483515;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit12";
	rename -uid "C32BB63C-D741-BF27-7E05-7986F7CD4564";
	setAttr -s 41 ".e[0:40]"  0.47113499 0.52886498 0.52886498 0.52886498
		 0.52886498 0.52886498 0.52886498 0.52886498 0.52886498 0.52886498 0.47113499 0.47113499
		 0.47113499 0.47113499 0.47113499 0.47113499 0.47113499 0.47113499 0.47113499 0.47113499
		 0.52886498 0.52886498 0.52886498 0.52886498 0.52886498 0.52886498 0.52886498 0.52886498
		 0.52886498 0.52886498 0.52886498 0.47113499 0.47113499 0.47113499 0.47113499 0.47113499
		 0.47113499 0.47113499 0.47113499 0.47113499 0.47113499;
	setAttr -s 41 ".d[0:40]"  -2147483515 -2147480747 -2147480746 -2147480745 -2147480744 -2147480743 
		-2147480742 -2147480741 -2147480740 -2147480739 -2147482169 -2147482183 -2147482209 -2147482256 -2147482310 -2147482378 -2147482453 -2147482529 
		-2147482617 -2147483511 -2147480728 -2147480727 -2147480726 -2147480725 -2147480724 -2147480723 -2147480722 -2147480721 -2147480720 -2147480719 
		-2147480718 -2147482957 -2147482989 -2147483008 -2147483052 -2147483091 -2147483146 -2147483215 -2147483300 -2147483416 -2147483515;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit13";
	rename -uid "36D4A45C-4E4B-D744-021B-59B5C664D434";
	setAttr -s 41 ".e[0:40]"  0.66778898 0.33221099 0.33221099 0.33221099
		 0.33221099 0.33221099 0.33221099 0.33221099 0.33221099 0.33221099 0.33221099 0.66778898
		 0.66778898 0.66778898 0.66778898 0.66778898 0.66778898 0.66778898 0.66778898 0.66778898
		 0.66778898 0.66778898 0.33221099 0.33221099 0.33221099 0.33221099 0.33221099 0.33221099
		 0.33221099 0.33221099 0.33221099 0.33221099 0.66778898 0.66778898 0.66778898 0.66778898
		 0.66778898 0.66778898 0.66778898 0.66778898 0.66778898;
	setAttr -s 41 ".d[0:40]"  -2147483498 -2147483496 -2147482627 -2147482535 -2147482447 -2147482358 
		-2147482280 -2147482219 -2147482171 -2147482127 -2147482097 -2147482091 -2147482099 -2147482129 -2147482173 -2147482221 -2147482282 -2147482360 
		-2147482449 -2147482537 -2147482629 -2147483495 -2147483493 -2147483401 -2147483289 -2147483216 -2147483151 -2147483112 -2147483068 -2147483045 
		-2147483013 -2147483005 -2147483012 -2147483044 -2147483067 -2147483111 -2147483154 -2147483219 -2147483292 -2147483404 -2147483498;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit14";
	rename -uid "F3EF7775-8046-191B-5E8D-7F950334D1BE";
	setAttr -s 41 ".e[0:40]"  0.47110301 0.52889699 0.52889699 0.52889699
		 0.52889699 0.52889699 0.52889699 0.52889699 0.52889699 0.52889699 0.52889699 0.47110301
		 0.47110301 0.47110301 0.47110301 0.47110301 0.47110301 0.47110301 0.47110301 0.47110301
		 0.47110301 0.47110301 0.52889699 0.52889699 0.52889699 0.52889699 0.52889699 0.52889699
		 0.52889699 0.52889699 0.52889699 0.52889699 0.47110301 0.47110301 0.47110301 0.47110301
		 0.47110301 0.47110301 0.47110301 0.47110301 0.47110301;
	setAttr -s 41 ".d[0:40]"  -2147483498 -2147480587 -2147480586 -2147480585 -2147480584 -2147480583 
		-2147480582 -2147480581 -2147480580 -2147480579 -2147480578 -2147482091 -2147482099 -2147482129 -2147482173 -2147482221 -2147482282 -2147482360 
		-2147482449 -2147482537 -2147482629 -2147483495 -2147480566 -2147480565 -2147480564 -2147480563 -2147480562 -2147480561 -2147480560 -2147480559 
		-2147480558 -2147480557 -2147483012 -2147483044 -2147483067 -2147483111 -2147483154 -2147483219 -2147483292 -2147483404 -2147483498;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit15";
	rename -uid "E2AB7D07-8F48-B010-8CBF-CFA2799A9AEF";
	setAttr -s 41 ".e[0:40]"  0.76080197 0.239198 0.239198 0.239198 0.239198
		 0.239198 0.239198 0.239198 0.239198 0.239198 0.239198 0.76080197 0.76080197 0.76080197
		 0.76080197 0.76080197 0.76080197 0.76080197 0.76080197 0.76080197 0.76080197 0.76080197
		 0.239198 0.239198 0.239198 0.239198 0.239198 0.239198 0.239198 0.239198 0.239198
		 0.239198 0.76080197 0.76080197 0.76080197 0.76080197 0.76080197 0.76080197 0.76080197
		 0.76080197 0.76080197;
	setAttr -s 41 ".d[0:40]"  -2147483639 -2147483637 -2147482547 -2147482455 -2147482354 -2147482268 
		-2147482199 -2147482152 -2147482093 -2147482059 -2147482025 -2147482019 -2147482027 -2147482061 -2147482095 -2147482154 -2147482201 -2147482270 
		-2147482356 -2147482457 -2147482549 -2147483635 -2147483633 -2147483481 -2147483393 -2147483293 -2147483228 -2147483179 -2147483131 -2147483104 
		-2147483072 -2147483064 -2147483071 -2147483103 -2147483130 -2147483178 -2147483231 -2147483296 -2147483396 -2147483484 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit16";
	rename -uid "FEFEB13B-554E-43D9-6BC0-368D831D8214";
	setAttr -s 41 ".e[0:40]"  0.53524202 0.46475801 0.46475801 0.46475801
		 0.46475801 0.46475801 0.46475801 0.46475801 0.46475801 0.46475801 0.46475801 0.53524202
		 0.53524202 0.53524202 0.53524202 0.53524202 0.53524202 0.53524202 0.53524202 0.53524202
		 0.53524202 0.53524202 0.46475801 0.46475801 0.46475801 0.46475801 0.46475801 0.46475801
		 0.46475801 0.46475801 0.46475801 0.46475801 0.53524202 0.53524202 0.53524202 0.53524202
		 0.53524202 0.53524202 0.53524202 0.53524202 0.53524202;
	setAttr -s 41 ".d[0:40]"  -2147483639 -2147480427 -2147480426 -2147480425 -2147480424 -2147480423 
		-2147480422 -2147480421 -2147480420 -2147480419 -2147480418 -2147482019 -2147482027 -2147482061 -2147482095 -2147482154 -2147482201 -2147482270 
		-2147482356 -2147482457 -2147482549 -2147483635 -2147480406 -2147480405 -2147480404 -2147480403 -2147480402 -2147480401 -2147480400 -2147480399 
		-2147480398 -2147480397 -2147483071 -2147483103 -2147483130 -2147483178 -2147483231 -2147483296 -2147483396 -2147483484 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit17";
	rename -uid "D3B9D419-7D40-27F2-A398-68A277ECBD10";
	setAttr -s 41 ".e[0:40]"  0.728338 0.271662 0.271662 0.271662 0.271662
		 0.271662 0.271662 0.271662 0.271662 0.271662 0.271662 0.271662 0.728338 0.728338
		 0.728338 0.728338 0.728338 0.728338 0.728338 0.728338 0.728338 0.728338 0.728338
		 0.728338 0.271662 0.271662 0.271662 0.271662 0.271662 0.271662 0.271662 0.271662
		 0.271662 0.728338 0.728338 0.728338 0.728338 0.728338 0.728338 0.728338 0.728338;
	setAttr -s 41 ".d[0:40]"  -2147483615 -2147483613 -2147482559 -2147482467 -2147482362 -2147482264 
		-2147482191 -2147482119 -2147482067 -2147482021 -2147481993 -2147481963 -2147481947 -2147481965 -2147481995 -2147482023 -2147482069 -2147482121 
		-2147482193 -2147482266 -2147482364 -2147482469 -2147482561 -2147483611 -2147483609 -2147483473 -2147483397 -2147483305 -2147483250 -2147483198 
		-2147483167 -2147483135 -2147483127 -2147483134 -2147483166 -2147483197 -2147483249 -2147483308 -2147483400 -2147483476 -2147483615;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit18";
	rename -uid "C1BF29FD-0748-DACD-221E-9DB029430EC6";
	setAttr -s 41 ".e[0:40]"  0.55679703 0.443203 0.443203 0.443203 0.443203
		 0.443203 0.443203 0.443203 0.443203 0.443203 0.443203 0.443203 0.55679703 0.55679703
		 0.55679703 0.55679703 0.55679703 0.55679703 0.55679703 0.55679703 0.55679703 0.55679703
		 0.55679703 0.55679703 0.443203 0.443203 0.443203 0.443203 0.443203 0.443203 0.443203
		 0.443203 0.443203 0.55679703 0.55679703 0.55679703 0.55679703 0.55679703 0.55679703
		 0.55679703 0.55679703;
	setAttr -s 41 ".d[0:40]"  -2147483615 -2147480267 -2147480266 -2147480265 -2147480264 -2147480263 
		-2147480262 -2147480261 -2147480260 -2147480259 -2147480258 -2147480257 -2147481947 -2147481965 -2147481995 -2147482023 -2147482069 -2147482121 
		-2147482193 -2147482266 -2147482364 -2147482469 -2147482561 -2147483611 -2147480244 -2147480243 -2147480242 -2147480241 -2147480240 -2147480239 
		-2147480238 -2147480237 -2147480236 -2147483134 -2147483166 -2147483197 -2147483249 -2147483308 -2147483400 -2147483476 -2147483615;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit19";
	rename -uid "DBEC4A87-0D4D-1F1F-160B-5CA13A53660C";
	setAttr -s 41 ".e[0:40]"  0.66190797 0.338092 0.338092 0.338092 0.338092
		 0.338092 0.338092 0.338092 0.338092 0.338092 0.338092 0.338092 0.338092 0.66190797
		 0.66190797 0.66190797 0.66190797 0.66190797 0.66190797 0.66190797 0.66190797 0.66190797
		 0.66190797 0.66190797 0.66190797 0.66190797 0.338092 0.338092 0.338092 0.338092 0.338092
		 0.338092 0.338092 0.338092 0.66190797 0.66190797 0.66190797 0.66190797 0.66190797
		 0.66190797 0.66190797;
	setAttr -s 41 ".d[0:40]"  -2147483599 -2147483597 -2147482571 -2147482479 -2147482380 -2147482272 
		-2147482187 -2147482111 -2147482051 -2147482004 -2147481953 -2147481934 -2147481910 -2147481894 -2147481912 -2147481936 -2147481955 -2147482006 
		-2147482053 -2147482113 -2147482189 -2147482274 -2147482382 -2147482481 -2147482573 -2147483595 -2147483593 -2147483477 -2147483409 -2147483339 
		-2147483271 -2147483238 -2147483204 -2147483194 -2147483203 -2147483237 -2147483270 -2147483338 -2147483412 -2147483480 -2147483599;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit20";
	rename -uid "679B2C42-274D-5EA8-C006-7EB7D480889C";
	setAttr -s 41 ".e[0:40]"  0.53140301 0.46859699 0.46859699 0.46859699
		 0.46859699 0.46859699 0.46859699 0.46859699 0.46859699 0.46859699 0.46859699 0.46859699
		 0.46859699 0.53140301 0.53140301 0.53140301 0.53140301 0.53140301 0.53140301 0.53140301
		 0.53140301 0.53140301 0.53140301 0.53140301 0.53140301 0.53140301 0.46859699 0.46859699
		 0.46859699 0.46859699 0.46859699 0.46859699 0.46859699 0.46859699 0.53140301 0.53140301
		 0.53140301 0.53140301 0.53140301 0.53140301 0.53140301;
	setAttr -s 41 ".d[0:40]"  -2147483599 -2147480107 -2147480106 -2147480105 -2147480104 -2147480103 
		-2147480102 -2147480101 -2147480100 -2147480099 -2147480098 -2147480097 -2147480096 -2147481894 -2147481912 -2147481936 -2147481955 -2147482006 
		-2147482053 -2147482113 -2147482189 -2147482274 -2147482382 -2147482481 -2147482573 -2147483595 -2147480082 -2147480081 -2147480080 -2147480079 
		-2147480078 -2147480077 -2147480076 -2147480075 -2147483203 -2147483237 -2147483270 -2147483338 -2147483412 -2147483480 -2147483599;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit21";
	rename -uid "0F219555-F74E-58AC-6780-16AB4782409C";
	setAttr -s 41 ".e[0:40]"  0.69003397 0.309966 0.309966 0.309966 0.309966
		 0.309966 0.309966 0.309966 0.309966 0.309966 0.309966 0.309966 0.309966 0.309966
		 0.69003397 0.69003397 0.69003397 0.69003397 0.69003397 0.69003397 0.69003397 0.69003397
		 0.69003397 0.69003397 0.69003397 0.69003397 0.69003397 0.69003397 0.309966 0.309966
		 0.309966 0.309966 0.309966 0.309966 0.309966 0.69003397 0.69003397 0.69003397 0.69003397
		 0.69003397 0.69003397;
	setAttr -s 41 ".d[0:40]"  -2147483607 -2147483605 -2147482579 -2147482491 -2147482399 -2147482284 
		-2147482195 -2147482107 -2147482041 -2147481985 -2147481942 -2147481896 -2147481885 -2147481861 -2147481851 -2147481863 -2147481887 -2147481898 
		-2147481944 -2147481987 -2147482043 -2147482109 -2147482197 -2147482286 -2147482401 -2147482493 -2147482581 -2147483603 -2147483601 -2147483489 
		-2147483435 -2147483371 -2147483315 -2147483285 -2147483267 -2147483284 -2147483314 -2147483370 -2147483434 -2147483492 -2147483607;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit22";
	rename -uid "7B7579B3-C444-14E8-2AB9-42910EF8C884";
	setAttr -s 41 ".e[0:40]"  0.43626899 0.56373101 0.56373101 0.56373101
		 0.56373101 0.56373101 0.56373101 0.56373101 0.56373101 0.56373101 0.56373101 0.56373101
		 0.56373101 0.56373101 0.43626899 0.43626899 0.43626899 0.43626899 0.43626899 0.43626899
		 0.43626899 0.43626899 0.43626899 0.43626899 0.43626899 0.43626899 0.43626899 0.43626899
		 0.56373101 0.56373101 0.56373101 0.56373101 0.56373101 0.56373101 0.56373101 0.43626899
		 0.43626899 0.43626899 0.43626899 0.43626899 0.43626899;
	setAttr -s 41 ".d[0:40]"  -2147483607 -2147479947 -2147479946 -2147479945 -2147479944 -2147479943 
		-2147479942 -2147479941 -2147479940 -2147479939 -2147479938 -2147479937 -2147479936 -2147479935 -2147481851 -2147481863 -2147481887 -2147481898 
		-2147481944 -2147481987 -2147482043 -2147482109 -2147482197 -2147482286 -2147482401 -2147482493 -2147482581 -2147483603 -2147479920 -2147479919 
		-2147479918 -2147479917 -2147479916 -2147479915 -2147479914 -2147483284 -2147483314 -2147483370 -2147483434 -2147483492 -2147483607;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit23";
	rename -uid "9A232660-ED4E-693C-A336-A38B76C3EE9D";
	setAttr -s 41 ".e[0:40]"  0.76433301 0.23566701 0.23566701 0.23566701
		 0.23566701 0.23566701 0.23566701 0.23566701 0.23566701 0.23566701 0.23566701 0.23566701
		 0.23566701 0.23566701 0.23566701 0.76433301 0.76433301 0.76433301 0.76433301 0.76433301
		 0.76433301 0.76433301 0.76433301 0.76433301 0.76433301 0.76433301 0.76433301 0.76433301
		 0.76433301 0.76433301 0.23566701 0.23566701 0.23566701 0.23566701 0.23566701 0.23566701
		 0.76433301 0.76433301 0.76433301 0.76433301 0.76433301;
	setAttr -s 41 ".d[0:40]"  -2147483631 -2147483629 -2147482575 -2147482499 -2147482411 -2147482298 
		-2147482203 -2147482115 -2147482037 -2147481977 -2147481926 -2147481889 -2147481853 -2147481831 -2147481811 -2147481801 -2147481813 -2147481833 
		-2147481855 -2147481891 -2147481928 -2147481979 -2147482039 -2147482117 -2147482205 -2147482300 -2147482413 -2147482501 -2147482577 -2147483627 
		-2147483625 -2147483526 -2147483458 -2147483419 -2147483391 -2147483367 -2147483390 -2147483418 -2147483457 -2147483524 -2147483631;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit24";
	rename -uid "CE26C6AE-B349-51CC-0950-00881188A3CA";
	setAttr -s 41 ".e[0:40]"  0.587735 0.412265 0.412265 0.412265 0.412265
		 0.412265 0.412265 0.412265 0.412265 0.412265 0.412265 0.412265 0.412265 0.412265
		 0.412265 0.587735 0.587735 0.587735 0.587735 0.587735 0.587735 0.587735 0.587735
		 0.587735 0.587735 0.587735 0.587735 0.587735 0.587735 0.587735 0.412265 0.412265
		 0.412265 0.412265 0.412265 0.412265 0.587735 0.587735 0.587735 0.587735 0.587735;
	setAttr -s 41 ".d[0:40]"  -2147483631 -2147479787 -2147479786 -2147479785 -2147479784 -2147479783 
		-2147479782 -2147479781 -2147479780 -2147479779 -2147479778 -2147479777 -2147479776 -2147479775 -2147479774 -2147481801 -2147481813 -2147481833 
		-2147481855 -2147481891 -2147481928 -2147481979 -2147482039 -2147482117 -2147482205 -2147482300 -2147482413 -2147482501 -2147482577 -2147483627 
		-2147479758 -2147479757 -2147479756 -2147479755 -2147479754 -2147479753 -2147483390 -2147483418 -2147483457 -2147483524 -2147483631;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit25";
	rename -uid "D16B989C-0F4D-45B3-B58C-DF8E7F6CB928";
	setAttr -s 41 ".e[0:40]"  0.72133303 0.278667 0.278667 0.278667 0.278667
		 0.72133303 0.72133303 0.72133303 0.278667 0.278667 0.278667 0.278667 0.278667 0.278667
		 0.278667 0.278667 0.278667 0.278667 0.278667 0.278667 0.278667 0.278667 0.278667
		 0.278667 0.72133303 0.72133303 0.72133303 0.72133303 0.72133303 0.72133303 0.72133303
		 0.72133303 0.72133303 0.72133303 0.72133303 0.72133303 0.72133303 0.72133303 0.72133303
		 0.72133303 0.72133303;
	setAttr -s 41 ".d[0:40]"  -2147483570 -2147483568 -2147483501 -2147483471 -2147483454 -2147483470 
		-2147483500 -2147483566 -2147483564 -2147482621 -2147482563 -2147482495 -2147482419 -2147482312 -2147482211 -2147482123 -2147482045 -2147481973 
		-2147481918 -2147481877 -2147481835 -2147481803 -2147481783 -2147481767 -2147481755 -2147481769 -2147481785 -2147481805 -2147481837 -2147481879 
		-2147481920 -2147481975 -2147482047 -2147482125 -2147482213 -2147482314 -2147482421 -2147482497 -2147482565 -2147482619 -2147483570;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit26";
	rename -uid "C1B20994-BB48-4F37-395E-50A541221E63";
	setAttr -s 41 ".e[0:40]"  0.50667399 0.49332601 0.49332601 0.49332601
		 0.49332601 0.50667399 0.50667399 0.50667399 0.49332601 0.49332601 0.49332601 0.49332601
		 0.49332601 0.49332601 0.49332601 0.49332601 0.49332601 0.49332601 0.49332601 0.49332601
		 0.49332601 0.49332601 0.49332601 0.49332601 0.50667399 0.50667399 0.50667399 0.50667399
		 0.50667399 0.50667399 0.50667399 0.50667399 0.50667399 0.50667399 0.50667399 0.50667399
		 0.50667399 0.50667399 0.50667399 0.50667399 0.50667399;
	setAttr -s 41 ".d[0:40]"  -2147483570 -2147479627 -2147479626 -2147479625 -2147479624 -2147483470 
		-2147483500 -2147483566 -2147479620 -2147479619 -2147479618 -2147479617 -2147479616 -2147479615 -2147479614 -2147479613 -2147479612 -2147479611 
		-2147479610 -2147479609 -2147479608 -2147479607 -2147479606 -2147479605 -2147481755 -2147481769 -2147481785 -2147481805 -2147481837 -2147481879 
		-2147481920 -2147481975 -2147482047 -2147482125 -2147482213 -2147482314 -2147482421 -2147482497 -2147482565 -2147482619 -2147483570;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit27";
	rename -uid "EAA47023-6245-CE1B-DCE6-CDBFA04F448E";
	setAttr -s 41 ".e[0:40]"  0.707865 0.292135 0.292135 0.292135 0.707865
		 0.707865 0.292135 0.292135 0.292135 0.292135 0.292135 0.292135 0.292135 0.292135
		 0.292135 0.292135 0.292135 0.292135 0.292135 0.292135 0.292135 0.292135 0.292135
		 0.707865 0.707865 0.707865 0.707865 0.707865 0.707865 0.707865 0.707865 0.707865
		 0.707865 0.707865 0.707865 0.707865 0.707865 0.707865 0.707865 0.707865 0.707865;
	setAttr -s 41 ".d[0:40]"  -2147483647 -2147483645 -2147483590 -2147483561 -2147483588 -2147483643 
		-2147483641 -2147482596 -2147482541 -2147482483 -2147482415 -2147482329 -2147482223 -2147482138 -2147482055 -2147481981 -2147481914 -2147481869 
		-2147481827 -2147481787 -2147481757 -2147481740 -2147481724 -2147481715 -2147481726 -2147481742 -2147481759 -2147481789 -2147481829 -2147481871 
		-2147481916 -2147481983 -2147482057 -2147482140 -2147482225 -2147482331 -2147482417 -2147482485 -2147482539 -2147482594 -2147483647;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit28";
	rename -uid "EE0517A4-A248-BC31-BC2D-4B9C1570C2AD";
	setAttr -s 41 ".e[0:40]"  0.54400998 0.45598999 0.45598999 0.45598999
		 0.54400998 0.54400998 0.45598999 0.45598999 0.45598999 0.45598999 0.45598999 0.45598999
		 0.45598999 0.45598999 0.45598999 0.45598999 0.45598999 0.45598999 0.45598999 0.45598999
		 0.45598999 0.45598999 0.45598999 0.54400998 0.54400998 0.54400998 0.54400998 0.54400998
		 0.54400998 0.54400998 0.54400998 0.54400998 0.54400998 0.54400998 0.54400998 0.54400998
		 0.54400998 0.54400998 0.54400998 0.54400998 0.54400998;
	setAttr -s 41 ".d[0:40]"  -2147483647 -2147479467 -2147479466 -2147479465 -2147483588 -2147483643 
		-2147479462 -2147479461 -2147479460 -2147479459 -2147479458 -2147479457 -2147479456 -2147479455 -2147479454 -2147479453 -2147479452 -2147479451 
		-2147479450 -2147479449 -2147479448 -2147479447 -2147479446 -2147481715 -2147481726 -2147481742 -2147481759 -2147481789 -2147481829 -2147481871 
		-2147481916 -2147481983 -2147482057 -2147482140 -2147482225 -2147482331 -2147482417 -2147482485 -2147482539 -2147482594 -2147483647;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit29";
	rename -uid "A9F0F4BB-6144-1D80-AA59-A3A902345BF1";
	setAttr -s 41 ".e[0:40]"  0.52017301 0.47982699 0.47982699 0.47982699
		 0.47982699 0.47982699 0.47982699 0.47982699 0.47982699 0.47982699 0.47982699 0.47982699
		 0.47982699 0.47982699 0.47982699 0.47982699 0.47982699 0.47982699 0.47982699 0.47982699
		 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301
		 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301
		 0.52017301 0.52017301 0.52017301 0.52017301 0.52017301;
	setAttr -s 41 ".d[0:40]"  -2147482601 -2147482600 -2147482585 -2147482557 -2147482516 -2147482461 
		-2147482403 -2147482324 -2147482231 -2147482156 -2147482063 -2147481989 -2147481922 -2147481865 -2147481819 -2147481779 -2147481744 -2147481717 
		-2147481700 -2147481688 -2147481682 -2147481690 -2147481702 -2147481719 -2147481746 -2147481781 -2147481821 -2147481867 -2147481924 -2147481991 
		-2147482065 -2147482158 -2147482233 -2147482326 -2147482405 -2147482459 -2147482514 -2147482555 -2147482583 -2147482598 -2147482601;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit30";
	rename -uid "9B4B4FF9-8F48-D9EB-B90B-B6ABFD48A7EA";
	setAttr -s 41 ".e[0:40]"  0.656434 0.343566 0.343566 0.343566 0.343566
		 0.343566 0.343566 0.343566 0.343566 0.343566 0.343566 0.343566 0.343566 0.343566
		 0.343566 0.343566 0.343566 0.343566 0.343566 0.343566 0.656434 0.656434 0.656434
		 0.656434 0.656434 0.656434 0.656434 0.656434 0.656434 0.656434 0.656434 0.656434
		 0.656434 0.656434 0.656434 0.656434 0.656434 0.656434 0.656434 0.656434 0.656434;
	setAttr -s 41 ".d[0:40]"  -2147482521 -2147482520 -2147482505 -2147482477 -2147482436 -2147482374 
		-2147482302 -2147482227 -2147482164 -2147482071 -2147481998 -2147481930 -2147481873 -2147481815 -2147481775 -2147481736 -2147481704 -2147481684 
		-2147481660 -2147481655 -2147481636 -2147481657 -2147481662 -2147481686 -2147481706 -2147481738 -2147481777 -2147481817 -2147481875 -2147481932 
		-2147482000 -2147482073 -2147482166 -2147482229 -2147482304 -2147482372 -2147482434 -2147482475 -2147482503 -2147482518 -2147482521;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit31";
	rename -uid "8A786423-B447-C59C-D15C-DCA84D8BFEDE";
	setAttr -s 41 ".e[0:40]"  0.48658499 0.51341498 0.51341498 0.51341498
		 0.51341498 0.51341498 0.51341498 0.51341498 0.51341498 0.51341498 0.51341498 0.51341498
		 0.51341498 0.51341498 0.51341498 0.51341498 0.51341498 0.51341498 0.51341498 0.51341498
		 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499
		 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499
		 0.48658499 0.48658499 0.48658499 0.48658499 0.48658499;
	setAttr -s 41 ".d[0:40]"  -2147482521 -2147479227 -2147479226 -2147479225 -2147479224 -2147479223 
		-2147479222 -2147479221 -2147479220 -2147479219 -2147479218 -2147479217 -2147479216 -2147479215 -2147479214 -2147479213 -2147479212 -2147479211 
		-2147479210 -2147479209 -2147481636 -2147481657 -2147481662 -2147481686 -2147481706 -2147481738 -2147481777 -2147481817 -2147481875 -2147481932 
		-2147482000 -2147482073 -2147482166 -2147482229 -2147482304 -2147482372 -2147482434 -2147482475 -2147482503 -2147482518 -2147482521;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit32";
	rename -uid "7A7D9523-D748-A6A7-6837-678AD0059D6A";
	setAttr -s 41 ".e[0:40]"  0.76151502 0.23848499 0.23848499 0.23848499
		 0.23848499 0.23848499 0.23848499 0.23848499 0.23848499 0.23848499 0.23848499 0.23848499
		 0.23848499 0.23848499 0.23848499 0.23848499 0.23848499 0.23848499 0.23848499 0.23848499
		 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502
		 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502
		 0.76151502 0.76151502 0.76151502 0.76151502 0.76151502;
	setAttr -s 41 ".d[0:40]"  -2147482441 -2147482440 -2147482425 -2147482390 -2147482342 -2147482278 
		-2147482215 -2147482160 -2147482075 -2147482008 -2147481938 -2147481881 -2147481823 -2147481771 -2147481732 -2147481696 -2147481664 -2147481638 
		-2147481625 -2147481613 -2147481602 -2147481615 -2147481627 -2147481640 -2147481666 -2147481698 -2147481734 -2147481773 -2147481825 -2147481883 
		-2147481940 -2147482010 -2147482077 -2147482162 -2147482217 -2147482276 -2147482340 -2147482388 -2147482423 -2147482438 -2147482441;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit33";
	rename -uid "A1FD1A90-2849-12E8-B542-09BB6F668E70";
	setAttr -s 41 ".e[0:40]"  0.48841301 0.51158702 0.51158702 0.51158702
		 0.51158702 0.51158702 0.51158702 0.51158702 0.51158702 0.51158702 0.51158702 0.51158702
		 0.51158702 0.51158702 0.51158702 0.51158702 0.51158702 0.51158702 0.51158702 0.51158702
		 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301
		 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301
		 0.48841301 0.48841301 0.48841301 0.48841301 0.48841301;
	setAttr -s 41 ".d[0:40]"  -2147482441 -2147479067 -2147479066 -2147479065 -2147479064 -2147479063 
		-2147479062 -2147479061 -2147479060 -2147479059 -2147479058 -2147479057 -2147479056 -2147479055 -2147479054 -2147479053 -2147479052 -2147479051 
		-2147479050 -2147479049 -2147481602 -2147481615 -2147481627 -2147481640 -2147481666 -2147481698 -2147481734 -2147481773 -2147481825 -2147481883 
		-2147481940 -2147482010 -2147482077 -2147482162 -2147482217 -2147482276 -2147482340 -2147482388 -2147482423 -2147482438 -2147482441;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit34";
	rename -uid "3C8AF143-D841-52BD-63B4-1EBD0B42EB09";
	setAttr -s 41 ".e[0:40]"  0.53188002 0.46812001 0.46812001 0.46812001
		 0.46812001 0.46812001 0.46812001 0.46812001 0.46812001 0.46812001 0.46812001 0.46812001
		 0.46812001 0.46812001 0.46812001 0.46812001 0.46812001 0.46812001 0.46812001 0.46812001
		 0.46812001 0.46812001 0.53188002 0.53188002 0.53188002 0.53188002 0.53188002 0.53188002
		 0.53188002 0.53188002 0.53188002 0.53188002 0.53188002 0.53188002 0.53188002 0.53188002
		 0.53188002 0.53188002 0.53188002 0.53188002 0.53188002;
	setAttr -s 41 ".d[0:40]"  -2147479067 -2147478988 -2147478949 -2147478950 -2147478951 -2147478952 
		-2147478953 -2147478954 -2147478955 -2147478956 -2147478957 -2147478958 -2147478959 -2147478960 -2147478961 -2147478962 -2147478963 -2147478964 
		-2147478965 -2147478966 -2147478967 -2147478968 -2147479049 -2147479050 -2147479051 -2147479052 -2147479053 -2147479054 -2147479055 -2147479056 
		-2147479057 -2147479058 -2147479059 -2147479060 -2147479061 -2147479062 -2147479063 -2147479064 -2147479065 -2147479066 -2147479067;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit35";
	rename -uid "4FE5D6AD-404F-404E-C0B0-6F915C610642";
	setAttr -s 41 ".e[0:40]"  0.53759301 0.46240699 0.46240699 0.46240699
		 0.46240699 0.46240699 0.46240699 0.46240699 0.46240699 0.46240699 0.46240699 0.46240699
		 0.46240699 0.46240699 0.46240699 0.46240699 0.46240699 0.46240699 0.46240699 0.46240699
		 0.46240699 0.46240699 0.53759301 0.53759301 0.53759301 0.53759301 0.53759301 0.53759301
		 0.53759301 0.53759301 0.53759301 0.53759301 0.53759301 0.53759301 0.53759301 0.53759301
		 0.53759301 0.53759301 0.53759301 0.53759301 0.53759301;
	setAttr -s 41 ".d[0:40]"  -2147483381 -2147481547 -2147481546 -2147481545 -2147481544 -2147481543 
		-2147481542 -2147481541 -2147481540 -2147481539 -2147481538 -2147481537 -2147481536 -2147481535 -2147481534 -2147481533 -2147481532 -2147481531 
		-2147481530 -2147481529 -2147481528 -2147481527 -2147482665 -2147482673 -2147482688 -2147482704 -2147482725 -2147482756 -2147482791 -2147482838 
		-2147482889 -2147482944 -2147483003 -2147483062 -2147483121 -2147483176 -2147483227 -2147483282 -2147483320 -2147483359 -2147483381;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit36";
	rename -uid "BCBCBFC6-0A48-0982-604D-D58DBCD1AD32";
	setAttr -s 41 ".e[0:40]"  0.490096 0.50990403 0.50990403 0.50990403
		 0.50990403 0.50990403 0.50990403 0.50990403 0.50990403 0.50990403 0.50990403 0.50990403
		 0.50990403 0.50990403 0.50990403 0.50990403 0.50990403 0.50990403 0.50990403 0.50990403
		 0.50990403 0.50990403 0.490096 0.490096 0.490096 0.490096 0.490096 0.490096 0.490096
		 0.490096 0.490096 0.490096 0.490096 0.490096 0.490096 0.490096 0.490096 0.490096
		 0.490096 0.490096 0.490096;
	setAttr -s 41 ".d[0:40]"  -2147483468 -2147483466 -2147483464 -2147483449 -2147483421 -2147483373 
		-2147483301 -2147483244 -2147483185 -2147483122 -2147483055 -2147482992 -2147482933 -2147482878 -2147482827 -2147482792 -2147482757 -2147482736 
		-2147482716 -2147482697 -2147482693 -2147482685 -2147482692 -2147482696 -2147482715 -2147482735 -2147482760 -2147482795 -2147482830 -2147482881 
		-2147482936 -2147482995 -2147483058 -2147483125 -2147483188 -2147483247 -2147483304 -2147483376 -2147483424 -2147483452 -2147483468;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "AC82139B-FF42-A8F3-E3A7-8588769A7C60";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 822\n            -height 466\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 820\n            -height 464\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n"
		+ "            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 822\n            -height 464\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1656\n            -height 1020\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n"
		+ "            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n"
		+ "            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n"
		+ "            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n"
		+ "                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n"
		+ "                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n"
		+ "                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1656\\n    -height 1020\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1656\\n    -height 1020\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "E7D96E5B-BE42-C053-D68A-9AA304849E9E";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 1 -ast 0 -aet 272.4 ";
	setAttr ".st" 6;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "855C5E25-A841-5CC2-46E0-5887EFEA4D41";
	setAttr ".ics" -type "componentList" 1 "f[0:2419]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 4 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.11654854 -8.1231995 -1.9073486e-06 ;
	setAttr ".rs" 1442671167;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -14.972402572631836 -96.24639892578125 -15.32344913482666 ;
	setAttr ".cbx" -type "double3" 15.205499649047852 80 15.323445320129395 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "81FDAA07-BD42-835C-5673-17BA444F12F3";
	setAttr ".uopa" yes;
	setAttr -s 1792 ".tk";
	setAttr ".tk[2]" -type "float3" 0 -0.46788904 0 ;
	setAttr ".tk[3]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[6]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[7]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[8]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[9]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[10]" -type "float3" 0 0.13193749 0 ;
	setAttr ".tk[11]" -type "float3" 0 0.13193749 0 ;
	setAttr ".tk[12]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[13]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[14]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[15]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[16]" -type "float3" 0 0.26736522 0 ;
	setAttr ".tk[22]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[23]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[24]" -type "float3" 0.40799859 0.1018896 0 ;
	setAttr ".tk[25]" -type "float3" 0.40799859 0.1018896 0 ;
	setAttr ".tk[26]" -type "float3" -1.9526949 0.10972726 6.7125598e-08 ;
	setAttr ".tk[27]" -type "float3" -1.9218841 0.10972726 -0.39149672 ;
	setAttr ".tk[28]" -type "float3" 0.062701292 0.062701292 0 ;
	setAttr ".tk[29]" -type "float3" 0.062701292 0.062701292 0 ;
	setAttr ".tk[30]" -type "float3" -0.39149585 0 -2.4718134 ;
	setAttr ".tk[31]" -type "float3" 9.7755139e-07 0 -2.5026243 ;
	setAttr ".tk[37]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[39]" -type "float3" 0 0.13639599 0 ;
	setAttr ".tk[40]" -type "float3" 0 0.13639599 0 ;
	setAttr ".tk[41]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[42]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[43]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[44]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[46]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[49]" -type "float3" 0 -0.30556023 0 ;
	setAttr ".tk[51]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[52]" -type "float3" 0.15436614 -0.11577462 0 ;
	setAttr ".tk[54]" -type "float3" -0.41259721 -0.043509118 0 ;
	setAttr ".tk[55]" -type "float3" 0 -0.16486122 0 ;
	setAttr ".tk[56]" -type "float3" 0.15436614 -0.11577462 0 ;
	setAttr ".tk[57]" -type "float3" -0.072811283 -0.2680105 0 ;
	setAttr ".tk[58]" -type "float3" -1.8302077 0.10972726 -0.77335352 ;
	setAttr ".tk[59]" -type "float3" -0.57335997 0.04410461 0 ;
	setAttr ".tk[60]" -type "float3" -0.57335997 0.04410461 0 ;
	setAttr ".tk[61]" -type "float3" -0.77335274 0 -2.3801374 ;
	setAttr ".tk[64]" -type "float3" 0 -0.46788904 0 ;
	setAttr ".tk[71]" -type "float3" 0 -0.30556023 0 ;
	setAttr ".tk[72]" -type "float3" -0.57335997 0.04410461 0 ;
	setAttr ".tk[73]" -type "float3" -0.90465736 -0.0066046156 0 ;
	setAttr ".tk[74]" -type "float3" 0.15436614 -0.11577462 0 ;
	setAttr ".tk[75]" -type "float3" -0.072811283 -0.2680105 0 ;
	setAttr ".tk[76]" -type "float3" -3.2313552 -0.0082294587 0 ;
	setAttr ".tk[77]" -type "float3" -3.0273278 -0.0082294587 0 ;
	setAttr ".tk[78]" -type "float3" -2.2580333 0 2.4167936 ;
	setAttr ".tk[79]" -type "float3" -2.5777078 0.18266365 1.645035 ;
	setAttr ".tk[80]" -type "float3" 1.542502 0 0 ;
	setAttr ".tk[81]" -type "float3" 1.0499331 0 0 ;
	setAttr ".tk[82]" -type "float3" 1.6450375 0.15983069 -5.0628953 ;
	setAttr ".tk[83]" -type "float3" 2.416796 0.15983069 -4.7432227 ;
	setAttr ".tk[84]" -type "float3" -2.7727156 0.18266365 0.83277011 ;
	setAttr ".tk[85]" -type "float3" 0.40799859 0.1018896 0 ;
	setAttr ".tk[86]" -type "float3" 0.40799859 0.1018896 0 ;
	setAttr ".tk[87]" -type "float3" 0.83277237 -0.045665909 -5.257905 ;
	setAttr ".tk[88]" -type "float3" -0.97512066 -0.10972726 0 ;
	setAttr ".tk[89]" -type "float3" -0.97512066 -0.10972726 0 ;
	setAttr ".tk[90]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[91]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[92]" -type "float3" -2.7487555 -0.0082294587 0 ;
	setAttr ".tk[93]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[94]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[95]" -type "float3" 1.99709 0 0 ;
	setAttr ".tk[96]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[98]" -type "float3" -1.9218841 0.10972726 0.39149681 ;
	setAttr ".tk[99]" -type "float3" 0.74738753 0.078376614 -2.4718134 ;
	setAttr ".tk[100]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[101]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[102]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[103]" -type "float3" 0 0.14850019 0 ;
	setAttr ".tk[104]" -type "float3" 0 0.14850019 0 ;
	setAttr ".tk[105]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[106]" -type "float3" -0.90465736 -0.0066046156 0 ;
	setAttr ".tk[107]" -type "float3" -0.072811283 -0.2680105 0 ;
	setAttr ".tk[111]" -type "float3" 0 0.14850019 0 ;
	setAttr ".tk[112]" -type "float3" 0 0.13639599 0 ;
	setAttr ".tk[113]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[114]" -type "float3" -0.57335997 0.04410461 0 ;
	setAttr ".tk[115]" -type "float3" 0.2659061 -0.10227158 0 ;
	setAttr ".tk[116]" -type "float3" 0 0.058028921 0 ;
	setAttr ".tk[117]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[120]" -type "float3" 0 0.12886183 0 ;
	setAttr ".tk[121]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[124]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[126]" -type "float3" -0.05695419 -0.16374329 0 ;
	setAttr ".tk[127]" -type "float3" -0.90465736 -0.0066046156 0 ;
	setAttr ".tk[128]" -type "float3" 0 -0.16486122 0 ;
	setAttr ".tk[129]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[132]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[133]" -type "float3" -0.05695419 -0.16374329 0 ;
	setAttr ".tk[134]" -type "float3" 0.40799859 0.1018896 0 ;
	setAttr ".tk[135]" -type "float3" -0.97512066 -0.10972726 0 ;
	setAttr ".tk[136]" -type "float3" -1.8302077 0.10972726 0.77335364 ;
	setAttr ".tk[137]" -type "float3" 1.1292447 0.078376614 -2.3801374 ;
	setAttr ".tk[138]" -type "float3" -1.8215659 0 3.1290424 ;
	setAttr ".tk[139]" -type "float3" 3.1290443 0.15983069 -4.3067565 ;
	setAttr ".tk[140]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[141]" -type "float3" 0 0.13639599 0 ;
	setAttr ".tk[142]" -type "float3" -0.57335997 0.04410461 0 ;
	setAttr ".tk[143]" -type "float3" 0.2659061 -0.10227158 0 ;
	setAttr ".tk[144]" -type "float3" -2.4025002 -0.0082294587 0 ;
	setAttr ".tk[145]" -type "float3" 2.402503 0.13585968 0 ;
	setAttr ".tk[146]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[147]" -type "float3" -0.05695419 -0.16374329 0 ;
	setAttr ".tk[148]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[149]" -type "float3" 0 -0.14191103 0 ;
	setAttr ".tk[150]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[151]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[154]" -type "float3" -0.90465736 -0.0066046156 0 ;
	setAttr ".tk[156]" -type "float3" 0 -0.077220105 0 ;
	setAttr ".tk[157]" -type "float3" 0 0.08316011 0 ;
	setAttr ".tk[158]" -type "float3" 0 0.12886183 0 ;
	setAttr ".tk[159]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[160]" -type "float3" 0 0.058028921 0 ;
	setAttr ".tk[161]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[162]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[163]" -type "float3" 0 -0.14191103 0 ;
	setAttr ".tk[173]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[175]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[176]" -type "float3" -0.51407617 0 0 ;
	setAttr ".tk[177]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[184]" -type "float3" -1.6799259 0.10972726 1.1361679 ;
	setAttr ".tk[185]" -type "float3" 1.492059 0.078376614 -2.2298548 ;
	setAttr ".tk[186]" -type "float3" -0.57335997 0.04410461 0 ;
	setAttr ".tk[187]" -type "float3" 0.2659061 -0.10227158 0 ;
	setAttr ".tk[188]" -type "float3" 0.72150522 0.062701292 0 ;
	setAttr ".tk[189]" -type "float3" -0.97512066 -0.10972726 0 ;
	setAttr ".tk[192]" -type "float3" -0.90465736 -0.0066046156 0 ;
	setAttr ".tk[193]" -type "float3" -0.24270429 -0.16382535 0 ;
	setAttr ".tk[194]" -type "float3" -1.2790519 0.15983069 3.7642436 ;
	setAttr ".tk[195]" -type "float3" 3.7642474 0.15983069 -3.7642431 ;
	setAttr ".tk[202]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[203]" -type "float3" -0.51407617 0 0 ;
	setAttr ".tk[204]" -type "float3" -1.9970874 -0.43521699 0 ;
	setAttr ".tk[205]" -type "float3" 2.7487569 0.13585968 0 ;
	setAttr ".tk[207]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[208]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[209]" -type "float3" 1.6162951 -0.092550576 0 ;
	setAttr ".tk[211]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[213]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[214]" -type "float3" 0 -0.077220105 0 ;
	setAttr ".tk[215]" -type "float3" 0 0.08316011 0 ;
	setAttr ".tk[216]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[217]" -type "float3" -0.51407617 0 0 ;
	setAttr ".tk[226]" -type "float3" -0.51407617 0 0 ;
	setAttr ".tk[227]" -type "float3" -0.57335997 0.04410461 0 ;
	setAttr ".tk[229]" -type "float3" -0.90465736 -0.0066046156 0 ;
	setAttr ".tk[230]" -type "float3" -0.24270429 -0.16382535 0 ;
	setAttr ".tk[231]" -type "float3" -1.4747369 0.10972726 1.4710058 ;
	setAttr ".tk[232]" -type "float3" 2.3598573 0.14107792 -2.0246656 ;
	setAttr ".tk[233]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[234]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[235]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[236]" -type "float3" 1.6162951 -0.092550576 0 ;
	setAttr ".tk[237]" -type "float3" 0.72150522 0.062701292 0 ;
	setAttr ".tk[238]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[241]" -type "float3" -1.1695646 0.15983069 4.3067565 ;
	setAttr ".tk[242]" -type "float3" 4.3067603 0 -3.1290412 ;
	setAttr ".tk[244]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[247]" -type "float3" -1.5425003 -0.43521699 0 ;
	setAttr ".tk[248]" -type "float3" 3.0273306 0.13585968 0 ;
	setAttr ".tk[249]" -type "float3" 0 -0.077220105 0 ;
	setAttr ".tk[250]" -type "float3" 0 0.08316011 0 ;
	setAttr ".tk[252]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[259]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[260]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[262]" -type "float3" -0.90465736 -0.0066046156 0 ;
	setAttr ".tk[263]" -type "float3" -0.24270429 -0.16382535 0 ;
	setAttr ".tk[264]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[265]" -type "float3" 1.6162951 -0.092550576 0 ;
	setAttr ".tk[267]" -type "float3" 0.020454314 -0.086930841 0 ;
	setAttr ".tk[268]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[272]" -type "float3" -1.1883435 -0.11756492 1.7696226 ;
	setAttr ".tk[273]" -type "float3" 2.6584747 0.14107792 -1.7696224 ;
	setAttr ".tk[274]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[275]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[280]" -type "float3" 0.72150522 0.062701292 0 ;
	setAttr ".tk[281]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[283]" -type "float3" 0 0.08316011 0 ;
	setAttr ".tk[284]" -type "float3" -0.45731613 0.15983069 4.7432232 ;
	setAttr ".tk[285]" -type "float3" 4.7432275 0 -2.4167919 ;
	setAttr ".tk[287]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[288]" -type "float3" -1.0499312 -0.19261044 0 ;
	setAttr ".tk[289]" -type "float3" 3.2313576 -0.029112799 0 ;
	setAttr ".tk[290]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[291]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[292]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[294]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[295]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[296]" -type "float3" 1.6162951 -0.092550576 0 ;
	setAttr ".tk[299]" -type "float3" -0.41259721 -0.043509118 0 ;
	setAttr ".tk[301]" -type "float3" 0 0.08316011 0 ;
	setAttr ".tk[302]" -type "float3" 0 -0.11880016 0 ;
	setAttr ".tk[303]" -type "float3" 0.15436614 -0.11577462 0 ;
	setAttr ".tk[304]" -type "float3" 0.020454314 -0.086930841 0 ;
	setAttr ".tk[305]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[311]" -type "float3" -0.88972646 -0.11756492 2.0246661 ;
	setAttr ".tk[312]" -type "float3" 2.9135175 0.14107792 -1.4710053 ;
	setAttr ".tk[314]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[315]" -type "float3" 0.72150522 0.062701292 0 ;
	setAttr ".tk[316]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[317]" -type "float3" -0.53150922 -0.19261044 0 ;
	setAttr ".tk[318]" -type "float3" 3.3558209 -0.029112799 0 ;
	setAttr ".tk[319]" -type "float3" -0.3068561 0.15983069 5.0628958 ;
	setAttr ".tk[320]" -type "float3" 5.0628986 0.18266365 -1.6450329 ;
	setAttr ".tk[321]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[322]" -type "float3" -0.98649168 0 0 ;
	setAttr ".tk[323]" -type "float3" 0 0.08316011 0 ;
	setAttr ".tk[324]" -type "float3" 0 -0.11880016 0 ;
	setAttr ".tk[325]" -type "float3" -0.98649168 -0.18477143 0 ;
	setAttr ".tk[328]" -type "float3" 0.56954187 -0.14238548 0 ;
	setAttr ".tk[329]" -type "float3" 1.3884785 0 0 ;
	setAttr ".tk[330]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[332]" -type "float3" 0 -0.16486122 0 ;
	setAttr ".tk[334]" -type "float3" 0 0.08316011 0 ;
	setAttr ".tk[336]" -type "float3" 0.15436614 -0.11577462 0 ;
	setAttr ".tk[337]" -type "float3" 0.020454314 -0.086930841 0 ;
	setAttr ".tk[338]" -type "float3" 3.4112739e-07 -0.43521699 0 ;
	setAttr ".tk[339]" -type "float3" 3.397649 -0.029112799 0 ;
	setAttr ".tk[344]" -type "float3" -1.4496738 -0.11756492 2.2298551 ;
	setAttr ".tk[345]" -type "float3" 3.1187065 0.14107792 -1.136167 ;
	setAttr ".tk[346]" -type "float3" -0.54601729 -0.045665909 5.2579055 ;
	setAttr ".tk[347]" -type "float3" 5.2579074 0.18266365 -0.8327682 ;
	setAttr ".tk[348]" -type "float3" 0.062701292 0.062701292 0 ;
	setAttr ".tk[349]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[350]" -type "float3" 0 0.14850019 0 ;
	setAttr ".tk[351]" -type "float3" 0 0.14850019 0 ;
	setAttr ".tk[352]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[354]" -type "float3" 0 0.14850019 0 ;
	setAttr ".tk[357]" -type "float3" 3.3558173 -0.029112799 0 ;
	setAttr ".tk[358]" -type "float3" 0.53150964 0 0 ;
	setAttr ".tk[360]" -type "float3" 1.3884785 0 0 ;
	setAttr ".tk[361]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[362]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[363]" -type "float3" -0.072811283 -0.2680105 0 ;
	setAttr ".tk[364]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[365]" -type "float3" 3.3522542e-07 -0.045665909 5.3234434 ;
	setAttr ".tk[366]" -type "float3" 5.3234425 0.18266365 1.0153663e-06 ;
	setAttr ".tk[367]" -type "float3" 0.15436614 -0.11577462 0 ;
	setAttr ".tk[369]" -type "float3" 0.062701292 0.062701292 0 ;
	setAttr ".tk[370]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[371]" -type "float3" -0.7733534 0 2.3801377 ;
	setAttr ".tk[372]" -type "float3" 3.2689893 0.14107792 -0.77335274 ;
	setAttr ".tk[377]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[378]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[379]" -type "float3" 3.2313554 -0.029112799 0 ;
	setAttr ".tk[380]" -type "float3" 1.0499314 0 0 ;
	setAttr ".tk[381]" -type "float3" 0.47014344 0.18164629 0 ;
	setAttr ".tk[382]" -type "float3" 5.2579017 0.18266365 0.83277094 ;
	setAttr ".tk[383]" -type "float3" 0.83277065 -0.045665909 5.2579055 ;
	setAttr ".tk[386]" -type "float3" 3.0273285 0.13585968 0 ;
	setAttr ".tk[387]" -type "float3" 1.5425004 0 0 ;
	setAttr ".tk[388]" -type "float3" -0.05695419 -0.16374329 0 ;
	setAttr ".tk[389]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[390]" -type "float3" 0.062701292 0.062701292 0 ;
	setAttr ".tk[391]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[392]" -type "float3" -0.072811283 -0.2680105 0 ;
	setAttr ".tk[393]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[394]" -type "float3" -0.39149657 0 2.4718134 ;
	setAttr ".tk[395]" -type "float3" 3.3606646 0.14107792 -0.39149576 ;
	setAttr ".tk[397]" -type "float3" 0.18408884 -0.066476524 0 ;
	setAttr ".tk[398]" -type "float3" 2.7487557 0.13585968 0 ;
	setAttr ".tk[399]" -type "float3" 1.9970889 0 0 ;
	setAttr ".tk[404]" -type "float3" 5.0628948 0.18266365 1.6450353 ;
	setAttr ".tk[405]" -type "float3" 1.645035 0.15983069 5.0628958 ;
	setAttr ".tk[406]" -type "float3" 2.4025004 0.13585968 0 ;
	setAttr ".tk[407]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[408]" -type "float3" -0.97512066 -0.10972726 0 ;
	setAttr ".tk[409]" -type "float3" 4.7432227 0 2.4167938 ;
	setAttr ".tk[410]" -type "float3" 2.4167936 0.15983069 4.7432246 ;
	setAttr ".tk[413]" -type "float3" 1.5736823e-07 0 2.5026243 ;
	setAttr ".tk[414]" -type "float3" 3.3914742 0.14107792 4.7733761e-07 ;
	setAttr ".tk[415]" -type "float3" -0.05695419 -0.16374329 0 ;
	setAttr ".tk[416]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[418]" -type "float3" 0.18408884 -0.066476524 0 ;
	setAttr ".tk[419]" -type "float3" -0.072811283 -0.2680105 0 ;
	setAttr ".tk[420]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[421]" -type "float3" 4.3067565 0 3.1290426 ;
	setAttr ".tk[422]" -type "float3" 3.1290424 0.15983069 4.306757 ;
	setAttr ".tk[423]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[424]" -type "float3" -0.97512066 -0.10972726 0 ;
	setAttr ".tk[425]" -type "float3" 3.7642434 0.15983069 3.7642438 ;
	setAttr ".tk[430]" -type "float3" 3.3606634 0.14107792 0.39149714 ;
	setAttr ".tk[431]" -type "float3" 0.74738657 0.078376614 2.4718134 ;
	setAttr ".tk[432]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[433]" -type "float3" -0.97512066 -0.10972726 0 ;
	setAttr ".tk[434]" -type "float3" 0.2659061 -0.10227158 0 ;
	setAttr ".tk[435]" -type "float3" 0.18408884 -0.066476524 0 ;
	setAttr ".tk[438]" -type "float3" 0 -0.16486122 0 ;
	setAttr ".tk[439]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[440]" -type "float3" -0.05695419 -0.16374329 0 ;
	setAttr ".tk[441]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[442]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[443]" -type "float3" -0.97512066 -0.10972726 0 ;
	setAttr ".tk[444]" -type "float3" 3.2689874 0.14107792 0.77335382 ;
	setAttr ".tk[445]" -type "float3" 1.1292436 0.078376614 2.3801377 ;
	setAttr ".tk[446]" -type "float3" -0.50486058 -0.18026626 0 ;
	setAttr ".tk[451]" -type "float3" 0.18408884 -0.066476524 0 ;
	setAttr ".tk[452]" -type "float3" 0.2659061 -0.10227158 0 ;
	setAttr ".tk[453]" -type "float3" 3.118705 0.14107792 1.136168 ;
	setAttr ".tk[454]" -type "float3" 1.4920574 0.078376614 2.2298551 ;
	setAttr ".tk[456]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[458]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[461]" -type "float3" 2.913516 0.14107792 1.4710062 ;
	setAttr ".tk[462]" -type "float3" 2.3598559 0.14107792 2.0246663 ;
	setAttr ".tk[463]" -type "float3" 0.18408884 -0.066476524 0 ;
	setAttr ".tk[464]" -type "float3" 0.2659061 -0.10227158 0 ;
	setAttr ".tk[465]" -type "float3" 2.6584728 0.14107792 1.7696234 ;
	setAttr ".tk[468]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[469]" -type "float3" -0.24270429 -0.16382535 0 ;
	setAttr ".tk[474]" -type "float3" 1.6162951 -0.092550576 0 ;
	setAttr ".tk[475]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[478]" -type "float3" 0.020454314 -0.086930841 0 ;
	setAttr ".tk[479]" -type "float3" 0.020454314 -0.086930841 0 ;
	setAttr ".tk[480]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[481]" -type "float3" -0.24270429 -0.16382535 0 ;
	setAttr ".tk[482]" -type "float3" 0.020454314 -0.086930841 0 ;
	setAttr ".tk[485]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[486]" -type "float3" 1.6162951 -0.092550576 0 ;
	setAttr ".tk[489]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[490]" -type "float3" -0.24270429 -0.16382535 0 ;
	setAttr ".tk[493]" -type "float3" -0.38832679 -0.1031493 0 ;
	setAttr ".tk[495]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[496]" -type "float3" 1.6162951 -0.092550576 0 ;
	setAttr ".tk[504]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[505]" -type "float3" 1.6162951 -0.092550576 0 ;
	setAttr ".tk[506]" -type "float3" 1.7017263 -0.11390839 0 ;
	setAttr ".tk[507]" -type "float3" 1.3884785 0 0 ;
	setAttr ".tk[510]" -type "float3" 1.3884785 0 0 ;
	setAttr ".tk[534]" -type "float3" -1.4496733 -0.11756492 -2.2298551 ;
	setAttr ".tk[535]" -type "float3" -1.6799259 0.10972726 -1.1361679 ;
	setAttr ".tk[536]" -type "float3" -0.98649168 0 0 ;
	setAttr ".tk[537]" -type "float3" 0 0.13193749 0 ;
	setAttr ".tk[538]" -type "float3" 2.0781458e-06 -0.045665909 -5.3234434 ;
	setAttr ".tk[539]" -type "float3" -2.8382564 0.18266365 1.4278584e-07 ;
	setAttr ".tk[541]" -type "float3" 0 -0.46788904 0 ;
	setAttr ".tk[543]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[544]" -type "float3" 0.53151089 0 0 ;
	setAttr ".tk[545]" -type "float3" -3.3558173 -0.0082294587 0 ;
	setAttr ".tk[547]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[548]" -type "float3" -0.88972622 -0.11756492 -2.0246663 ;
	setAttr ".tk[549]" -type "float3" -1.4747365 0.10972726 -1.4710062 ;
	setAttr ".tk[550]" -type "float3" -1.16838 0 -1.6000018 ;
	setAttr ".tk[551]" -type "float3" -1.4055632 0 -1.3984529 ;
	setAttr ".tk[552]" -type "float3" -1.6081371 0 -1.1624694 ;
	setAttr ".tk[555]" -type "float3" 0.062701292 0.062701292 0 ;
	setAttr ".tk[556]" -type "float3" 0.40799859 0.1018896 0 ;
	setAttr ".tk[557]" -type "float3" -1.1883433 -0.11756492 -1.7696233 ;
	setAttr ".tk[558]" -type "float3" -0.90242732 0 -1.7621534 ;
	setAttr ".tk[559]" -type "float3" -1.7711128 0 -0.89786214 ;
	setAttr ".tk[560]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[563]" -type "float3" 0 0.26736522 0 ;
	setAttr ".tk[564]" -type "float3" -1.0596601 0 0 ;
	setAttr ".tk[565]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[566]" -type "float3" -0.5460158 -0.045665909 -5.257905 ;
	setAttr ".tk[567]" -type "float3" -2.7727156 0.18266365 -0.83276999 ;
	setAttr ".tk[568]" -type "float3" 0 -0.46788904 0 ;
	setAttr ".tk[569]" -type "float3" 0 0.057292566 0 ;
	setAttr ".tk[572]" -type "float3" -0.61425376 0 -1.8809148 ;
	setAttr ".tk[573]" -type "float3" -1.8904781 0 -0.61114657 ;
	setAttr ".tk[574]" -type "float3" 0.72150522 0.062701292 0 ;
	setAttr ".tk[575]" -type "float3" 0.40799859 0.1018896 0 ;
	setAttr ".tk[576]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[577]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[578]" -type "float3" 1.4703015e-06 -0.43521699 0 ;
	setAttr ".tk[579]" -type "float3" -3.397649 -0.0082294587 0 ;
	setAttr ".tk[583]" -type "float3" 0 0.058028921 0 ;
	setAttr ".tk[585]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[586]" -type "float3" 0 -0.11880016 0 ;
	setAttr ".tk[587]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[588]" -type "float3" 0.72150522 0.062701292 0 ;
	setAttr ".tk[589]" -type "float3" 0.72150522 0.062701292 0 ;
	setAttr ".tk[590]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[591]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[592]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[593]" -type "float3" -0.31095532 0 -1.9533622 ;
	setAttr ".tk[594]" -type "float3" -1.9632936 0 -0.30938241 ;
	setAttr ".tk[595]" -type "float3" -0.30685419 0.15983069 -5.0628953 ;
	setAttr ".tk[596]" -type "float3" -2.5777078 0.18266365 -1.6450349 ;
	setAttr ".tk[597]" -type "float3" 0.72150522 0.062701292 0 ;
	setAttr ".tk[598]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[599]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[601]" -type "float3" 0 0.26736522 0 ;
	setAttr ".tk[602]" -type "float3" 0 -0.46788904 0 ;
	setAttr ".tk[603]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[604]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[606]" -type "float3" -0.53150827 -0.19261044 0 ;
	setAttr ".tk[607]" -type "float3" -3.3558173 -0.0082294587 0 ;
	setAttr ".tk[609]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[610]" -type "float3" -0.51407617 0 0 ;
	setAttr ".tk[611]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[612]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[613]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[614]" -type "float3" -0.45731542 0.15983069 -4.7432232 ;
	setAttr ".tk[615]" -type "float3" -2.2580333 0 -2.4167936 ;
	setAttr ".tk[619]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[620]" -type "float3" -1.1080704e-07 0 -1.9777106 ;
	setAttr ".tk[621]" -type "float3" -1.9877664 0 -3.7721824e-07 ;
	setAttr ".tk[622]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[623]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[624]" -type "float3" 0 -0.11880016 0 ;
	setAttr ".tk[625]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[627]" -type "float3" 0 -0.062072508 0 ;
	setAttr ".tk[628]" -type "float3" -1.1695625 0.15983069 -4.306757 ;
	setAttr ".tk[629]" -type "float3" -1.8215654 0 -3.1290426 ;
	setAttr ".tk[633]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[634]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[635]" -type "float3" -1.0499297 -0.19261044 0 ;
	setAttr ".tk[636]" -type "float3" -3.2313552 -0.0082294587 0 ;
	setAttr ".tk[637]" -type "float3" -1.2790512 0.15983069 -3.7642438 ;
	setAttr ".tk[640]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[641]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[643]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[645]" -type "float3" 0 0.26736522 0 ;
	setAttr ".tk[647]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[648]" -type "float3" -1.9632951 0 0.30938137 ;
	setAttr ".tk[649]" -type "float3" 0.31095511 0 -1.9533622 ;
	setAttr ".tk[650]" -type "float3" 2.3735802 -0.14191103 0 ;
	setAttr ".tk[657]" -type "float3" -1.5424998 -0.43521699 0 ;
	setAttr ".tk[658]" -type "float3" -3.0273278 -0.0082294587 0 ;
	setAttr ".tk[659]" -type "float3" -0.51407617 0 0 ;
	setAttr ".tk[660]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[662]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[663]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[664]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[670]" -type "float3" 0 -0.062072508 0 ;
	setAttr ".tk[671]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[672]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[673]" -type "float3" -1.9970864 -0.43521699 0 ;
	setAttr ".tk[674]" -type "float3" -2.7487552 -0.0082294587 0 ;
	setAttr ".tk[680]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[681]" -type "float3" -2.4024997 -0.0082294587 0 ;
	setAttr ".tk[683]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[686]" -type "float3" -1.8904797 0 0.61114562 ;
	setAttr ".tk[687]" -type "float3" 0.61425358 0 -1.8809148 ;
	setAttr ".tk[692]" -type "float3" 0 -0.30556023 0 ;
	setAttr ".tk[693]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[694]" -type "float3" 0 -0.077220105 0 ;
	setAttr ".tk[695]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[698]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[699]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[701]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[705]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[706]" -type "float3" 0 -0.14191103 0 ;
	setAttr ".tk[709]" -type "float3" 0 -0.062072508 0 ;
	setAttr ".tk[712]" -type "float3" -0.51407617 0 0 ;
	setAttr ".tk[713]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[714]" -type "float3" 0 -0.14507233 0 ;
	setAttr ".tk[715]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[720]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[721]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[722]" -type "float3" 0 -0.077220105 0 ;
	setAttr ".tk[723]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[724]" -type "float3" -0.73355412 0.073355407 0 ;
	setAttr ".tk[733]" -type "float3" -1.7711142 0 0.89786118 ;
	setAttr ".tk[734]" -type "float3" 0.90242696 0 -1.7621531 ;
	setAttr ".tk[735]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[736]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[738]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[740]" -type "float3" 0 -0.062072508 0 ;
	setAttr ".tk[743]" -type "float3" 0 -0.30556023 0 ;
	setAttr ".tk[744]" -type "float3" 0 0.047743805 0 ;
	setAttr ".tk[745]" -type "float3" 0 -0.077220105 0 ;
	setAttr ".tk[746]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[748]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[750]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[751]" -type "float3" 0 -0.14191103 0 ;
	setAttr ".tk[755]" -type "float3" -0.51407617 0 0 ;
	setAttr ".tk[756]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[760]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[763]" -type "float3" 0 0.058028921 0 ;
	setAttr ".tk[764]" -type "float3" 0 -0.062072508 0 ;
	setAttr ".tk[765]" -type "float3" 0 0.071280099 0 ;
	setAttr ".tk[766]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[767]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[770]" -type "float3" -1.6081381 0 1.1624689 ;
	setAttr ".tk[771]" -type "float3" 1.1683798 0 -1.6000016 ;
	setAttr ".tk[772]" -type "float3" 0 0.12886183 0 ;
	setAttr ".tk[773]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[781]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[785]" -type "float3" 0 0.058028921 0 ;
	setAttr ".tk[786]" -type "float3" 0 0.058028921 0 ;
	setAttr ".tk[787]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[788]" -type "float3" 0 0.047743805 0 ;
	setAttr ".tk[792]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[793]" -type "float3" 0 0.13639599 0 ;
	setAttr ".tk[794]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[799]" -type "float3" 0 0.13193749 0 ;
	setAttr ".tk[800]" -type "float3" 0 0.13193749 0 ;
	setAttr ".tk[801]" -type "float3" 0 0.12886183 0 ;
	setAttr ".tk[802]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[803]" -type "float3" 0 0.13193749 0 ;
	setAttr ".tk[811]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[812]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[815]" -type "float3" -1.4055644 0 1.3984525 ;
	setAttr ".tk[816]" -type "float3" 1.4055632 0 -1.3984526 ;
	setAttr ".tk[818]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[820]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[821]" -type "float3" 0 0.13639599 0 ;
	setAttr ".tk[822]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[825]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[826]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[830]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[833]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[834]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[837]" -type "float3" -0.944987 0.1932928 0 ;
	setAttr ".tk[844]" -type "float3" 0 0.13639599 0 ;
	setAttr ".tk[845]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[848]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[849]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[852]" -type "float3" -1.1683811 0 1.6000013 ;
	setAttr ".tk[853]" -type "float3" 1.6081369 0 -1.1624691 ;
	setAttr ".tk[857]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[859]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[860]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[861]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[868]" -type "float3" 0 0.13639599 0 ;
	setAttr ".tk[869]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[877]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[878]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[879]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[880]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[882]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[883]" -type "float3" -0.90242839 0 1.7621529 ;
	setAttr ".tk[884]" -type "float3" 1.7711127 0 -0.8978619 ;
	setAttr ".tk[886]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[888]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[889]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[890]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[897]" -type "float3" -0.83656192 0 0 ;
	setAttr ".tk[898]" -type "float3" 0 0.26736522 0 ;
	setAttr ".tk[899]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[905]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[906]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[907]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[909]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[910]" -type "float3" -0.61425465 0 1.8809144 ;
	setAttr ".tk[911]" -type "float3" 1.8904781 0 -0.61114627 ;
	setAttr ".tk[916]" -type "float3" 0 0.26736522 0 ;
	setAttr ".tk[917]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[922]" -type "float3" 0 0.057292566 0 ;
	setAttr ".tk[923]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[924]" -type "float3" 0 0.26736522 0 ;
	setAttr ".tk[925]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[927]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[930]" -type "float3" -0.31095609 0 1.9533617 ;
	setAttr ".tk[931]" -type "float3" 1.9632938 0 -0.30938217 ;
	setAttr ".tk[932]" -type "float3" 0 -0.03587269 0 ;
	setAttr ".tk[933]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[934]" -type "float3" 0 0.26736522 0 ;
	setAttr ".tk[935]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[938]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[939]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[940]" -type "float3" 0 -0.038195074 0 ;
	setAttr ".tk[944]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[945]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[946]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[947]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[948]" -type "float3" -7.4522512e-07 0 1.9777106 ;
	setAttr ".tk[949]" -type "float3" 1.9877664 0 -5.3046328e-08 ;
	setAttr ".tk[950]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[951]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[954]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[955]" -type "float3" 0 0.047743805 0 ;
	setAttr ".tk[958]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[959]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[960]" -type "float3" 0 0.047743805 0 ;
	setAttr ".tk[963]" -type "float3" 0.31095451 0 1.9533617 ;
	setAttr ".tk[964]" -type "float3" 1.9632938 0 0.30938211 ;
	setAttr ".tk[965]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[966]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[967]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[968]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[973]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[974]" -type "float3" 0 -0.15955012 0 ;
	setAttr ".tk[976]" -type "float3" 0.61425304 0 1.8809144 ;
	setAttr ".tk[977]" -type "float3" 1.8904781 0 0.61114627 ;
	setAttr ".tk[978]" -type "float3" 0 0.12409453 0 ;
	setAttr ".tk[981]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[982]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[983]" -type "float3" 0.90242648 0 1.7621531 ;
	setAttr ".tk[984]" -type "float3" 1.7711127 0 0.8978619 ;
	setAttr ".tk[989]" -type "float3" 1.1683793 0 1.6000017 ;
	setAttr ".tk[990]" -type "float3" 1.6081365 0 1.1624694 ;
	setAttr ".tk[991]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[992]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[993]" -type "float3" 1.4055626 0 1.3984528 ;
	setAttr ".tk[1001]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[1002]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[1003]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[1004]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[1012]" -type "float3" 0 0.11957566 0 ;
	setAttr ".tk[1040]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1041]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1042]" -type "float3" -0.22781676 0.092550576 0 ;
	setAttr ".tk[1043]" -type "float3" -0.22781676 0.092550576 0 ;
	setAttr ".tk[1044]" -type "float3" -0.22781676 0.092550576 0 ;
	setAttr ".tk[1045]" -type "float3" -0.42715648 0.092550576 0 ;
	setAttr ".tk[1046]" -type "float3" 0.028477095 -0.18510112 0 ;
	setAttr ".tk[1047]" -type "float3" 0.028477095 -0.18510112 0 ;
	setAttr ".tk[1049]" -type "float3" -0.11390838 -0.12814695 0 ;
	setAttr ".tk[1050]" -type "float3" -0.11390838 -0.12814695 0 ;
	setAttr ".tk[1051]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1052]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1053]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1054]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1055]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1056]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1057]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1058]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1059]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1060]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1061]" -type "float3" -0.62649608 0.0071192738 0 ;
	setAttr ".tk[1062]" -type "float3" -0.11390838 -0.12814695 0 ;
	setAttr ".tk[1063]" -type "float3" -0.11390838 -0.12814695 0 ;
	setAttr ".tk[1065]" -type "float3" 0.028477095 -0.18510112 0 ;
	setAttr ".tk[1066]" -type "float3" 0.028477095 -0.18510112 0 ;
	setAttr ".tk[1067]" -type "float3" -0.42715648 0.092550576 0 ;
	setAttr ".tk[1068]" -type "float3" -0.22781676 0.092550576 0 ;
	setAttr ".tk[1069]" -type "float3" -0.22781676 0.092550576 0 ;
	setAttr ".tk[1070]" -type "float3" -0.22781676 0.092550576 0 ;
	setAttr ".tk[1071]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1072]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1073]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1074]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1075]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1076]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1077]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1078]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1079]" -type "float3" -0.45563361 -0.0071192738 0 ;
	setAttr ".tk[1080]" -type "float3" 0.097081713 0.072811283 -1.8911434 ;
	setAttr ".tk[1081]" -type "float3" 0.097081713 0.072811283 -2.2750487 ;
	setAttr ".tk[1082]" -type "float3" -0.43866131 0.072811283 -2.6029346 ;
	setAttr ".tk[1083]" -type "float3" -0.43866131 0.072811283 -2.8667274 ;
	setAttr ".tk[1084]" -type "float3" -0.535743 0 -3.0599325 ;
	setAttr ".tk[1085]" -type "float3" -0.63282466 0.097081698 -3.177794 ;
	setAttr ".tk[1086]" -type "float3" -0.63282466 0.097081698 -3.2174044 ;
	setAttr ".tk[1087]" -type "float3" -0.75417674 -0.097081721 -3.177794 ;
	setAttr ".tk[1088]" -type "float3" -0.535743 0.11540285 -3.0599325 ;
	setAttr ".tk[1089]" -type "float3" -0.36585009 -0.096963391 -2.8667274 ;
	setAttr ".tk[1090]" -type "float3" 0.34079728 -0.054136254 -2.6029346 ;
	setAttr ".tk[1091]" -type "float3" 0.34079728 -0.054136254 -2.2750478 ;
	setAttr ".tk[1092]" -type "float3" 0.17090431 0.15822998 -1.8911417 ;
	setAttr ".tk[1093]" -type "float3" 0.17090431 0.15822998 -1.4606702 ;
	setAttr ".tk[1094]" -type "float3" 0.17090431 0.15822998 -0.99423128 ;
	setAttr ".tk[1095]" -type "float3" 0.17090431 0.15822998 -0.50331151 ;
	setAttr ".tk[1096]" -type "float3" 0.17090431 0.15822998 6.1367069e-07 ;
	setAttr ".tk[1097]" -type "float3" 0.17090431 0.15822998 0.5033133 ;
	setAttr ".tk[1098]" -type "float3" 0.17090431 0.15822998 0.99423277 ;
	setAttr ".tk[1099]" -type "float3" 0.17090431 0.15822998 1.4606711 ;
	setAttr ".tk[1100]" -type "float3" 0.17090431 0.15822998 1.8911434 ;
	setAttr ".tk[1101]" -type "float3" 0.34079728 -0.054136254 2.275049 ;
	setAttr ".tk[1102]" -type "float3" 0.34079728 -0.054136254 2.6029348 ;
	setAttr ".tk[1103]" -type "float3" -0.36585009 -0.096963391 2.8667278 ;
	setAttr ".tk[1104]" -type "float3" -0.535743 0.11540285 3.0599346 ;
	setAttr ".tk[1105]" -type "float3" -0.75417674 -0.097081721 3.177794 ;
	setAttr ".tk[1106]" -type "float3" -0.63282466 0.097081698 3.2174044 ;
	setAttr ".tk[1107]" -type "float3" -0.63282466 0.097081698 3.177794 ;
	setAttr ".tk[1108]" -type "float3" -0.535743 0 3.0599346 ;
	setAttr ".tk[1109]" -type "float3" -0.43866131 0.072811283 2.8667278 ;
	setAttr ".tk[1110]" -type "float3" -0.43866131 0.072811283 2.6029346 ;
	setAttr ".tk[1111]" -type "float3" 0.097081713 0.072811283 2.2750478 ;
	setAttr ".tk[1112]" -type "float3" 0.097081713 0.072811283 1.8911434 ;
	setAttr ".tk[1113]" -type "float3" -0.21843386 0.21236618 1.4606711 ;
	setAttr ".tk[1114]" -type "float3" -0.21843386 0.21236618 0.99423265 ;
	setAttr ".tk[1115]" -type "float3" -0.21843386 0.21236618 0.50331312 ;
	setAttr ".tk[1116]" -type "float3" -0.21843386 0.21236618 8.6297462e-08 ;
	setAttr ".tk[1117]" -type "float3" -0.21843386 0.21236618 -0.503313 ;
	setAttr ".tk[1118]" -type "float3" -0.21843386 0.21236618 -0.99423265 ;
	setAttr ".tk[1119]" -type "float3" -0.21843386 0.21236618 -1.4606711 ;
	setAttr ".tk[1120]" -type "float3" -1.9076438 0.057866648 0.40376472 ;
	setAttr ".tk[1121]" -type "float3" -1.669471 0.057866648 0.48572981 ;
	setAttr ".tk[1122]" -type "float3" -1.4650576 0.025451712 0.55573452 ;
	setAttr ".tk[1123]" -type "float3" -1.1523677 0.025451712 0.61205518 ;
	setAttr ".tk[1124]" -type "float3" -0.81355268 0.025451712 0.65330505 ;
	setAttr ".tk[1125]" -type "float3" -0.36559954 0.13393575 0.67846847 ;
	setAttr ".tk[1126]" -type "float3" 1.2245563 0 0.68692553 ;
	setAttr ".tk[1127]" -type "float3" 1.5901568 0 0.67846847 ;
	setAttr ".tk[1128]" -type "float3" 1.5891798 0.089393869 0.65330505 ;
	setAttr ".tk[1129]" -type "float3" 1.9279954 0.089393869 0.61205506 ;
	setAttr ".tk[1130]" -type "float3" 2.2406847 0.089393869 0.55573446 ;
	setAttr ".tk[1131]" -type "float3" 2.8771245 0 0.4857296 ;
	setAttr ".tk[1132]" -type "float3" 2.6782613 0.13409081 0.40376458 ;
	setAttr ".tk[1133]" -type "float3" 2.8698769 0.13409081 0.31185749 ;
	setAttr ".tk[1134]" -type "float3" 3.0102191 0.13409081 0.21227147 ;
	setAttr ".tk[1135]" -type "float3" 3.0958307 0.13409081 0.10745858 ;
	setAttr ".tk[1136]" -type "float3" 3.1246023 0.13409081 -1.3102061e-07 ;
	setAttr ".tk[1137]" -type "float3" 3.095829 0.13409081 -0.10745893 ;
	setAttr ".tk[1138]" -type "float3" 3.0102174 0.13409081 -0.21227176 ;
	setAttr ".tk[1139]" -type "float3" 2.8698759 0.13409081 -0.31185773 ;
	setAttr ".tk[1140]" -type "float3" 2.6782596 0.13409081 -0.40376472 ;
	setAttr ".tk[1141]" -type "float3" 2.8771226 0 -0.48572981 ;
	setAttr ".tk[1142]" -type "float3" 2.2406838 0.089393869 -0.55573457 ;
	setAttr ".tk[1143]" -type "float3" 1.927994 0.089393869 -0.61205518 ;
	setAttr ".tk[1144]" -type "float3" 1.5891786 0.089393869 -0.65330517 ;
	setAttr ".tk[1145]" -type "float3" 1.5901558 0 -0.67846853 ;
	setAttr ".tk[1146]" -type "float3" 1.2245556 0 -0.68692553 ;
	setAttr ".tk[1147]" -type "float3" -0.3656002 0.13393575 -0.67846853 ;
	setAttr ".tk[1148]" -type "float3" -0.81355339 0.025451712 -0.65330517 ;
	setAttr ".tk[1149]" -type "float3" -1.1523681 0.025451712 -0.61205518 ;
	setAttr ".tk[1150]" -type "float3" -1.4650581 0.025451712 -0.55573446 ;
	setAttr ".tk[1151]" -type "float3" -1.6694711 0.057866648 -0.48572975 ;
	setAttr ".tk[1152]" -type "float3" -1.907644 0.057866648 -0.40376472 ;
	setAttr ".tk[1153]" -type "float3" -2.0992606 0.057866648 -0.3118577 ;
	setAttr ".tk[1154]" -type "float3" -2.2226977 0.13393575 -0.21227171 ;
	setAttr ".tk[1155]" -type "float3" -2.8189323 0.2819013 -0.10745885 ;
	setAttr ".tk[1156]" -type "float3" -2.8477054 0.2819013 -1.8424775e-08 ;
	setAttr ".tk[1157]" -type "float3" -2.8189323 0.2819013 0.10745882 ;
	setAttr ".tk[1158]" -type "float3" -2.2226977 0.13393575 0.21227166 ;
	setAttr ".tk[1159]" -type "float3" -2.0992606 0.057866648 0.3118577 ;
	setAttr ".tk[1160]" -type "float3" -1.7244461 -0.053830892 0 ;
	setAttr ".tk[1161]" -type "float3" -1.6738298 -0.053830892 0 ;
	setAttr ".tk[1162]" -type "float3" -1.2311523 -0.16268833 0 ;
	setAttr ".tk[1163]" -type "float3" -1.0711732 -0.26773983 0 ;
	setAttr ".tk[1164]" -type "float3" -0.93035829 -0.26773983 0 ;
	setAttr ".tk[1165]" -type "float3" -0.53203726 -0.022619685 0 ;
	setAttr ".tk[1166]" -type "float3" -0.34716582 -0.022619685 0 ;
	setAttr ".tk[1167]" -type "float3" -0.14684796 -0.022619685 0 ;
	setAttr ".tk[1168]" -type "float3" -0.21615359 -0.16268833 0 ;
	setAttr ".tk[1169]" -type "float3" 5.2284287e-07 -0.16268833 0 ;
	setAttr ".tk[1170]" -type "float3" 0.21615466 -0.16268833 0 ;
	setAttr ".tk[1171]" -type "float3" 0.42698634 -0.44739291 0 ;
	setAttr ".tk[1172]" -type "float3" 0.62730426 -0.44739291 0 ;
	setAttr ".tk[1173]" -type "float3" 0.81217575 -0.44739291 0 ;
	setAttr ".tk[1174]" -type "float3" 0.97704875 -0.44739291 0 ;
	setAttr ".tk[1175]" -type "float3" 1.1178638 -0.44739291 0 ;
	setAttr ".tk[1176]" -type "float3" 1.2311531 -0.44739291 0 ;
	setAttr ".tk[1177]" -type "float3" 1.3141277 -0.44739291 0 ;
	setAttr ".tk[1178]" -type "float3" 1.3647435 -0.44739291 0 ;
	setAttr ".tk[1179]" -type "float3" 1.3817545 -0.44739291 0 ;
	setAttr ".tk[1180]" -type "float3" 1.3647429 -0.44739291 0 ;
	setAttr ".tk[1181]" -type "float3" 1.3141267 -0.44739291 0 ;
	setAttr ".tk[1182]" -type "float3" 1.2311523 -0.44739291 0 ;
	setAttr ".tk[1183]" -type "float3" 1.1178629 -0.44739291 0 ;
	setAttr ".tk[1184]" -type "float3" 0.97704798 -0.44739291 0 ;
	setAttr ".tk[1185]" -type "float3" 0.81217492 -0.44739291 0 ;
	setAttr ".tk[1186]" -type "float3" 0.6273036 -0.44739291 0 ;
	setAttr ".tk[1187]" -type "float3" 0.42698568 -0.44739291 0 ;
	setAttr ".tk[1188]" -type "float3" 0.2161541 -0.16268833 0 ;
	setAttr ".tk[1189]" -type "float3" 7.6255439e-08 -0.16268833 0 ;
	setAttr ".tk[1190]" -type "float3" -0.21615401 -0.16268833 0 ;
	setAttr ".tk[1191]" -type "float3" -0.14684838 -0.022619685 0 ;
	setAttr ".tk[1192]" -type "float3" -0.34716612 -0.022619685 0 ;
	setAttr ".tk[1193]" -type "float3" -0.53203738 -0.022619685 0 ;
	setAttr ".tk[1194]" -type "float3" -0.93035841 -0.26773983 0 ;
	setAttr ".tk[1195]" -type "float3" -1.0711734 -0.26773983 0 ;
	setAttr ".tk[1196]" -type "float3" -1.2311523 -0.16268833 0 ;
	setAttr ".tk[1197]" -type "float3" -1.6738298 -0.053830892 0 ;
	setAttr ".tk[1198]" -type "float3" -1.7244461 -0.053830892 0 ;
	setAttr ".tk[1199]" -type "float3" -1.7414577 -0.053830892 0 ;
	setAttr ".tk[1200]" -type "float3" 0.38042867 -0.015890118 0 ;
	setAttr ".tk[1201]" -type "float3" 1.3455702 0 0 ;
	setAttr ".tk[1202]" -type "float3" 1.3233117 -0.13324025 0 ;
	setAttr ".tk[1203]" -type "float3" 1.2073114 -0.13324025 0 ;
	setAttr ".tk[1204]" -type "float3" 1.0631272 -0.13324025 0 ;
	setAttr ".tk[1205]" -type "float3" 0.83160782 0 0 ;
	setAttr ".tk[1206]" -type "float3" 0.64231271 -0.12540258 0 ;
	setAttr ".tk[1207]" -type "float3" 0.092344791 0.14107792 0 ;
	setAttr ".tk[1208]" -type "float3" -0.12353157 0.14107792 0 ;
	setAttr ".tk[1209]" -type "float3" -0.34485766 0.14107792 0 ;
	setAttr ".tk[1210]" -type "float3" -0.22132665 0 0 ;
	setAttr ".tk[1211]" -type "float3" -0.87611222 0.078376614 0 ;
	setAttr ".tk[1212]" -type "float3" -1.0812231 0.078376614 0 ;
	setAttr ".tk[1213]" -type "float3" -1.2705179 0.078376614 0 ;
	setAttr ".tk[1214]" -type "float3" -1.0004272 0 0 ;
	setAttr ".tk[1215]" -type "float3" -1.0819101 -0.094051935 0 ;
	setAttr ".tk[1216]" -type "float3" -1.1979101 -0.094051935 0 ;
	setAttr ".tk[1217]" -type "float3" -1.2828698 -0.094051935 0 ;
	setAttr ".tk[1218]" -type "float3" -1.3346969 -0.094051935 0 ;
	setAttr ".tk[1219]" -type "float3" -1.3521148 -0.094051935 0 ;
	setAttr ".tk[1220]" -type "float3" -1.3346963 -0.094051935 0 ;
	setAttr ".tk[1221]" -type "float3" -1.2828692 -0.094051935 0 ;
	setAttr ".tk[1222]" -type "float3" -1.1979094 -0.094051935 0 ;
	setAttr ".tk[1223]" -type "float3" -1.0819094 -0.094051935 0 ;
	setAttr ".tk[1224]" -type "float3" -1.0004262 0 0 ;
	setAttr ".tk[1225]" -type "float3" -1.270517 0.078376614 0 ;
	setAttr ".tk[1226]" -type "float3" -1.0812224 0.078376614 0 ;
	setAttr ".tk[1227]" -type "float3" -0.87611163 0.078376614 0 ;
	setAttr ".tk[1228]" -type "float3" -0.22132614 0 0 ;
	setAttr ".tk[1229]" -type "float3" -0.34485719 0.14107792 0 ;
	setAttr ".tk[1230]" -type "float3" -0.12353115 0.14107792 0 ;
	setAttr ".tk[1231]" -type "float3" 0.092345148 0.14107792 0 ;
	setAttr ".tk[1232]" -type "float3" 0.64231318 -0.12540258 0 ;
	setAttr ".tk[1233]" -type "float3" 0.831608 0 0 ;
	setAttr ".tk[1234]" -type "float3" 1.0631274 -0.13324025 0 ;
	setAttr ".tk[1235]" -type "float3" 1.2073116 -0.13324025 0 ;
	setAttr ".tk[1236]" -type "float3" 1.3233117 -0.13324025 0 ;
	setAttr ".tk[1237]" -type "float3" 1.3455702 0 0 ;
	setAttr ".tk[1238]" -type "float3" 0.38042867 -0.015890118 0 ;
	setAttr ".tk[1239]" -type "float3" 0.39784703 -0.015890118 0 ;
	setAttr ".tk[1240]" -type "float3" 0.26466894 0.023512986 8.6269276e-09 ;
	setAttr ".tk[1241]" -type "float3" 0.2575506 0.023512986 0.090446591 ;
	setAttr ".tk[1242]" -type "float3" 0.23637107 0.023512986 0.17866606 ;
	setAttr ".tk[1243]" -type "float3" 0.20165169 0.023512986 0.26248625 ;
	setAttr ".tk[1244]" -type "float3" 0.15424734 0.023512986 0.33984303 ;
	setAttr ".tk[1245]" -type "float3" 0.095325232 0.023512986 0.40883189 ;
	setAttr ".tk[1246]" -type "float3" 0.026336424 0.023512986 0.46775389 ;
	setAttr ".tk[1247]" -type "float3" -0.051020473 0.023512986 0.51515818 ;
	setAttr ".tk[1248]" -type "float3" 0.17866585 0 0.5498777 ;
	setAttr ".tk[1249]" -type "float3" 0.090446353 0 0.57105732 ;
	setAttr ".tk[1250]" -type "float3" -2.2610598e-07 0 0.57817554 ;
	setAttr ".tk[1251]" -type "float3" -0.68935949 0 0.57105732 ;
	setAttr ".tk[1252]" -type "float3" -0.77757901 0 0.5498777 ;
	setAttr ".tk[1253]" -type "float3" -0.86139923 0 0.51515818 ;
	setAttr ".tk[1254]" -type "float3" -0.75065207 0.094051935 0.46775371 ;
	setAttr ".tk[1255]" -type "float3" -0.81964087 0.094051935 0.40883178 ;
	setAttr ".tk[1256]" -type "float3" -0.87856293 0.094051935 0.33984298 ;
	setAttr ".tk[1257]" -type "float3" -1.1140711 0 0.26248601 ;
	setAttr ".tk[1258]" -type "float3" -0.89798546 -0.12540258 0.17866588 ;
	setAttr ".tk[1259]" -type "float3" -0.91916507 -0.12540258 0.090446368 ;
	setAttr ".tk[1260]" -type "float3" -0.92628306 -0.12540258 -8.6143388e-08 ;
	setAttr ".tk[1261]" -type "float3" -0.91916472 -0.12540258 -0.090446636 ;
	setAttr ".tk[1262]" -type "float3" -0.89798498 -0.12540258 -0.17866611 ;
	setAttr ".tk[1263]" -type "float3" -1.1140709 0 -0.26248619 ;
	setAttr ".tk[1264]" -type "float3" -0.87856257 0.094051935 -0.33984298 ;
	setAttr ".tk[1265]" -type "float3" -0.81964052 0.094051935 -0.40883189 ;
	setAttr ".tk[1266]" -type "float3" -0.75065166 0.094051935 -0.46775389 ;
	setAttr ".tk[1267]" -type "float3" -0.86139888 0 -0.5151583 ;
	setAttr ".tk[1268]" -type "float3" -0.77757877 0 -0.54987758 ;
	setAttr ".tk[1269]" -type "float3" -0.68935925 0 -0.57105732 ;
	setAttr ".tk[1270]" -type "float3" -3.5898395e-08 0 -0.57817554 ;
	setAttr ".tk[1271]" -type "float3" 0.090446547 0 -0.57105732 ;
	setAttr ".tk[1272]" -type "float3" 0.17866597 0 -0.54987758 ;
	setAttr ".tk[1273]" -type "float3" -0.051020354 0.023512986 -0.51515806 ;
	setAttr ".tk[1274]" -type "float3" 0.026336424 0.023512986 -0.46775377 ;
	setAttr ".tk[1275]" -type "float3" 0.095325351 0.023512986 -0.40883178 ;
	setAttr ".tk[1276]" -type "float3" 0.15424734 0.023512986 -0.33984298 ;
	setAttr ".tk[1277]" -type "float3" 0.20165169 0.023512986 -0.26248616 ;
	setAttr ".tk[1278]" -type "float3" 0.23637107 0.023512986 -0.17866606 ;
	setAttr ".tk[1279]" -type "float3" 0.2575506 0.023512986 -0.090446576 ;
	setAttr ".tk[1280]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1281]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1282]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1283]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1284]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1285]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1286]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1287]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1288]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1289]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1291]" -type "float3" 0 0.078376614 0 ;
	setAttr ".tk[1292]" -type "float3" 0 0.078376614 0 ;
	setAttr ".tk[1295]" -type "float3" 0 -0.094051935 0 ;
	setAttr ".tk[1296]" -type "float3" 0 -0.094051935 0 ;
	setAttr ".tk[1297]" -type "float3" 0 -0.094051935 0 ;
	setAttr ".tk[1298]" -type "float3" 0 -0.094051935 0 ;
	setAttr ".tk[1300]" -type "float3" 0 -0.086214274 0 ;
	setAttr ".tk[1301]" -type "float3" 0 -0.086214274 0 ;
	setAttr ".tk[1302]" -type "float3" 0 -0.086214274 0 ;
	setAttr ".tk[1303]" -type "float3" 0 -0.086214274 0 ;
	setAttr ".tk[1304]" -type "float3" 0 -0.086214274 0 ;
	setAttr ".tk[1306]" -type "float3" 0 -0.094051935 0 ;
	setAttr ".tk[1307]" -type "float3" 0 -0.094051935 0 ;
	setAttr ".tk[1308]" -type "float3" 0 -0.094051935 0 ;
	setAttr ".tk[1309]" -type "float3" 0 -0.094051935 0 ;
	setAttr ".tk[1312]" -type "float3" 0 0.078376614 0 ;
	setAttr ".tk[1313]" -type "float3" 0 0.078376614 0 ;
	setAttr ".tk[1315]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1316]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1317]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1318]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1319]" -type "float3" 0.65880388 0.078376614 0 ;
	setAttr ".tk[1320]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1321]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1322]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1323]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1324]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1325]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1326]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1327]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1328]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1331]" -type "float3" 0 0.086214274 0 ;
	setAttr ".tk[1332]" -type "float3" 0 0.086214274 0 ;
	setAttr ".tk[1333]" -type "float3" 0 0.086214274 0 ;
	setAttr ".tk[1335]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1336]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1337]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1338]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1339]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1340]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1341]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1342]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1343]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1344]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1345]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1346]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1347]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1348]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1349]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1351]" -type "float3" 0 0.086214274 0 ;
	setAttr ".tk[1352]" -type "float3" 0 0.086214274 0 ;
	setAttr ".tk[1353]" -type "float3" 0 0.086214274 0 ;
	setAttr ".tk[1356]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1357]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1358]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1359]" -type "float3" 0 0.11756492 0 ;
	setAttr ".tk[1400]" -type "float3" 0 -0.25554606 0 ;
	setAttr ".tk[1401]" -type "float3" 0 0.030975295 0 ;
	setAttr ".tk[1402]" -type "float3" 0 0.030975295 0 ;
	setAttr ".tk[1403]" -type "float3" 0 0.030975295 0 ;
	setAttr ".tk[1404]" -type "float3" 0 0.030975295 0 ;
	setAttr ".tk[1405]" -type "float3" 0 0.030975295 0 ;
	setAttr ".tk[1406]" -type "float3" 0 -0.25554606 0 ;
	setAttr ".tk[1407]" -type "float3" 0 -0.10841348 0 ;
	setAttr ".tk[1408]" -type "float3" 0 -0.10841348 0 ;
	setAttr ".tk[1409]" -type "float3" 0 0.10841348 0 ;
	setAttr ".tk[1410]" -type "float3" 0 0.10841348 0 ;
	setAttr ".tk[1413]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1414]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1415]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1416]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1417]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1418]" -type "float3" -0.17685822 0.11505303 0 ;
	setAttr ".tk[1419]" -type "float3" -0.17685822 0.11505303 0 ;
	setAttr ".tk[1420]" -type "float3" -0.17685822 0.0180104 0 ;
	setAttr ".tk[1421]" -type "float3" -0.17685822 0.0180104 0 ;
	setAttr ".tk[1422]" -type "float3" -0.17685822 0.018010415 0 ;
	setAttr ".tk[1423]" -type "float3" -0.17685822 0.018010415 0 ;
	setAttr ".tk[1424]" -type "float3" -0.17685822 0.018010415 0 ;
	setAttr ".tk[1425]" -type "float3" -0.17685822 0.0180104 0 ;
	setAttr ".tk[1426]" -type "float3" -0.17685822 0.0180104 0 ;
	setAttr ".tk[1427]" -type "float3" -0.17685822 0.11505303 0 ;
	setAttr ".tk[1428]" -type "float3" -0.17685822 0.11505303 0 ;
	setAttr ".tk[1429]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1430]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1431]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1432]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1433]" -type "float3" -0.17685822 0.28002548 0 ;
	setAttr ".tk[1436]" -type "float3" 0 0.10841348 0 ;
	setAttr ".tk[1437]" -type "float3" 0 0.10841348 0 ;
	setAttr ".tk[1438]" -type "float3" 0 -0.10841348 0 ;
	setAttr ".tk[1439]" -type "float3" 0 -0.10841348 0 ;
	setAttr ".tk[1440]" -type "float3" -1.1025497 -0.18859401 0 ;
	setAttr ".tk[1441]" -type "float3" -1.1025497 -0.18859401 0 ;
	setAttr ".tk[1442]" -type "float3" -1.1025497 -0.10522208 0 ;
	setAttr ".tk[1443]" -type "float3" -1.1025497 -0.063536085 0 ;
	setAttr ".tk[1444]" -type "float3" -1.7903689 -0.063536085 0 ;
	setAttr ".tk[1445]" -type "float3" -1.7903689 -0.063536085 0 ;
	setAttr ".tk[1446]" -type "float3" -1.7903689 -0.063536085 0 ;
	setAttr ".tk[1447]" -type "float3" -1.1025497 -0.063536085 0 ;
	setAttr ".tk[1448]" -type "float3" -1.1025497 -0.10522208 0 ;
	setAttr ".tk[1449]" -type "float3" -1.1025497 -0.18859401 0 ;
	setAttr ".tk[1450]" -type "float3" -1.1025497 -0.18859401 0 ;
	setAttr ".tk[1451]" -type "float3" 0 -0.058325317 0 ;
	setAttr ".tk[1452]" -type "float3" 0 -0.058325317 0 ;
	setAttr ".tk[1453]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1454]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1456]" -type "float3" 0 -0.24662296 0 ;
	setAttr ".tk[1457]" -type "float3" 0 -0.16846175 0 ;
	setAttr ".tk[1458]" -type "float3" 0 -0.16846175 0 ;
	setAttr ".tk[1459]" -type "float3" 0 -0.36125946 0 ;
	setAttr ".tk[1460]" -type "float3" 0 -0.36125946 0 ;
	setAttr ".tk[1461]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1462]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1463]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1464]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1465]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1466]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1467]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1468]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1469]" -type "float3" 0 -0.12156507 0 ;
	setAttr ".tk[1470]" -type "float3" 0 -0.36125946 0 ;
	setAttr ".tk[1471]" -type "float3" 0 -0.36125946 0 ;
	setAttr ".tk[1472]" -type "float3" 0 -0.16846175 0 ;
	setAttr ".tk[1473]" -type "float3" 0 -0.16846175 0 ;
	setAttr ".tk[1474]" -type "float3" 0 -0.24662296 0 ;
	setAttr ".tk[1476]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1477]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1478]" -type "float3" 0 -0.058325317 0 ;
	setAttr ".tk[1479]" -type "float3" 0 -0.058325317 0 ;
	setAttr ".tk[1480]" -type "float3" 0 -0.071395189 1.7590523 ;
	setAttr ".tk[1481]" -type "float3" 0.46423125 -0.071395189 1.4622188 ;
	setAttr ".tk[1482]" -type "float3" 0.46423125 -0.071395189 1.1293815 ;
	setAttr ".tk[1483]" -type "float3" -0.29608977 0.13056509 0.76873398 ;
	setAttr ".tk[1484]" -type "float3" -0.29608977 0.13056509 0.3891584 ;
	setAttr ".tk[1485]" -type "float3" -0.29608977 0.13056509 6.6724624e-08 ;
	setAttr ".tk[1486]" -type "float3" -0.29608977 0.13056509 -0.38915804 ;
	setAttr ".tk[1487]" -type "float3" -0.29608977 0.13056509 -0.7687341 ;
	setAttr ".tk[1488]" -type "float3" 0.46423125 -0.071395189 -1.1293815 ;
	setAttr ".tk[1489]" -type "float3" 0.46423125 -0.071395189 -1.4622189 ;
	setAttr ".tk[1490]" -type "float3" 0 -0.071395189 -1.7590525 ;
	setAttr ".tk[1491]" -type "float3" 0 0.13056509 -2.0125723 ;
	setAttr ".tk[1492]" -type "float3" 0 0.011764951 -2.2165344 ;
	setAttr ".tk[1493]" -type "float3" 0 0.011764951 -2.365921 ;
	setAttr ".tk[1494]" -type "float3" 0 0.13056509 -2.4570475 ;
	setAttr ".tk[1495]" -type "float3" 0 0 -2.4876761 ;
	setAttr ".tk[1496]" -type "float3" 0 -0.24662296 -2.4570475 ;
	setAttr ".tk[1497]" -type "float3" 0 -0.24662296 -2.365921 ;
	setAttr ".tk[1498]" -type "float3" 0 -0.11594284 -2.2165341 ;
	setAttr ".tk[1499]" -type "float3" 0 -0.11594284 -2.0125718 ;
	setAttr ".tk[1500]" -type "float3" 0 -0.24662296 -1.759052 ;
	setAttr ".tk[1501]" -type "float3" 0 -0.1753429 -1.4622185 ;
	setAttr ".tk[1502]" -type "float3" 0 -0.1753429 -1.1293806 ;
	setAttr ".tk[1503]" -type "float3" 0 -0.1753429 -0.76873291 ;
	setAttr ".tk[1504]" -type "float3" 0 -0.1753429 -0.38915727 ;
	setAttr ".tk[1505]" -type "float3" 0 -0.1753429 4.7448634e-07 ;
	setAttr ".tk[1506]" -type "float3" 0 -0.1753429 0.38915861 ;
	setAttr ".tk[1507]" -type "float3" 0 -0.1753429 0.76873416 ;
	setAttr ".tk[1508]" -type "float3" 0 -0.1753429 1.1293817 ;
	setAttr ".tk[1509]" -type "float3" 0 -0.1753429 1.4622189 ;
	setAttr ".tk[1510]" -type "float3" 0 -0.24662296 1.7590525 ;
	setAttr ".tk[1511]" -type "float3" 0 -0.11594284 2.0125725 ;
	setAttr ".tk[1512]" -type "float3" 0 -0.11594284 2.2165346 ;
	setAttr ".tk[1513]" -type "float3" 0 -0.24662296 2.365921 ;
	setAttr ".tk[1514]" -type "float3" 0 -0.24662296 2.4570479 ;
	setAttr ".tk[1515]" -type "float3" 0 0 2.4876761 ;
	setAttr ".tk[1516]" -type "float3" 0 0.13056509 2.4570479 ;
	setAttr ".tk[1517]" -type "float3" 0 0.011764951 2.365921 ;
	setAttr ".tk[1518]" -type "float3" 0 0.011764951 2.2165346 ;
	setAttr ".tk[1519]" -type "float3" 0 0.13056509 2.0125721 ;
	setAttr ".tk[1520]" -type "float3" -0.27284145 0 1.8568999 ;
	setAttr ".tk[1521]" -type "float3" -0.58738863 0.083147146 1.6229898 ;
	setAttr ".tk[1522]" -type "float3" -0.85603738 0.083147146 1.3491168 ;
	setAttr ".tk[1523]" -type "float3" -1.0721719 0.083147146 1.0420238 ;
	setAttr ".tk[1524]" -type "float3" -1.2304721 0.083147146 0.70927268 ;
	setAttr ".tk[1525]" -type "float3" -1.3270383 0.083147146 0.35905695 ;
	setAttr ".tk[1526]" -type "float3" -1.3594933 0.083147146 6.1563512e-08 ;
	setAttr ".tk[1527]" -type "float3" -1.3270383 0.083147146 -0.35905686 ;
	setAttr ".tk[1528]" -type "float3" -1.2304721 0.083147146 -0.70927262 ;
	setAttr ".tk[1529]" -type "float3" -1.0721719 0.083147146 -1.0420238 ;
	setAttr ".tk[1530]" -type "float3" -0.85603738 0.083147146 -1.3491169 ;
	setAttr ".tk[1531]" -type "float3" -0.58738828 0.083147146 -1.6229905 ;
	setAttr ".tk[1532]" -type "float3" -0.27284122 0 -1.8569 ;
	setAttr ".tk[1533]" -type "float3" 0.079859406 0 -2.0450869 ;
	setAttr ".tk[1534]" -type "float3" 0.46202815 0.055431429 -2.1829169 ;
	setAttr ".tk[1535]" -type "float3" -0.41238055 0.055431429 -2.2669964 ;
	setAttr ".tk[1536]" -type "float3" 1.1076302e-06 0.055431429 -2.2952545 ;
	setAttr ".tk[1537]" -type "float3" 0.41238293 0.055431429 -2.2669964 ;
	setAttr ".tk[1538]" -type "float3" 0.81461048 0 -2.1829169 ;
	setAttr ".tk[1539]" -type "float3" 0.55846143 -0.073908575 -2.0450866 ;
	setAttr ".tk[1540]" -type "float3" 0.91116178 -0.073908575 -1.8568997 ;
	setAttr ".tk[1541]" -type "float3" 1.2257094 -0.073908575 -1.6229897 ;
	setAttr ".tk[1542]" -type "float3" 1.4943576 0 -1.3491163 ;
	setAttr ".tk[1543]" -type "float3" 1.7104924 0.075110868 -1.0420231 ;
	setAttr ".tk[1544]" -type "float3" 1.8687921 0.075110868 -0.70927191 ;
	setAttr ".tk[1545]" -type "float3" 1.9653585 0.13613848 -0.35905603 ;
	setAttr ".tk[1546]" -type "float3" 1.9978116 0.13613848 4.3778488e-07 ;
	setAttr ".tk[1547]" -type "float3" 1.9653566 0.13613848 0.35905725 ;
	setAttr ".tk[1548]" -type "float3" 1.8687904 0.075110868 0.70927298 ;
	setAttr ".tk[1549]" -type "float3" 1.7104907 0.075110868 1.0420241 ;
	setAttr ".tk[1550]" -type "float3" 1.4943557 0 1.3491169 ;
	setAttr ".tk[1551]" -type "float3" 1.2257074 -0.073908575 1.6229905 ;
	setAttr ".tk[1552]" -type "float3" 0.91116047 -0.073908575 1.8569006 ;
	setAttr ".tk[1553]" -type "float3" 0.55845988 -0.073908575 2.0450871 ;
	setAttr ".tk[1554]" -type "float3" 0.81460935 0 2.1829171 ;
	setAttr ".tk[1555]" -type "float3" 0.41238189 0.055431429 2.2669966 ;
	setAttr ".tk[1556]" -type "float3" 2.5156757e-07 0.055431429 2.2952545 ;
	setAttr ".tk[1557]" -type "float3" -0.41238144 0.055431429 2.2669966 ;
	setAttr ".tk[1558]" -type "float3" 0.46202761 0.055431429 2.1829171 ;
	setAttr ".tk[1559]" -type "float3" 0.079858571 0 2.0450869 ;
	setAttr ".tk[1562]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1563]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1564]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1565]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1566]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1567]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1568]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1569]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1570]" -type "float3" 0 0.13857858 0 ;
	setAttr ".tk[1581]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1582]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1583]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1584]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1585]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1586]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1587]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1588]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1589]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1590]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1591]" -type "float3" 0 0.064670004 0 ;
	setAttr ".tk[1600]" -type "float3" 0 0.12010143 0 ;
	setAttr ".tk[1601]" -type "float3" 0 0.12010143 0 ;
	setAttr ".tk[1602]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1603]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1604]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1605]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1606]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1607]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1608]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1609]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1610]" -type "float3" 0 0.21248715 0 ;
	setAttr ".tk[1611]" -type "float3" 0 0.12010143 0 ;
	setAttr ".tk[1612]" -type "float3" 0 0.12010143 0 ;
	setAttr ".tk[1613]" -type "float3" 0 0.12010143 0 ;
	setAttr ".tk[1619]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1620]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1621]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1622]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1623]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1624]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1625]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1626]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1627]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1628]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1629]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1630]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1631]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1632]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1633]" -type "float3" 0 -0.18859401 0 ;
	setAttr ".tk[1639]" -type "float3" 0 0.12010143 0 ;
	setAttr ".tk[1640]" -type "float3" -1.6062093 0.17274579 1.8707291 ;
	setAttr ".tk[1641]" -type "float3" -1.9322731 0.17274579 1.635077 ;
	setAttr ".tk[1642]" -type "float3" -2.2107575 0.17274579 1.3591642 ;
	setAttr ".tk[1643]" -type "float3" -2.4348059 -0.20310125 1.0497843 ;
	setAttr ".tk[1644]" -type "float3" -2.5989015 -0.20310125 0.71455503 ;
	setAttr ".tk[1645]" -type "float3" -2.6990035 -0.20310125 0.36173114 ;
	setAttr ".tk[1646]" -type "float3" -2.7326465 -0.20310125 1.713631e-07 ;
	setAttr ".tk[1647]" -type "float3" -2.6990035 -0.20310125 -0.36173081 ;
	setAttr ".tk[1648]" -type "float3" -2.5989015 -0.20310125 -0.71455473 ;
	setAttr ".tk[1649]" -type "float3" -2.4348059 -0.20310125 -1.0497842 ;
	setAttr ".tk[1650]" -type "float3" -2.2107575 0.17274579 -1.3591642 ;
	setAttr ".tk[1651]" -type "float3" -1.9322724 0.17274579 -1.635077 ;
	setAttr ".tk[1652]" -type "float3" -1.6062089 0.17274579 -1.8707291 ;
	setAttr ".tk[1653]" -type "float3" -1.2405951 0.17274579 -2.0603168 ;
	setAttr ".tk[1654]" -type "float3" -0.84443349 -0.20310125 -2.1991735 ;
	setAttr ".tk[1655]" -type "float3" -0.42747921 -0.20310125 -2.283879 ;
	setAttr ".tk[1656]" -type "float3" 1.0325463e-06 0 -2.3123481 ;
	setAttr ".tk[1657]" -type "float3" 0.42748138 -0.24020286 -2.283879 ;
	setAttr ".tk[1658]" -type "float3" 0.84443575 0 -2.1991735 ;
	setAttr ".tk[1659]" -type "float3" 1.2405972 -0.21476975 -2.0603168 ;
	setAttr ".tk[1660]" -type "float3" 1.6062112 -0.21476975 -1.8707284 ;
	setAttr ".tk[1661]" -type "float3" 1.9322747 -0.21476975 -1.6350766 ;
	setAttr ".tk[1662]" -type "float3" 2.2107596 -0.21476975 -1.3591634 ;
	setAttr ".tk[1663]" -type "float3" 2.434808 0 -1.0497831 ;
	setAttr ".tk[1664]" -type "float3" 2.5989032 -0.23096429 -0.71455401 ;
	setAttr ".tk[1665]" -type "float3" 2.6990051 -0.23096429 -0.36172992 ;
	setAttr ".tk[1666]" -type "float3" 2.7326465 -0.23096429 5.503864e-07 ;
	setAttr ".tk[1667]" -type "float3" 2.6990035 -0.23096429 0.36173135 ;
	setAttr ".tk[1668]" -type "float3" 2.5989015 -0.23096429 0.71455514 ;
	setAttr ".tk[1669]" -type "float3" 2.4348063 0 1.0497844 ;
	setAttr ".tk[1670]" -type "float3" 2.210758 -0.21476975 1.3591644 ;
	setAttr ".tk[1671]" -type "float3" 1.9322731 -0.21476975 1.6350775 ;
	setAttr ".tk[1672]" -type "float3" 1.6062096 -0.21476975 1.8707293 ;
	setAttr ".tk[1673]" -type "float3" 1.2405957 -0.21476975 2.0603175 ;
	setAttr ".tk[1674]" -type "float3" 0.84443444 0 2.1991742 ;
	setAttr ".tk[1675]" -type "float3" 0.42748031 -0.24020286 2.2838795 ;
	setAttr ".tk[1676]" -type "float3" 1.5021281e-07 0 2.3123481 ;
	setAttr ".tk[1677]" -type "float3" -0.42748004 -0.20310125 2.2838795 ;
	setAttr ".tk[1678]" -type "float3" -0.84443402 -0.20310125 2.1991742 ;
	setAttr ".tk[1679]" -type "float3" -1.2405955 0.17274579 2.0603175 ;
	setAttr ".tk[1720]" -type "float3" 0.39551651 0 -1.5793049 ;
	setAttr ".tk[1721]" -type "float3" 0.27841544 0 -1.4339792 ;
	setAttr ".tk[1722]" -type "float3" -0.98874497 0 -1.2533435 ;
	setAttr ".tk[1723]" -type "float3" -1.3499873 0 -1.0418468 ;
	setAttr ".tk[1724]" -type "float3" -1.6406164 0 -0.80469608 ;
	setAttr ".tk[1725]" -type "float3" -1.8534758 0 -0.54773128 ;
	setAttr ".tk[1726]" -type "float3" -1.9833245 0 -0.27727947 ;
	setAttr ".tk[1727]" -type "float3" -2.0269663 0 -4.7542006e-08 ;
	setAttr ".tk[1728]" -type "float3" -1.9833245 0 0.27727932 ;
	setAttr ".tk[1729]" -type "float3" -1.8534758 0 0.54773122 ;
	setAttr ".tk[1730]" -type "float3" -1.6406164 0 0.80469608 ;
	setAttr ".tk[1731]" -type "float3" -1.3499868 0 1.0418468 ;
	setAttr ".tk[1732]" -type "float3" -0.98874474 0 1.2533436 ;
	setAttr ".tk[1733]" -type "float3" 0.27841592 0 1.4339792 ;
	setAttr ".tk[1734]" -type "float3" 0.39551783 0 1.5793049 ;
	setAttr ".tk[1735]" -type "float3" 0.90940619 0 1.6857432 ;
	setAttr ".tk[1736]" -type "float3" 0.60606575 0 1.7506731 ;
	setAttr ".tk[1737]" -type "float3" 1.874904 0 1.7724953 ;
	setAttr ".tk[1738]" -type "float3" 1.2688401 0 1.7506731 ;
	setAttr ".tk[1739]" -type "float3" 1.0953766 0 1.6857432 ;
	setAttr ".tk[1740]" -type "float3" 0.66765642 0.097407818 1.5793049 ;
	setAttr ".tk[1741]" -type "float3" 1.1419199 0.097407818 1.4339786 ;
	setAttr ".tk[1742]" -type "float3" 1.8571033 0.097407818 1.2533432 ;
	setAttr ".tk[1743]" -type "float3" 2.8677306 0 1.0418463 ;
	setAttr ".tk[1744]" -type "float3" 3.1583602 0.035851896 0.80469561 ;
	setAttr ".tk[1745]" -type "float3" 3.3712189 0.035851896 0.54773062 ;
	setAttr ".tk[1746]" -type "float3" 3.5010681 0 0.27727866 ;
	setAttr ".tk[1747]" -type "float3" 3.5447071 0 -3.3807652e-07 ;
	setAttr ".tk[1748]" -type "float3" 3.5010657 0 -0.27727968 ;
	setAttr ".tk[1749]" -type "float3" 3.3712168 0.035851896 -0.5477314 ;
	setAttr ".tk[1750]" -type "float3" 3.1583576 0.035851896 -0.80469608 ;
	setAttr ".tk[1751]" -type "float3" 2.8677282 0 -1.0418468 ;
	setAttr ".tk[1752]" -type "float3" 1.857101 0.097407818 -1.2533437 ;
	setAttr ".tk[1753]" -type "float3" 1.1419179 0.097407818 -1.4339795 ;
	setAttr ".tk[1754]" -type "float3" 0.66765475 0.097407818 -1.5793051 ;
	setAttr ".tk[1755]" -type "float3" 1.0953752 0 -1.6857433 ;
	setAttr ".tk[1756]" -type "float3" 1.2688388 0 -1.7506731 ;
	setAttr ".tk[1757]" -type "float3" 1.874903 0 -1.7724953 ;
	setAttr ".tk[1758]" -type "float3" 0.60606444 0 -1.7506731 ;
	setAttr ".tk[1759]" -type "float3" 0.90940523 0 -1.6857433 ;
	setAttr ".tk[1760]" -type "float3" 0 0.097407818 0 ;
	setAttr ".tk[1761]" -type "float3" 0 0.097407818 0 ;
	setAttr ".tk[1762]" -type "float3" 0 0.097407818 0 ;
	setAttr ".tk[1763]" -type "float3" 0 0.097407818 0 ;
	setAttr ".tk[1764]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1765]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1766]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1767]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1768]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1769]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1770]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1771]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1772]" -type "float3" 0 -0.12175977 0 ;
	setAttr ".tk[1773]" -type "float3" 0 0.097407818 0 ;
	setAttr ".tk[1774]" -type "float3" 0 0.097407818 0 ;
	setAttr ".tk[1775]" -type "float3" 0 0.097407818 0 ;
	setAttr ".tk[1776]" -type "float3" 0 0.097407818 0 ;
	setAttr ".tk[1781]" -type "float3" 0 0.091550857 0 ;
	setAttr ".tk[1782]" -type "float3" 0 0.091550857 0 ;
	setAttr ".tk[1783]" -type "float3" 0 0.091550857 0 ;
	setAttr ".tk[1784]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1785]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1786]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1787]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1788]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1789]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1790]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1791]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1792]" -type "float3" 0 0.20344636 0 ;
	setAttr ".tk[1793]" -type "float3" 0 0.091550857 0 ;
	setAttr ".tk[1794]" -type "float3" 0 0.091550857 0 ;
	setAttr ".tk[1795]" -type "float3" 0 0.091550857 0 ;
	setAttr ".tk[1800]" -type "float3" 0.52386254 0 -1.6122838 ;
	setAttr ".tk[1801]" -type "float3" 0.76962972 0 -1.5104836 ;
	setAttr ".tk[1802]" -type "float3" 0.99644595 0 -1.3714904 ;
	setAttr ".tk[1803]" -type "float3" 1.1987263 0 -1.1987265 ;
	setAttr ".tk[1804]" -type "float3" 1.3714904 0 -0.99644613 ;
	setAttr ".tk[1805]" -type "float3" 1.5104835 0 -0.76962984 ;
	setAttr ".tk[1806]" -type "float3" 1.6122835 0 -0.52386272 ;
	setAttr ".tk[1807]" -type "float3" 1.6743838 0 -0.26519644 ;
	setAttr ".tk[1808]" -type "float3" 1.695255 0 -4.547028e-08 ;
	setAttr ".tk[1809]" -type "float3" 1.6743838 0 0.26519632 ;
	setAttr ".tk[1810]" -type "float3" 1.6122835 0 0.52386272 ;
	setAttr ".tk[1811]" -type "float3" 1.5104835 0 0.7696299 ;
	setAttr ".tk[1812]" -type "float3" 1.3714901 0 0.99644613 ;
	setAttr ".tk[1813]" -type "float3" 1.1987262 0 1.1987265 ;
	setAttr ".tk[1814]" -type "float3" 0.99644566 0 1.3714907 ;
	setAttr ".tk[1815]" -type "float3" 0.76962924 0 1.5104836 ;
	setAttr ".tk[1816]" -type "float3" 0.52386212 0 1.6122836 ;
	setAttr ".tk[1817]" -type "float3" 0.26519576 0 1.6743839 ;
	setAttr ".tk[1818]" -type "float3" -6.5875145e-07 0 1.6952553 ;
	setAttr ".tk[1819]" -type "float3" -0.2651971 0 1.6743839 ;
	setAttr ".tk[1820]" -type "float3" -0.52386349 0 1.6122836 ;
	setAttr ".tk[1821]" -type "float3" -0.76963073 0 1.5104836 ;
	setAttr ".tk[1822]" -type "float3" -0.99644697 0 1.3714901 ;
	setAttr ".tk[1823]" -type "float3" -1.1987275 0 1.1987262 ;
	setAttr ".tk[1824]" -type "float3" -1.3714913 0 0.99644566 ;
	setAttr ".tk[1825]" -type "float3" -1.5104845 0 0.76962924 ;
	setAttr ".tk[1826]" -type "float3" -1.6122845 0 0.52386212 ;
	setAttr ".tk[1827]" -type "float3" -1.6743847 0 0.26519573 ;
	setAttr ".tk[1828]" -type "float3" -1.695255 0 -3.2334415e-07 ;
	setAttr ".tk[1829]" -type "float3" -1.6743838 0 -0.26519665 ;
	setAttr ".tk[1830]" -type "float3" -1.6122835 0 -0.5238629 ;
	setAttr ".tk[1831]" -type "float3" -1.5104835 0 -0.76962996 ;
	setAttr ".tk[1832]" -type "float3" -1.3714904 0 -0.99644613 ;
	setAttr ".tk[1833]" -type "float3" -1.1987264 0 -1.1987268 ;
	setAttr ".tk[1834]" -type "float3" -0.99644601 0 -1.3714907 ;
	setAttr ".tk[1835]" -type "float3" -0.76962984 0 -1.5104837 ;
	setAttr ".tk[1836]" -type "float3" -0.52386278 0 -1.6122838 ;
	setAttr ".tk[1837]" -type "float3" -0.26519644 0 -1.6743841 ;
	setAttr ".tk[1838]" -type "float3" -1.0908307e-07 0 -1.6952553 ;
	setAttr ".tk[1839]" -type "float3" 0.26519626 0 -1.6743841 ;
	setAttr ".tk[1845]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1846]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1847]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1848]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1849]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1850]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1851]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1852]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1853]" -type "float3" -0.87534261 0 0 ;
	setAttr ".tk[1880]" -type "float3" -0.31800053 0.19351508 2.2916131 ;
	setAttr ".tk[1881]" -type "float3" -0.62817097 0.035779536 2.2066205 ;
	setAttr ".tk[1882]" -type "float3" -0.92287362 0.035779536 2.0672936 ;
	setAttr ".tk[1883]" -type "float3" -1.1948522 0.035779536 1.8770635 ;
	setAttr ".tk[1884]" -type "float3" -1.4374096 0.035779536 1.6406136 ;
	setAttr ".tk[1885]" -type "float3" -1.6445732 -0.19365396 1.3637666 ;
	setAttr ".tk[1886]" -type "float3" -1.8112416 -0.19365396 1.0533389 ;
	setAttr ".tk[1887]" -type "float3" -1.9333115 -0.19365396 0.71697444 ;
	setAttr ".tk[1888]" -type "float3" -2.007777 -0.19365396 0.36295581 ;
	setAttr ".tk[1889]" -type "float3" -2.032804 -0.19365396 6.2232004e-08 ;
	setAttr ".tk[1890]" -type "float3" -2.007777 -0.19365396 -0.36295578 ;
	setAttr ".tk[1891]" -type "float3" -1.9333115 -0.19365396 -0.71697444 ;
	setAttr ".tk[1892]" -type "float3" -1.8112416 -0.19365396 -1.0533389 ;
	setAttr ".tk[1893]" -type "float3" -1.6445731 -0.19365396 -1.3637666 ;
	setAttr ".tk[1894]" -type "float3" -1.4374094 0.035779536 -1.6406138 ;
	setAttr ".tk[1895]" -type "float3" -1.1948519 0.035779536 -1.8770636 ;
	setAttr ".tk[1896]" -type "float3" -0.9228732 0.035779536 -2.0672936 ;
	setAttr ".tk[1897]" -type "float3" -0.62817037 0.035779536 -2.2066202 ;
	setAttr ".tk[1898]" -type "float3" -0.31799993 0.19351508 -2.2916126 ;
	setAttr ".tk[1899]" -type "float3" 7.7455394e-07 0.19351508 -2.3201778 ;
	setAttr ".tk[1900]" -type "float3" 0.31800151 0.19351508 -2.2916126 ;
	setAttr ".tk[1901]" -type "float3" 0.62817204 0.19351508 -2.2066202 ;
	setAttr ".tk[1902]" -type "float3" 0.92287487 0 -2.0672934 ;
	setAttr ".tk[1903]" -type "float3" 1.1948534 0 -1.877063 ;
	setAttr ".tk[1904]" -type "float3" 1.4374111 0 -1.6406131 ;
	setAttr ".tk[1905]" -type "float3" 1.6445742 0 -1.3637657 ;
	setAttr ".tk[1906]" -type "float3" 1.8112429 0 -1.0533381 ;
	setAttr ".tk[1907]" -type "float3" 1.9333129 0 -0.71697366 ;
	setAttr ".tk[1908]" -type "float3" 2.0077782 0 -0.36295488 ;
	setAttr ".tk[1909]" -type "float3" 2.032804 0 4.4253864e-07 ;
	setAttr ".tk[1910]" -type "float3" 2.0077767 0 0.36295608 ;
	setAttr ".tk[1911]" -type "float3" 1.9333116 0 0.71697468 ;
	setAttr ".tk[1912]" -type "float3" 1.8112419 0 1.0533389 ;
	setAttr ".tk[1913]" -type "float3" 1.6445732 0 1.3637666 ;
	setAttr ".tk[1914]" -type "float3" 1.4374096 0 1.6406138 ;
	setAttr ".tk[1915]" -type "float3" 1.1948524 0 1.8770638 ;
	setAttr ".tk[1916]" -type "float3" 0.92287385 0 2.0672939 ;
	setAttr ".tk[1917]" -type "float3" 0.62817115 0.19351508 2.2066205 ;
	setAttr ".tk[1918]" -type "float3" 0.31800073 0.19351508 2.2916131 ;
	setAttr ".tk[1919]" -type "float3" 1.505891e-07 0.19351508 2.3201778 ;
	setAttr ".tk[1920]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1921]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1922]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1923]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1924]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1925]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1926]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1927]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1928]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1929]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1930]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1931]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1932]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1933]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1934]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1935]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1936]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1937]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1938]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1939]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1940]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1941]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1959]" -type "float3" 0 -0.21962145 0 ;
	setAttr ".tk[1960]" -type "float3" 2.0604907e-07 0 3.1216321 ;
	setAttr ".tk[1961]" -type "float3" -0.51936024 0 3.0831997 ;
	setAttr ".tk[1962]" -type "float3" -1.0259324 0 2.9688485 ;
	setAttr ".tk[1963]" -type "float3" -1.5072422 0 2.7813942 ;
	setAttr ".tk[1964]" -type "float3" -1.9514397 0 2.5254533 ;
	setAttr ".tk[1965]" -type "float3" -2.347585 0 2.2073271 ;
	setAttr ".tk[1966]" -type "float3" -2.6859264 0 1.8348492 ;
	setAttr ".tk[1967]" -type "float3" -2.9581296 0 1.4171911 ;
	setAttr ".tk[1968]" -type "float3" -3.1574945 0 0.96463734 ;
	setAttr ".tk[1969]" -type "float3" -3.2791128 0 0.4883309 ;
	setAttr ".tk[1970]" -type "float3" -3.3199871 0 8.3728629e-08 ;
	setAttr ".tk[1971]" -type "float3" -3.2791128 0 -0.48833084 ;
	setAttr ".tk[1972]" -type "float3" -3.1574945 0 -0.96463734 ;
	setAttr ".tk[1973]" -type "float3" -2.9581296 0 -1.4171911 ;
	setAttr ".tk[1974]" -type "float3" -2.685926 0 -1.8348495 ;
	setAttr ".tk[1975]" -type "float3" -2.3475845 0 -2.2073271 ;
	setAttr ".tk[1976]" -type "float3" -1.9514387 0 -2.5254538 ;
	setAttr ".tk[1977]" -type "float3" -1.5072416 0 -2.7813942 ;
	setAttr ".tk[1978]" -type "float3" -1.0259314 0 -2.9688482 ;
	setAttr ".tk[1979]" -type "float3" -0.51935923 0 -3.0831993 ;
	setAttr ".tk[1980]" -type "float3" 1.3154424e-06 0 -3.1216321 ;
	setAttr ".tk[1981]" -type "float3" 0.51936185 0 -3.0831993 ;
	setAttr ".tk[1982]" -type "float3" 1.0259342 -0.23871896 -2.9688482 ;
	setAttr ".tk[1983]" -type "float3" 1.5072448 -0.23871896 -2.7813942 ;
	setAttr ".tk[1984]" -type "float3" 1.9514413 -0.23871896 -2.5254533 ;
	setAttr ".tk[1985]" -type "float3" 2.3475878 -0.23871896 -2.2073267 ;
	setAttr ".tk[1986]" -type "float3" 2.6859279 -0.23871896 -1.8348483 ;
	setAttr ".tk[1987]" -type "float3" 2.9581325 -0.23871896 -1.4171904 ;
	setAttr ".tk[1988]" -type "float3" 3.1574969 -0.23871896 -0.96463621 ;
	setAttr ".tk[1989]" -type "float3" 3.2791147 -0.23871896 -0.4883295 ;
	setAttr ".tk[1990]" -type "float3" 3.3199871 -0.23871896 5.9540389e-07 ;
	setAttr ".tk[1991]" -type "float3" 3.2791128 -0.23871896 0.48833126 ;
	setAttr ".tk[1992]" -type "float3" 3.1574948 -0.23871896 0.96463776 ;
	setAttr ".tk[1993]" -type "float3" 2.9581296 -0.23871896 1.4171911 ;
	setAttr ".tk[1994]" -type "float3" 2.6859262 -0.23871896 1.8348495 ;
	setAttr ".tk[1995]" -type "float3" 2.3475857 -0.23871896 2.2073274 ;
	setAttr ".tk[1996]" -type "float3" 1.9514399 -0.23871896 2.525454 ;
	setAttr ".tk[1997]" -type "float3" 1.5072427 -0.23871896 2.7813945 ;
	setAttr ".tk[1998]" -type "float3" 1.0259327 -0.23871896 2.9688485 ;
	setAttr ".tk[1999]" -type "float3" 0.51936066 0 3.0831995 ;
	setAttr ".tk[2000]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2001]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2002]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2003]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2004]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2005]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2006]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2010]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2011]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2012]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2013]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2014]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2015]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2016]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2022]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2023]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2024]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2025]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2026]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2027]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2028]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2032]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2033]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2034]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2035]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2036]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2037]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2038]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2039]" -type "float3" 0 -0.12413389 0 ;
	setAttr ".tk[2040]" -type "float3" -1.2664703 -0.12413389 -0.15609632 ;
	setAttr ".tk[2041]" -type "float3" -1.219499 -0.12413389 -0.30834886 ;
	setAttr ".tk[2042]" -type "float3" -1.1424994 -0.12413389 -0.45300883 ;
	setAttr ".tk[2043]" -type "float3" -1.0373679 -0.12413389 -0.58651423 ;
	setAttr ".tk[2044]" -type "float3" -0.90669268 -0.12413389 -0.70557785 ;
	setAttr ".tk[2045]" -type "float3" -0.75369191 -0.12413389 -0.80726773 ;
	setAttr ".tk[2046]" -type "float3" -0.5821327 -0.12413389 -0.88907999 ;
	setAttr ".tk[2047]" -type "float3" -0.39623937 0 -0.94900012 ;
	setAttr ".tk[2048]" -type "float3" -0.20058936 0 -0.98555279 ;
	setAttr ".tk[2049]" -type "float3" -1.4502103e-07 0 -0.99783766 ;
	setAttr ".tk[2050]" -type "float3" 0.20058909 -0.2100727 -0.98555279 ;
	setAttr ".tk[2051]" -type "float3" 0.39623907 -0.2100727 -0.94900012 ;
	setAttr ".tk[2052]" -type "float3" 0.5821324 -0.2100727 -0.88907987 ;
	setAttr ".tk[2053]" -type "float3" 0.75369173 -0.2100727 -0.80726761 ;
	setAttr ".tk[2054]" -type "float3" 0.90669262 -0.2100727 -0.70557773 ;
	setAttr ".tk[2055]" -type "float3" 1.0373677 -0.2100727 -0.58651423 ;
	setAttr ".tk[2056]" -type "float3" 1.1424993 -0.2100727 -0.45300883 ;
	setAttr ".tk[2057]" -type "float3" 1.2194989 -0.2100727 -0.30834877 ;
	setAttr ".tk[2058]" -type "float3" 1.2664703 -0.2100727 -0.15609619 ;
	setAttr ".tk[2059]" -type "float3" 1.2822572 -0.2100727 2.601187e-08 ;
	setAttr ".tk[2060]" -type "float3" 1.2664703 -0.2100727 0.15609626 ;
	setAttr ".tk[2061]" -type "float3" 1.2194989 -0.2100727 0.30834883 ;
	setAttr ".tk[2062]" -type "float3" 1.1424993 -0.2100727 0.45300886 ;
	setAttr ".tk[2063]" -type "float3" 1.0373676 -0.2100727 0.58651441 ;
	setAttr ".tk[2064]" -type "float3" 0.90669245 -0.2100727 0.70557797 ;
	setAttr ".tk[2065]" -type "float3" 0.75369132 -0.2100727 0.80726773 ;
	setAttr ".tk[2066]" -type "float3" 0.5821321 -0.2100727 0.88907999 ;
	setAttr ".tk[2067]" -type "float3" 0.39623874 -0.2100727 0.94900012 ;
	setAttr ".tk[2068]" -type "float3" 0.20058867 -0.2100727 0.98555273 ;
	setAttr ".tk[2069]" -type "float3" -5.6744227e-07 0 0.99783766 ;
	setAttr ".tk[2070]" -type "float3" -0.20058987 0 0.98555273 ;
	setAttr ".tk[2071]" -type "float3" -0.39623997 0 0.94900012 ;
	setAttr ".tk[2072]" -type "float3" -0.58213341 -0.12413389 0.88907987 ;
	setAttr ".tk[2073]" -type "float3" -0.75369275 -0.12413389 0.80726761 ;
	setAttr ".tk[2074]" -type "float3" -0.90669364 -0.12413389 0.70557773 ;
	setAttr ".tk[2075]" -type "float3" -1.0373688 -0.12413389 0.58651412 ;
	setAttr ".tk[2076]" -type "float3" -1.1425004 -0.12413389 0.45300856 ;
	setAttr ".tk[2077]" -type "float3" -1.2194998 -0.12413389 0.30834854 ;
	setAttr ".tk[2078]" -type "float3" -1.2664711 -0.12413389 0.15609586 ;
	setAttr ".tk[2079]" -type "float3" -1.2822572 -0.12413389 -1.3754644e-07 ;
	setAttr ".tk[2080]" -type "float3" 2.2489004 -0.46788904 0 ;
	setAttr ".tk[2081]" -type "float3" 2.1069043 -0.46788904 0 ;
	setAttr ".tk[2082]" -type "float3" 1.9130288 -0.46788904 0 ;
	setAttr ".tk[2083]" -type "float3" 1.6720486 -0.38195026 0 ;
	setAttr ".tk[2084]" -type "float3" 1.3898968 -0.30556023 0 ;
	setAttr ".tk[2085]" -type "float3" 1.0735214 -0.2100727 0 ;
	setAttr ".tk[2086]" -type "float3" 0.73071223 -0.2100727 0 ;
	setAttr ".tk[2087]" -type "float3" 0.36991057 0 0 ;
	setAttr ".tk[2088]" -type "float3" 3.4050493e-07 0 0 ;
	setAttr ".tk[2089]" -type "float3" -0.36990985 0.057292566 0 ;
	setAttr ".tk[2090]" -type "float3" -0.73071146 0.12413389 0 ;
	setAttr ".tk[2091]" -type "float3" -1.0735209 0.12413389 0 ;
	setAttr ".tk[2092]" -type "float3" -1.3898965 0.047743805 0 ;
	setAttr ".tk[2093]" -type "float3" -1.6720479 0.17187777 0 ;
	setAttr ".tk[2094]" -type "float3" -1.9130285 0.17187777 0 ;
	setAttr ".tk[2095]" -type "float3" -1.648563 0.17187777 0 ;
	setAttr ".tk[2096]" -type "float3" -1.7905596 0.17187777 0 ;
	setAttr ".tk[2097]" -type "float3" -1.8771795 0.17187777 0 ;
	setAttr ".tk[2098]" -type "float3" -1.9062928 0.17187777 0 ;
	setAttr ".tk[2099]" -type "float3" -1.8771795 0.17187777 0 ;
	setAttr ".tk[2100]" -type "float3" -1.7905596 0.17187777 0 ;
	setAttr ".tk[2101]" -type "float3" -1.648563 0.17187777 0 ;
	setAttr ".tk[2102]" -type "float3" -1.9130284 0.17187777 0 ;
	setAttr ".tk[2103]" -type "float3" -1.6720474 0.17187777 0 ;
	setAttr ".tk[2104]" -type "float3" -1.3898959 0.047743805 0 ;
	setAttr ".tk[2105]" -type "float3" -1.0735201 0.12413389 0 ;
	setAttr ".tk[2106]" -type "float3" -0.7307111 0.12413389 0 ;
	setAttr ".tk[2107]" -type "float3" -0.36990908 0.057292566 0 ;
	setAttr ".tk[2108]" -type "float3" 1.0976165e-06 0 0 ;
	setAttr ".tk[2109]" -type "float3" 0.36991146 0 0 ;
	setAttr ".tk[2110]" -type "float3" 0.73071325 -0.2100727 0 ;
	setAttr ".tk[2111]" -type "float3" 1.0735228 -0.2100727 0 ;
	setAttr ".tk[2112]" -type "float3" 1.3898984 -0.30556023 0 ;
	setAttr ".tk[2113]" -type "float3" 1.6720502 -0.38195026 0 ;
	setAttr ".tk[2114]" -type "float3" 1.91303 -0.46788904 0 ;
	setAttr ".tk[2115]" -type "float3" 2.1069057 -0.46788904 0 ;
	setAttr ".tk[2116]" -type "float3" 2.2489014 -0.46788904 0 ;
	setAttr ".tk[2117]" -type "float3" 2.3355227 -0.46788904 0 ;
	setAttr ".tk[2118]" -type "float3" 2.3646338 -0.46788904 0 ;
	setAttr ".tk[2119]" -type "float3" 2.3355205 -0.46788904 0 ;
	setAttr ".tk[2120]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[2121]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[2122]" -type "float3" 0 -0.30556023 0 ;
	setAttr ".tk[2123]" -type "float3" 0 -0.30556023 0 ;
	setAttr ".tk[2124]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2125]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2126]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2129]" -type "float3" 0 0.057292566 0 ;
	setAttr ".tk[2130]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[2131]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[2132]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[2133]" -type "float3" 0 0.047743805 0 ;
	setAttr ".tk[2134]" -type "float3" 0 0.047743805 0 ;
	setAttr ".tk[2135]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[2136]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[2137]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[2138]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[2139]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[2140]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[2141]" -type "float3" 0 0.17187777 0 ;
	setAttr ".tk[2142]" -type "float3" 0 0.047743805 0 ;
	setAttr ".tk[2143]" -type "float3" 0 0.047743805 0 ;
	setAttr ".tk[2144]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[2145]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[2146]" -type "float3" 0 0.12413389 0 ;
	setAttr ".tk[2147]" -type "float3" 0 0.057292566 0 ;
	setAttr ".tk[2150]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2151]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2152]" -type "float3" 0 -0.2100727 0 ;
	setAttr ".tk[2153]" -type "float3" 0 -0.30556023 0 ;
	setAttr ".tk[2154]" -type "float3" 0 -0.30556023 0 ;
	setAttr ".tk[2155]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[2156]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[2157]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[2158]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[2159]" -type "float3" 0 -0.38195026 0 ;
	setAttr ".tk[2200]" -type "float3" 0.82366878 -0.43047234 -0.48465627 ;
	setAttr ".tk[2201]" -type "float3" 0.68467796 -0.43047234 -0.55450624 ;
	setAttr ".tk[2202]" -type "float3" 0.52882797 -0.43047234 -0.6107024 ;
	setAttr ".tk[2203]" -type "float3" 0.35995656 0 -0.65186119 ;
	setAttr ".tk[2204]" -type "float3" 0.18222177 -0.23915133 -0.67696887 ;
	setAttr ".tk[2205]" -type "float3" 7.2925765e-08 -0.23915133 -0.68540728 ;
	setAttr ".tk[2206]" -type "float3" -0.18222164 -0.23915133 -0.67696887 ;
	setAttr ".tk[2207]" -type "float3" -0.35995635 0 -0.65186119 ;
	setAttr ".tk[2208]" -type "float3" -0.52882785 0.22719379 -0.6107024 ;
	setAttr ".tk[2209]" -type "float3" -0.6846779 0.22719379 -0.55450612 ;
	setAttr ".tk[2210]" -type "float3" -0.82366878 0.22719379 -0.48465616 ;
	setAttr ".tk[2211]" -type "float3" -0.9423781 0.22719379 -0.40287232 ;
	setAttr ".tk[2212]" -type "float3" -1.037883 0.22719379 -0.31116843 ;
	setAttr ".tk[2213]" -type "float3" -1.1078321 0.22719379 -0.21180251 ;
	setAttr ".tk[2214]" -type "float3" -1.1505024 0.22719379 -0.10722134 ;
	setAttr ".tk[2215]" -type "float3" -1.1648433 0.22719379 -1.838405e-08 ;
	setAttr ".tk[2216]" -type "float3" -1.1505024 0.22719379 0.10722133 ;
	setAttr ".tk[2217]" -type "float3" -1.1078321 0.22719379 0.21180251 ;
	setAttr ".tk[2218]" -type "float3" -1.037883 0.22719379 0.31116843 ;
	setAttr ".tk[2219]" -type "float3" -0.94237804 0.22719379 0.40287238 ;
	setAttr ".tk[2220]" -type "float3" -0.82366848 0.22719379 0.48465621 ;
	setAttr ".tk[2221]" -type "float3" -0.68467742 0.22719379 0.55450618 ;
	setAttr ".tk[2222]" -type "float3" -0.52882755 0.22719379 0.6107024 ;
	setAttr ".tk[2223]" -type "float3" -0.35995606 0 0.65186113 ;
	setAttr ".tk[2224]" -type "float3" -0.18222125 -0.23915133 0.67696881 ;
	setAttr ".tk[2225]" -type "float3" 4.4782487e-07 -0.23915133 0.68540728 ;
	setAttr ".tk[2226]" -type "float3" 0.18222219 -0.23915133 0.67696881 ;
	setAttr ".tk[2227]" -type "float3" 0.35995701 0 0.65186113 ;
	setAttr ".tk[2228]" -type "float3" 0.52882856 -0.43047234 0.61070228 ;
	setAttr ".tk[2229]" -type "float3" 0.68467855 -0.43047234 0.55450612 ;
	setAttr ".tk[2230]" -type "float3" 0.82366949 -0.43047234 0.4846561 ;
	setAttr ".tk[2231]" -type "float3" 0.94237882 -0.43047234 0.4028722 ;
	setAttr ".tk[2232]" -type "float3" 1.0378839 -0.43047234 0.31116819 ;
	setAttr ".tk[2233]" -type "float3" 1.1078329 -0.43047234 0.21180227 ;
	setAttr ".tk[2234]" -type "float3" 1.150503 -0.43047234 0.10722107 ;
	setAttr ".tk[2235]" -type "float3" 1.1648433 -0.43047234 -1.3073102e-07 ;
	setAttr ".tk[2236]" -type "float3" 1.1505022 -0.43047234 -0.10722142 ;
	setAttr ".tk[2237]" -type "float3" 1.1078321 -0.43047234 -0.21180259 ;
	setAttr ".tk[2238]" -type "float3" 1.0378833 -0.43047234 -0.31116846 ;
	setAttr ".tk[2239]" -type "float3" 0.94237828 -0.43047234 -0.40287238 ;
	setAttr ".tk[2240]" -type "float3" 0.82423556 -0.25110888 -0.47398818 ;
	setAttr ".tk[2241]" -type "float3" 0.68853635 -0.25110888 -0.54230064 ;
	setAttr ".tk[2242]" -type "float3" 0.53637743 -0.25110888 -0.59725982 ;
	setAttr ".tk[2243]" -type "float3" 0.37150532 0 -0.63751256 ;
	setAttr ".tk[2244]" -type "float3" 0.19797981 0.14349078 -0.66206771 ;
	setAttr ".tk[2245]" -type "float3" 0.020073676 0.14349078 -0.67032039 ;
	setAttr ".tk[2246]" -type "float3" -0.15783249 0.14349078 -0.66206771 ;
	setAttr ".tk[2247]" -type "float3" -0.33135799 0.095660515 -0.63751256 ;
	setAttr ".tk[2248]" -type "float3" -0.4962301 0.095660515 -0.59725982 ;
	setAttr ".tk[2249]" -type "float3" -0.64838892 0.095660515 -0.54230052 ;
	setAttr ".tk[2250]" -type "float3" -0.78408831 0.095660515 -0.47398806 ;
	setAttr ".tk[2251]" -type "float3" -0.89998645 0.095660515 -0.39400446 ;
	setAttr ".tk[2252]" -type "float3" -0.99322927 0.095660515 -0.30431908 ;
	setAttr ".tk[2253]" -type "float3" -1.0615216 0.095660515 -0.20714042 ;
	setAttr ".tk[2254]" -type "float3" -1.1031816 0.095660515 -0.10486121 ;
	setAttr ".tk[2255]" -type "float3" -1.1171829 0.095660515 -1.7979389e-08 ;
	setAttr ".tk[2256]" -type "float3" -1.1031816 0.095660515 0.10486121 ;
	setAttr ".tk[2257]" -type "float3" -1.0615216 0.095660515 0.20714039 ;
	setAttr ".tk[2258]" -type "float3" -0.99322927 0.095660515 0.30431908 ;
	setAttr ".tk[2259]" -type "float3" -0.89998609 0.095660515 0.39400449 ;
	setAttr ".tk[2260]" -type "float3" -0.78408796 0.095660515 0.47398818 ;
	setAttr ".tk[2261]" -type "float3" -0.6483888 0.095660515 0.54230058 ;
	setAttr ".tk[2262]" -type "float3" -0.4962298 0.095660515 0.59725982 ;
	setAttr ".tk[2263]" -type "float3" -0.33135769 0.095660515 0.63751251 ;
	setAttr ".tk[2264]" -type "float3" -0.1578321 0.14349078 0.66206759 ;
	setAttr ".tk[2265]" -type "float3" 0.020074045 0.14349078 0.67032039 ;
	setAttr ".tk[2266]" -type "float3" 0.19798024 0.14349078 0.66206759 ;
	setAttr ".tk[2267]" -type "float3" 0.37150583 0 0.63751251 ;
	setAttr ".tk[2268]" -type "float3" 0.53637803 -0.25110888 0.5972597 ;
	setAttr ".tk[2269]" -type "float3" 0.68853688 -0.25110888 0.54230052 ;
	setAttr ".tk[2270]" -type "float3" 0.82423621 -0.25110888 0.47398794 ;
	setAttr ".tk[2271]" -type "float3" 0.94013405 -0.25110888 0.39400432 ;
	setAttr ".tk[2272]" -type "float3" 1.0333775 -0.25110888 0.30431888 ;
	setAttr ".tk[2273]" -type "float3" 1.1016697 -0.25110888 0.20714015 ;
	setAttr ".tk[2274]" -type "float3" 1.1433293 -0.25110888 0.10486095 ;
	setAttr ".tk[2275]" -type "float3" 1.1573302 -0.25110888 -1.2785341e-07 ;
	setAttr ".tk[2276]" -type "float3" 1.1433287 -0.25110888 -0.1048613 ;
	setAttr ".tk[2277]" -type "float3" 1.101669 -0.25110888 -0.20714048 ;
	setAttr ".tk[2278]" -type "float3" 1.0333766 -0.25110888 -0.30431917 ;
	setAttr ".tk[2279]" -type "float3" 0.94013381 -0.25110888 -0.39400449 ;
	setAttr ".tk[2280]" -type "float3" -1.4616272 0.086879566 0 ;
	setAttr ".tk[2281]" -type "float3" -1.214983 0.086879566 0 ;
	setAttr ".tk[2282]" -type "float3" -0.93842232 0 0 ;
	setAttr ".tk[2283]" -type "float3" -0.63875455 0 0 ;
	setAttr ".tk[2284]" -type "float3" -0.32335851 0.10859945 0 ;
	setAttr ".tk[2285]" -type "float3" -1.8465698e-07 0.10859945 0 ;
	setAttr ".tk[2286]" -type "float3" 0.32335806 0.10859945 0 ;
	setAttr ".tk[2287]" -type "float3" 0.63875419 0.10859945 0 ;
	setAttr ".tk[2288]" -type "float3" 0.93842226 0.10859945 0 ;
	setAttr ".tk[2289]" -type "float3" 1.2149829 -0.1194594 0 ;
	setAttr ".tk[2290]" -type "float3" 1.4616265 -0.1194594 0 ;
	setAttr ".tk[2291]" -type "float3" 1.6722802 -0.1194594 0 ;
	setAttr ".tk[2292]" -type "float3" 1.8417571 -0.1194594 0 ;
	setAttr ".tk[2293]" -type "float3" 1.9658834 -0.1194594 0 ;
	setAttr ".tk[2294]" -type "float3" 2.0416033 -0.1194594 0 ;
	setAttr ".tk[2295]" -type "float3" 2.0670521 -0.1194594 0 ;
	setAttr ".tk[2296]" -type "float3" 2.0416033 -0.1194594 0 ;
	setAttr ".tk[2297]" -type "float3" 1.9658834 -0.1194594 0 ;
	setAttr ".tk[2298]" -type "float3" 1.8417571 -0.1194594 0 ;
	setAttr ".tk[2299]" -type "float3" 1.6722802 -0.1194594 0 ;
	setAttr ".tk[2300]" -type "float3" 1.4616262 -0.1194594 0 ;
	setAttr ".tk[2301]" -type "float3" 1.2149825 -0.1194594 0 ;
	setAttr ".tk[2302]" -type "float3" 0.93842149 0.10859945 0 ;
	setAttr ".tk[2303]" -type "float3" 0.63875371 0.10859945 0 ;
	setAttr ".tk[2304]" -type "float3" 0.32335746 0.10859945 0 ;
	setAttr ".tk[2305]" -type "float3" -8.6425507e-07 0.10859945 0 ;
	setAttr ".tk[2306]" -type "float3" -0.32335922 0.10859945 0 ;
	setAttr ".tk[2307]" -type "float3" -0.63875544 0 0 ;
	setAttr ".tk[2308]" -type "float3" -0.93842345 0 0 ;
	setAttr ".tk[2309]" -type "float3" -1.2149845 0.086879566 0 ;
	setAttr ".tk[2310]" -type "float3" -1.4616282 0.086879566 0 ;
	setAttr ".tk[2311]" -type "float3" -1.6722821 0.086879566 0 ;
	setAttr ".tk[2312]" -type "float3" -1.841759 0.086879566 0 ;
	setAttr ".tk[2313]" -type "float3" -1.9658854 0.086879566 0 ;
	setAttr ".tk[2314]" -type "float3" -2.041605 0.086879566 0 ;
	setAttr ".tk[2315]" -type "float3" -2.0670521 0.086879566 0 ;
	setAttr ".tk[2316]" -type "float3" -2.0416033 0.086879566 0 ;
	setAttr ".tk[2317]" -type "float3" -1.9658834 0.086879566 0 ;
	setAttr ".tk[2318]" -type "float3" -1.8417572 0.086879566 0 ;
	setAttr ".tk[2319]" -type "float3" -1.6722806 0.086879566 0 ;
	setAttr ".tk[2320]" -type "float3" 2.0425656 -0.17375915 0.38873199 ;
	setAttr ".tk[2321]" -type "float3" 1.6978912 -0.17375915 0.44475722 ;
	setAttr ".tk[2322]" -type "float3" 1.311408 -0.17375915 0.489831 ;
	setAttr ".tk[2323]" -type "float3" 0.89263433 0 0.52284342 ;
	setAttr ".tk[2324]" -type "float3" 0.45188043 0 0.5429818 ;
	setAttr ".tk[2325]" -type "float3" 9.4978411e-08 -0.33665833 0.54975003 ;
	setAttr ".tk[2326]" -type "float3" -0.45188031 -0.33665833 0.5429818 ;
	setAttr ".tk[2327]" -type "float3" -0.89263415 0 0.52284342 ;
	setAttr ".tk[2328]" -type "float3" -1.311408 0 0.48983088 ;
	setAttr ".tk[2329]" -type "float3" -1.697891 -0.1628992 0.4447571 ;
	setAttr ".tk[2330]" -type "float3" -2.0425656 -0.1628992 0.38873196 ;
	setAttr ".tk[2331]" -type "float3" -2.3369465 -0.1628992 0.32313499 ;
	setAttr ".tk[2332]" -type "float3" -2.5737829 -0.1628992 0.24958131 ;
	setAttr ".tk[2333]" -type "float3" -2.7472441 -0.1628992 0.16988213 ;
	setAttr ".tk[2334]" -type "float3" -2.8530598 -0.1628992 0.085999861 ;
	setAttr ".tk[2335]" -type "float3" -2.8886247 -0.1628992 1.4745442e-08 ;
	setAttr ".tk[2336]" -type "float3" -2.8530598 -0.1628992 -0.085999854 ;
	setAttr ".tk[2337]" -type "float3" -2.7472441 -0.1628992 -0.16988212 ;
	setAttr ".tk[2338]" -type "float3" -2.5737829 -0.1628992 -0.24958131 ;
	setAttr ".tk[2339]" -type "float3" -2.3369465 -0.1628992 -0.32313502 ;
	setAttr ".tk[2340]" -type "float3" -2.0425653 -0.1628992 -0.38873199 ;
	setAttr ".tk[2341]" -type "float3" -1.6978896 -0.1628992 -0.44475716 ;
	setAttr ".tk[2342]" -type "float3" -1.3114073 0 -0.48983088 ;
	setAttr ".tk[2343]" -type "float3" -0.89263314 0 -0.52284342 ;
	setAttr ".tk[2344]" -type "float3" -0.45187953 -0.33665833 -0.54298174 ;
	setAttr ".tk[2345]" -type "float3" 1.0442642e-06 -0.33665833 -0.54975003 ;
	setAttr ".tk[2346]" -type "float3" 0.45188165 0 -0.54298174 ;
	setAttr ".tk[2347]" -type "float3" 0.89263558 0 -0.52284342 ;
	setAttr ".tk[2348]" -type "float3" 1.31141 -0.17375915 -0.48983085 ;
	setAttr ".tk[2349]" -type "float3" 1.6978922 -0.17375915 -0.44475704 ;
	setAttr ".tk[2350]" -type "float3" 2.0425677 -0.17375915 -0.3887319 ;
	setAttr ".tk[2351]" -type "float3" 2.3369477 -0.17375915 -0.32313484 ;
	setAttr ".tk[2352]" -type "float3" 2.5737848 -0.17375915 -0.24958111 ;
	setAttr ".tk[2353]" -type "float3" 2.7472475 -0.17375915 -0.16988192 ;
	setAttr ".tk[2354]" -type "float3" 2.8530612 -0.17375915 -0.085999645 ;
	setAttr ".tk[2355]" -type "float3" 2.8886247 -0.17375915 1.0485645e-07 ;
	setAttr ".tk[2356]" -type "float3" 2.8530598 -0.17375915 0.085999936 ;
	setAttr ".tk[2357]" -type "float3" 2.7472441 -0.17375915 0.16988218 ;
	setAttr ".tk[2358]" -type "float3" 2.5737829 -0.17375915 0.24958132 ;
	setAttr ".tk[2359]" -type "float3" 2.3369465 -0.17375915 0.32313502 ;
	setAttr ".tk[2360]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2361]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2362]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2363]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2364]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2365]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2366]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2367]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2368]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2369]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2370]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2371]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2372]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2373]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2375]" -type "float3" 0 0.10859945 0 ;
	setAttr ".tk[2376]" -type "float3" 0 0.10859945 0 ;
	setAttr ".tk[2377]" -type "float3" 0 0.10859945 0 ;
	setAttr ".tk[2378]" -type "float3" 0 0.10859945 0 ;
	setAttr ".tk[2379]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2380]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2381]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2382]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2383]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2384]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2385]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2386]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2387]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2388]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2389]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2390]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2391]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2392]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2393]" -type "float3" 0 -0.1194594 0 ;
	setAttr ".tk[2394]" -type "float3" 0 0.10859945 0 ;
	setAttr ".tk[2395]" -type "float3" 0 0.10859945 0 ;
	setAttr ".tk[2396]" -type "float3" 0 0.10859945 0 ;
	setAttr ".tk[2397]" -type "float3" 0 0.10859945 0 ;
	setAttr ".tk[2399]" -type "float3" 0 0.086879566 0 ;
	setAttr ".tk[2400]" -type "float3" 1.1729327 0.035596371 0.50045884 ;
	setAttr ".tk[2401]" -type "float3" 0.60205269 0 0.60205299 ;
	setAttr ".tk[2402]" -type "float3" 0.50045842 0 0.68882239 ;
	setAttr ".tk[2403]" -type "float3" 0.38654137 0 0.75863087 ;
	setAttr ".tk[2404]" -type "float3" 0.37701482 -0.14950474 0.80975938 ;
	setAttr ".tk[2405]" -type "float3" 0.24710125 -0.14950474 0.84094876 ;
	setAttr ".tk[2406]" -type "float3" 0.11390802 -0.14950474 0.85143137 ;
	setAttr ".tk[2407]" -type "float3" -0.1331936 0 0.84094876 ;
	setAttr ".tk[2408]" -type "float3" 0.24948055 -0.2064589 0.80975938 ;
	setAttr ".tk[2409]" -type "float3" 0.12604553 -0.2064589 0.75863063 ;
	setAttr ".tk[2410]" -type "float3" 0.012128435 -0.2064589 0.68882239 ;
	setAttr ".tk[2411]" -type "float3" -0.60205334 0 0.60205269 ;
	setAttr ".tk[2412]" -type "float3" -0.68882275 0 0.50045848 ;
	setAttr ".tk[2413]" -type "float3" 0.12415869 -0.064073473 0.38654149 ;
	setAttr ".tk[2414]" -type "float3" 0.073030218 -0.064073473 0.26310641 ;
	setAttr ".tk[2415]" -type "float3" 0.041840844 -0.064073473 0.13319287 ;
	setAttr ".tk[2416]" -type "float3" 0.031358898 -0.064073473 -1.6239755e-07 ;
	setAttr ".tk[2417]" -type "float3" 0.041841201 -0.064073473 -0.13319333 ;
	setAttr ".tk[2418]" -type "float3" 0.073030695 -0.064073473 -0.26310688 ;
	setAttr ".tk[2419]" -type "float3" 0.12415905 -0.064073473 -0.38654172 ;
	setAttr ".tk[2420]" -type "float3" -0.68882239 0 -0.50045884 ;
	setAttr ".tk[2421]" -type "float3" -0.60205275 0 -0.60205281 ;
	setAttr ".tk[2422]" -type "float3" 0.012128972 -0.2064589 -0.68882239 ;
	setAttr ".tk[2423]" -type "float3" 0.12604606 -0.2064589 -0.75863099 ;
	setAttr ".tk[2424]" -type "float3" 0.2494809 -0.2064589 -0.80975938 ;
	setAttr ".tk[2425]" -type "float3" -0.13319328 0 -0.84094894 ;
	setAttr ".tk[2426]" -type "float3" 0.11390829 -0.14950474 -0.85143137 ;
	setAttr ".tk[2427]" -type "float3" 0.24710155 -0.14950474 -0.84094894 ;
	setAttr ".tk[2428]" -type "float3" 0.37701502 -0.14950474 -0.80975938 ;
	setAttr ".tk[2429]" -type "float3" 0.3865416 0 -0.75863087 ;
	setAttr ".tk[2430]" -type "float3" 0.5004586 0 -0.68882239 ;
	setAttr ".tk[2431]" -type "float3" 0.60205281 0 -0.60205275 ;
	setAttr ".tk[2432]" -type "float3" 1.1729329 0.035596371 -0.50045878 ;
	setAttr ".tk[2433]" -type "float3" 1.2427411 0.035596371 -0.38654172 ;
	setAttr ".tk[2434]" -type "float3" 1.2938694 0.035596371 -0.26310676 ;
	setAttr ".tk[2435]" -type "float3" 1.3250588 0.035596371 -0.13319325 ;
	setAttr ".tk[2436]" -type "float3" 1.3355414 0.035596371 -2.283716e-08 ;
	setAttr ".tk[2437]" -type "float3" 1.3250588 0.035596371 0.13319318 ;
	setAttr ".tk[2438]" -type "float3" 1.2938694 0.035596371 0.26310676 ;
	setAttr ".tk[2439]" -type "float3" 1.2427411 0.035596371 0.38654172 ;
	setAttr ".tk[2440]" -type "float3" -1.1471688 0 -0.87824953 ;
	setAttr ".tk[2441]" -type "float3" -1.0026622 0 -1.0565358 ;
	setAttr ".tk[2442]" -type "float3" -1.0370485 -0.18176973 -1.2088068 ;
	setAttr ".tk[2443]" -type "float3" -0.84733033 -0.18176973 -1.3313127 ;
	setAttr ".tk[2444]" -type "float3" -0.64176106 -0.18176973 -1.4210374 ;
	setAttr ".tk[2445]" -type "float3" 0.18534413 0.37081018 -1.4757714 ;
	setAttr ".tk[2446]" -type "float3" 0.40716493 0.37081018 -1.4941673 ;
	setAttr ".tk[2447]" -type "float3" 0.62898576 0.37081018 -1.4757714 ;
	setAttr ".tk[2448]" -type "float3" 0.43818042 0 -1.4210374 ;
	setAttr ".tk[2449]" -type "float3" 0.14933619 -0.10179107 -1.3313127 ;
	setAttr ".tk[2450]" -type "float3" 0.33905441 -0.10179107 -1.2088065 ;
	setAttr ".tk[2451]" -type "float3" 0.50825012 -0.10179107 -1.0565355 ;
	setAttr ".tk[2452]" -type "float3" 1.5252508 0.10906186 -0.87824917 ;
	setAttr ".tk[2453]" -type "float3" 1.6415102 0.10906186 -0.67833722 ;
	setAttr ".tk[2454]" -type "float3" 1.7266599 0.10906186 -0.46172252 ;
	setAttr ".tk[2455]" -type "float3" 1.778603 0.10906186 -0.23373866 ;
	setAttr ".tk[2456]" -type "float3" 1.7960596 0.10906186 2.8498968e-07 ;
	setAttr ".tk[2457]" -type "float3" 1.7786019 0.10906186 0.23373947 ;
	setAttr ".tk[2458]" -type "float3" 1.7266589 0.10906186 0.46172327 ;
	setAttr ".tk[2459]" -type "float3" 1.6415094 0.10906186 0.67833787 ;
	setAttr ".tk[2460]" -type "float3" 1.52525 0.10906186 0.87824953 ;
	setAttr ".tk[2461]" -type "float3" 0.50824916 -0.10179107 1.0565361 ;
	setAttr ".tk[2462]" -type "float3" 0.33905357 -0.10179107 1.208807 ;
	setAttr ".tk[2463]" -type "float3" 0.14933547 -0.10179107 1.331313 ;
	setAttr ".tk[2464]" -type "float3" 0.43817979 0 1.4210378 ;
	setAttr ".tk[2465]" -type "float3" 0.62898529 0.37081018 1.4757717 ;
	setAttr ".tk[2466]" -type "float3" 0.40716448 0.37081018 1.4941673 ;
	setAttr ".tk[2467]" -type "float3" 0.18534368 0.37081018 1.4757717 ;
	setAttr ".tk[2468]" -type "float3" -0.64176142 -0.18176973 1.4210378 ;
	setAttr ".tk[2469]" -type "float3" -0.84733069 -0.18176973 1.3313127 ;
	setAttr ".tk[2470]" -type "float3" -1.0370489 -0.18176973 1.2088066 ;
	setAttr ".tk[2471]" -type "float3" -1.0026624 0 1.0565357 ;
	setAttr ".tk[2472]" -type "float3" -1.147169 0 0.87824953 ;
	setAttr ".tk[2473]" -type "float3" -1.5833428 0.27628991 0.67833787 ;
	setAttr ".tk[2474]" -type "float3" -1.6684926 0.27628991 0.46172312 ;
	setAttr ".tk[2475]" -type "float3" -1.7204356 0.27628991 0.23373929 ;
	setAttr ".tk[2476]" -type "float3" -1.7378933 0.27628991 4.0076674e-08 ;
	setAttr ".tk[2477]" -type "float3" -1.7204356 0.27628991 -0.23373924 ;
	setAttr ".tk[2478]" -type "float3" -1.6684926 0.27628991 -0.46172312 ;
	setAttr ".tk[2479]" -type "float3" -1.5833428 0.27628991 -0.67833781 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "FD3B1237-E947-A3CA-6E9E-49B2D236245F";
	setAttr ".ics" -type "componentList" 1 "f[0:2419]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 4 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.11654854 -8.1231995 -1.9073486e-06 ;
	setAttr ".rs" 1778056840;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -14.972402572631836 -96.24639892578125 -15.32344913482666 ;
	setAttr ".cbx" -type "double3" 15.205499649047852 80 15.323445320129395 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "72001623-D04F-EC1D-3C3A-209E998BB8E1";
	setAttr ".ics" -type "componentList" 1 "f[0:2419]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 4 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.11654854 -8.1231995 -1.4305115e-06 ;
	setAttr ".rs" 2146362139;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -14.972402572631836 -96.24639892578125 -15.323448181152344 ;
	setAttr ".cbx" -type "double3" 15.205499649047852 80 15.323445320129395 ;
	setAttr ".raf" no;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyExtrudeFace3.out" "polySurfaceShape1.i";
connectAttr "groupId1.id" "polySurfaceShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape1.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr ":defaultArnoldRenderOptions.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr ":defaultArnoldDriver.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr ":defaultArnoldDisplayDriver.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr ":defaultArnoldFilter.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "polySurfaceShape2.o" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polySplit9.ip";
connectAttr "polySplit9.out" "polySplit10.ip";
connectAttr "polySplit10.out" "polySplit11.ip";
connectAttr "polySplit11.out" "polySplit12.ip";
connectAttr "polySplit12.out" "polySplit13.ip";
connectAttr "polySplit13.out" "polySplit14.ip";
connectAttr "polySplit14.out" "polySplit15.ip";
connectAttr "polySplit15.out" "polySplit16.ip";
connectAttr "polySplit16.out" "polySplit17.ip";
connectAttr "polySplit17.out" "polySplit18.ip";
connectAttr "polySplit18.out" "polySplit19.ip";
connectAttr "polySplit19.out" "polySplit20.ip";
connectAttr "polySplit20.out" "polySplit21.ip";
connectAttr "polySplit21.out" "polySplit22.ip";
connectAttr "polySplit22.out" "polySplit23.ip";
connectAttr "polySplit23.out" "polySplit24.ip";
connectAttr "polySplit24.out" "polySplit25.ip";
connectAttr "polySplit25.out" "polySplit26.ip";
connectAttr "polySplit26.out" "polySplit27.ip";
connectAttr "polySplit27.out" "polySplit28.ip";
connectAttr "polySplit28.out" "polySplit29.ip";
connectAttr "polySplit29.out" "polySplit30.ip";
connectAttr "polySplit30.out" "polySplit31.ip";
connectAttr "polySplit31.out" "polySplit32.ip";
connectAttr "polySplit32.out" "polySplit33.ip";
connectAttr "polySplit33.out" "polySplit34.ip";
connectAttr "polySplit34.out" "polySplit35.ip";
connectAttr "polySplit35.out" "polySplit36.ip";
connectAttr "polyTweak1.out" "polyExtrudeFace1.ip";
connectAttr "polySurfaceShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polySplit36.out" "polyTweak1.ip";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "polySurfaceShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace2.out" "polyExtrudeFace3.ip";
connectAttr "polySurfaceShape1.wm" "polyExtrudeFace3.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "polySurfaceShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
// End of ggj_tube_base.ma
