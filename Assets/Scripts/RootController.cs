﻿using System;
using UnityEngine;

public class RootController : MonoBehaviour
{
    public GameObject[] ant;
    public GameObject[] warm;
    [HideInInspector] public Bounds interiorBounds;
    [HideInInspector] public Bounds exteriorBounds;

    private void Awake()
    {
        interiorBounds = transform.Find("InteriorCollider").GetComponent<Collider2D>().bounds;
        exteriorBounds = transform.Find("ExteriorCollider").GetComponent<Collider2D>().bounds;
    }
}